<h1>Transformaciones y Transiciones en CSS</h1>


<h3>Estefany Aguilar</h3>

<h1>Tabla de Contenido</h1>

- [1. Importancia de las animaciones web](#1-importancia-de-las-animaciones-web)
  - [5 razones para usar animaciones en la web](#5-razones-para-usar-animaciones-en-la-web)
- [2. Conceptos iniciales](#2-conceptos-iniciales)
  - [Propiedades para crear animaciones con CSS y propiedades animables](#propiedades-para-crear-animaciones-con-css-y-propiedades-animables)
  - [Pseudo-clases y pseudo-elementos en las animaciones](#pseudo-clases-y-pseudo-elementos-en-las-animaciones)
  - [Timing functions, planos y ejes](#timing-functions-planos-y-ejes)
      - [❤Timing o easing functions](#timing-o-easing-functions)
      - [🧡El contexto de apilamiento](#el-contexto-de-apilamiento)
      - [💛Planos y ejes](#planos-y-ejes)
      - [💚Lecturas Recomendadas](#lecturas-recomendadas)
- [3. Transformaciones en 2D y 3D](#3-transformaciones-en-2d-y-3d)
  - [Transform translate](#transform-translate)
  - [Transform rotate, scale, skew y matrix](#transform-rotate-scale-skew-y-matrix)
    - [**Un pequeño ejemplo 🥺**](#un-pequeño-ejemplo-)
    - [**CODIGO**](#codigo)
      - [Slicebox: un nuevo control deslizante de imágenes en 3D con un elegante respaldo.](#slicebox-un-nuevo-control-deslizante-de-imágenes-en-3d-con-un-elegante-respaldo)
    - [Transform rotate, scale, skew y matrix](#transform-rotate-scale-skew-y-matrix-1)
  - [Transform origin](#transform-origin)
  - [Transform style y perspective](#transform-style-y-perspective)
  - [Backface visibility](#backface-visibility)
    - [**Articulo relacionado**](#articulo-relacionado)
    - [🍿Backface visibility](#backface-visibility-1)
      - [❤](#)
      - [🧡Sintaxis](#sintaxis)
      - [💛Lecturas recomendadas](#lecturas-recomendadas-1)
- [4. Parallax con transformaciones](#4-parallax-con-transformaciones)
  - [Efecto parallax: estructura HTML](#efecto-parallax-estructura-html)
  - [Efecto parallax: estilos CSS](#efecto-parallax-estilos-css)
- [5. Transiciones](#5-transiciones)
  - [Transition property y duration](#transition-property-y-duration)
      - [❤Que hace transition-property](#que-hace-transition-property)
      - [🧡Que es transition-duration](#que-es-transition-duration)
      - [💛Lecturas Recomendadas](#lecturas-recomendadas-2)
  - [Transition timing function y delay](#transition-timing-function-y-delay)
      - [❤transition-timing-function](#transition-timing-function)
      - [🧡transition-delay](#transition-delay)
      - [transition-timing-function](#transition-timing-function-1)
      - [transition-delay](#transition-delay-1)
- [6. Tips de UX](#6-tips-de-ux)
  - [Movimiento impulsado por la acción](#movimiento-impulsado-por-la-acción)
      - [❤Que es?](#que-es)
  - [Tiempos de espera](#tiempos-de-espera)
      - [❤Que problemas podemos evitar con tiempos de espera?](#que-problemas-podemos-evitar-con-tiempos-de-espera)
      - [🧡Que logramos con un tiempo de espera?](#que-logramos-con-un-tiempo-de-espera)
  - [Problemas de parpadeo](#problemas-de-parpadeo)
      - [❤Porque ocurre el parpadeo?](#porque-ocurre-el-parpadeo)
- [7. Rendimiento y accesibilidad](#7-rendimiento-y-accesibilidad)
  - [Propiedades recomendadas y no recomendadas para animar](#propiedades-recomendadas-y-no-recomendadas-para-animar)
  - [Aceleración de hardware y la propiedad will-change](#aceleración-de-hardware-y-la-propiedad-will-change)
  - [Preferencias de movimiento reducido](#preferencias-de-movimiento-reducido)
- [8. Libros recomendados y próximos pasos](#8-libros-recomendados-y-próximos-pasos)
  - [Continúa en el Curso de Animaciones con CSS](#continúa-en-el-curso-de-animaciones-con-css)


# 1. Importancia de las animaciones web

## 5 razones para usar animaciones en la web

☝ Las animaciones también son importantes para brindar uuna buena experiencia de usuario. ¿Algunas vez has entrado a una de estas páginas modernas y has sentido como las animaciones le dan un toque más suave? El uso de animaciones puede llegar a ser muy importante, ya que de alguna manera puedes sentir que tu página está “viva” 😄

Adicionalmente, hacer animaciones con CSS es ridículamente fácil, tal vez muchos lleguen pensando aquí: “No, es que esto es difícil, seguro será mucho códigp”, pero para nada es así, con muy pocas propiedades puedes crear animaciones bien pro 😉. De hecho, seguramente escribas más reglas para darle estilos a tu elemento que las reglas que escribirás para animar.

**- Las animaciones tienen beneficio para nuestro cerebro:** Ayudan a reducir la carga cognitiva, y generar atención en espacios realmente importantes.

**- Las animaciones se comunican: **Las figuras emiten diversos mensajes que nos ayudan a asociar situaciones en la vida real.

**- Las animaciones conectan contextos: **Si ejecutaríamos una App móvil y la web de un mismo sitio(ejemplo: Platzi), la animación ayudaría a que logremos relacionarlos y reconozcamos de manera rápida de quien se trata. (Platzi)

**- Coreografía de UI:** Las animaciones deben comunicar el mismo mensaje.

**- Las animaciones llaman la atención: **Si bien es cierto que las animaciones son visualmente entretenidas y cómodas, ayudan a resaltar ciertas cosas de nuestro sitio web.

**5 Razones para usar animaciones en la web**
1 - Las animaciones tienen beneficios para el cerebro: nos ayudan a reducir la carga cognitiva, Con animaciones podemos hacer que los usuarios se enfoquen en las partes importantes de un web dejando de lado lo que no es tan importante.
2 - Las animaciones se comunican: Los seres humanos tratamos de conectar absolutamente todo, con animaciones podemos facilitar la comunicación entre diferentes objetos.
3 – Las animaciones conectan contextos: Independientemente de la plataforma por la que entro (web o app), debo tener la sensación como usuario de que estoy en el mismo lugar.
4 – Coreografía de UI: Todas las animaciones que creemos deben comunicar de la misma manera. Debe haber coherencia cuando creamos las animaciones.
5 – Las animaciones llaman la atención: Nos ayudan a reducir la cargar cognitiva, nos ayudan a comunicar y también a expresar muchas cosas.

[transiciones-y-transformaciones-css.pdf](https://drive.google.com/file/d/1lf5Kfa4K2IAGnHFeWPksEvAe5xzNPjW6/view?usp=sharing)

# 2. Conceptos iniciales

## Propiedades para crear animaciones con CSS y propiedades animables

Básicamente la propiedad `transform()` nos permité modificar **cualquier** elemento que tengamos, podemos agrandarla, achicarla, escalarla, moverla, girarla, etc. Solo con esta propiedad ya tenemos las suficiente herramientas para manipular nuestro elemento, es decir, con ella ya podemos hacer que nuestro elemento se mueva a X posición, o que gire X grados 😄.

Sin embargo, únicamente con esta propiedad no lograremos ver ninguna animación, es decir, si tu usas la propiedad, verás que al cargar la página, el elemento aparecerá ya con la propiedad apicada, por ejemplo, si lo moviste 6 pixeles a la derecha entonces el elemento ya aparecerá movido, y es aquí donde entra `transition`. Esta propiedad permite generar ese movimiento, pero para ello necesita un punto inicial y un punto final.

Imagina que quieres mover un elemento 6 píxeles a la derecha, entonces, tu estado inicial va a ser 0 píxeles (cuando aún no se ha movido) y tu estado final va a ser 6 píxeles (cuando ya se movió), y la propiedad `transition` se encargará se que ese movimiento se vea suave, es decir, lo animará 👇

![animation](https://media.giphy.com/media/gCSOFQybTbM3pome6c/giphy.gif)



[animaciones.pdf](https://drive.google.com/file/d/1Lcmf6iuSiyR8ODv1q1I51388TEOSUDFt/view?usp=sharing)

[transformaciones-en-2d-y-3d.pdf](https://drive.google.com/file/d/1SZrIXp_6Li79nVcgS_ouVae64mtOJ6ME/view?usp=sharing)

[transiciones.pdf](https://drive.google.com/file/d/1L4IDyYqQ8J8jskBaTGZ3TdB93Mfpfi62/view?usp=sharing)

## Pseudo-clases y pseudo-elementos en las animaciones

Una nota importante con la pseudo clase hover es que hay dispositivos a los que no se le puede hacer hover porque no tienen puntero (siendo los celulares el ejemplo más claro) y es posible que añadirle estilos que dependan de un hover pueda ser contraproducente para la funcionalidad de estos elementos.

Lo bueno es que ahora CSS tiene un media query llamado **hover** que permite agregarle estilos solamente en dispositivos que permiten hacer hover.

Por ejemplo si tenemos algún tag a que queremos que al hacer hover tenga un background-color distinto pero sólo queremos que pase en equipos que acepten hover podríamos usar esta línea de código:

```css
@media (hover: hover) {
  a:hover {
    background: yellow;
  }
}
```

Es importante tener esto en cuenta por ejemplo en caso de que hagamos una card que sólo muestra el contenido completo si haces hover sobre esta. Esto obviamente no sería posible desde un celular, entonces lo que se haría sería dejar todas las propiedades de la card sin animaciones y en el media query de hover poner todo lo respecto a su animación para que pueda ser accesible a través de un celular o una tablet sin problemas.

También es bueno tener en cuenta que las pseudo-clases y los pseudo-elementos son distintos entre sí, ambos se escriben como una extensión de nuestro selector después de dos puntos: `:hover`, `after`; sin embargo, una pseudo-clase es básicamente un estado que podemos manipular, por ejemplo, el estado hover, el estado active, el estado visited, etc.

Un pseudo-elemento, como su nombre lo dice, es un elemento que no está declarado explícitamente desde nuestro HTML, pero lo podemos crear con CSS, por ejemplo `before` y `after`, ¿cómo sabes que es un pseudo-elemento y no una pseudo-clase? ¡Fácil! Simplemente preguntate: ¿existe el estado `before`? Verás que la respuesta es “no”, no existe ningún estado `before` porque este es un pesudo-elemento, por el contrario, si te preguntas: ¿existe el estado `:hover`? Aquí la respuesta es “sí”, porque `hover` sí es una pseudo-clase 😄

En este link puedes encontrar una lista de pseudo-clases diponibles:

[Indice de las pseudo-clases estándar](https://developer.mozilla.org/es/docs/Web/CSS/Pseudo-classes#indice_de_las_pseudo-clases_estándar)

Y en este otro verás una lista de los pseudoelementos disponibles 😉:

[Lista de pseudoelementos](https://developer.mozilla.org/es/docs/Web/CSS/Pseudo-elements#lista_de_pseudoelementos)

![img](https://www.google.com/s2/favicons?domain=https://htmlcolorcodes.com//assets/images/favicon.png)[HTML Color Codes](https://htmlcolorcodes.com/)

![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/CSS/:hover/favicon-48x48.97046865.png)[:hover - CSS: Cascading Style Sheets | MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/:hover)

![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/CSS/:focus/favicon-48x48.97046865.png)[:focus - CSS: Cascading Style Sheets | MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/:focus)

![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/CSS/:active/favicon-48x48.97046865.png)[:active - CSS: Cascading Style Sheets | MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/:active)

![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/CSS/:disabled/favicon-48x48.97046865.png)[:disabled - CSS: Cascading Style Sheets | MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/:disabled)

## Timing functions, planos y ejes

Seguramente has visto esta imagen en muchos lugares cuando se habla de 3D. Esta es básicamente una forma más abstracta de representar una figura en 3D, básicamente el eje Y es la altura, el eje X es la anchura y el eje Z es la profundidad, es decir, eso que sale hacia ti.

<img src="https://i.ibb.co/wdC9nwX/direccion.webp" alt="direccion" border="0">

Alo largo del eje Z podemos tener varios ejes X y Y, es decir, podemos tener varias capas, una debajo de otra, y básicamente eso es el contexto de apilamiento, vas apilando cada capita una debajo de la otra 😄 👇

<img src="https://i.ibb.co/q15vBNt/apilamiento.gif" alt="apilamiento" border="0">

**Timinig function
**La función de tiempo de animación especifica la curva de velocidad de una animación. La curva de velocidad define el TIEMPO que usa una animación para cambiar de un conjunto de estilos CSS a otro.
animation-timing-function: linear|ease|ease-in|ease-out|ease-in-out|step-start|step-end|steps(int,start|end)|cubic-bezier(n,n,n,n).

**Opciones**
• linear La animación tiene la misma velocidad de principio a fin.
• ease Valor por defecto. La animación tiene un comienzo lento, luego rápido, antes de que termine lentamente.
• ease-in La animación tiene un comienzo lento.
• ease-out La animación tiene un final lento.
• ease-in-out La animación tiene un comienzo lento y un final lento.
• step-start Equivalente a pasos (1, inicio)
• step-end Equivalente a pasos (1, fin)
• steps(int,start|end) Especifica una función paso a paso, con dos parámetros. El primer parámetro especifica el número de intervalos en la función. Debe ser un número entero positivo (mayor que 0). El segundo parámetro, que es opcional, es el valor “inicio” o “final”, y especifica el punto en el que se produce el cambio de valores dentro del intervalo. Si se omite el segundo parámetro, se le asigna el valor “fin”
• cubic-bezier(n,n,n,n) Defina sus propios valores en la función cubic-bezier. Los valores posibles son valores numéricos de 0 a 1.

#### ❤Timing o easing functions

Es la forma de representación que tience CSS para generar las aceleraciones de un cambio a nuestro elementos.

#### 🧡El contexto de apilamiento

El contexto de apilamiento es la conceptualización tridimensional de los elementos HTML a lo largo de un eje-Z imaginario relativo al usuario que se asume está de cara al viewport o página web. [**Video**](https://www.youtube.com/watch?v=86zPz3J4MdM)

#### 💛Planos y ejes

**X** se refiere a la posición horizontal, de izquierda a derecha, con la propiedad left, right de css

**Y** se refiere a la posición vertical, de arriba hacia abajo, con la propiedad top, bottom

**Z** se refiere a la posición, de adelante, hacia atrás o viceversa.

#### 💚Lecturas Recomendadas

C[**ubic bezier**](https://cubic-bezier.com/#.17,.67,.83,.67)

E[**asings**](https://easings.net/)

E[**asing function**](https://developer.mozilla.org/en-US/docs/Web/CSS/easing-function)

[**El contexto de apilamiento**](https://developer.mozilla.org/es/docs/Web/CSS/CSS_Positioning/Understanding_z_index/The_stacking_context)

[**Articulo de jjypeyez Platzi**](https://platzi.com/tutoriales/1050-programacion-basica/1418-3d-entendiendo-el-espacio-tridimensional-con-css-para-dummies/)

![img](https://www.google.com/s2/favicons?domain=https://cubic-bezier.com/about:blank)[cubic-bezier.com](https://cubic-bezier.com/)

![img](https://www.google.com/s2/favicons?domain=https://easings.net/96.8aa68ac4.png)[Easing Functions Cheat Sheet](https://easings.net/)

![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/CSS/easing-function/favicon-48x48.97046865.png)[<easing-function> - CSS: Cascading Style Sheets | MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/easing-function)

![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/es/docs/Web/CSS/CSS_Positioning/Understanding_z_index/The_stacking_context/favicon-48x48.97046865.png)[El contexto de apilamiento - CSS | MDN](https://developer.mozilla.org/es/docs/Web/CSS/CSS_Positioning/Understanding_z_index/The_stacking_context)

# 3. Transformaciones en 2D y 3D

## Transform translate

Básicamente `translate()` te permite mover un elemento en el eje X o Y… ¡O ambos! 👀, básicamente este ejemplo que ya había puesto en una clase anterior, vas de un punto A a un punto B:

<img src="https://i.ibb.co/h76YWGs/1.gif" alt="1" border="0">

Ahora, ¡la propiedad transform usos muy interesantes! Un uso que a mí me encantó mucho cuando lo conocí es la posibilidad de poder centrar elementos, tanto verticalmente como horizontalmente, por ejemplo, suponiendo que tenemos un elemento padre con `position: relative;` y un elemento hijo con `position: absolute`…

<img src="https://i.ibb.co/NFVLr0G/2.webp" alt="2" border="0">

¡Podemos centrarlo usando `transform` y las propiedades `top` y `left`! ¿Cómo? Simplemente hay que decirle al elemento hijo que se muevan 50% en su `top` y su `left`:

<img src="https://i.ibb.co/1LgW4fS/3.jpg" alt="3" border="0">

🤔 Pero esto no está centrado… Aquí es donde podemos usar `transform` 😉 Simplemente le decimos que tanto en X como en Y se mueva `-50%`, es decir, que retroceda el 50% **de sí mismo**, de esa forma quedará totalmente centrado 😄

<img src="https://i.ibb.co/m4tkJp6/4.jpg" alt="4" border="0">

De esa forma siempre se mantendrá centrado el elemento, esta forma de centrar me encanta cuando trabajamos con elementos posicionados absolutamente 😉.

**Código para jugar con las propiedades:**

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>

        .shadow{
            background: lightblue;
            width: 100px;
            height: 100px;
            border-radius: 50%;
        }

        .circle{
            background-color: #4158D0;
            background-image: linear-gradient(43deg, #4158D0 0%, #C850C0 46%, #FFCC70 100%);
            width: 100px;
            height: 100px;
            border-radius: 50%;
        }

        .circle:hover{
            transform: translate(-20px, 20px);
	    Aqui podemos jugar con todas las propiedades que tenemos en los recursos.
        }

    </style>
</head>
<body>
    <div class="shadow">
        <div class="circle"></div>
    </div>
</body>
</html>
```

![img](https://www.google.com/s2/favicons?domain=https://cssgradient.io/gradient-backgrounds//images/favicon-23859487.png)[Gradient Backgrounds – 🌈 The Best Gradient Sites All in One Place](https://cssgradient.io/gradient-backgrounds/)

![img](https://www.google.com/s2/favicons?domain=https://caniuse.com/?search=transform/img/favicon-128.png)["transform" | Can I use... Support tables for HTML5, CSS3, etc](https://caniuse.com/?search=transform)

## Transform rotate, scale, skew y matrix

<img src="https://i.ibb.co/0tG3QSn/rotar.gif" alt="rotar" border="0">

1. **Rotate:** Te ayuda a girar un elemento, puedes rotarlo en los ejes X, Y o Z.
2. **Scale:** Te ayuda a cambiar el tamañode un elemento, básicamente puedes agrandarlo o achicarlo.
3. **Skew:** Te ayuda a deformar un elemento tanto en X como en Y, esta propiedad es muy útil cando queires dar un efecto 3D.
4. **Matrix:** Esta es una propiedad un poquito máscompleja. Te permite escalar, deformar y mover tu elemento a través de diferentes propiedades, recomiendo darle un ojo a [la documentación](https://developer.mozilla.org/en-US/docs/Web/CSS/transform-function/matrix()) de esta propiedad para entenderla mejor 😄.

### **Un pequeño ejemplo 🥺**

Abajo pueden encontrar el codigo 😃


![img](https://media.giphy.com/media/7UJRs5LdviWtNUcKfx/giphy.gif)


### **CODIGO**

```html
	<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .cabeza{
            background-color: #3c8500;
            background-image: linear-gradient(160deg, #0093E9 0%, #80D0C7 100%);
            width: 100px;
            height: 100px;
            border-radius: 50%;
            margin-left: auto;
            margin-right: auto;
        }

        .piertaLeft{
            width: 20px;
            height: 100px;
            background-color: antiquewhite;
            position: fixed;
            left: 725px;
            top: 200px;
            z-index: 5;
        }

        .piertaRight{
            width: 20px;
            height: 100px;
            background-color: antiquewhite;
            position: fixed;
            left: 780px;
            top: 200px;
            z-index: 5;
        }

        .estomago{
            background-color: rgb(0, 112, 250);
            width: 150px;
            height: 100px;
            margin-left: auto;
            margin-right: auto;
        }

        .ojoLeft{
            width: 20px;
            height: 20px;
            background-color: #004e7b;
            border-radius: 50%;
            position: fixed;
            top: 30px;
            left: 740px;
        }
        .ojoRight{
            width: 20px;
            height: 20px;
            background-color: #004e7b;
            border-radius: 50%;
            position: fixed;
            top: 30px;
            left: 775px;
        }

        .persona{
            width: 200px;
            margin-left: auto;
            margin-right: auto;
        }

        .boca{
            width: 45px;
            height: 10px;
            background-color: #004e7b;
            position: fixed;
            top: 80px;
            left: 745px;
        }

        .bocaLeft{
            width: 20px;
            height: 10px;
            background-color: #004e7b;
            position: fixed;
            top: 83px;
            left: 730px;
            transform: rotate(-30deg);
        }

        .bocaRight{
            width: 20px;
            height: 10px;
            background-color: #004e7b;
            position: fixed;
            top: 83px;
            left: 788px;
            transform: rotate(30deg);
        }

        .brazoRight{
            width: 20px;
            height: 100px;
            background-color: antiquewhite;
            position: fixed;
            left: 825px;
            top: 120px;
            z-index: 5;
        }


    
        .brazoLeft{
            width: 20px;
            height: 100px;
            background-color: antiquewhite;
            position: fixed;
            left: 685px;
            top: 120px;
            z-index: 5;
        }

        .persona:hover .brazoLeft{
            transform: rotate(-20deg);
        }


        .persona:hover .brazoRight{
            transform: rotate(-20deg);
        }

        .persona:hover .bocaRight{
            transform: rotate(-30deg);
            top:77px ;
        }

        .persona:hover .bocaLeft{
            transform: rotate(30deg);
            top:77px ;
            left: 728px;
        }
    </style>
</head>
<body>

    <div class="persona">
        <div class="brazoLeft">

        </div>
        <div class="brazoRight">

        </div>
        <div class="cabeza">

        </div>
        <div class="piertaLeft"> 

        </div>
        <div class="piertaRight">

        </div>
        <div class="estomago">

        </div>
        <div class="ojoLeft">

        </div>
        <div class="ojoRight">
            
        </div>
        <div class="boca">

        </div>
        <div class="bocaLeft">

        </div>
        <div class="bocaRight">

        </div>
    </div>
    
</body>
</html>
```

####  [Slicebox:](https://tympanus.net/Development/Slicebox/index.html) un nuevo control deslizante de imágenes en 3D con un elegante respaldo.

 [GitHub](https://github.com/codrops)

### Transform rotate, scale, skew y matrix

![Captura de pantalla de 2021-05-26 09-29-30.png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%20de%202021-05-26%2009-29-30-11a7b4d8-400f-40d1-86de-0aa131f4dcb9.jpg)
**Escalado (scale()):** Estas hace como una transformación en la que aumentan o reducen el tamaño de un elemento, y se basan en el parámetro indicado, que es solo un factor de escala.
**Rotaciones(rotate):** Se podría decir que las funciones de rotación simplemente giran el elemento el número de grados indicado.
Ejemplo:
**Con transform: rotate(5deg) realizamos una rotación de 5 grados del elemento sobre si mismo.**
**Deformaciones(skew):** Establecen un ángulo para torcer, tumbar o inclinar un elemento en 2D.

![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/CSS/transform/favicon-48x48.97046865.png)[transform - CSS: Cascading Style Sheets | MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/transform)

![img](https://www.google.com/s2/favicons?domain=https://cssgradient.io/gradient-backgrounds//images/favicon-23859487.png)[Gradient Backgrounds – 🌈 The Best Gradient Sites All in One Place](https://cssgradient.io/gradient-backgrounds/)

## Transform origin

Básicamente transform origin es el punto base desde el cual se aplica una transformación, por ejemplo, en este caso podemos ver distintos origins, en la primera figura el punto morado está en el centro, por lo que la figura rotará desde ese mismo centro.

En la segunda figura, el punto morado está en una de las esquinas, por lo que podemos ver cómo la figura va rotando pero desde esa esquina 😄

<img src="https://i.ibb.co/mHB2hN4/rotar1.gif" alt="rotar1" border="0">

Así como podemos cambiar el punto de origen de la rotación, podemos hacerlo con cualquier propiedad de `transform` 😉

**Transform origin** es una propiedad el punto o posición especifica en la cual se aplica la transformación. Esta recibe valores para X, Y, Z; sus valores por defecto son:

```css
transform-origin: 50% 50%;
```

a lo que es igual

```css
transform-origin: center;  
```

Los valores relativos que recibe transform origin como: top, left, bottom left, 80% 20%, etc. Hacen referencia al tamaño o el espacio que ocupa el elemento en contexto.

El elemento en contexto en la mayoría de los casos será el elemente al cual se le este aplicando la transformación, sin embargo puede haber casos en la cual será su padre o uno de sus ancestros, como pasa los SVG; pero tranquilo, si te topas con un caso así existe la propiedad **transform-box** para definir el elemento en contexto con el cual el transform y transform-origin esta relacionado.

Si quieres saber más sobre **Trasnform Box** [aquí tienes documentación](https://developer.mozilla.org/en-US/docs/Web/CSS/transform-box) 😉

![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/CSS/transform/favicon-48x48.97046865.png)[transform - CSS: Cascading Style Sheets | MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/transform)

## Transform style y perspective

La propiedad perspective es básicamente desde dónde estás viendo el objeto, la perspectiva puede cambiar dependendiendo de qué tan lejos o cerca esté.

Esta propiedad es importante cuando queremos manejar el 3D en las animaciones 😄

<img src="https://i.ibb.co/3Wbp1Q0/3D.gif" alt="3D" border="0">

En esta imagen también podemos ver un ejemplo mucho más claro de lo que es la perspectiva 😄

<img src="https://i.ibb.co/pxcsVLM/3d1.jpg" alt="3d1" border="0">

***[Transformaciones CSS](https://lenguajecss.com/css/animaciones/transformaciones/)\***

Las propiedades principales para realizar transformaciones son las siguientes:
**transform**
Aplica una o varias funciones de transformación sobre un elemento.
**transform-origin**
Cambia el punto de origen del elemento en una transformación.
**transform-style**
flat | preserve-3d Modifica el tratamiento de los elementos hijos.
**Funciones 2D**
![img](https://lenguajecss.com/css/animaciones/transformaciones/css-transforms.png)
**Funciones 3D**
Recordar siempre que el eje X es el eje horizontal, el eje Y es el eje vertical y el eje Z es el eje de profundidad.
![img](https://lenguajecss.com/css/animaciones/transformaciones/eje-x-y-z.png)

![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/CSS/transform/favicon-48x48.97046865.png)[transform - CSS: Cascading Style Sheets | MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/transform)

## Backface visibility

Con la propiedad `backface-visibility` podemos hacer cosas geniales como tarjetitas que se voltean 😈. Por ejemplo, estas tarjetitas que hice para un blog de Platzi donde aprendemos a hacer un juego con Vue 👀👇

<img src="https://i.ibb.co/yp3qpHq/morty.gif" alt="morty" border="0">

El concepto es simple, puede costar un poquito entenderlo al inicio, pero al cuando lo entiendes se vuelve sencillo:

Tenemos un elemento, ese elemento tiene una parte de adelante y una parte de atrás, no importa si el elemento es plano. La propiedad `backface-visibility` mostrará u ocultará esa cara de atrás dependiendo de lo que esta propiedad indique.

¿Esto que significa? Bueno, que si pones tu `backface-visibility` como `hidden`, la parte de atrás de tu tarjeta no se verá, pero la de adelante sí; es decir, si tu le pusieras un `transform: rotateX(180deg)` no verías nada, porque justamente estarías viendo la cara de atrás del elemento, que está oculta por el `backface-visibility` ☝👀.

Por cierto, si quieres ver el blog donde hago el juego usando estas propiedades de `backface-visibility` puedes mirarlo aquí: [Guía desde cero: tu propio juego de Rick and Morty con Vue.js](https://platzi.com/blog/juego-rick-morty-vuejs/)

Complementando la explicación que dió la maestra, por si alguien se quedó con la duda del problema con las clases `.face .front`:

La razón por la que no se estaban aplicando los estilos a los elementos es porque al inicio los selectores estaban escritos así: `.face .front`. Al estar escritos de esa manera, CSS **esta buscando un elemento con clase** `.front` **dentro de un elemento con clase** `.face`, lo cual no tenemos. Ese espacio entre un selector y otro es lo que ocasiona este comportamiento. Es como si hiciéramos lo siguiente:

```html
// HTML
<div class="padre">
	<div class="hijo"></div>
</div>
// CSS
.padre .hijo {
	background-color: red;
}
```

En el CSS anterior, gracias a ese espacio entre selectores es que podemos acceder al elemento con clase `.hijo` **dentro** del elemento con clase `.padre`

PERO ese no era el HTML que teníamos nosotros. Lo que teníamos era lo siguiente:

```html
// HTML
<div class="card">
	<div class="face front"></div>
	<div class="face back"></div>
</div>
```

Entonces, ¿cómo seleccionamos esos div en CSS? **Quitándole el espacio entre selectores.**

```css
// CSS
.face.front {
	background-color: red;
}
```

De esta manera, al escribir los selectores sin espacios (todo junto), le estamos diciendo a CSS que queremos aplicar esos estilos al elemento que tenga **las dos clases, tanto** `.face` **como** `.front`.

### **Articulo relacionado**

En este enlace podemos ver un poco mas sobre las propiedades y demás de la propiedad backface-visibility.
👇
[backface-visibility](https://developer.mozilla.org/es/docs/Web/CSS/backface-visibility)

### 🍿Backface visibility

#### ❤

determina si la cara posterior de un elemento es visible de frente al usuario. La cara posterior de un elemento siempre es un fondo transparente, permitiendo, cuando es visible, que se muestre una imagen de espejo de la cara frontal.

- Codigo

  ```css
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Document</title>
      <style>
          .card {
              width: 200px;
              height: 200px;
              transform-style: preserve-3d;
              position: relative;
          }
          .card:hover {
              transform: rotateY(180deg);
              transition: 1000ms;
          }
          .face {
              border-radius: 20px;
              width: 100%;
              height: 100%;
              position: absolute;
              backface-visibility: hidden;
          }
          .face.front {
              background: pink;
          }
          .face.back {
              background: papayawhip;
              transform: rotateX(180deg);
          }
      </style>
  </head>
  <body>
      <div class="card">
          <div class="face front"></div>
          <div class="face back"></div>
      </div>
  </body>
  </html>
  ```

#### 🧡[Sintaxis](https://developer.mozilla.org/es/docs/Web/CSS/backface-visibility#sintaxis)

```css
backface-visibility: visible;
backface-visibility: hidden;
```

#### 💛Lecturas recomendadas

[Documentación en ingles ( con ejemplos practicos )](https://developer.mozilla.org/en-US/docs/Web/CSS/backface-visibility)

[Documentación es español ( sin ejemplos practicos )](https://developer.mozilla.org/es/docs/Web/CSS/backface-visibility)

[Explicación de por que no se aplican los estilos en el ejercicio](https://platzi.com/comentario/2593981/)



![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/CSS/transform/favicon-48x48.97046865.png)[transform - CSS: Cascading Style Sheets | MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/transform)

![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/es/docs/Web/CSS/backface-visibility/favicon-48x48.97046865.png)[backface-visibility - CSS | MDN](https://developer.mozilla.org/es/docs/Web/CSS/backface-visibility)

# 4. Parallax con transformaciones

## Efecto parallax: estructura HTML

**HISTORIA**
el Parallax es en realidad un truco con mucha historia, inventado por [**Disney**](https://www.youtube.com/watch?v=86zPz3J4MdM) para dotar a sus películas de dibujos animados de una ligera tridimensionalidad, intentando lograr con ello un mayor realismo. El invento se bautizó como **«cámara multiplano»**

***¿QUE ES PARALLAX?\***
efecto de paralaje o parallax es una técnica en la que el fondo se mueve a una velocidad distinta que la del contenido. El resultado es un ligero efecto de profundidad, dejando ver partes que antes no podías visualizar. Te ayuda a sumergirte totalmente en el contenido, similar al efecto 3D.

**Efecto parallax: estructura HTML**
Consiste en superponer “capas” o elementos sobre el eje z (z-index) de un contenedor o elemento padre.

El objetivo del efecto parallax es crear una ilusión de movimiento al establecer distintas velocidades de scroll entre las capas contenidas dentro del padre.

Para lograr dicho efecto, la estructura base necesaria en HTML consiste de:

- Contenedor (elemento padre)
- capa de fondo (background)
- capa intermedia (middleground)
- capa delantera (foreground)

```html
<div class="parallax-container">
        <div class="parallax-container__background"> 
          <div class="parallax-background-element"></div>
        </div>
        <div class="parallax-container__middleground">
          <div class="card one"> </div>
          <div class="card two"> </div>
          <div class="card three"> </div>
        </div>
        <div class="parallax-container__foreground">
          <div class="parallax-foreground-element"></div>
        </div>
      </div>
```

![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)[Alicia](https://i.ibb.co/vJdbRkj/Alice.png)

![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)[background](https://i.ibb.co/jbLKgvX/Background.png)

## Efecto parallax: estilos CSS

**Para calcular el scale():**

```css
(perspective.value - translateZ().value)  / perspective.value = scale().value
// Los datos en este caso:
perspective: 8
translateZ(5); 5

(8 - 5) / 8 = 0.375

Daria scale(0.375)
```

en la clase card del ejemplo, se coloca 20px en la funcion rotate, pero para que funcione esa propiedad se debe colocar la unidad deg,como indicó la profe en una clase anterior.

```css
transform: rotate(-20deg);
```

`.card` para que funcione el `transform` tienen que ser el `rotate` en **grados,** no en **pixeles.** 😄

```css
 	.card {
           position: absolute;
           border-radius: 8px;
           background-color: hotpink;
           width: 80px;
           height: 120px;
           transform: rotate(20deg);
        }
```

[transform-function/rotate()](https://platzi.com/clases/2336-transformaciones-transiciones-css/38120-efecto-parallax-estilos-css/url)

# 5. Transiciones

## Transition property y duration

¡La propiedad `transition` es la que hace toda la magía! Si ya tienes un elemento con un estado inicial y otro elemento con un estado final, entonces simplemente basta con que le ponas esta propiedad y se animará sin problema 😉

Es decir, pasaras de tener esto:

<img src="https://i.ibb.co/02kYC6P/1.gif" alt="1" border="0">

A tener esto:

<img src="https://i.ibb.co/dkQPpVD/2.gif" alt="2" border="0">

#### ❤Que hace transition-property

Se usa para definir los nombres de las propiedades CSS en las que el efecto de la transición debe aplicarse.

#### 🧡Que es transition-duration

Establece el tiempo que debe tardar una animación de transición en completarse. Por defecto, el valor es de 0s, esto quiere decir que no se producirá ninguna animación.

Cuando tu quieras que se le aplique transiciones a solo dos propiedades, puedes hacerlo solo con una coma. Veamos el siguiente ejemplo:


```css
transition-property: transform, border-radius;
```

Ya con eso, ya no tenemos que poner el valor `all`.

Ejercicio
![img](https://media.giphy.com/media/lcuwSOEAfIUX8HeSVB/giphy.gif)

#### 💛Lecturas Recomendadas

- [Documentación transition](https://developer.mozilla.org/en-US/docs/Web/CSS/transition)
- [Documentación transition-property](https://developer.mozilla.org/en-US/docs/Web/CSS/transition-property)
- [Documentación transition-duration](https://developer.mozilla.org/en-US/docs/Web/CSS/transition-duration)

## Transition timing function y delay

#### ❤transition-timing-function

Establece cómo se calculan los valores intermedios para las propiedades de CSS que se ven afectadas por un efecto de transición. Básicamente es como nosotros afectamos la aceleración de una transición.
**[Documentación](https://developer.mozilla.org/en-US/docs/Web/CSS/transition-timing-function)**

#### 🧡transition-delay

Es el tiempo de espera, antes de que comience una animación
**[Documentación](https://developer.mozilla.org/en-US/docs/Web/CSS/transition-delay)**

#### transition-timing-function

Establece cómo se calculan los valores intermedios para las propiedades de CSS que se ven afectadas por un efecto de transición.

Basicamente es como nosotros afectamos la aceleracion de una transicion.

```css
transition-timing-function: linear | ease | ease-in | ease-out |
ease-in-out | step-start | step-end | steps(int, start | end) | cubicbezier(n, n, n, n) | initial | inherit;
```

#### transition-delay

El tiempo puede estar dado en segundos (s) o milisegundos(ms)

```css
transition-delay: time | initial | inherit;
```

[Ejercicio](https://codepen.io/camilo315853/pen/KKWQyye?editors=0100)

![transition.PNG](https://static.platzi.com/media/user_upload/transition-07669285-cfbc-44f1-bff6-f5b9a99bc285.jpg)

HTML

```html
<div class="container">
<div class="circle one"></div>
<div class="circle two"></div>
<div class="circle three"></div>
<div class="circle four"></div>
<div class="circle five"></div>
<div class="circle six"></div>
</div>
```

CSS

```css
body {
  background:
linear-gradient(27deg, #151515 5px, transparent 5px) 0 5px,
linear-gradient(207deg, #151515 5px, transparent 5px) 10px 0px,
linear-gradient(27deg, #222 5px, transparent 5px) 0px 10px,
linear-gradient(207deg, #222 5px, transparent 5px) 10px 5px,
linear-gradient(90deg, #1b1b1b 10px, transparent 10px),
linear-gradient(#1d1d1d 25%, #1a1a1a 25%, #1a1a1a 50%, transparent 50%, transparent 75%, #242424 75%, #242424);
background-color: #131313;
background-size: 20px 20px;
}

.container {
  margin: 20px 40px;
  
  
}
.container:hover .circle {
  transform: translateX(350px);

}

.circle {
  width: 80px;
  height: 80px;
  border-radius: 50%;
  margin-bottom: 12px;
  box-shadow: 0px 0px 30px 0px rgba(30,255,0,0.6), inset 0px 0px 18px 12px rgba(30,255,0,0.6);
  transform: scale(.8);
}
.one {
  
  transition: transform 1.5s linear;
  transition-delay: 0.5s;
  
}

.two {
  transition: transform 1s ease;
  transition-delay: 1s;
}
.three {
  
  transition: transform 2s ease-out;
  transition-delay: 0.5s;
}
.four {
 
  transition: transform 2s ease-out;
  transition-delay: 0.5s;
}
.five {
 
  transition: transform 1s ease;
  transition-delay: 1s;
}
.six {

  transition: transform 1.5s linear;
  transition-delay: 0.5s;
}
```

![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/CSS/transition/favicon-48x48.97046865.png)[transition - CSS: Cascading Style Sheets | MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/transition)

# 6. Tips de UX

## Movimiento impulsado por la acción

#### ❤Que es?

La pseudo-clase :hover de CSS coincide cuando el usuario interactúa con un elemento con un dispositivo señalador, pero no necesariamente lo activa. Generalmente se activa cuando el usuario se desplaza sobre un elemento con el cursor (puntero del mouse).

🧡[**Documentación :hover**](https://developer.mozilla.org/es/docs/Web/CSS/:hover)

Se trata de darle un toque más natural a la animación 🤔, es decir, en lugar de que sea una animación completamente linear podemos jugar con ellas para que tenga diferentes velocidades tanto de subida como de bajada, incluso podemos lograr que un objeto se comporte de forma más natural, por ejemplo, el efecto de un rebote 😄

<img src="https://i.ibb.co/93Zs3Kf/rebote.gif" alt="rebote" border="0">



![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/CSS/transition/favicon-48x48.97046865.png)[transition - CSS: Cascading Style Sheets | MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/transition)

## Tiempos de espera

#### ❤Que problemas podemos evitar con tiempos de espera?

- Que se cierra un menú desplegable de manera abrupta
- Que el usuario no llegue al apartado que este buscando
- Que el usuario tenga una mala experiencia

#### 🧡Que logramos con un tiempo de espera?

- Que el menu desplegable se vea mas bonito
- Una mejor experiencia de usuario o UX
- Que el acceso a todas las secciones sea muy fácil.

**Código** :

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        body {
            box-sizing: border-box;
            margin: 0;
            padding: 0;
        }
        ul {
            list-style: none;
        }
        a {
            text-decoration: none;
        }
        .nav {
            display: flex;
        }
        .nav > li {
            margin-left: 20px;
            padding: 15px 25px;
            background-image: linear-gradient(to top, #cfd9df 0%, #e2ebf0 100%);
            border-radius: 10px;
        }
        .nav li ul{
            display: block;
            opacity: 0;
            position: absolute;
            transition: opacity 500ms, visibility 500ms;
            transition-delay: 400ms;
            visibility: hidden;
        }
        .nav li:hover > ul {
            opacity: 1;
            transition: opacity 500ms, visibility 500ms;
            transition-delay: 400ms;
            visibility: visible;
        }
        .nav li ul li{
            position: relative;
        }
        .nav li ul li{
            margin: 5px 0;
            padding: 10px 5px;
            top: 20px;
            background-image: linear-gradient(120deg, #e0c3fc 0%, #8ec5fc 100%);
            text-align: center;
            border-radius: 10px;
        }
    </style>
</head>
<body>
    <header>
        <ul class="nav">
            <li>
                <a href="">Mujer</a>
                <ul>
                    <li><a href="">Camisetas</a></li>
                    <li><a href="">Pantlones</a></li>
                    <li>
                        <a href="">Shorts</a>
                        <ul>
                            <li><a href="">Cortos</a></li>
                            <li><a href="">Largos</a></li>
                            <li><a href="">Transparantes</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <a href="">Hombre</a>
            </li>
            <li>
                <a href="">Niños</a>
            </li>
        </ul>
    </header>
</body>
</html>
```

Cuando queremos hacer un menú desplegable, hay que jugar con el factor del tiempo, para que la experiencia del usuario (**UX**) sea mucho mas amena. Con esto podemos evitar

- Que se cierra un menu desplegable de manera abrupta
- Que el usuario no llegue al apartado que este buscando
- Que el usuario tenga una mala experiencia

Ademas, con esto logramos:

- Que el menu desplegable se vea mas bonito
- Una mejor experiencia de usuario o UX
- Que el acceso a todas las secciones sea muy fácil.

`Navbar`, `html`

```html
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400&display=swap" rel="stylesheet">
<header>
  <span>
    <i class="fab fa-galactic-republic"></i>
  </span>
  <nav class="nav">
    <ul class="main-menu">
      <li class="main-menu__item"><a href="#">About Us</a></li>
      <li class="main-menu__item"><a href="#">Products</a>
        <ul class="sub-menu">
          <li class="sub-menu__item"><a href="#">Hardware</a></li>
          <li class="sub-menu__item"><a href="#">Software</a>
            <ul class="sub-submenu">
              <li class="sub-submenu__item"><a href="">Web</a></li>
              <li class="sub-submenu__item"><a href="">Apps</a></li>
              <li class="sub-submenu__item"><a href="">A.I</a></li>
            </ul>
          </li>
          <li class="sub-menu__item"><a href="#">Testing</a></li>
        </ul>
      </li>
      <li class="main-menu__item"><a href="#">Contact</a></li>
    </ul>
  </nav>
</header>

```

`Css`

```css
* {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
  list-style: none;
  text-decoration: none;
}
header {
  width: 100%;
  height: 60px;
  padding: 10px 20px;
  display: flex;
  align-items:Center;
  background: #2C363F;
}
span {
  display:block;
  width: 10%;
}
.fa-galactic-republic {
  font-size: 40px;
  color: #F2F5EA;
}
.nav {
  width: 90%;
}
.main-menu {
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-around;
  /*position:relative;*/
}
.main-menu__item {
  position: relative;
}
.main-menu__item  > a:hover {
  border-bottom: 1px solid white;
}
.main-menu__item a {
  color: #F2F5EA;
  font-family: 'Poppins', sans-serif;
}
.sub-menu {
  background: #212930;
  visibility: hidden;
  opacity: 0;
  position: absolute;
  text-align:center;
  top: 90%;
  left: -15%;
  transition: opacity 450ms;
  transition-delay: 450ms;
}
.main-menu__item:hover .sub-menu {
  visibility: visible;
  opacity: 1;
  transition: opacity 450ms;
  transition-delay: 450ms;
}
.sub-menu__item {
  position:relative;
  padding: 10px;
  border-bottom: 1px solid white;
  transition: background 450ms;
}
.sub-menu__item:hover {
  background: #556877;
}
.sub-submenu {
  visibility: hidden;
  opacity: 0;
  background: #191F24;
  position:absolute;
  top:0;
  left:100%;
  transition: all 250ms;
  transition-delay: 250ms;
}
.sub-submenu__item {
  padding:10px;
  border-bottom: 1px solid white;
}
.sub-menu__item:hover .sub-submenu {
  visibility: visible;
  opacity: 1;
  transition: all 350ms;
  transition-delay: 350ms;
}
.sub-submenu__item:hover {
  background: #546878;
  transition: background 450ms;
}
```



## Problemas de parpadeo

#### ❤Porque ocurre el parpadeo?

El “parpadeo” se da porque cuando queremos hacer :hover en el item si este se mueve hay momentos donde no estamos haciendo :hover (ya que se va de donde esta nuestro puntero).

Al contener al item y hacer :hover en su container, por mas que el item se mueva, nunca dejamos de hacer :hover en el container ya que este no se mueve.

# 7. Rendimiento y accesibilidad
## Propiedades recomendadas y no recomendadas para animar

Al hacer animaciones debemos fijarnos que no sean demasiado costosas computacionalmente para que no parezcan inestables y poco fluidas.

Para ello, debemos comprender un concepto clave llamado: el proceso de renderizado.

Resulta que, como el navegador no entiende el código que hacemos, debe hacer una transformación de ese código para que finalmente pueda ser entendido y visualizado en la pantalla.

Esa transformación se hace en una serie de pasos como los que puedes ver a continuación:

<img src="https://i.ibb.co/yWtGKGT/browser-rendering.jpg" alt="browser-rendering" border="0">

Sin embargo, los pasos que nos interesan en este momento son los últimos 3: **Layout**, **Paint** y **Composite**. Cada uno cumple un papel muy importante, pero no todas las propiedades pasan por estos 3 procesos.

Si una propiedad debe pasar por el paso de Layout, obligatoriamente debe pasar por Paint y Composite también. Si una propiedad debe pasar por el paso de Paint, obligatoriamente debe pasar por Composite también. Pero, si una propiedad debe pasar por el paso de Composite, no debe pasar por ningún otro paso.

Con lo anterior, podemos darnos cuenta de que hay propiedades que requieren un costo mayor que otras al tener que pasar por más pasos. Puedes revisar el proceso de renderizado que realiza cada propiedad en esta página: https://csstriggers.com/. Revisemos algunas de ellas:

- **Propiedad height:** En cada uno de los motores de renderizado, podemos darnos cuenta por la imagen de abajo que requiere de los pasos de Layout, Paint y Composite, lo cual es bastante costoso.

<img src="https://i.ibb.co/3C1CW0m/height.jpg" alt="height" border="0">

- **Propiedad background-color:** Es una propiedad que no afecta el diseño (Layout) pero requiere una nueva capa de pintura (Paint), lo cual la hace una propiedad también costosa.

<img src="https://i.ibb.co/7kdDyBX/background-color.jpg" alt="background-color" border="0">

- **Propiedades transform y opacity:** Estas dos propiedades sólo requieren del paso de Composite, lo cual las hace muy baratas de animar. Si necesitas modificar propiedades como width y left (propiedades costosas), puedes reemplazarlas usando la propiedad transform para tratar de lograr el mismo efecto.

<img src="https://i.ibb.co/tPRRC3P/transform.jpg" alt="transform" border="0">

<img src="https://i.ibb.co/WcqRS9x/opacity.jpg" alt="opacity" border="0">

Finalmente, si sabemos por cuáles pasos de renderizado pasa cada una de las propiedades, sabremos con exactitud cuáles propiedades son más costosas y menos recomendadas para animar (como `height`, `width` y `background-color`), como también, cuáles propiedades son menos costosas y más recomendadas para animar (como `transform` y `opacity`).

Te comparto esta lectura por si quieres conocer más a profundidad cómo trabaja el motor de cada navegador con cada uno de los pasos que describimos anteriormente: https://hacks.mozilla.org/2017/08/inside-a-super-fast-css-engine-quantum-css-aka-stylo/

## Aceleración de hardware y la propiedad will-change

Hay algunas propiedades que no son buenas de *animar o transicionar*, pero esto porque pasa, no todas las propiedades son lo mismo?

La principal diferencia es que algunas propiedades tienen que lidiar con sus posiciones en la pantalla, lo que significa que el navegador tiene que modificar al DOM y mover algunos pixeles para que algunas propiedades funcionen, esto se debe a que el CSS `engine` funciona en 5 pasos:

- **Parse**: Aquí solo identifica todas las etiquetas del `archivo.html`
- **Style**: Aquí el `engine` agrega las etiquetas que vienen el el `documento.css`
- **Layout**: Aquí el `engine` identifica donde debería aparecer un nodo en la pantalla (aqui es donde se lidia con pixeles, tamaño de monitor, etc.)
- **Paint**: Aquí también se lidia con pixeles, ya que si un elemento tiene el background rojo, todos los pixeles dentro de ese elemento cambian.
- **Composite & render**: Esto es la parte final, aqui ya solo se renderiza y se muestran todos los pixeles en la pantalla.


Sabiendo esto, si nuestros elementos a *transicionar* lidian con pixeles y cambian de cierta forma el DOM, van a causar un mayor esfuerzo a nuestra computadora. Lo que se hace es trabajar con elementos que no dañen al DOM, como lo son `opacity` y `transform.`

Por ultimo, porque nosotros somo buena onda y el navegador nos cae bien, le podemos decir que se ponga trucha y que empiece a prepararse para optimizar ciertas transformaciones que nosotros sabemos se van a tener que hacer, eso lo hacemos con la propiedad `will-change`

### will-change

#### Propiedad css para anticipar y preparar los cambios

Este tipo de optimizaciones puede aumentar la capacidad de respuesta de una página al realizar un trabajo potencialmente costoso antes de que realmente se requieran.
**Propiedades**

```css
will-change: auto;
will-change: scroll-position;
will-change: contents;
will-change: transform;    
will-change: opacity;          
will-change: left, top;      
```

La propiedad “will-change” admite múltiples valores, en cuyo caso se deben separar por comas:

```css
.miele {will-change: transform, opacity, contents;}
```

#### Recomendación

No aplique will-change a demasiados elementos porque puede hacer que la página se ralentice o consuma muchos recursos.
“will-change” está destinado a ser utilizado como último recurso, con el fin de tratar de hacer frente a los problemas de rendimiento existentes. No debe usarse para anticipar problemas de desempeño.

La aceleración por hardware permite usar componentes específicos de tu ordenador para quitar trabajo al procesador de tu dispositivo. Uno de estos componentes puede ser una tarjeta gráfica, que puede usarse para renderizar o mostrar el contenido del navegador en tu pantalla.

#### 🧡Propiedades al animar

Hay propiedades que se deben animar y otras que no, esto se debe a un proceso de renderizado, este proceso tiene consta de los últimos 3 pasos que son los indispensables al trabajar con animaciones, que son el layout, paint y composite.

Justamente estos 3 nos hablaran de que propiedades son adecuiadas o no, en lo general, las propeidades adecuadas son opacity y transform, por que estas solo necesitan del composite.

#### 💛**Problemas de la aceleración por hardware**

- Las imágenes no cargan correctamente.
- En los vídeos, la imagen o el sonido no se reproduce correctamente.
- Algunas partes del navegador aparecen mal diseñadas.

#### 💚Propiedad will-change

Es una propiedad de CSS para anticipar y preparar los cambios. ****Este tipo de optimizaciones puede aumentar la capacidad de respuesta de una página al realizar un trabajo potencialmente costoso antes de que realmente se requieran.

#### 💙Lecturas Recomendadas

[**will-change en ingles**](https://developer.mozilla.org/en-US/docs/Web/CSS/will-change)

[**will-change español**](https://escss.blogspot.com/2014/05/will-change-propiedad-css.html)

Escribir

```css
transition: transform 500ms ease;
```

Es lo mismo que escribir

```css
transition-property: transform;
transition-duration: 500ms;
transition-timing-function: ease;
```

> ### **La aceleración de hardware**
>
> Es una funcionalidad de Chrome en donde el sistema comparte ciertas cargas con la GPU para liberar a la CPU. De esta manera, el navegar mejorará su rendimiento dejando las tareas de reproducción de videos, por ejemplo, a la GPU, librando al procesador de esta carga
>
> **Problemas conocidos por tener activada la aceleración por hardware**
>
> - Las imágenes no cargan correctamente.
> - En los vídeos, la imagen o el sonido no se reproduce correctamente.
> - Algunas partes de Google Chrome aparecen mal diseñadas.

> #### ❤**La aceleración de hardware**
>
> La aceleración por hardware permite usar componentes específicos de tu ordenador para quitar trabajo al procesador de tu dispositivo. Uno de estos componentes puede ser una tarjeta gráfica, que puede usarse para renderizar o mostrar el contenido del navegador en tu pantalla.
>
> #### 🧡Propiedades al animar
>
> Hay propiedades que se deben animar y otras que no, esto se debe a un proceso de renderizado, este proceso tiene consta de los últimos 3 pasos que son los indispensables al trabajar con animaciones, que son el layout, paint y composite.
>
> Justamente estos 3 nos hablaran de que propiedades son adecuiadas o no, en lo general, las propeidades adecuadas son opacity y transform, por que estas solo necesitan del composite.
>
> #### 💛**Problemas de la aceleración por hardware**
>
> - Las imágenes no cargan correctamente.
> - En los vídeos, la imagen o el sonido no se reproduce correctamente.
> - Algunas partes del navegador aparecen mal diseñadas.
>
> #### 💚Propiedad will-change
>
> Es una propiedad de CSS para anticipar y preparar los cambios. Este tipo de optimizaciones puede aumentar la capacidad de respuesta de una página al realizar un trabajo potencialmente costoso antes de que realmente se requieran.

## Preferencias de movimiento reducido

### [Preferencias de Usuario](https://lenguajecss.com/css/responsive-web-design/preferencias-usuario/) @media (propiedad: valor) {}

Dentro del apartado de media queries disponemos de una mecánica para detectar ciertas preferencias de usuario, o lo que es lo mismo, si el usuario ha indicado algunas preferencias por ciertos detalles en su dispositivo, sistema operativo o navegador que utiliza.

#### [prefers-color-scheme](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/prefers-color-scheme)

sistema que permite seleccionar un tema claro o un tema oscuro
***Valores:*** dark, light

#### [prefers-reduced-motion](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/prefers-reduced-motion)

***Valores***
**no-preference**
Indica que el usuario no ha dado a conocer ninguna preferencia al sistema.
**reduce**
Indica que el usuario ha notificado al sistema que prefiere una interfaz que elimine o reemplace los tipos de animación basada en movimiento que provocan incomodidad para las personas con trastornos del movimiento [vestibular](https://vestibular.org/article/what-is-vestibular/desorden-vestibular-vertigo-en-espanol/) que puede causarles mareos, náuseas, dolores de cabeza o algo peor.

**Entonces, ¿debería ir a lo seguro y deshacerme de toda mi animación?**
Hacerlo sería una opción dramática y no necesariamente válida. Si se usan correctamente, las animaciones pueden incluso ayudar a la accesibilidad al ayudar a abordar los problemas de accesibilidad cognitiva.

Informate mas sobre la sensibilidad del movimiento [Link](https://alistapart.com/article/designing-safer-web-animation-for-motion-sensitivity/)

> #### ❤Accesibilidad con prefers-reduced-motion
>
> Existen personas que previeron no ver ciertos movimientos de animación, y desean algo mas suave a la vista. La función de movimiento reducido se utiliza para detectar si el usuario ha solicitado que el sistema minimice la cantidad de movimiento no esencial que utiliza, permitiendo una mayor accesibilidad a todos los usuarios ❤️

En relacion a la accesibilidad, cuando hablamos de transiciones, hay personas que pueden no querer ver las transiciones. Tu te podrías preguntar, ¿Quién no quiere ver las transiciones, si estas son geniales? Bueno, hay personas diversas en la viña del Señor…

El punto es que en el navegador hay un opción para “*deshabilitar*” este tipo de movimientos. Cuando esta opción este prendida, nosotros tendríamos que haber puesto un media queria, mejor conocido como `@media`. Y si, los media queries no solo sirven para hacer responsive design!

La manera en como lo hacemos es la siguiente:

```css
@media (prefers-reduced-motion: no-preference){
	Your: code;
}
```

En el apartado de `Your: code;`, solo tendríamos que cambiar el código para que ya no haya transiciones tan alocadas y el usuario pueda ver algo menos escandaloso. En la clase dan un muy buen ejemplo practico

# 8. Libros recomendados y próximos pasos

## Continúa en el Curso de Animaciones con CSS

![img](https://www.google.com/s2/favicons?domain=http://valhead.com//favicon.ico)[Val Head - Designer & Interface Animation Consultant](http://valhead.com/)

