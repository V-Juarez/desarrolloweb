<h1>Optimización Web</h1>

<h3>Jonathan Alvarez</h3>

<h1>Tabla de Contenido</h1>

- [1. Entendiendo el rendimiento](#1-entendiendo-el-rendimiento)
  - [Todo lo que aprenderás sobre optimización web](#todo-lo-que-aprenderás-sobre-optimización-web)
  - [¿Vale el esfuerzo optimizar un sitio web?](#vale-el-esfuerzo-optimizar-un-sitio-web)
  - [🚗 ¿Cuándo realmente un sitio es rápido o lento?](#-cuándo-realmente-un-sitio-es-rápido-o-lento)
- [2. Antes de optimizar...](#2-antes-de-optimizar)
  - [Aprendiendo a medir](#aprendiendo-a-medir)
  - [User Performance Metrics](#user-performance-metrics)
    - [Web Vital](#web-vital)
  - [Nuestro proyecto](#nuestro-proyecto)
- [3. Crítical Rendering Path](#3-crítical-rendering-path)
  - [Etapas de render del navegador](#etapas-de-render-del-navegador)
  - [Network waterfall y recursos que bloquean el navegador](#network-waterfall-y-recursos-que-bloquean-el-navegador)
  - [Priorización de recursos](#priorización-de-recursos)
  - [Preloading y prefetching de recursos](#preloading-y-prefetching-de-recursos)
  - [Fases Paint y Layout del Critical Render Path](#fases-paint-y-layout-del-critical-render-path)
- [4. CSS](#4-css)
  - [Detectando Paints costosos y optimizando animaciones](#detectando-paints-costosos-y-optimizando-animaciones)
  - [Bloqueos y complejidad en selectores](#bloqueos-y-complejidad-en-selectores)
- [5. WebFonts](#5-webfonts)
  - [WebFonts y su impacto en redimiento](#webfonts-y-su-impacto-en-redimiento)
- [6. Imágenes, Iconos y SVG](#6-imágenes-iconos-y-svg)
  - [Imágenes, formato y compresión](#imágenes-formato-y-compresión)
  - [Imágenes y compresión](#imágenes-y-compresión)
  - [¿WebFont, Imagen o SVG?](#webfont-imagen-o-svg)
  - [Técnicas avanzadas con Lazy Loading](#técnicas-avanzadas-con-lazy-loading)
  - [Técnicas avanzadas con Responsive Loading](#técnicas-avanzadas-con-responsive-loading)
- [7. Aplicaciones JavaScript](#7-aplicaciones-javascript)
  - [JavaScript y aplicaciones modernas y Utilizando un servidor de producción](#javascript-y-aplicaciones-modernas-y-utilizando-un-servidor-de-producción)
  - [Analizando el bundle de la aplicación](#analizando-el-bundle-de-la-aplicación)
  - [Reduciendo el tamaño del bundle](#reduciendo-el-tamaño-del-bundle)
  - [Code Splitting](#code-splitting)
  - [Lazy Module Loading](#lazy-module-loading)
  - [Llevando los listeners a otro nivel](#llevando-los-listeners-a-otro-nivel)
  - [Instalando Modal video](#instalando-modal-video)
  - [Lazy loading del modal](#lazy-loading-del-modal)
  - [Moviendo la carga de rendering hacia el servidor: Server Side Rendering](#moviendo-la-carga-de-rendering-hacia-el-servidor-server-side-rendering)
  - [Aplicando SSR](#aplicando-ssr)
  - [Pre-renderizando el contenido: Static Generation](#pre-renderizando-el-contenido-static-generation)
- [8. Caché](#8-caché)
  - [Cómo funciona el Caché de recursos y CDN](#cómo-funciona-el-caché-de-recursos-y-cdn)
  - [Deploy en Netlify y automatización de contenido en GitHub Actions](#deploy-en-netlify-y-automatización-de-contenido-en-github-actions)
  - [Aplicando Github Actions](#aplicando-github-actions)
  - [Interceptando los requests del navegador con Service Workers](#interceptando-los-requests-del-navegador-con-service-workers)
- [9. Performance Budget](#9-performance-budget)
  - [Performance budget y auditorias automatizadas](#performance-budget-y-auditorias-automatizadas)
  - [Automatizando una auditoría con Lighthouse CI](#automatizando-una-auditoría-con-lighthouse-ci)
  - [Medidas reales y monitoreo constante](#medidas-reales-y-monitoreo-constante)
- [10Conclusiones](#10conclusiones)
  - [Conclusiones](#conclusiones)

# 1. Entendiendo el rendimiento

## Todo lo que aprenderás sobre optimización web

[]()

## ¿Vale el esfuerzo optimizar un sitio web?

Performance.- es referente a la velocidad de carga

La optimización web tata de las personas

- Amazon descubrio que cada 100 milisegundos de latencia les cuesta 1% de las ganacias
  - Amazon gano el 2019 280 billones de dolares
- El 50% de usuarios que están en sitios mobiles van a dejar el sitio web si tarda 3 segundos en cargar

Mientras más rápido sea un sitio web, se tendrá mayores beneficios para el que propone el sitio web, entre ellos esta una mejor experiencia de usuario

Ganancias en de las empresas mas grándes de tecnología. (Amazon, Apple, Alphabet, Facebook, Microsoft). 🔥🤑💸

![income1.jpg](https://static.platzi.com/media/user_upload/income1-72b5cc6d-f065-4873-802a-03aa1ec4fae0.jpg)

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)https://developers.google.com/web/fundamentals/performance/why-performance-matters](https://developers.google.com/web/fundamentals/performance/why-performance-matters)

[![img](https://www.google.com/s2/favicons?domain=https://cdn-images-1.medium.com/fit/c/152/152/1*8I-HPL0bfoIzGied-dzOvA.png)A Tinder Progressive Web App Performance Case Study | by Addy Osmani | Medium](https://medium.com/@addyosmani/a-tinder-progressive-web-app-performance-case-study-78919d98ece0)

## 🚗 ¿Cuándo realmente un sitio es rápido o lento?

Response: Dicta los tiempos ideales de respuesta a las acciones en la página web.
Animation: La duración ideal de las animaciones del sitio web.
Idle: Aprovechamiento de los tiempos muertos en el navegador
Load: El tiempo que tarda en cargar la página por completo.

**RAIL** es un modelo de rendimiento centrado en el usuario que proporciona una estructura para pensar en el rendimiento. El modelo desglosa la experiencia del usuario en acciones clave (por ejemplo, tocar, desplazarse, cargar) y le ayuda a definir objetivos de rendimiento para cada uno de ellos.

- Response ⇒ Tiempos ideales en las cuales las acciones de nuestras paginas web deberían responder
- Animation ⇒ Animaciones que el sitio web realiza
- Idle ⇒ Tiempos muertos del navegador como ser cargar recursos que no son necesarios al momento
- Load ⇒ Cuando tarda un sitio en cargar, una vez que el usuario ingrese a la URL del sitio

<h3>Tiempos de carga recomendados</h3>

- Entre 0 a 300 milisegundos son perfectos 😄
- desde 1000 milisegundos son buenos 🙂
- Cuando tarda mas de 1500 milisegundos ya el usuario percibe que esta pasando algo malo 😐
- luego de los 2 segundos todo es malo 🤨

Cuando tenemos un sitio web lo importante es tener un limite que no exceda los 3 segundos de carga

[![img](https://www.google.com/s2/favicons?domain=https://developers.google.com/web/fundamentals/performance/rail/images/favicon.ico)Measure performance with the RAIL model](https://developers.google.com/web/fundamentals/performance/rail)

# 2. Antes de optimizar...

## Aprendiendo a medir

> lo que no mide, no se mejora

- Rail Model:
  - Centrado en el usuario
  - Métricas de rendimiento basadas en la **experiencia de usuario**

> Entregar el contenido y ser interactiva en menos de 5 segundos

- Esta frase parcialmente es cierta por que debemos recordar que no todos los usuarios tendrán la misma velocidad de internet

> Cumplir las métricas para los usuarios del percentil 75%

- No basta concentrarse en un numero concreto si no que consiste en dar un buen recorrido por todas la pagina

Lo importante es dar una experiencia de usuario en TODO el sitio web.

> El performance es sobre la experiencia que le damos a nuestros usuarios en nuestros sitios web.

[![img](https://www.google.com/s2/favicons?domain=https://www.pingdom.com/wp-content/themes/sw-global/favicon.ico)Website Performance and Availability Monitoring | Pingdom](https://www.pingdom.com/)

[![img](https://www.google.com/s2/favicons?domain=https://webpagetest.org//images/favicon.ico)WebPageTest - Website Performance and Optimization Test](https://webpagetest.org/)

## User Performance Metrics

### Web Vital

**LCP: Larget Contentful Paint**

El tiempo que tarda en pintar el mayor elemento visible en la ventana.

**FID: First Input Delay (Demora del primer input)**

El tiempo que tarda la página en responder a las acciones del usuario.

**\*El FID mide por el mayor tiempo de respuesta.\***

**CLS: Cumulative Layout Shift (Acumulaciones en los altos de los elementos)**

Mide todos los cambios inesperados en el layout de una página.

![img](https://webdev.imgix.net/vitals/fid_ux.svg)

- **Largest Contentful Paint (LCP):** Mide el rendimiento de carga. Para proporcionar una buena experiencia de usuario, el LCP debe ocurrir dentro de los 2.5 segundos posteriores al inicio de la carga de la página.
- **First Input Delay (FID):** Mide la interactividad. Para proporcionar una buena experiencia de usuario, las páginas deben tener un FID de menos de 100 milisegundos.
- **Cumulative Layout Shift (CLS):** Mide la estabilidad visual. Para proporcionar una buena experiencia de usuario, las páginas deben mantener un CLS de menos de 0.1.

[![img](https://www.google.com/s2/favicons?domain=https://web.dev/vitals//images/favicon.ico)Web Vitals](https://web.dev/vitals/)

## Nuestro proyecto

`clonamos el proyecto`

```sh
git clone https://github.com/jonalvarezz/platzi-optimizacion-web
```

Traemos los tags a nuestro proyecto

```sh
git fetch --tag
```

Creamos una rama, con el numero de tag

```sh
# optimization - nombre | agregar el nombre que desees
# Numero o nombre de tag
git checkout -b optimization 10-with-audits
```

Instalamos las dependencias

```sh
npm install

# corremos el proyecto
npm run start
```

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - jonalvarezz/platzi-optimizacion-web: 🚀 Curso de Optimización Web de Platzi por @jonalvarezz](https://github.com/jonalvarezz/platzi-optimizacion-web)

# 3. Crítical Rendering Path

## Etapas de render del navegador

<h4>Ideas/conceptos claves</h4>

Critical Render Path. - es el proceso de tomar el código HTML, CSS y JS para convertirlos en pixeles en la pantalla

DOM. - Document Object Model

CSSOM. - un árbol similar al DOM, pero para CSS

<h4>Apuntes</h4> <h3>Critical Render Path</h3>

Se compone de 5 etapas.

<h4>Object Model [etapas 1 y 2]</h4>

- Tenemos un index.html

```html
<body>
  <h1>Hallo</h1>
  <p>Hallo</p>
</body>
```

- Tenemos un CSS

```css
h1 {
  color: salmon;
}

p {
  display: none;
}
```

- Se construye el DOM
- Construye un árbol de arriba hacia abajo
- También se construye el CSSOM

![1.png](https://static.platzi.com/media/user_upload/1-2740f12d-b115-4bc6-a4fd-f7c1e9632672.jpg)

<h4>Render Tree [etapa 3]</h4>

- Seguiríamos teniendo los mismos elementos HTML
- solo que en este proceso se distingue que va a pintar y que no

![2.png](https://static.platzi.com/media/user_upload/2-5096828b-f194-49b7-b949-027714bfaa98.jpg)

<h4>Layout | Paint [etapas 4 y 5]</h4>

- Lo primero que hace es ver el ancho disponible para pintar ⇒ Viewport ⇒ width-device
- Dependiendo del ancho se estima algunas cosas como el box model

![3.png](https://static.platzi.com/media/user_upload/3-51484774-70a6-4658-ad8b-a7de4fe665c4.jpg)

**RESUMEN:** Lo que ha sucedido aquí fue que se construyó el DOM [HTML], CSSOM [CSS], Se renderizo [HTML + CSS], Se hizo el layout [cálculos geométricos] y se pintó en la pantalla [Dibujar los pixeles en la pantalla].

**Proceso de renderizado del navegador**

![Desktop - 1.png](https://static.platzi.com/media/user_upload/Desktop%20-%201-d8062a03-242b-4576-9d8b-6986b6604518.jpg)

**\*Composite\***: Maneja las capas individuales y asegura el orden correcto de estas capas.
'
**\*\*\***: Los procesos que están marcados con un \*, son procesos que podrían ser re ejecutados debido a cambios visuales, debido a que con JS, con CSS o con la API de Web Animations podemos hacer cambios visuales en la página, por ejemplo: ordenar una lista o agregar elementos al DOM.

**Critical Render Path**

1. Construye el DOM
2. Construye el CSSOM
3. Se renderiza, (Fusiona ambos)
4. Layout (calcula el ancho segun el viewport le da un modelo de caja = box model)
5. Pinta todo en la pantalla

Enlaces de estudio:

1. [Modelo de objetos](https://developers.google.com/web/fundamentals/performance/critical-rendering-path/constructing-the-object-model?hl=es)
2. [Construcción, diseño y pintura del árbol de representación](https://developers.google.com/web/fundamentals/performance/critical-rendering-path/render-tree-construction?hl=es)

[![img](https://www.google.com/s2/favicons?domain=https://abs.twimg.com/responsive-web/web/icon-svg.9e211f64.svg)https://twitter.com/lydiahallie/status/1231255327032541185?s=20](https://twitter.com/lydiahallie/status/1231255327032541185?s=20)

## Network waterfall y recursos que bloquean el navegador

**Tanto JavaScript como CSS son recursos bloqueantes**. Esto quiere decir que cada vez que el navegador encuentra estos archivos, debe parar e interpretarlos mientras sigue haciendo parsing del HTML.

**Script por defecto**
bloquea el parsing durante la descarga y ejecución del script

![script.png](https://static.platzi.com/media/user_upload/script-6b49d095-c6a9-4eac-9d06-567c44bae413.jpg)
**Script Defer**
descarga el script JS pero no lo ejecuta hasta que se finaliza el parsing del HTML.

![defer.png](https://static.platzi.com/media/user_upload/defer-2e3c94f3-a21e-4d1b-9688-0b05e112e761.jpg)

**Script Async**
descarga el script JS durante el parsing y una vez se termine de descargar lo ejecuta inmediatamente, bloqueando solo una “pequeña” parte del parsing.

![async.png](https://static.platzi.com/media/user_upload/async-4b2b2094-d9f6-4677-847a-11fc3851b871.jpg)

**Defer**:
![defer.png](https://static.platzi.com/media/user_upload/defer-dfe18123-46a3-4732-9abf-93af8922117f.jpg)

**Async**:
![async.png](https://static.platzi.com/media/user_upload/async-a1f9d491-fbc7-44ea-85db-c7e82a22fa78.jpg)

## Priorización de recursos

<h4>Ideas/conceptos claves</h4>

Se debe tener un balance entre lo que necesita la app y como le podemos ayudar al navegador

Todas las herramientas que serán vistas dentro de todo el curso serán de doble filo como podemos ayudar al navegador también podemos hacer que su trabajo sea más duro

- No todos los recursos tienen la misma prioridad
- Por ejemplo, en el CSS pasa que metemos todos los estilos para todos los casos que se pueden llegar a dar
  - Estilos dark mode
  - Estilos de desktop
  - Estilos de tablet
  - Estilos de mobile
- Pénesenos en el caso de alguien que navega en el móvil, el deberá descargar todos estos estilos, así no le sea de interés o relevantes
- Con la priorización de recursos en CSS podemos ayudarle a navegador a darles “pistas” para decirle cual tiene mayor prioridad

```html
<!-- Especificamos el atributo media -->
<link
  rel="stylesheet"
  href="/desktop.css"
  media="screen and (min-width: 600px)"
/>
```

- Esta técnica es simple pero eficiente
- Nos ayuda bastante a decirle al navegador que puede ser importante que cargue y que no
- Se debe considerar que cada vez que hagamos esto sera un nuevo request que debamos hacer hacia el servidor

**RESUMEN:** Podemos decirle al navegador que recursos tengan una prioridad mayor con el atributo media en los elementos link, pero se debe tener cuidado porque cada archivo nuevo sera una nueva petición HTTP

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)Unnecesary DOMContentLoaded event · jonalvarezz/platzi-optimizacion-web@ae053e4 · GitHub](https://github.com/jonalvarezz/platzi-optimizacion-web/commit/ae053e49218a7c740007219a1c17c8a635818b6c)

[![img](https://www.google.com/s2/favicons?domain=https://jonalvarezz.com//favicon.ico)Jonathan Alvarez](https://jonalvarezz.com/)

## Preloading y prefetching de recursos

**Preload**: Recurso que se descarga junto con el html
**Prefetch**: recurso que se descarga para un uso futuro
**preconnect**: conexión anticipada a recursos de servidores remotos.

- Podemos decir al navegador cuales son los recursos y dominos que se debe conectar o descargar de forma anticipada
- Existen 3 estrategias para poderlo hacerlo
  - Preload (recursos)
  - Prefetch (recursos)
  - Preconnect (dominios)

```html
<link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin />
<link rel="dns-prefetch" href="https://fonts.gstatic.com/" />
```

- podemos especificar con el atributo rel las estrategias para realizar este proceso

**RESUMEN:** Si le decimos al navegador de forma anticipada que recursos necesita o a que dominios se puede conectar de una forma anticipada podemos mejorar el rendimiento de nuestros sitios.

```js
 <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin />
    <link rel="dns-prefetch" href="https://fonts.gstatic.com/" />
    <link rel="preconnect" href="https://kitsu.io/" crossorigin />
    <link rel="dns-prefetch" href="https://kitsu.io/" />
```

[![img](https://www.google.com/s2/favicons?domain=https://hpbn.co/primer-on-web-performance/assets/icons/icon-192.png)HTTP: Primer on Web Performance - High Performance Browser Networking (O'Reilly)](https://hpbn.co/primer-on-web-performance)

[![img](https://www.google.com/s2/favicons?domain=https://cdn-images-1.medium.com/fit/c/152/152/1*8I-HPL0bfoIzGied-dzOvA.png)Preload, Prefetch And Priorities in Chrome | by Addy Osmani | reloading | Medium](https://medium.com/reloading/preload-prefetch-and-priorities-in-chrome-776165961bbf)

## Fases Paint y Layout del Critical Render Path

<h4>Ideas/conceptos claves</h4>

> El paint es la operación más costosa que puede hacer un navegador

- Estas etapas son inevitables al momento de cargar CSS y JS
  - Podemos tener cuidado al momento de ejecución de ambos

> Cualquier cambio en una propiedad que no sea opacity o transform genera un Paint

- Toda la etapa de renderización se puede ver bloqueada y afectada por lo que pase en el paint
- Podemos controlarlo con nuestras animaciones
- Si tenemos cuidado con las animaciones y las reglas del CSS con los elementos de la pagina
  - Podemos ayudar que el navegador reduzca Complejidad y cantidad de procesos que debe hacer para volver a pintar los elementos
- Facebook está consciente acerca de esto al punto que ellos para su navbar usan una sprites antes que una sombra
  - Esta técnica es usar una imagen pequeña y multiplicarla varias veces
  - Decidieron usar esta técnica debido a que el CSS causaba muchos problemas al momento de hacer scroll

> Debemos tener bastante cuidado con el paint debido a que es un proceso bastante pesado y puede afectar a la experiencia de nuestros usuarios para ello podemos usar técnicas como lo hizo Facebook

> El Paint es la operación mas costosa.

Cualquier cambio en una propiedad que no sea _opacity_ o _transform_, genera un Paint.

<h4>Fases Paint y Layout del Critical Render Path</h4>

Paint es **la operación más costosa que puede hacer un navegador**.

[![img](https://www.google.com/s2/favicons?domain=https://s.ytimg.com/yts/img/favicon-vfl8qSV2F.ico)Building The New Facebook With React and Relay | Ashley Watkins - YouTube](https://www.youtube.com/watch?v=KT3XKDBZW7M)

# 4. CSS

## Detectando Paints costosos y optimizando animaciones

- Este tipo de optimizaciones no es algo que se haga al inicio si no que, al momento de detectar el problema, procedemos con la optimización del lugar visto
- Por lo general se trata de CSS y animaciones
- Esto es bastante notable de percibir ya que nuestro sitio web va lento o las animaciones no van fluidas
- Podemos medir los paints costosos con las dev tools en la sección en **performance**
- Debemos tener siempre en mente que todas las propiedades que cambiarán serán costosas a excepción y el transform
- Podemos preparar al navegador de futuros cambios con la propiedad `will-change` en CSS

**RESUMEN:** Podemos detectar paints costosos con las dev tools integradas de chrome. Tambien podemos reducirlas usando el concepto de Cualquier cambio en una propiedad que no sea opacity o transform genera un Paint y también usando la propiedad will-change de CSS.

`will-change` Sirve para decirle al navegador que algo cambiará y debe estar listo para optimizarlo.

[![img](https://www.google.com/s2/favicons?domain=https://ishadeed.com/article/new-facebook-css//assets/favicon-32x32.png)CSS Findings From The New Facebook Design](https://ishadeed.com/article/new-facebook-css/)

[![img](https://www.google.com/s2/favicons?domain=https://calibreapp.com/blog/investigate-animation-performance-with-devtools/favicon.ico)Investigate Animation Performance with DevTools - Calibre](https://calibreapp.com/blog/investigate-animation-performance-with-devtools)

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)CSS Triggers](https://csstriggers.com/)

## Bloqueos y complejidad en selectores

BEM es una forma de escribir clases en CSS. Viene de Bloque Elemento y Modificado

- Si no le damos la debida atención al CSS se puede volver complejo a lo largo del tiempo
  - Complejo de mantenimiento en equipo
  - Complejidad para el navegador
- Entre más pequeño sea nuestro CSS, mejor
- Entre menos complejos sean los selectores que usemos, el navegador tendrá que hacer un menor esfuerzo
- Anidar selectores genera más trabajo al navegador `.menu > div > img`
- Podemos ayudar al navegador usando BEM
  - Nos dará mayor contexto de que bloques estamos editando
  - No daremos selectores complejos por lo cual facilitaremos el trabajo del navegador
- Nuestro código debería tener como máximo 1 solo selector, 1 sola clase y tratar de evitar los id’s

- Si deseamos priorizar un recurso en el critical render path lo que deberíamos hacer es ponerlo en una etiqueta img

**RESUMEN:** El CSS puede bloquear recursos importantes como una imagen de un logo. Tambien debemos estar conscientes de no dar selectores complejos para hacer que el navegador haga un menor esfuerzo.

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/static/images/logos/platzi_favicon.png)Curso de Tailwind CSShttps://platzi.com/clases/tailwind-css/](https://platzi.com/clases/tailwind-css/)

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)TACHYONS - Css Toolkit](https://tachyons.io/)

[![img](https://www.google.com/s2/favicons?domain=https://tailwindcss.com/apple-touch-icon.png)Tailwind CSS - A Utility-First CSS Framework for Rapidly Building Custom Designs](https://tailwindcss.com/)

# 5. WebFonts

## WebFonts y su impacto en redimiento

- Los webs fonts son bastantes dañinos para el performance
- Puede impactar al punto de que el máximo debería ser 2, Lo recomendable es 1 pero si el rendimiento es crítico entonces no deberías traer web fonts
- En general hay tres formas de cargar fuentes y cada una causa un problem

> 1. **Como estilo**

- <link> común (i.e.: Google fonts)
- HTML parsing no continúa hasta que se descargue la fuente
- **Bloqueante** ⇒ Para el parsing del HTML, para descargar la fuente y una vez descargada, se continua con el parsing

2. **De forma alterna**

- Fuente por defecto mientras carga la web Font
- Flash of Unstyled Text (FOUT)
- Ese cambio genera un parpadeo, el cual es perceptible para los usuarios

3. **Luego del HTML parsing**

- No mostrar texto hasta que se descargue la fuente
- Flash of Invisible Text (FOIT)

- Google Fonts en las últimas versiones nos permite tener una fuente por defecto hasta que se cargue la que deseemos poniendo en el link `&display=swap`

- Tambien podemos hacerlo con una librería open source llamada

**web font loader**

- Esta librería nos brinda eventos de los estados de nuestras fuentes a través de clases

> Las webs fonts son recursos pesados y tienen bastante costo a nivel de performance, debemos tener un límite de dos fuentes y debemos tomar en cuenta todas las estrategias que tenemos para cargarlas.

> FOUT: Flash Of Unstyled Text
> FOIT: Flash Of Invisible Text

Los web fonts son muy populares en el diseño web para darle estilo e identidad en términos de diseño a los sitios web web. Sin embargo, en términos de rendimiento resultan ser muy dañinos. Son tan poco recomendados que incluso las comunidades de diseñadores y desarrolladores recomiendan como máximo 2 web fonts. Si está en tus manos solo deberías un web font. Pero si realmente te interesa el rendimiento de tu sitio web, ya sea porque tus usuarios dependen del rendimiento, no deberías ni siquiera usar web font.

Pero por lo general no tener un web font en los sitios web es muy difícil. Por eso, existen 3 métodos de cargar los web fonts para reducir el impacto de la carga del sitio web. No obstante estas tres maneras traen consigo 3 problemas. miremos:

1. **MODO ESTILO:** La primera forma de hacerlo es cargando los fonts como si fueran estilos CSS con la etiqueta `<Link>` Pero sabemos que por el CRP la carga de estilos son bloqueantes para el parsing del HTML. La carga de los fonts como estilos resultan ser bloqueantes también.
2. **FORMA ALTERNA:** La segunda forma de carga es mostrando una fuente por defecto, una fuente común que ya esté en el sistema de manera predeterminada. Por ejemplo _Arial_ es una fuente que ya viene en todos los sistemas operativos. Descargar la fuente en paralelo, y una vez que ya la fuente esté descargada, hacer el cambio de fuente. Ese cambio de fuentes resulta ser perceptible al ojo humano por eso este método se conoce como: **\*Flash Of Unstyled Text (FOUT).\***
3. **DESPUES DEL HTML PARSING:** esta 3ra estrategia consiste en no mostrar ningún texto hasta que se descargue la fuente. Esta estrategia se conoce como **\*Flash Of Invisible Text (FOIT)\***

Google fonts es uno de los proveedores de fonts mas populares en Internet, y desde la liberación de sus últimas versiones, permite definir el comportamiento de carga de la fuente. \**En los parámetros de su url puedes puedes usar el *query-param\* `display`

- Para usar **\*Flash Of Unstyled Text (FOUT).\*** Podemos usar el valor `swap`
- Para usar **\*Flash Of Invisible Text (FOIT).\*** Podemos usar el valor `block`

```html
<link
  href="https://fonts.googleapis.com/css?family=Muli&display=swap"
  rel="stylesheet"
/>
```

Pero no todos los proveedores tienen esta opción para facilitar la definición del comportamiento de carga de la fuente. Para eso podemos usar otras alternativas como:

_webfontloader :_ https://github.com/typekit/webfontloader

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)Developing a Robust Font Loading Strategy for CSS-Tricks—zachleat.com](https://www.zachleat.com/web/css-tricks-web-fonts/)

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - typekit/webfontloader: Web Font Loader gives you added control when using linked fonts via @font-face.](https://github.com/typekit/webfontloader)

# 6. Imágenes, Iconos y SVG

## Imágenes, formato y compresión

Las imágenes son una de las formas mas simples y más seguras en la que podemos reducir el tamaño de nuestras páginas web.

Las imágenes por lo general se recomienda que pesen 70Kb sin embargo cuando vamos a mirar a lo largo de la historia el tamaño de las imágenes en un sitio web ha crecido en dispositivos móviles hasta 900Kb y en el escritorio hasta 1Mb.

Existen diferentes maneras de reducir el peso de las imágenes: Desde versiones online, hasta API’s o servicios de terceros que nos ayudan con esta reducción.

[TinyPNG](https://tinypng.com/) Es una herramienta online que te permite cargar imágenes para reducir el peso. Y como API’s tenemos a [Cloudinary](https://cloudinary.com/), que es un hosting de media que te ayuda a optimizar tus imágenes sin que te estés preocupando, entre otros como Netlify, y la misma API de TinyPNG.

Pero También podemos optimizar nuestras imágenes desde nuestro código usando herramientas de empaquetado como Webpack, Grunt, Gulp, etc.

Con las API’s y con las herramientas de empaquetado no tenemos que preocuparnos de hacer la optimización de las imágenes de manera manual.

> **Formatos**

Como desarrolladores debemos conocer la diferencia de formato de imágens que existen, y en que caso utilizar uno u otro.

- GIF.
- PNG & PNG-8
- JPG

**GIF** Todos lo conocemos porque siempre que pensamos en una imagen animada, pensamos en GIF y en Efecto es el único formato que nos permite animar imágenes. Pero no solo nos sirve para animar. GIF es un formato super liviano que solo nos permite tener 256 colores, y es buenísimo cuando nuestras imágenes son muy simples y pequeñas porque nos va a dar un formato de compresión genial.

**PNG-8** Trae todo lo que ya trae GIF pero nos permite tener transparencias.

**PNG - PNG24** Trae todo lo que PNG-8 ofrece pero con colores ilimitados, nos permite manejar degradados, Es decir: no solo maneja dos colores, sino toda la gama de colores que hay entre un color y otro. Entonces PNG es un formato optimo para manejar millones de colores como por ejemplo los degradados.

**JPG** Es el formato perfecto para fotografías, siempre que hablemos de fotografías debe estar en nuestra mente el formato JPG. La única diferencia entre JPG y PNG es la transparencia, porque al igual que PNG, JPG tenemos a disposición todos los millones de colores disponibles en una pantalla, y todos los degradados.

**JPG** tiene otra cualidad y es que permite la carga de imágenes en modo no progresivo y en modo progresivo.

Modo NO progresivo.

Modo progresivo.

<h3>Reducir peso de imágenes</h3>

**Only**

- TinyPNG

**API**

- Cloudinary
- Netlify
- TinyPNG

**Dev Flow**

- Webpack
- Grunt
- Gulp
- post-commit

<h3>Formatos de compresión</h3>

Los más populares

- GIF
- PNG (y PNG-8)
- JPG

<h4>GIF</h4>

- Es un formato liviano
- 256 colores
- No degradados
- Perfecto cuando hay pocos colores, colores planos y sin transparencia

<h4>PNG-8</h4>

- Nos da todas las ventajas de GIF + transparencias
- Perfecto para íconos y algunos logos

<h4>PNG (24)</h4>

- PNG-8 + colores ilimitados
- imágenes con degradados o muchos colores con **transparencia**

<h4>JPG o JPEG</h4>

- Nos sirve para **fotografías**
- Millones de colores
- Degradados
- Sin transparencia
- Tiene dos modos de progresión de carga
  - Progresivo
  - No progresivo
- Siempre sera bueno dar un JPG progresivo

- Por lo general maximo 70Kb en el peso de nuestras imagenes
- [TinyPNG](https://tinypng.com/) : Permite cargar imágenes para reducir el peso.
- [Cloudinary](https://cloudinary.com/) : Hosting de media que te ayuda a optimizar tus imágenes.
- [Netlify](https://www.netlify.com/)
- GIF : Formato que nos permite animar imágenes.

PNG-8 Trae todo lo que ya trae GIF pero tiene transparencias.

- PNG - PNG24 : formato optimo para manejar millones de colores como por ejemplo los degradados.
- JPG : Formato perfecto para fotografías. Permite la carga de imágenes en modo progresivo

[![img](https://www.google.com/s2/favicons?domain=https://www.photopea.com/promo/icon512.png)Photopea | Online Photo Editor](https://www.photopea.com/)

[![img](https://www.google.com/s2/favicons?domain=https://tinypng.com//images/favicon.ico)TinyPNG – Compress PNG images while preserving transparency](https://tinypng.com/)

## Imágenes y compresión 🚀

![Captura.PNG](https://static.platzi.com/media/user_upload/Captura-fd826699-ee7b-4d0c-9ee5-d8e42617c2b5.jpg)

AVIF es un nuevo formato que llegará a la web. Hasta el momento ha demostrado mejor compresión que [WebP](https://netflixtechblog.com/avif-for-next-generation-image-coding-b1d75675fe4)

NOTA:
Todavía no lo uses porque solo es soportado por chrome (por el momento)

[JSON Viewer](https://chrome.google.com/webstore/detail/json-viewer/gbmdgpbipfallnflgajpaliibnhdgobh?hl=es)

## ¿WebFont, Imagen o SVG?

Una **imagen** es una matriz dividida en cuadrados cada uno, es la representación de un pixel

**SVG** o vectores son elementos generados matemáticamente por el navegador

**Above the fold** primeros 500 a 600 pixeles de la pantalla, es la parte más importante porque es lo primero que ven los usuarios

<h4>Apuntes</h4>

<h3>Imagen vs Vectores</h3>

- Si aplicamos zoom a cada uno
  - Imagen ⇒ Se verán los pixeles
  - Vector ⇒ Mantendrá la calidad

<h3>Web Fonts</h3>

**Ventajas**

- Son prácticos
- Fáciles de usar y distribuir

**Desventajas**

- Un recurso más
- HTTP Request
- Bloqueantes
- No son accesibles

<h3>SVG</h3>

**Ventajas**

- Livianos
- Inline en el HTML
- Accesibilidad
- Animaciones
- Data URI

**Desventajas**

- Se quita facilidad
- Requiere un diseñador
- Incrementa el largo del HTML ⇒ Puede ser incómodo para el equipo de trabajo, pero para el navegador no

**¿Cuándo usarlos?**

- Logos (especialmente “**Above the fold**”)
- Ilustraciones
- Ilustraciones animadas
- En general: gráficos de los que puedas mantener un fácil control

Tenemos dos opciones las webfonts que son bastantes fáciles y practicas al momento de usarlas, pero implican un recurso más que pedir mediante HTTP, volviéndolo bloqueante a nivel de performance, una alternativa es un SVG que nos da una mejor accesibilidad, pero la desventaja es que puede llegar a ser extenso en HTML, solo afectando un poco a la experiencia de desarrollo.

Pueden usar Figma para convertir sus íconos en SVG muy fácil

- Solo lo arrastran, lo sueltan en el app de Figma y luego lo exportan como svg.
- Figma tiene una versión Web por ende no necesitan instalarlo 😀
- Pueden instalar un plugin llamado iconify que tiene muchos íconos SVG ya listos.

## Técnicas avanzadas con Lazy Loading 👨‍🚀

Intersection observer es una API del navegador la cual ve cuales son los elementos visibles del documento y según a esa información genera eventos

<h4>Recursos</h4>

[eloquent-joliot-wcc52](https://codesandbox.io/s/eloquent-joliot-wcc52)

[ApoorvSaxena/lozad.js](https://github.com/ApoorvSaxena/lozad.js)

[Can I use… Support tables for HTML5, CSS3, etc](https://caniuse.com/?search=intersection)

- Existen lazy loading de carga y de progresión
- Medium aplica una técnica de lazy loading en las imágenes
  - Al principio nos aparecerá una imagen borrosa y pixelada hasta que la tengamos lista y sea visible en pantalla
  - Ira progresivamente mejorando la imagen
- El lazy loading de carga, trata de cargar las imágenes cuando la imagen sea de carga
  - Esto es conveniente para ahorrar recursos a los usuarios
- Existen tres formas de hacer lazy loading de carga

<h3>De forma nativa</h3>

El navegador se encargará de hacerlo de forma automática

```jsx
<img src="..." loading="lazy" alt="..." />
```

**Ventajas**

- Nativo
- Facil
- Conveniente
- Video, iframes

**Desventajas**

- Es tan nuevo, que tiene poco soporte (actualmente)

<h4>Intersection Observer</h4>

**Ventajas**

- Técnicamente más correcto (luego de nativo)
- Relativamente fácil

**Desventajas**

- No soportado por internet explorer
- Versión 2 en proceso

<h3>Scroll Listener</h3>

**ventajas**

- Soportado en todos los navegadores

**Desventajas**

- Scroll listener
- Muchos cálculos por evento

Para poder cargar las imágenes dinámicamente tenemos diferentes alternativas, en cada una debemos considerar el soporte que tiene con los navegadores y como lo implementaremos en el proyecto

[![img](https://www.google.com/s2/favicons?domain=https://codesandbox.io/s/eloquent-joliot-wcc52/favicon.ico)eloquent-joliot-wcc52 - CodeSandbox](https://codesandbox.io/s/eloquent-joliot-wcc52)

## Técnicas avanzadas con Responsive Loading

[- HTML: Lenguaje de etiquetas de hipertexto | MDN](https://developer.mozilla.org/es/docs/Web/HTML/Elemento/picture)

- Una imagen de 300 kb
- Un usuario que se conecte con un dispositivo de gama alta con una red 5G va ser diferente con un usuario de gama baja con una red 2G

> Como ayudamos a que nuestros usuarios tengan la mejor imagen sin importar su conexión y dispositivo

- Es ahi donde viene el responsive loading el cual se encarga de esta tarea
- Esto nos ayuda a cargar imágenes según el tamaño del dispositivo
- Existen servicios o librerías como cloudinary o Gatsby el cual nos ayudan a usar esta característica

Podemos mejorar la experiencia de usuario con las imágenes brindando soporte para diferentes tamaños de dispositivo a las imágenes y que estas carguen la resolución y el tamaño según a este criterio.

> El elemento HTML `<picture>` es un contenedor usado para especificar múltiples elementos <source> y un elemento <img> contenido en él para proveer versiones de una imagen para diferentes escenarios de dispositivos.

[![img](https://www.google.com/s2/favicons?domain=https://www.gatsbyjs.org/showcase//favicon-32x32.png?v=4a9773549091c227cd2eb82ccd9c5e3a)Showcase | GatsbyJS](https://www.gatsbyjs.org/showcase/)

# 7. Aplicaciones JavaScript

## JavaScript y aplicaciones modernas y Utilizando un servidor de producción

> "if client-side JavaScript isn't benefiting the user-experience, ask yourself if it's really necesary" - Addy Osmani - The cost of JavaScript

- Web moderna está basada en JavaScript
- Muchas veces no somos conscientes al momento de instalar muchas dependencias, estamos creando una mala experiencia para los usuarios
- En el 2018 se estimó que el tamaño medio de los sitios web esta alrededor de 350kB [Tamaño moderadamente aceptable]
  - El tiempo de espera para que una aplicación sea interactiva tarda más de 15 segundos

> Si el JavaScript de lado del cliente no está beneficiando a la experiencia de usuario, pregúntate a ti mismo, si realmente es necesario

- Webpack en modo de desarrollo no realiza ninguna optimización de código

- lo que haremos sera:

  1. Compilar webpack en producción
  2. Utilizar un servidor listo para producción

- Si ponemos

  ```
  -p
  ```

  en webpack hace dos cosas

  - Pone `NODE_ENV=production` a node
  - Pasa el parámetro `mode=production` a webpack

* Es importante tomar en cuenta el modo de producción para analizar si vamos bien con nuestro JavaScript a nivel de performance

Instalar server

```sh
npm install --save serve
```

`package.json`

1. Compilar Webpack produccion

   ```json
   "build": "webpack -p"
   ```

2. Utilizar un servidor para produccion

   ```json
   "serve" : " serve ."
   ```

3. Construyendo un build.js más liviano con el script “npm run build”-
4. Creando un servidor, usando “npm run serve”, que lea el proyecto de la raíz que incluye el build.js, que acaba de construirse con “npm run build”

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)The cost of JavaScript in 2019 · V8](https://v8.dev/blog/cost-of-javascript-2019)

## Analizando el bundle de la aplicación

- webpack tienen herramientas y plugins que nos ayudan a analizar el bundle del JS
- Podemos usar webpack bundler analyzer para analizar y tener una medida exacta de que está pasando dentro del bundler a través de una grafica

Podemos verificar el tamaño de nuestras dependencias y nuestro código a través de herramientas de análisis de tamaño

`analyzer` install

```sh
npm install --save-dev webpack-bundle-analyzer
```

En el archivo package.json, de forma que sea más sencillo utilizar este nuevo comando de webpack.

```sh
"analyze": "webpack -p --analyze"
```

Para usarlo solo ejecuten en consola **npm run analyze**

file `webpack.config.js`

```js
const BundleAalyzerPlugin = require('webpack-bundle-analyzer')
	.BundleAnalyzerPlugin

const config = {
    ...
	plugins: [new BundleAnalyzerPlugin()],
}
```

config `webpack.config.js`

```javascript
...
const shouldAnalyze = process.argv.inlcudes('--analyze');
...

// Mover de config -> bundleAnalyzer plugins.
const plugins = []
if (shouldAnalyze) {
    plugins.push(new BundleAnalyzerPlugin())
}

const config = {
    ...
	plugins,
}
```

en terminal ejecutamos

```sh
npm run build -- --analyze
```

[<img src="https://res.cloudinary.com/practicaldev/image/fetch/s--0ca-E1mS--/c_imagga_scale,f_auto,fl_progressive,h_900,q_auto,w_1600/https://dev-to-uploads.s3.amazonaws.com/uploads/articles/xedb93rflxd23rgft0y2.jpeg" alt="Next.js — The Scalable Way to Internationalize Using next-i18next - DEV  Community" style="zoom:2%;" /> next.js](https://github.com/vercel/next.js/tree/canary/examples/analyze-bundles)

[![img](https://www.google.com/s2/favicons?domain=http://bundlephobia.com/apple-touch-icon.png)BundlePhobia ❘ cost of adding a npm package](http://bundlephobia.com/)

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - webpack-contrib/webpack-bundle-analyzer: Webpack plugin and CLI utility that represents bundle content as convenient interactive zoomable treemap](https://github.com/webpack-contrib/webpack-bundle-analyzer)

## Reduciendo el tamaño del bundle

Para reducir el peso de moment, se puede usar este plugin https://www.npmjs.com/package/moment-locales-webpack-plugin que te permite elegir los idiomas que se quieren incluir en el bundle final.

En nuestro caso, necesitamos español, por lo tanto en el webpack config se agrega:

```js
const MomentLocalesPlugin = require("moment-locales-webpack-plugin");
```

Y a plugins:

```js
new MomentLocalesPlugin({
    localesToKeep: ['es'],
}),
```

Actualmente se estaba usando ingles en el sistema. Para usar español, en carouselItem.js ir donde se importa moment, y dejarlo así:

```js
import moment from "moment";
moment.locale("es");
```

Así el bundle queda mucho más liviano, y se usa fechas en español. Al parece, el paquete moment incluye internamente el ingles, en caso que no se use ningún paquete de idioma.

[BundlePhobia](https://bundlephobia.com/)

- Podemos usar soluciones online para analizar que puede estar pesando dentro de nuestra aplicación una de ellas es bundle Phobia
- Existen librerías que son “Tree Shakable”
  - Ayudan al empaquetador de solo sacar lo que se necesitan
- Nos podemos ayudar de bundle Phobia para encontrar paquetes que sean más pequeños a la hora de hacer el bundle de nuestra aplicación

Para reducir el tamaño de nuestro bundle es importante tomar en cuenta las dependencias que usamos y encontrar la forma de reducir el tamaño ya sea usando las funciones necesarias si es que la libreria es tree shakable o buscando una alternativa liviana

[![img](https://www.google.com/s2/favicons?domain=https://date-fns.org//assets/be442fef61accd577b80a30cc8c59ac7.png)date-fns - modern JavaScript date utility library](https://date-fns.org/)

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)require moment without locales · Issue #2416 · moment/moment · GitHub](https://github.com/moment/moment/issues/2416#issuecomment-111713308)

[![img](https://www.google.com/s2/favicons?domain=https://momentjs.com//static/img/moment-favicon.png)Moment.js | Home](https://momentjs.com/)

## Code Splitting 🏹

Code splitting por paginas es una manera de hacer code splitting el cual consiste en cuando el usuario cargue una página en específico el bundle que se enviara al navegador sera una sola fracción y lo que esa página necesite

- Si bien podemos reducir el tamaño de nuestros bundle, llegará un momento donde no tendrá más reducción
- En ese momento debemos tomar otras estrategias y el code splitting es una de ellas
- En vez de tener un bundle gigante de nuestra aplicación, lo que hace el code splitting sera dividirlo en diferentes partes para que sea mucho más fácil y pese menos a la hora de enviarlo al navegador
  - Frameworks como Angular, Next y Gatsby se genera un code splitting **basado en paginas**
- Otra técnica es dividir el código de las librerías [vender] de nuestro bundle final
  - Esta técnica es beneficiosa en el sentido de que los navegadores hacen cache de los archivos, por lo cual los vendors se quedaran en cache ya que estos no se suelen actualizar mucho.
  - Como efecto reduciremos la cantidad de requests

Podemos ver que el code splitting es dividir el código, es beneficioso el uso que se le quiera dar, ya que se puede implementar de diferentes formas

**Dividir el codigo en pedazos**

```js
optimization : {
    splitChunks: {
      chunks: 'all',
    },
  },
```

`filename: '[name].bundle.js',`
`index.html`

```js
<script async src="dist/vendors~main.bundle.js"></script>
    <script async src="dist/main.bundle.js"></script>
```

Este metodo es fundamental para la caché

[![img](https://www.google.com/s2/favicons?domain=https://webpack.js.org/configuration/optimization//bc3effb418df77da9e04825c48a58a49.ico)Optimization | webpack](https://webpack.js.org/configuration/optimization/)

## Lazy Module Loading

En carouselItem.js agregar la clase _carousel-item_ en e div

```javascript
h(
    'div.carousel-item',
    h('img', { src: imageUrl, alt: '', loading: 'lazy' }),
```

Instalar la dependencia de lozad

```sh
npm install --save lozad
```

En el index.js que está en la carpeta src

```javascript
import lozad from 'lozad'
...
...
...
!(async function(document) {
  const mountReference = document.querySelector('.main').lastElementChild

  if (!mountReference) {
    return 0
  }
...
...
...

// Add lazy loading
    const carouseImages = document.querySelectorAll('.carousel-item__img');
    const observer = lozad(carouseImages);
    observer.observe();

    const allYouTubeLinks = document.querySelectorAll('.js-video-link');
    // console.log(allYouTubeLinks);
    allYouTubeLinks.forEach((link) => {
      link.addEventListener('click', modalListener);
    });

})(document, window)
```

En el index.js del modal

```javascript
export const modalListener = event => {
  event.preventDefault();
  const link = event.target;
  console.log(link);
};
```

---

**lazy load como lo hizo el profesor por eso puede que haya código repetido)**:😅

1. instale dependencia:

```sh
npm install lozad -S
```

1. Creamos en la carpeta **src** del proyecto la carpeta **modal** y dentro de modal se creó el archivo **index.js**.
2. en src/**modal**/index.js se escribió:

```javascript
export const modalListener = event => {
  event.preventDefault();
  const img = event.target;
  const link = img.parentElement;
  console.log(link.href);
};
```

1. en src/**index.js** se agregó el siguiente código:

```javascript
import lozad from 'lozad';
import { modalListener } from './modal';
...
...
    .insertAdjacentElement(
      'afterend',
      Carousel({
        itemsList: popular,
      })
    )

    /* ENTRE ESTA LINEA!! */

    // Add lazy loading
    const carouseImages = document.querySelectorAll('.carousel-item__img');
    const observer = lozad(carouseImages);
    observer.observe();

    const allYouTubeLinks = document.querySelectorAll('.js-video-link');
      // console.log(allYouTubeLinks);
      allYouTubeLinks.forEach((link) => {
        link.addEventListener('click', modalListener);
    });

    /* Y ENTRE ESTA LINEA */
})(document, window)
```

1. y por ultimo se agrego en el archivo **CarouselItem.js**:

- SE AGREGÓ EL ATRIBUTO CLASSNAME A LA ETIQUETA ‘a’, este código comienza como en la linea 8, al menos en mi editor.

```javascript
const Controls = ({ slug, youtubeVideoId }) =>
  h(
    'div',
    h(
      'a', // esta 'a' ó 'a.js-video-link y QUITAMOS EL ATRIBUTO className'
      {
        className: 'js-video-link',
        href: `https://www.youtube.com/watch?v=${youtubeVideoId}`,
        title: 'Watch trailer',
        target: '_blank',
        rel: 'noreferrer',
      },
```

y ya! , me gustaria feedback, para saber donde me equivoque, gracias 😄

[![img](https://www.google.com/s2/favicons?domain=https://codesandbox.io/s/eloquent-joliot-wcc52?file=/lazy-loading.js/favicon.ico)eloquent-joliot-wcc52 - CodeSandbox](https://codesandbox.io/s/eloquent-joliot-wcc52?file=/lazy-loading.js)

## Llevando los listeners a otro nivel

Event Delegation, aquí está explicado en detalle: https://javascript.info/event-delegation

En pocas palabras:

- En vez de tener varios Event Listeners haciendo lo mismo en diferentes nodos
- Se tiene solo uno que ‘escuche’ todos esos nodos.

## Instalando Modal video

La solución que encontré para que se pudiese reproducir el video en el modal fue solo cambiar una letra jeje

Cambiar esto

```jsx
button.dataset.videoid = videoId;
```

por esto

```jsx
button.dataset.videoId = videoId;
```

Tuve que recurrir a la documentación de [MDN](https://developer.mozilla.org/es/docs/Web/API/HTMLElement/dataset), pasa que la librería de modal-video necesita el dataset de esta forma

```js
data - video - id = "XJS_UYNq4No";
```

y cuando asignamos el dataset como en la clase, le estamos entregando a la librería esto

```js
data - videoid = "XJS_UYNq4No";
```

De esa forma la librería no obtiene el id y nos entrega un **undefined**, para que con JS pongamos un - entre video y id tenemos que usar camelCase, y se resuelve, ejemplo:
`button.dataset.videoid = videoId`
da como resultado

```html
<button data-videoid="XJS_UYNq4No">Open Video</button>
```

Mientras que con `button.dataset.videId = videoId`
Genera esto

```html
<button data-video-id="XJS_UYNq4No">Open Video</button>
```

Y la librería de JS ya reconoce el id 😄

Espero haberme explicado bien

[![img](https://www.google.com/s2/favicons?domain=https://javascript.info/event-delegation/img/favicon/apple-touch-icon-precomposed.png)Event delegation](https://javascript.info/event-delegation)

[![img](https://www.google.com/s2/favicons?domain=https://static.npmjs.com/58a19602036db1daee0d7863c94673a4.png)modal-video - npm](https://www.npmjs.com/package/modal-video)

## Lazy loading del modal

En la pestaña “network”, y una vez cargado el sitio, cambiamos la velocidad de network a “slow 3g”, va a demorar una eternidad en abrir un modal. A simple vista pareciera que es un error o el sitio es extremadamente lento para cargar.

A- Descargar todo lo necesario en el sitio en una primera carga
B- Descargar lo mínimo, y luego inmediatamente empezar a descargar el código del modal, mientras ya puedo navegar por el sitio. Claro, aplicando controles para no degradar la UX

[![img](https://www.google.com/s2/favicons?domain=https://static.npmjs.com/58a19602036db1daee0d7863c94673a4.png)modal-video - npm](https://www.npmjs.com/package/modal-video)

[![img](https://www.google.com/s2/favicons?domain=https://www.snowpack.dev//favicon/apple-touch-icon.png)Snowpack](https://www.snowpack.dev/)

## 🚚 Moviendo la carga de rendering hacia el servidor: Server Side Rendering

Client Side Rendering es que toda la carga suceda en el navegador y en el cliente

- Client-Side Rendering tiene la característica que
  - No se mostrará nada hasta que el navegador haya descargado y ejecutado el JS

<h3>🆚 Client Side Rendenring vs Server Side Rendering</h3>

**Client Side Rendering**

- Tenemos un navegador y un servidor
- Cada vez que entramos a un sitio web el servidor responde un archivo JS
- La página comienza a renderizar HTML a partir del JS recibido
- Todo nuestro contenido principal sucede dentro del cliente [Navegador o Browser]

**Server Side Rendering**

- Nuestra página se conecta al servidor, pero este le envía un HTML ya renderizado, es decir que se salta el proceso de esperar el JS para mostrar el contenido
- El JS seguirá siendo usado, pero lo separaremos para la interactividad

> Server Side Rendering es una estrategia de enviar “ya comido” nuestra aplicación a nivel HTML es decir que no sera necesario esperar el JS para mostrar contenido

> En este contexto cuando hablamos de “Render” es el proceso de transformar JavaScript en HTML/CSS.En una Single Page Application, el JavaScript se interpreta en el navegador y produce HTML/CSS. Moverlo al servidor, es mover ese proceso en específico.
>
> Ahora, El Render, como proceso de pintar píxeles en la pantalla siempre se realizará en el navegador a través del Critical Rendering Path!. En efecto tienen los mismos nombres, nunca lo había pensado así! “Render” es una palabra muy genérica que aplica para describir muchas cosas 😄
>
> > Solo renderizar, pasar de JS a HTML, es muy barato en términos de costo para un computador, y más si es un servidor. Sólo se vuelve pesado si hay operaciones muy muy costosas en el medio; por ejemplo calcular un nuevo decimal para el número PI y luego renderizarlo en HTML. Lo costoso es lo primero.
> >
> > Por eso depende de las APIs a las que se conecte la app y lo rápida que sean…
> >
> > Será tan rápido como la respuesta API más lenta.

## Aplicando SSR

Mas relevante es que con SSR tarda menos tiempo en “Scripting”.

![q.png](https://static.platzi.com/media/user_upload/q-0f3e9186-8963-48c1-a206-fb6c98f9b470.jpg)

[![img](https://www.google.com/s2/favicons?domain=https://overreacted.io/goodbye-clean-code//icons/icon-48x48.png?v=b6e5f83ec2e878ec4501f7b46f0374ed)Goodbye, Clean Code — Overreacted](https://overreacted.io/goodbye-clean-code/)

## 🏛️ Pre-renderizando el contenido: Static Generation

- Actualmente en el server side rendering on demand, Cada vez que nuestro usuario haga un request, el servidor traerá la información de la API
  - Si se conectan dos usuarios, se harán dos request a las Apis que estamos utilizando
- Esto depende del proyecto, existen proyectos los cuales no es tan necesario que cada vez en un intervalo corto de tiempo el servidor tenga que ir hasta la API, porque es probable que la información sea exactamente la misma
- En estos casos podemos usar **Static Site Generation**
- Consiste en hacer que en el tiempo de construcción del proyecto traiga todo el contenido y que genere una respuesta para enviar a todas las conexiones

Static Site Generation nos renderiza y busca los datos una sola vez, pero nos da la ventaja de tener todo listo, pero solo se debe usar cuando los datos no cambien en tiempo real.

> JAMStack
>
> J -> Javascript = interacción del sitio web
> A -> Api’s = interfaz de aplicaciones para consultar información que ya es abstraida y ha realizado un proceso en un servidor
> M -> Markup = plantilla construida en tiempo de compilación

- Se trata de pre-renderizar el contenido
- Ensenia al servido que en el tiempo de construccion del proyecto, traiga todo el contenido de la API’s de una vez, lo guarda y esa unica respuesta generada se la envia a todas las conexiones
- Lo podemos subir a cualquier servidor en Internet
- No aplica para metricas y datos que se necesitan visualizar en tiempo real
- [SSR y SSG](https://matiashernandez.dev/blog/post/jamstack-que-es-ssg-y-ssr)

> Cada vez que un usuario se conecta, hace una petición al servidor, esa petición va a traer la información de la API.
>
> Server Side Rendering on Demand: Cada vez que hay una nueva conexión vamos a ir a las API a traer información.

[![img](https://www.google.com/s2/favicons?domain=https://jamstack.org//img/favicon.ico)Jamstack | JavaScript, APIs, and Markup](https://jamstack.org/)

# 8. Caché

## 🕷️ Cómo funciona el Caché de recursos y CDN

**CDN** viene de Content Delivery Network, nos ayuda a poner servidores distribuidos por todo el mundo

**latencia** es el tiempo de ida y vuelta de un cliente hacia el servidor

- Redes Distribuidas ⇒ CDN ⇒ **Content Delivery Network,**
- Cuando tenemos nuestros sitios web, ese navegador se está conectando a un servidor que está en otro lugar
  - Con ayuda del servidor, el navegador sabe cuándo ha cambiado la información
- Los servidores tienen diferentes métodos para indicar que el contenido cambio
  - Como ser HTTP 304 ⇒ El contenido no ha cambiado
  - ETAG
  - Headers del navegador para dar un tiempo de vida para manejar el tiempo de vida
- Cuando únicamente tenemos un servidor, podemos tener un cliente bastante lejos del mismo eso significara que tomara más tiempo que uno que esté más cerca del cliente
  - CDN se encarga de este tipo de operaciones para mejorar la velocidad de carga
- Eso nos ayuda a reducir la **latencia**

> Podemos mejorar el tiempo de carga de nuestros sitios utilizando un CDN de tal forma que se pueda encontrar un servidor lo más cerca posible del usuario para mandarle la información y reducir la latencia
>
> Son redes distribuidas por el mundo que nos ayudan a optimizar nuestras paginas.

## Deploy en Netlify y automatización de contenido en GitHub Actions

[![img](https://www.google.com/s2/favicons?domain=https://www.netlify.com//v3/static/favicon/apple-touch-icon.png)Netlify: All-in-one platform for automating modern web projects](https://www.netlify.com/)

[![img](https://www.google.com/s2/favicons?domain=https://a0.awsstatic.com/libra-css/images/site/fav/favicon.ico)Develop mobile and web apps fast – AWS Amplify – Amazon Web Services](https://aws.amazon.com/amplify/)

[![img](https://www.google.com/s2/favicons?domain=https://assets.vercel.com/image/upload/q_auto/front/favicon/vercel/57x57.png)Develop. Preview. Ship. For the best frontend teams – Vercel](https://vercel.com/)

## Aplicando Github Actions

`.github/workflows/build.yml`

```yml
# Flujo para ejecutar un build de produccion (rama master)
# En Netlify con el propósito de que nuestros contenido se
# actualice.
name: Trigger Netlify Build

on:
  schedule:
    # You can use https://crontab.guru to generate the
    # desired cron sheduled :)
    - cron: "0 10 * * 1" # At 10:00 on Monday.

jobs:
  build:
    name: Request Netlify Webhook
    runs-on: ubuntu-latest
    steps:
      - name: Ping
        run: curl -X POST -d {} https://api.netlify.com/build_hooks/5edca83478745263425750d1
```

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)Features • GitHub Actions · GitHub](https://github.com/features/actions)

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)Crontab.guru - The cron schedule expression editor](https://crontab.guru/)

## Interceptando los requests del navegador con Service Workers

`service-worker.js`

```js
const CACHE_NAME = "app-v1";

self.addEventListener("fetch", myCustomFetch);
self.addEventListener("activate", clearCache);

function myCustomFetch(event) {
  const response = cacheOrFetch(event);
  event.respondWith(response);
}

async function cacheOrFetch(event) {
  // event.request contiene la informacion del request, i.e.: la url
  // 1. Verificar la respuesta que necesitamos ya se encuentra en el cache
  let response = await caches.match(event.request);

  // 2. Si es cierto, retornamos la respuesta desde el cache > end
  if (response) {
    return response;
  }

  // 3. Si no, hacemos un fetch al servidor para obtener la respuesta
  response = await fetch(event.request);
  // 3.1 Si la respuesta no es valida > end
  if (
    !response ||
    response.status !== 200 ||
    response.type !== "basic" ||
    !isAssetCSS(event.request.url)
  ) {
    return response;
  }

  // 4. Cuando tengamos la respuesta devuelta del servidor, la almacenamos
  //    en el cache para proximas respuestas.
  const clonedResponse = response.clone(); // Stream que solo se puede leer una vez
  caches.open(CACHE_NAME).then(cache => {
    cache.put(event.request, clonedResponse);
  });

  return response;
}
const assetsRegExp = /.png|.gif|.jpg|.jpeg|.css|.js/g;
function isAssetCSS(url) {
  return assetsRegExp.test(url);
}

function clearCache(event) {
  const deletePromise = caches.delete(CACHE_NAME);
  event.waitUntil(deletePromise);
}
```

Debemos llamar el service worker en el index.template.html de la siguiente forma:

```js
<script>
  if ('serviceWorker' in navigator){" "}
  {window.addEventListener("load", () => {
    navigator.serviceWorker.register("/service-worker.js");
  })}
</script>
```

[![img](https://www.google.com/s2/favicons?domain=https://www.gstatic.com/devrel-devsite/prod/v2e3f09d6e6536badfdb5bf4153d08404c10f0bdcdc9056b4896a90327dc2c4ff/developers/images/favicon.png)Service Workers: an Introduction | Web Fundamentals](https://developers.google.com/web/fundamentals/primers/service-workers/)

# 9. Performance Budget

## Performance budget y auditorias automatizadas

Linux, pbcopy no existe, pero pueden usar xclip, de la siguiente manera:

1. Lo instalan:

   ```shell
   npx lighthouse ttys003
   ```

```shell
sudo apt-get install xclip -y
```

2. Lo ponen en lugar de pbcopy:

```shell
npx lighthouse http://localhost:5000 --output json | xclip -sel clip
```

`.github/workflows/audit.yml`

```yml
# Flujo automatizada para auditar cada Pull Request con Lighthouse

name: Performance Audit

# Control: Ejecute la acción para cada Pull Request
# Y cada Push a nuestro bello branch de producción
on:
  pull_request:
  push:
    branches:
      - master

jobs:
  lighthouse:
    runs-on: ubuntu-latest
    steps:
      - uses: actions/checkout@v2

      - name: Use Node.js 12.x
        uses: actions/setup-node@v1
        with:
          node-version: 12.x

      - name: Install and build
        run: |
          npm install
          npm run build
      - name: Audit with lighthouse CI
        uses: treosh/lighthouse-ci-action@v2
        with:
          # Configuración para auditar sobre un sito estático
          # Lee más en:
          # treosh/lg-ci-action options https://github.com/marketplace/actions/lighthouse-ci-action
          runs: 5
          uploadArtifacts: true
          configPath: "./.github/workflows/setup/lighthouse-audit.json"
```

**`.github/workflows/setup/lighthouse-audit.json`**

```json
{
  "ci": {
    "collect": {
      "staticDistDir": "."
    },
    "assert": {
      "assertions": {
        "categories:performance": ["error", { "minScore": 0.8 }],
        "resource-summary:font:count": ["error", { "maxNumericValue": 1 }],
        "resource-summary:script:size": ["error", { "maxNumericValue": 150000 }]
      }
    }
  }
}
```

`.github/dependabot.yml`

```yml
# dependabot.yml file with updates

version: 2
updates:
  # Enable version updates for npm
  - package-ecosystem: "npm"
    # Look for `package.json` and `lock` files in the `root` directory
    directory: "/"
    schedule:
      interval: "monthly"
    open-pull-requests-limit: 10
    allow:
      - dependency-type: "direct"
```

[![img](https://www.google.com/s2/favicons?domain=https://web.dev/performance-budgets-101//images/favicon.ico)Performance budgets 101](https://web.dev/performance-budgets-101/)

[![img](https://www.google.com/s2/favicons?domain=https://www.performancebudget.io/favicon.ico)Performance Budget Calculator](https://www.performancebudget.io/)

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)Lighthouse CI Action · Actions · GitHub Marketplace · GitHub](https://github.com/marketplace/actions/lighthouse-ci-action)

## Automatizando una auditoría con Lighthouse CI

#### 👾 Performance budget y auditorias automatizadas

Performance badged ⇒ Presupuesto del performance del sitio, con auditorias constantes, además que el sitio deba cumplir estos presupuestos

- Debemos asegurar las optimizaciones a lo largo del tiempo
- No vale la pena, se vean opacadas por nuevos cambios

> No se controla lo que no se continúa midiendo

- El Performance budget trata tres puntos
  1. Elige tus métricas
  2. Establece un límite
  3. Automatiza la auditoría
- Automatizar:
  - Lighthouse
  - Webpack
  - Scripts
- Podemos mandar todo este trabajo al CI

> Podemos automatizar el proceso de medir nuestro sitio mediante lighthouse CLI el cual puede funcionar en GitHub Actions. Para eso debemos tener claro cuáles son nuestras métricas y establecer los límites de las mismas

## 🚇 Medidas reales y monitoreo constante

**RUM** viene de Real User Metric

**medidas de laboratorio** Son aquellas las cuales tenemos pleno control de lo que está sucediendo y el proyecto este o en local o en el servidor propio

- Nuestras medidas actuales son llamadas **medidas de laboratorio**
- Esto no representa la realidad para nuestros usuarios
- Por ello debemos tomar medidas reales de nuestro sitio

<h3>Métricas reales</h3>

1. Medidas en el sitio de producción
2. Monitoreo constante e histórico
3. Cumplir las métricas para los usuarios del percentil 75

> Deberíamos medir nuestro sitio en producción debido a que las métricas vistas son de laboratorio

[![img](https://www.google.com/s2/favicons?domain=https://newrelic.com//etc/designs/newrelic/products-icon/apple-touch-icon.png)New Relic | Deliver more perfect software](https://newrelic.com/)

[![img](https://www.google.com/s2/favicons?domain=https://webpagetest.org//images/favicon.ico)WebPageTest - Website Performance and Optimization Test](https://webpagetest.org/)

[![img](https://www.google.com/s2/favicons?domain=https://www.pingdom.com/wp-content/themes/sw-global/favicon.ico)Website Performance and Availability Monitoring | Pingdom](https://www.pingdom.com/)

# 10. Conclusiones

## Conclusiones

Nunca pares de Ap
