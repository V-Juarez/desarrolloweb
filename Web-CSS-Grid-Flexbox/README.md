<h1>Diseño Web con CSS Grid y Flexbox</h1>

<h3>Estefany Aguilar</h3>

<h1>Tabla de Contenido</h1>

- [1. Descubre qué ha cambiado en nuestro medio](#1-Descubre-qué-ha-cambiado-en-nuestro-medio)
  - [Todo sobre el diseño de páginas web acaba de cambiar](#Todo-sobre-el-diseño-de-páginas-web-acaba-de-cambiar)
- [2. Conceptos que forman parte del diseño en CSS](#2-Conceptos-que-forman-parte-del-diseño-en-CSS)
  - [La importancia de recordar las herramientas existentes](#La-importancia-de-recordar-las-herramientas-existentes)
  - [Flujo normal del documento: Display block, inline e inline-block](#Flujo-normal-del-documento-Display-block-inline-e-inline-block)
  - [Contextos de formato: Formato de Contexto de Bloque (BFC)](#Contextos-de-formato-Formato-de-Contexto-de-Bloque-BFC)
  - [Posicionamiento + Dinámica: ¿Cómo se vería?](#Posicionamiento-Dinámica-¿Cómo-se-vería?)
- [3. ¿Flexbox o CSS Grid?](#3-¿Flexbox-o-CSS-Grid?)
  - [Diferencias entre Flexbox y CSS Grid](#Diferencias-entre-Flexbox-y-CSS-Grid)
  - [Similitudes entre Flexbox y CSS Grid](#Similitudes-entre-Flexbox-y-CSS-Grid)
  - [¿Puedo trabajar con Flexbox y CSS Grid al tiempo?](#¿Puedo-trabajar-con-Flexbox-y-CSS-Grid-al-tiempo?)
  - [Dinámica: ¿Qué usarías? (Parte 1)](#Dinámica-¿Qué-usarías?-Parte-1)
  - [Dinámica: ¿Qué usarías? (Parte 2) + Reto](#Dinámica-¿Qué-usarías?-Parte-2-Reto)
  - [¿Cuándo usar Flexbox y cuándo usar CSS Grid?](#¿Cuándo-usar-Flexbox-y-cuándo-usar-CSS-Grid?)
- [4. Modern Layouts con CSS Grid](#4-Modern-Layouts-con-CSS-Grid)
  - [¿Qué son los Modern CSS Layouts?](#¿Qué-son-los-Modern-CSS-Layouts?)
  - [Patrones para usar como punto de partida](#Patrones-para-usar-como-punto-de-partida)
  - [Layouts: Super Centered, The Deconstructed Pancake, Sidebar Says, Pancake Stack, Classic Holy Grail Layout](#Layouts-Super-Centered-The-Deconstructed-Pancake-Sidebar-Says-Pancake-Stack-Classic-Holy-Grail-Layout)
  - [Layouts: 12-Span Grid, RAM (Repeat, Auto, MinMax), Line Up, Clamping My Style, Respect for Aspect](#Layouts-12-Span-Grid-RAM-Repeat-Auto-MinMax-Line-Up-Clamping-My-Style-Respect-for-Aspect)
- [5. Diseño web para desarrolladores](#5-Diseño-web-para-desarrolladores)
  - [Dinámica: No puedo dejar de ver](#Dinámica-No-puedo-dejar-de-ver)
  - [Design System y detalles visuales a tener en cuenta](#Design-System-y-detalles-visuales-a-tener-en-cuenta)
  - [Tendencias de diseño UI/UX: Fase de inspiración y creatividad](#Tendencias-de-diseño-UI-UX-Fase-de-inspiración-y-creatividad)
  - [Wireframes y comunicación visual simple, intuitiva y atractiva](#Wireframes-y-comunicación-visual-simple-intuitiva-y-atractiva)
  - [Figma para devs: Auto Layout y Neumorphism (Parte 1)](#Figma-para-devs-Auto-Layout-y-Neumorphism-Parte-1)
  - [Figma para devs: Auto Layout y Neumorphism (Parte 2)](#Figma-para-devs-Auto-Layout-y-Neumorphism-Parte-2)
- [6. Del diseño al código](#6-Del-diseño-al-código)
  - [Primeros pasos y estructura inicial](#Primeros-pasos-y-estructura-inicial)
  - [Ubicación y creación de elementos](#Ubicación-y-creación-de-elementos)
- [7. El futuro de CSS Grid](#7-El-futuro-de-CSS-Grid)
  - [Entendiendo las versiones de CSS: ¿existirá CSS4?](#Entendiendo-las-versiones-de-CSS-¿existirá-CSS4?)
  - [CSS Subgrid](#CSS-Subgrid)
  - [Native CSS Massonry Layout](#Native-CSS-Massonry-Layout)
  - [CSS feature queries: @supports](#CSS-feature-queries-supports)
  - [Nosotros y el futuro de la web: tips para seguir aprendiendo y mantenerse al día](#Nosotros-y-el-futuro-de-la-web-tips-para-seguir-aprendiendo-y-mantenerse-al-día)

---

# 1. Descubre qué ha cambiado en nuestro medio

## Todo sobre el diseño de páginas web acaba de cambiar

- El diseño gráfico en la web ha evolucionado significativamente dúrate los últimos 25 años
  - Inicialmente se tenía una paleta de 216 colores
  - Las tipografías eran limitadas
- Con la llegada y avance de CSS ha mejorado la web
  - CSS Grid ⇒ nueva tecnología poderosa para crear layouts

**Consejos de @teffcode**

1. Probar y jugar las nuevas características de CSS
2. No construir los mismos diseños antiguos con las mismas tecnologías antiguas
3. Descubre todo lo que ha cambiado
4. No asumas que ya sabemos en qué se convertirá la web. O que ya lo hemos dominado todo.

- No existe una forma “correcta” de hacer los diseños

![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)[GitHub - platzi/CSS2020: Repo de los nuevos cursos de CSS](https://github.com/platzi/CSS2020)

![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)[GitHub - platzi/CSS2020: Todo sobre el diseño de páginas web acaba de cambiar](https://github.com/platzi/CSS2020#1-todo-sobre-el-dise%C3%B1o-de-p%C3%A1ginas-web-acaba-de-cambiar)

![img](https://www.google.com/s2/favicons?domain=https://static.canva.com/static/images/favicon.ico)[Slides - Todo sobre el diseño de páginas web acaba de cambiar](https://www.canva.com/design/DAEQFpuHLco/nb-w5JtFmlNT8jtGGPsVmw/view?utm_content=DAEQFpuHLco&amp;utm_campaign=designshare&amp;utm_medium=link&amp;utm_source=sharebutton)

# 2. Conceptos que forman parte del diseño en CSS

## La importancia de recordar las herramientas existentes

¡Hola! Antes de comenzar con este curso de Diseño Web con CSS Grid y Flexbox, quiero recomendarte el [Curso de CSS Grid Layout](https://platzi.com/clases/css-grid-layout/), ya que conceptos que vimos en ese curso van a ser fundamentales para este.

En el Curso de CSS Grid Layout vimos algunas herramientas que nos han facilitado el camino como desarrolladores a lo largo de todos estos años ([si no has hecho el curso, aquí te dejo los apuntes de esa clase en particular](https://github.com/platzi/CSS2020#4-herramientas-que-nos-han-facilitado-el-camino)) y hablamos de que independientemente de cómo creemos nuestros sitios web a futuro, es importante que veamos el potencial que tiene CSS en este momento, como también que aumentemos nuestra comprensión del por qué suceden ciertas cosas.

Dando una pequeña continuación a esas herramientas que nos han facilitado el camino y especialmente a los “trucos” que tanto diseñadores como desarrolladores web han creado de forma ingeniosa a lo largo del tiempo, quiero que revisemos en este módulo algunos conceptos de CSS que son fundamentales para comprender a profundidad todo lo relacionado al diseño con CSS.

El objetivo al revisar los conceptos de display block e inline, contextos de formato, y posicionamiento en este módulo; es que revisemos todo el panorama, que tengamos una visión amplia del diseño con CSS, que ampliemos o reforcemos nuestros conocimientos base, y también, que veamos el impacto que estos conceptos tienen en la forma en la que diseñamos hoy en día.

## Flujo normal del documento: Display block, inline e inline-block

**Display** ⇒ Verbo: desplegar, colocar a la vista, exhibir, lucir, mostrar, presentar

**flujo normal del documento** ⇒ como se comportan los elementos HTML por defecto

- Display ⇒ Definir el tipo de visualización de un elemento
- Cuando usamos `display: flex;` o `display: grid;` Nos centramos más en los valores y no en la propiedad por si sola

> ### Block and Inline elements

- Block ⇒ Se extienden ocupando todo el espacio disponible para ellos
- Inline
  - Son como palabras en una oración, una después de otra
  - Se separan por medio de un espacio en blanco entre ellos

> El comportamiento de los elementos en bloque y en línea es fundamental para CSS y el hecho de que un documento HTML marcado correctamente será legible por defecto. Este diseño se conoce como “Diseño de bloque y en línea” o “Flujo normal” porque esta es la forma en que los elementos se presentan si no les hacemos nada más.

#### Inner y Outer

- Block e Inline definen:

  - Valor externo de visualización
  - Valor interno de visualización

- Lo que significa es que cuando usamos `display: grid;` estamos diciendo `display: block-grid;`

- Un elemento que tenga los atributos de

   

  **bloque**

   

  puede establecer:

  - Ancho
  - Alto
  - Padding
  - Margin
  - Estirarse en todo el espacio

- Pero los hijos de un contenedor `display: grid;` se les ha dado un valor interno de grid

- Esta forma de pensar es util porque cuando usamos

   

  ```css
  display: inline-flex;
  ```

  - Pensaremos que el primer valor es el comportamiento externo y el segundo valor es el interno del contenedor

- Siempre volveremos al

   

  flujo normal del documento

  - Si trabajamos de esta forma obtendremos:
    - Flujo más agradable y legible
    - CSS mucho más fácil
    - Menos probable de tener problemas de accesibilidad (ya que está trabajando con el orden de los documentos que es exactamente lo que está haciendo un lector de pantalla o una persona que revisa el documento)

**RESUMEN:** El flujo normal del documento sera la forma en que se mostraran los elementos si no modificamos nada, se basan en block e inline ambas propiedades se usan a lo largo del desarrollo como cuando usamos `display: grid;` o `display: inline-grid;` estarás marcando de alguna forma si el comportamiento externo es de bloque o en línea además que en la segunda propiedad indicas que comportamiento interno tendrá.



```css
display: inline-flex;
```

En este caso, **`flex` es el comportamiento interno del contenedor, mientras que `inline` es el comportamiento externo del contenedor ante todo el HTML**. Lo mismo pasa con `inline-grid` y también con grid y flex (que en realidad son block-grid y block-flex).

**Primer valor**: Comportamiento externo de un contenedor.
**Segundo valor**: Comportamiento interno de un contendor.

El `inline-block` es muy útil para poner un logo enfrente de un texto, ya que con este atributo (inline-block), tenemos un elemento que permanece en la linea pero que tiene ancho, largo, padding y margin. Toda una delicia.

## Contextos de formato: Formato de Contexto de Bloque (BFC)

**Block formatting context (BFC)**

Es el layout interno de un elemento, que se comporta de manera independiente a como se comporta el resto de la página

Si bien maneja la estructura interna de un elemento, utilizando position se puede sacar al elemento del flujo normal del documento, haciendo que este se reordene de una forma distinta

**¿Que pasa con flexbox y grid?**

Ambos formatos nacieron con la intensión de facilitar el diseño de la página. Mientras flexbox se basa en un formato donde se le da flexibilidad a los elementos y al contenedor, grid adquiere un formato de cuadricula** realmente facil de ordenar**

**¿E Inline-block?**

Inline-block es bastante facil de entender. Consta en una fusión de ambas partes, donde al igual que inline-flex e inline-block, externamente el elemento se situa de forma inline, pero por dentro puede adoptar propiedades block como width, left, etc…

**Sobre flow-root**
Personalmente no entendí cual es la funcionalidad de flow-root. Tuve que buscar en documentación y aun así no me quedó del todo clado su uso, pero entre sus caracteristicas resetea el flujo del documento, permitiendo posicionar correctamente los elementos float (que, al ser float, se descolocan del flujo, y al resetearlo lo vuelve a ubicar) esto es genial para casos donde tenes que usar elementos flotantes y no queres que se te desordene toda la caja

Aquí dejo más [documentacion al respecto](https://www.programandoamedianoche.com/2018/05/conoce-flow-root-la-solucion-al-desborde-de-un-elemento-con-float/)

[https://www.campusmvp.es/recursos/post/display-flow](https://www.campusmvp.es/recursos/post/display-flow-root-para-limpiar-floats-css.aspx)

> Ahora todo tiene sentido, siempre que aprendía sobre CSS decían: “Coloca el float: left; pero también ponle un overflow para que se arregle”, pero nadie decía por qué se arreglaba, ahora comprendo mejor el concepto.
> .
> Básicamente es como crear pequeños layouts dentro del mismo elemento por lo que entiendo, y en el caso del inline-block, se crean ambos layouts, lo que nos permite trabajar con las propiedades de ambas 🤔

El BFC no tiene porque suceder dentro de otro elemento, ya que podemos tener un elemento `display: fixed`o un float que no tengan padre.

El BFC es parte del renderizado y es accesorio al proceso de colocar los elementos en el dom dentro de su posición natural. Por lo tanto hay que pensar el BFC como un instancia que sucede cuando ciertos elementos tienen algunas propiedades y que nosotros podemos modificar para adaptarlas a nuestras necesidades. Por ejemplo:

Si a un elemento hijo le ponemos la propiedad `float: left` y este elemento supera el alto o ancho del padre, este se comportará de manera separada a este y “le pasará por encima”. En cambio si cambiamos la propiedad `overflow` podemos evitar eso. De esa manera estamos modificando el contexto en el que el bloque se renderiza en el navegador y adaptándolo para que encaje con el diseño que queremos.

Solucion cualquiera de las siguientes corrige el error

```css
.outer {
display: inline-block;
}
.float {
float:left;
}
O esta otra
.outer {
display: iflow-root;
}
.float {
float:left;
}
O esta otra
.outer {
display: iflex;
}
.float {
elimino esta float:left;
}
```

## Posicionamiento + Dinámica: ¿Cómo se vería?

**mix-blend-mode**
La propiedad mix-blend-mode define cómo debe combinarse el contenido de un elemento con su fondo. Por ejemplo, el texto de un <h1> podría mezclarse con el fondo detrás de él de formas interesantes.
Examples of uses

![img](https://codemyui.com/wp-content/uploads/2016/09/hero-section-video-with-mix-blend-mode-overlay-text.gif)

***NOTA:\*** el eje “Z” (Profundidad) se comporta como las capas en photoshop o en ilustrator 

![capas.png](https://static.platzi.com/media/user_upload/capas-58776c6d-25e7-4dc9-9970-8f4167467daf.jpg)

La foto es viejita pero ahí se entiende 😄

> Admito… que sí he hecho el típico: `z-index: 900000000000000` jajaja, ahora entiendo, realmente el problema ya no es tanto mirar en CSS, sino mirar en HTML para ver que todo esté correcto. - RetaxMaster

El Z-index o el eje Z imaginario se ve de la siguiente forma, desde el punto de vista del ojo humano hacia una pantalla:

![z-index-illustration.jpg](https://static.platzi.com/media/user_upload/z-index-illustration-fd5435eb-d1e1-45f3-938f-6fd4ecf1b371.jpg)

De esta forma, entre más cerca este el elemento al ojo humano, mayor prioridad tendrá.

La parte que me ayudo mucho a entender `el contexto de apilamiento` es la parte donde se explica la imagen.

![understanding_zindex_04.png](https://static.platzi.com/media/user_upload/understanding_zindex_04-53c16f18-3e1e-4d2c-b273-7b8ad186e30e.jpg)

- DIV #4 es renderizado debajo de DIV #1 porque el z-index (5) de DIV #1 es válido dentro del contexto de apilamiento del elemento raiz, mientras que el z-index (6) de DIV #4 es válido dentro del contexto de apilamiento de DIV #3. Así que DIV #4 está debajo de DIV #1, porque DIV #4 pertenece a DIV #3, que tiene un valor z-index menor.
- Por la misma razón DIV #2 (z-index 2) es renderizado bajo DIV#5 (z-index 1) porqué DIV #5 pertenece a DIV #3, que tiene un valor z-index mayor.
- El valor z-index de DIV #3 es 4, pero este valor es independiente del z-index de DIV #4, DIV #5 and DIV #6, porque pertenece a un contexto de apilamiento diferente.

**mix-blend-mode**

La propiedad mix-blend-mode define cómo debe combinarse el contenido de un elemento con su fondo.

- Todo son cuadros en nuestros sitios web
  - Podemos controlar el tipo de cuadro que se genera utilizando la propiedad display
- Positions ⇒ Nos ayudan a manipular la ubicación de un elemento
  - En la parte superior
  - Uno alado de otro
  - Uno encima de otro ⇒ Estaremos hablando de **contexto de apilamiento**

> *“El contexto de apilamiento es la conceptualización tridimensional de los elementos HTML a lo largo de un eje-Z imaginario relativo al usuario que se asume está de cara al viewport o página web. Los elementos HTML ocupan este espacio por orden de prioridad basado en sus atributos.”*

- - Cuando trabajamos con la web debemos trabajar con 3 ejes
    - Ejes X & Y
    - Eje Z imaginario ⇒ Desde la pantalla hasta la vista del usuario
      - Cajas que se van sobreponiendo
      - Ejemplo ⇒ Un modal se pone encima de todo nuestro sitio web
  - El contexto de apilamiento es como una pila que se va ubicando por orden de prioridad
    - No todos los elementos pueden crearlo
  - Un contexto de apilamiento es formado, en cualquier lugar del documento, por cualquier elemento que:
    - Sea el elemento raíz (HTML)
    - Tenga posición (absoluta o relativa) con un valor `z-index` distinto de `auto`
    - `Position: fixed`
    - Un elemento flex con un valor `z-index` distinto de `auto`, que sera el elemento padre `display: flex/inline-flex`
    - Sean elementos con un valor `opacity` menor de 1
    - Elementos con un valor `transform` distinto de `none`
    - Elementos con un valor `mix-blend-mode` distinto de `normal`
    - Elementos con un valor `filter` distinto de `none`
    - Elementos con un valor `perspective` distinto de `none`
    - Elementos con un valor `isolation` igual a `isolate`
    - Especifican cualquier atributo superior en `will-change` incluso si no especificas valores para estos atributos directamente
    - Elementos con un valor `-webkit-overflow-scrolling` igual a `touch`

![www.canva.com_design_DAEQhmoxgqg_yApl4zSE9ricNZ4u9qNnYA_view_utm_content=DAEQhmoxgqg&utm_campaign=designshare&utm_medium=link&utm_source=sharebutton.png](https://static.platzi.com/media/user_upload/www.canva.com_design_DAEQhmoxgqg_yApl4zSE9ricNZ4u9qNnYA_view_utm_content%3DDAEQhmoxgqg%26utm_campaign%3Ddesignshare%26utm_medium%3Dlink%26utm_source%3Dsharebutton-7dbdfba7-dd46-4c46-9e23-1bbbba52dc20.jpg)

- Ejemplo del contexto de apilamiento

# 3. ¿Flexbox o CSS Grid?

## Diferencias entre Flexbox y CSS Grid

**Flexbox**

Nos ayuda a distribuir los elementos y alinearlos, es **unidireccional**, eso quiere decir que solo va en un a dirección, va vertical o va en horizontal.

Las propiedades que tiene un contenedor padre en flexbox son:

- display
- flex-direction
- flex-wrap
- flex-flow
- justify-content
- align-items
- align-content

Las propiedades de elementos hijos son:

- order
- floex-grow
- flex-shrink
- flex-basis
- flex
- align-self

**Grid**

Nos permite alinear elementos en filas y columnas, es bidemensional, podemos alinear elementos en dos direcciones.

Las propiedades de contenedores padre son:

- display
- grid-template
- gap
- justify-items
- align-items
- justify-content
- align-content

Propiedades de elementos hijo

- grid-column
- grid-row
- grid-area
- justify-self
- align-self

**Flexbox**

- Es un método que puede ayudar a distribuir el espacio entre los ítems de una interfaz y mejorar las capacidades de alineación
- Es unidimensional ⇒ Una sola dirección

**CSS Grid**

- Es un sistema de diseño que permite al autor alinear elementos en columnas y filas
- Es bidimensional

**RESUMEN:** La principal diferencia entre flexbox y Grid es la capacidad de dimensiones que cuenta cada uno flexbox es unidimensional y CSS Grid es bidimensional

**PROPIEDADES**

<img src="https://i.ibb.co/z6MTj77/f1.gif" alt="f1" border="0">

<img src="https://i.ibb.co/7ph3DNK/f2.gif" alt="f2" border="0">

<img src="https://i.ibb.co/QbwmwKG/f3.gif" alt="f3" border="0">

<img src="https://i.ibb.co/c6Jv8DL/f4.gif" alt="f4" border="0">

## Similitudes entre Flexbox y CSS Grid

*Flexbox*
[Juego de flex](https://flexboxfroggy.com/#eshttps://flexboxfroggy.com/#es)
	*Grid*
[Juego de grid](https://cssgridgarden.com/)

Sí que se puede, de hecho, CSS Grid + Flexbox = Combinación ganadora, cuando hacía mis proyectos con CSS Grid siempre los combinaba con Flexbox para poder tener aún más control de los elementos y ufff, era hermoso.

El hecho de trabajar con CSS Grid ya hace que todo sea sencillo, pero si a eso le añadimos Flexbox, prácticamente tienes el control de todo, de hecho, puedes trabajar flexbox dentro de una celda de una grid y es completamente válido!

Flexbox y CSS Grid tienen 2 similitudes:

1. Relaciones Padres e hijos inmediatos
2. Ejes de alineamiento

#### Relaciones Padres e hijos inmediatos

- Tantos los padres tienen hijos, pero los hijos también pueden ser padres
- Si se define correctamente que contenedores serán padre, hijos o ambos entonces sera más sencillo definir la técnica
- Segun a eso tendremos propiedades para
  - Padres
  - Hijos

#### Ejes de alineamiento

- CSS siempre tienen dos axis o ejes

  1. Justify
     - inline axis (row axis)
  2. Align
     - Block axis (column axis)

  - Las propiedades tienen un inicio (start) y un final (end)
    - Por defecto de izquierda a derecha para inline
    - Por defecto de arriba a abajo para block

**RESUMEN:** Flexbox y CSS Grid comparten dos características las relaciones padre e hijo y los ejes de alineamiento que tiene cada una.

1. Relacion entre padres e hijos. ⇒ *padres | hijos*
2. Ejes de alineamiento ⇒
   - *ustify | align*
   - *inline | block*
   - *horizontal | vertical*

## ¿Puedo trabajar con Flexbox y CSS Grid al tiempo?

De primera vista se ve y se intuye que podemos usar CSS Grid, aunque también podríamos usar Flexbox 👀 (esto para elcaso en donde solo nos enfocamos al diseño en dispositivos móviles, sin pensar en responsive).

Con Flexbox podríamos tener un div que sea cada nueva fila que tenga un space between con ambos elementos al 50% del tamaño, al ser Flexbox un elemento bloque… ¡funcionaría!

Pero claramente la mejor opción es CSS Grid jaja

> Para crear una galería de imágenes, un grupo de tarjetas, etc. usaría grid y para crear algo como un navbar, footer usaría flexbox

En el contenedor padre se utiliza grid, algo como:

```css
grid-template-columns: repeat(auto-fill, minmax(160px, 1fr));
/* Para que se alinien como en el ejemplo y así quedaría perfecto para el responsive 😄 */
```

Y en cada cuadrito del grid yo utilizaría display flex, como:

```css
display: flex;
flex-direction: column; // Alineación vertical
justify-content: center; // Centrados horizontalmente
```

Y de esa manera quedaría muy parecido al ejemplo 😃

¿Puedo trabajar con Flexbox y CSS Grid al tiempo?

- Si

- Pero va a depender del diseño que tengamos o necesitemos
- por ejemplo, esta pantalla, si identificamos los elementos.

<img src="https://i.ibb.co/Vp3WbZd/fx3.jpg" alt="fx3" border="0">

- Podremos observar que necesitaremos un contenedor

<img src="https://i.ibb.co/hDk1WTt/fx2.jpg" alt="fx2" border="0">

- Este contenedor tendrá hijos, los cuales podemos definir por dentro con flexbox

<img src="https://i.ibb.co/QHPLhHK/fx1.jpg" alt="fx1" border="0">

## Dinámica: ¿Qué usarías? (Parte 1)

**Primer caso:**

- **CSS-Grid**: Si bien hay un solo eje de trabajo, el sistema de grillas me puede ayudar a acomodar los elementos en celdas con tamaños específicos y ayudarme a manejar mejor y de forma simple el responsive. Si utilizara FlexBox me vería obligado a agregar contenedores hijos en el medio y para alinearlos por dentro tendría que volverlos también contendores FlexBox para conseguir el mismo resultado visual.

------

**Segundo caso:**

- **CSS-Grid**: una grilla de tres por tres, donde las primeras dos filas estarán ocupadas de punta a punta por el header y el texto y la última fila ocuparían las dos últimas columnas cada call to action. Si hubiera que trabajar responsive se haría muy fácil de modificar trabajando con grid-area.

**Code**

```css
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <title>Class9</title>
</head>
<body>
    <section>
        <h1>Dialog Header</h1>
        <p>Dialog body text</p>
        <footer>
            <button>Action1</button>
            <button>Action2</button>
        </footer>
    </section>
</body>
</html>
*{
    box-sizing: border-box;
    margin: 0;
    padding: 0;
}
body {
    font-size: 62.5%;
    font-family: 'Roboto', sans-serif;
}
section{
    display: grid;
    grid-template-areas: "header header header"
                         "text text text"
                         "side footer footer";
    background-color: grey;
    border-radius: 5px;
    padding: 1.5rem;
    width: 300px;

}
h1 {
    font-size: 2rem;
    grid-area: header;
    color: pink;
}
p {
    font-size: 1.5rem;
    grid-area: text;
    color: pink;
}
footer {
    grid-area: footer;
    justify-content: end;
}
button{
    background-color: transparent;
    border: none;
    color: white;
}
*{
    box-sizing: border-box;
    margin: 0;
    padding: 0;
}
body {
    font-size: 62.5%;
    font-family: 'Roboto', sans-serif;
}
section{
    background-color: grey;
    border-radius: 5px;
    padding: 1.5rem;
    width: 300px;
}
h1 {
    font-size: 2rem;
    grid-area: header;
    color: pink;
}
p {
    font-size: 1.5rem;
    grid-area: text;
    color: pink;
}
footer {
    display: flex;
    justify-content: flex-end;
}
button{
    margin: 0 5px;
    background-color: transparent;
    border: none;
    color: white;
}
```

## Dinámica: ¿Qué usarías? (Parte 2) + Reto

**Tercer caso:**

- **Flexbox**: Es una sola línea de íconos y es muy fácil de ordenar y alinear con ***display: flex\***;

------

**Cuarto caso:**

- **CSS-Grid**: Es más fácil de trabajar las filas, y si aparecieran nuevas filas opciones en los checkbox se podrían acomodar solas en la interfaz de forma muy fácil. No hay necesidad de añadir contenedores adicionales.

------

**Quinto caso:**

- **CSS-Grid y Flexbox**: Haría una grilla para la estructura de los elementos más grandes y luego agruparía en contenedores que sean flex para alinear lo más pequeño.

------

[Resolución del reto en CodePen](https://codepen.io/patugarte/pen/LYWzYwm)

![material-cat.png](https://static.platzi.com/media/user_upload/material-cat-7b8aa3fb-a869-42ba-837d-965177091b21.jpg)

1. De nuevo Flexbox, es más fácil ir metiendo todos los íconos y que se vayan acomodando solitos jaja
2. También Flexbox, lo veo más como elementos en bloque que puede tener dos hijos alineados de forma inline 🤔. Aunque también podría usar CSS Grid


El 5 lo hice únicamete con Flexbox y el flujo normal del documento uwu:

![card.png](https://static.platzi.com/media/user_upload/card-dd3d55a3-c292-44e3-8c4d-4f7b02438a8e.jpg)

**Acá el código:**

**HTML**

```html
<div class="card">
    
    <div class="card-header">
        
        <div class="circle-image">
            <img src="https://randomfox.ca/images/85.jpg" alt="Zorrito:3">
        </div>

        <div class="info">
            <h2>Title goes here</h2>
            <p>Secondary text</p>
        </div>

    </div>


    <img src="https://randomfox.ca/images/46.jpg" alt="Otro zorrito:3">

    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus libero unde ipsa facere atque debitis omnis! Ipsum commodi unde sed fugiat, libero necessitatibus nihil. At temporibus perferendis rerum ea quas.</p>

    <div class="links">
        <a href="#">ACTION 1</a>
        <a href="#">ACTION 2</a>
    </div>

</div>
```

**CSS:**

```css
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

img {
    width: 100%;
}

.card {
    width: 500px;
    box-shadow: 0 0 4px 0 rgb(0 0 0 / 8%), 0 2px 4px 0 rgb(0 0 0 / 12%);
    border-radius: 3px;
    background: #fff;
    padding: 20px;
    margin: 20px;
}

.card-header {
    display: flex;
    padding: 10px 0;
}

.card-header .circle-image {
    width: 80px;
    height: 80px;
}

.circle-image img {
    width: 100%;
    height: 100%;
    border-radius: 100%;
    object-fit: cover;
}

.card-header .info {
    display: flex;
    flex-direction: column;
    justify-content: center;
    padding: 0 13px;
}

.card p {
    margin: 20px 0;
}

a {
    text-decoration: none;
    font-weight: bold;
    color: #444fe0;
}
```

## ¿Cuándo usar Flexbox y cuándo usar CSS Grid?

Al momento de implementar, crear tareas para la creación de componentes en este ejemplo serían:

1. Crear la grid principal.
2. Crear la grid de los hijos.
3. Crear 3 tipos de cards
4. Ubicar cards

Flexbox = componentes de escala pequeña.
Grid = componentes de escala más grandes como layout.

Al momento de implementar, crear tareas para la creación de componentes:

1. Crear la grid principal.
2. Crear la grid de los hijos.
3. Crear 3 tipos de cards
4. Ubicar cards

> [Workshop de la plataforma Egghead](https://egghead.io/lessons/flexbox-choose-between-grid-or-flexbox-for-css-layout?pl=watch-later-04d51e0a0fb26ec0), donde se va armando una Landing Page paso a paso, utilizando Grid y Flexbox. La instructora muestra muy buenas prácticas y va explicando cómo y por qué usa cada tipo de display.

# 4. Modern Layouts con CSS Grid

## ¿Qué son los Modern CSS Layouts?

Hoy en día existe una mejor opción a Object Oriented CSS, y se llama Storybook, aquí hay una **[→ guía de como instalarlo y usarlo ←](https://www.youtube.com/playlist?list=PLfWyZ8S-Xzeed53YOiAa1U5WUSA4cRxFQ)** hecha en youtube por un profesor de platzi **[→ Sacha Lifszyc ←](https://platzi.com/profesores/sacha-lifszyc-65/)**

- **Layout** viene del inglés el cual significa diseño
- Colocar tus cajas en el lugar que elijas con respecto a la ventana principal y el resto de cajas

------

- Cuando hablamos de Modern CSS Layouts es un concepto de hace más de 10 años
- Características de la web en 2010:
  - Progresivamente mejorado
  - Adaptable a diversos usuarios
  - Modulares y eficientes
  - Tipográficamente ricos

> #### Progresivamente mejorado

- Base sólida e ir añadiendo estilos complejos para aquellos navegadores que pudieran soportarlo
- En ese momento la compatibilidad entre navegadores era notorio

> #### Adaptable a diversos usuarios

- Al tener una gran cantidad:
  - La amplia gama de navegadores
  - Dispositivos
  - Resoluciones de pantalla
  - Tamaños de fuente
  - Tecnologías de asistencia
- Se pretendía llegar de una manera óptima a todos

> #### Modulares y eficiente

- El CSS se pueda dividir en fragmentos que funcionan de forma independiente para crear componentes de diseño que se pueden reutilizar de forma independiente.
- Se hablaba de un framework que permitiera a los desarrolladores escribir código frontend modular rápido, fácil de mantener y basado en estándares.

------

- Muchas de esas cosas son las que deseamos lograr en la actualidad
- CSS nació en 1996 (lanzamiento inicial)
  - Basado en un diseño de revista

> #### Así fue como:

1. Se idearon un modelo de diseño “basado en marcos” (“frame-based” layout model) en 1996
2. Luego, lanzaron el “Advanced Layout Module” en 2005
3. Que luego, pasó a ser “Template Layout Module” en 2016

- Es decir que siempre se ha deseado lo mismo como tener control en:
  - Columnas
  - Filas
  - Tipografías
  - Contenido organizado (Header, Footer, …)

Básicamente el layout CSS ha cambiado drásticamente, así como la forma en que desarrollamos la interfaz de nuestros sitios

- Ahora tenemos una opción real en términos de los métodos de diseño que usamos en CSS para desarrollar nuestros, sitios, lo que significa que a menudo tenemos que elegir qué enfoque tomar.
- Como desarrolladores debemos asegurarnos la comprensión actual del layout esté actualizada
- Veremos diferentes layouts y formas de construirlos.

El layout es el diseño que tenemos, de como colocamos nuestros elementos su organización. Modern CSS layouts es algo que tiene 10 años, igual usaban HTML5 y CSS3, y en 2010 había ciertas características que se requerían como:

- Progresivamente mejorando: Una base solida de CSS y con base se pueda construir lo demás.
- Adaptable a diversos usuarios: Que se pueda ver en diferentes tipos de navegadores y que sea responsive.
- Modulares y eficientes: Tener pequeños componentes y de ahí fueran creciendo los demás y se puedan reutilizar. Antes usaban frameworks de Object Oriented para CSS para tener un código rápido, fácil de mantener y basado en estándares.
- Tipográficamente ricos: Había muchas tipos de tipografías.

Las cosas que querían lograr antes es igual a lo que queremos ahora, la idea de CSS grid lleva años desarrollándose la idea de tener un layout basado en filas y columnas y en 2017 esa idea se concreto. Queríamos tener la misma estructura para layouts pero las cosas van evolucionado y mejorando y ahora podemos tener mejores y más sencillos layouts.

## Patrones para usar como punto de partida

¡Hola! Para comenzar con este módulo de Modern Layouts con CSS Grid, quiero que revisemos algunos patrones para usar como punto de partida.

El link de la página que te compartiré, contiene una diversa colección de patrones que pueden ser usados tanto para el diseño de una página completa como para el diseño de un componente pequeño (así como lo hablamos en nuestra clase de: ¿Cuándo usar Flexbox y cuándo usar CSS Grid?).

Adicionalmente, en esta página podrás encontrar la explicación, tips, el chequeo de compatibilidad usando Feature Queries (que lo veremos más adelante en el curso), y las técnicas usadas para crear cada patrón.

Link: https://gridbyexample.com/patterns/

Te veo en la siguiente clase, no sin antes recordarte dos cosas:

- Me alegra mucho que personas como tú estén leyendo esto. ¡Eso quiere decir que eres una persona que Nunca Para de Aprender y te felicito enormemente por eso!
- Cualquier duda, por mínima que sea, ¡escríbela en los comentarios!

❤️

## Layouts: Super Centered, The Deconstructed Pancake, Sidebar Says, Pancake Stack, Classic Holy Grail Layout

[web diseños](https://web.dev/one-line-layouts/)

Deconstructed pancake usa la propiedad flex y sus valores son grow, shrink y basis.

**Flex-grow** ocupa equitativamente en cada elemento el espacio sobrante.

**Flex-basis** establece un ancho alto de cada item, va a ser alto o ancho dependiendo la dirección en que este.

**Flex-shrink** especifica el valor de contracción de un elemento, cuanto va a reducir.

Super Centered

HTML

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Super centered</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="parent">
        <div class="child">:)</div>
    </div>
</body>
</html>
```

CSS

```css
* {
    padding: 0;
    margin: 0;
    box-sizing: border-box;
}

html {
    font-size: 62.5%;
}

.parent {
    height: 100vh;
    width: 100vw;
    display: grid;
    place-items: center;
    background-color: aqua;
}

.child {
    height: 50px;
    width: 20px;
    display: grid;
    place-items: center;
    background-color: blueviolet;
    font-size: 1.2rem;
    font-weight: bold;
}
```

Sidebar Says
HTML

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sidebar Says</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="parent">
        <div class="sidebar">min: 150 / max: 25%</div>
        <div class="element">Takes the second grid position</div>
    </div>
</body>
</html>
```

CSS

```css
* {
    padding: 0;
    margin: 0;
    box-sizing: border-box;
}

html {
    font-size: 62.5%;
}

.parent {
    height: 100vh;
    width: 100vw;
    display: grid;
    grid-template-columns: minmax(150px, 25%) 1fr;
}

.sidebar, .element {
    display: grid;
    place-items: center;
    font-size: 1.2rem;
    font-weight: bold;
}

.sidebar {
    background-color: yellow;
}

.element {
    background-color: palevioletred;
}
```

The Deconstructed Pancake

HTML

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Deconstructed pancake</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="parent">
        <div class="child">1</div>
        <div class="child">2</div>
        <div class="child">3</div>
    </div>
</body>
</html>
```

CSS

```css
* {
    padding: 0;
    margin: 0;
    box-sizing: border-box;
}

html {
    font-size: 62.5%;
}

.parent {
    height: 100vh;
    width: 100vw;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
}

.child {
    flex: 0 1 300px;
    display: grid;
    place-items: center;
    margin: 15px;
    background-color: aqua;
    font-size: 1.2rem;
    font-weight: bold;
}
```

Holy Grail
HTML

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Holy Grail</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="parent">
        <header>Header</header>
        <section class="left-sidebar">Left Sidebar</section>
        <main>Main</main>
        <section class="right-sidebar">Right Sidebar</section>
        <footer>Footer Content</footer>
    </div>
</body>
</html>
```

CSS

```css
* {
    padding: 0;
    margin: 0;
    box-sizing: border-box;
}

html {
    font-size: 62.5%;
}

.parent {
    height: 100vh;
    width: 100vw;
    display: grid;
    grid-template: auto 1fr auto / auto 1fr auto;
}

header, main, .left-sidebar, .right-sidebar, footer {
    font-size: 1.8rem;
    font-weight: bold;
}

header {
    grid-column: 1 / 4;
    background-color: yellow;
}

.left-sidebar {
    grid-column: 1 / 2;
    background-color: coral;
}

main {
    grid-column: 2 / 3;
    background-color: palevioletred;
}

.right-sidebar {
    grid-column: 3 / 4;
    background-color: greenyellow;
}

footer {
    grid-column: 1 / 4;
    background-color: cadetblue;
}
```

## Layouts: 12-Span Grid, RAM (Repeat, Auto, MinMax), Line Up, Clamping My Style, Respect for Aspect

[10 single-line CSS layouts](https://www.youtube.com/watch?v=qm0IfG1GyZU)

[ diseños web ](https://web.dev/one-line-layouts/)


<img src="https://i.ibb.co/Vwr5Wnv/flex.jpg" alt="flex" border="0">

Estas CARDS las hice teniendo en cuenta estas dos opciones

<img src="https://i.ibb.co/6XtkXKg/flex1.jpg" alt="flex1" border="0">

7. RAM (Repeat, Auto, Minmax)
8. Clamping My Style

HTML

```html
<div class="container">
  <div class="card">
  <h1>MAMERTOLANDIA</h1>
  <div class="visual"></div>
  <p>El despertar de Colombia</p>
  <br/>
  <p>Somos la resistencia inteligente.</p>
</div>
<div class="card">
  <h1>MAMERTOLANDIA</h1>
  <div class="visual"></div>
  <p>El despertar de Colombia</p>
  <br/>
  <p>¿Renuncia Duque o golpe de estado?</p>
</div>  
</div>
```

CSS

```css
body {
  display: grid;
  place-items: center;
  height: 100vh;
}
.container {
  display: grid;
  place-items: center;
  grid-gap: 1rem;
  grid-template-columns: repeat(auto-fit, minmax(275px, 1fr));
  width: 100vw;
  
}

.visual {
  background-image: url(https://i.ytimg.com/vi/Bx6BTVQu9xA/maxresdefault.jpg);
  background-position: bottom;
  background-repeat: no-repeat;
  border-radius: 20px;
  height: 90px;
  width: 100%;
  background-size: cover;
  margin: 0.5rem 0;
}

.card {
  width: clamp(30ch, 43%, 55ch);
  display: flex;
  flex-direction: column;
  background: #dda15e;
  padding: 1rem;
  height:90%;
  border-radius: 10px;
  -webkit-box-shadow: 5px 4px 15px 1px rgba(0,0,0,0.58); 
box-shadow: 5px 4px 15px 1px rgba(0,0,0,0.58);
}

body {
  font-family: system-ui, sans-serif;
}

h1 {
  font-size: 1.5rem;
  font-weight: bold;
  color: white;
  text-align: center;
  letter-spacing: 3px;
}
p {
  color: white;
  text-align: center;
}
```

# 5. Diseño web para desarrolladores

## Dinámica: No puedo dejar de ver

![img](https://www.google.com/s2/favicons?domain=https://cantunsee.space//favicon.ico)[Can't Unsee](https://cantunsee.space/)

![img](https://www.google.com/s2/favicons?domain=https://static.canva.com/static/images/favicon.ico)[Slides - Dinámica: No puedo dejar de ver](https://www.canva.com/design/DAEP495rHJU/htRUusEMyRvBTexxztk6Ow/view?utm_content=DAEP495rHJU&utm_campaign=designshare&utm_medium=link&utm_source=sharebutton)

Objetivo: Entrar nuestro ojo 👁💪🏻 como desarrolladores 👨🏻‍💻👩🏻‍💻 para tener un muy buen nivel de detalle 🤔🧐 .

- **1 = B**
  El botón tiene una mejor distribución del espacio entre la cámara y el texto gracias a las dimensiones de la misma cámara.
- **2 = A**
  Por temas de accesibilidad en el uso de contrastes el primer texto es más lejible.
- **3 = A**
  De la misma manera el contraste juega un papel muy importante, no puedes poner texto oscuro sobre un backgroun tambien oscuro.
- **4 = A** X
  Pero la respuesta correcta es la **B** ya que el fondo de el searcher es mucho más legible con respecto a los contrastes entre el fondo y el elemento.
- **5 = A**
  Tiene que ver con el alineamiento de los elementos dentro del botón en el eje Y ya que establece equilibrio visual.
- **6 = B**
  Con respecto a las imágenes debemos evitar que se modifiquen de su tamaño original como en la foto ya que esta en la opción **A** ha sido estirada de izquiera a derecha gracias al tamaño de la tarjeta.
  Porpiedad recomendada:

```css
img 
{
	object-fit: cover;
}
```

- **7 = A**
  Sigue siendo los contrastes entre los iconos y su background, con colores que tengan relación 1:2 en contraste.
  “Si me equivoco por favor corrijanme”
- **8 = B**
  Por la diferenciación de elementos.
- **9 = B**
  La tipografia debe ser homogenea.
  “No encontré nada malo con los íconos, tal vez uno es más oscuro que otro”
- **10 = A**
  El tamaño de los íconos siendo homogeneos con el tamaño de la tipografía.
- **11 = A**
  Le atiné, pero la verdad no estoy seguro. Pero siento que el fonto hace aún más contraste con sus elementos que en la opción **B** .
- **12 = A**
  La alineación de ciertos íconos dentro de sus contenedores. Los de la opción **B** No están completamente centrados.

## Design System y detalles visuales a tener en cuenta

![img](https://www.google.com/s2/favicons?domain=https://bit.dev//favicon.ico)[The shared component cloud · Bit](https://bit.dev/)

- **Design Sistem** ⇒ Es una colección de componentes reutilizables guiados por estándares claros.
- Se puede definir como un plan maestro, el cual será la fuente de la verdad
  - Una referencia para asegurarse de que todos los que trabajan en el producto estén siempre en la misma página.
- Empresas como Airbnb, Uber e IBM han cambiado la forma en que diseñan productos digitales al incorporar sus propios sistemas de diseño únicos.
- Nos permite tener **consistencia**
- Cada una de estas empresas ha podido cambiar el ritmo de creación e innovación dentro de sus equipos.

#### ¿Pero cómo?

- Utilizando una colección de componentes repetibles.
- Utilizando un conjunto de estándares que guían el uso de esos componentes.

#### ¿Quiénes crean un Design System?

- Todo un equipo de producto (programadores, ingenieros, diseñadores, gerentes de producto, equipo C-suite, etc.)

##### Proceso

- Se hace un inventario:
  - Colores, logotipos, encabezados, pies de página, formularios, código, etc.
  - Y se llega a un consenso sobre cómo se deben diseñar, codificar, presentar y hablar de las cosas.
- Podemos tener un boceto donde encontraremos: Ideas, Colores, Etc.
- Los integrantes de un equipo deben tomar decisiones para el producto el cual llegara a afectar al código
- Basado en ese tipo de decisiones las cuales se guían de las necesidades, se puede encontrar herramientas

#### Por dónde comenzar a revisar estos temas?

[Design - Shopify Polaris](https://polaris.shopify.com/design/design)

- Una, guía de diseño que nos puede ayudar es la de Shopify
- También podemos usar Frameworks CSS
- Tailwind CSS ⇒ nos ayuda a guiarnos en ciertos aspectos como espaciados, breakpoints, etc.

Un **Design System** por lo general es construido por diferentes miembros de un equipo para tener varias propuestas y por ende un sistema más sólido, claro y objetivo para cada sección de la plataforma. El objetivo de un Sistema de Diseño es crear tecnología de manera rápida y eficaz.


Tambien vale la pena leer un poco de lo que son los **Design tokens**. Basicamente, los design tokens son una forma agnostica de crear variables para la tipografia, colores, breakpoints, etc, pero no en cada lenguaje de programacion o para cada aplicacion, sino mas bien se crea un JSON y se consume en cada app, asi logramos tenes consistencia y estabilidad.

> Esto sirve mucho para empresas que tiene una pagina web y aplicaciones en varias plataformas.

[Guías de diseño de Shopify](https://polaris.shopify.com/design/design)

[Design Tokens](https://www.uifrommars.com/design-tokens-que-son-ventajas/)

## Tendencias de diseño UI/UX: Fase de inspiración y creatividad

Increíble joya 💎💎💎para hacer contenedores [neomorfismo](https://neumorphism.io/#e0e0e0)

Es una herramienta web con la que basimcamente puedes ajustar sombras, redondeados, intensidad y distancia con unos scrolls y cuando se ajusta uno los demas se autojustan dependiendo del que estás moviendo para que así siempre sea como diseño de neomorfismo

Tips:
• Jerarquía
• Contraste
• Proximidad
• Balance
• Responsive design
• Ilustraciones animadas
• Garantizar performance
• Micro interaciones
• Realidad aumentada y realidad virtual
• Neo morfismo
• Asymmetrical layouts
• Storytelling

El **Neomorfismo** lo que busca más que todo es darle al usuario elementos que se parezcan a objetos cotidianos con el objetivo de mejorar la comprensión e interpretación del diseño. Por ejemplo: simular objetos con relieve.

animaciones 3D, hay una librería bastante ligera de JavaScript que nos permite crear objetos 3D directamente en el navegador usando código y no vídeos para hacerlo más rápido. Se llama [Three.js](https://threejs.org/), les dejo el enlace a su página oficial. 

**Jerarquia**, tener unos elementos mas grandes que otros, basados en la relevancia e importancias que le queremos dar a cada uno de ellos.

**Contraste**, ejemplo el botón nos indica que nos están ayudando para poder presionarlo y ya con eso podemos jugar con el tema de contraste y de colores y ayudar también al usuario a leer sin leer prácticamente.

**Proximidad**, que ver con el tema de consistencia por ejemplo vemos items en nuestras app que tienen mucha semejanza ya sea en colores, iconos y demás, ya sabemos que tiene que ver con una sección o una parte que es del mismo grupo etc.

**Balance**, El tema de contraste y de colores, también puede jugar un papel bastante importante cuando queremos hablar de importancia o relevancia de un contenido en especifico.

*Podemos buscar también inspiración para poder realizar nuestros proyectos, uno de ellos es The state of UX in 2021, 100 lecciones bastante utiles a nivel de todo*

***Ilustraciones animadas***, Es muy importante tener en cuenta el tema de rendimiento y de performance

**Garantizar**, también garantizar a las personas que independientemente que tengan datos o no puedan ver nuestros sitios,

**Micro intereciones**, normalmente no son animaciones, pero es lo que le indica al usuario que si esta haciendo clic o tag en algún lado, esos pequeños detalles en la web nos indican ciertas cosas y con eso le damos un gran peso a nuestra pagina, los pequeños detalles también son importantes

**UI**, Es tema mas que todo visual de colores y demas

**UX**, es la experiencia o emociones que transmite el proyecto (sistema, app, etc)

**Realidad Virtual**, normalmente existen app que pueden decirnos como ver ciertos sitios, que si te pones tus gafas vas a ver de esta forma y de esta otra, ejemplo de que si tienes que hacer un tour virtual de de una casa o algun carro, etc.

**Realidad aumentada**, supongamos que queremos comprar una planta, y esa planta la sumergimos en el lugar donde quisieramos colocarla ya sea dentro de una casa, inmediatamente vamos la poder ver en el sitio que la necesitemos, todo depende el, juega un papel super interesante

***Neo morfismo,*** juega mucho con las sombras y también esta relacionado con otras tendencias como lo es material desing y flat desing que ya todo depende de la dirección de la luz.

***Asymmetrical layouts***, es poder jugar con layouts asimétricos, en este caso tenemos imagenes que pueden ser muy sutil, que se vean tambien muy elegantes pero que sean distribuidas en diferentes partes.

[**The state of UX in 2021**](https://trends.uxdesign.cc/)

## Wireframes y comunicación visual simple, intuitiva y atractiva

**1- Saber que quiero**
*Ejemplo:*

Queremos hacer un reproductor de musica para un dispositivo mobile.

**2- Hacer un boceto (Wireframe)**

Podemos hacerlo con lapiz y papel pero tambien lo podemos hacer con esta herramienta online gratuita https://app.moqups.com/#

**3- Pensar en la guía de estilos**
*Despues de tener una base de que es lo que queremos conseguir.*

- temas de colores
- tipografia
- ilustraciones
- iconos

**Tips:**

1. Pensemos siempre en una comunicación visual simple intuitiva y atractiva.
2. Juega al papel de usuario siempre, usando tus propios bocetos.
3. Se tu propio tester, para tener una mejor calidad en nuestro desarrollo.

## Figma para devs: Auto Layout y Neumorphism (Parte 1)

Algunos comandos básicos para ahorrar tiempo en Figma:

- Para crear un Frame solo oprime la tecla F.
- Para crear un rectángulo solo oprime la tecla R (si deseas un cuadrado perfecto oprime las teclas Shift + Option mientras arrastras el mouse, de esta forma crear un cuadrado en vez de un rectángulo)
- Para crear un circulo perfecto oprime la tecla O
- Para alinear un elemento en el centro del FRAME (de todo el frame, no del contenedor) oprime Option + H
- Para alinear en el centro del Frame de forma vertical oprime Option + V
- Si desea cambiar el nombre de varios elementos al tiempo (Si te das cuenta en el panel izquierdo los elementos son nombrados como Rectangle 1, Rectangle 2, o Ellipse 1, Ellipse 2… Ellipse 9, etc y quizás tu los quieras llamar music button o player buttons) solo los seleccionas (puede ser dentro directamente en el Frame de diseño o seleccionas las capas en el panel izquierdo) y oprimes Command + R, de esta forma puedes renombrar muchos elementos al tiempo.

“Neumorphism”, considero lo siguiente:

- Creo que primero tenemos que investigar a profundidad las necesidades de nuestro usuario, o cliente.

- Considero que, es necesario estudiar a profundidad los aspectos sobre accesibilidad ( ya no podemos dejar aún lado éste tema).

- En lo personal, lo usaría pero, en proyectos personales ( me gustó mucho sus paletas de color).

- Por último, considero mucho apoyarnos de las métricas ( las tienes).
  Yo, llevó apenas dos años estudiando sobre desarrollo web, diseño de experiencais, y diseño ( no tengo muchas experiencia dentro de una empresa).

  

- <img src="https://i.ibb.co/sWXMwph/neo.jpg" alt="neo" border="0">

- En figma existen al lado derecho propiedades donde podemos establecer hacia que lado queremos que se alineen los elementos. Para el caso del quiz podemos usar el boton de alineación inferior, es realmente sencillo.

## Figma para devs: Auto Layout y Neumorphism (Parte 2)

**Neomorfismo**

<img src="https://i.ibb.co/Z6KV8g3/neo1.jpg" alt="neo1" border="0">

<img src="https://i.ibb.co/B4VBWYX/music.webp" alt="music" border="0">

# 6. Del diseño al código

## Primeros pasos y estructura inicial

Me encanta el término de ponerse/quitarse las gafas de desarrollador porque es algo que siempre hago cuando inicio con un diseño, básicamente miro todo con el menor detalle posible para poder empezar a armar el esqueleto de la página.

mmet del html

```html
header.header+section.cards+section.progress__bar+footer.buttons
```

## Ubicación y creación de elementos

Que es el blur? Es la propiedad encargada de difuminar o desenfocar.

- https://blog.codepen.io/documentation/autocomplete/

# 7. El futuro de CSS Grid

## Entendiendo las versiones de CSS: ¿existirá CSS4?

¡Hola, bienvenid@ al último módulo de nuestro curso !

Quiero contarte que este tema de CSS4 es uno de los temas que más opiniones genera y en el que también existe mayor desconocimiento, precisamente porque se debe comprender primero cómo funciona actualmente CSS3, qué ha pasado con CSS1, CSS2 y CSS2.1, y a partir de ahí, revisar si efectivamente lo que necesitamos como comunidad es tener o no una nueva versión de CSS (teniendo en cuenta también todo el trabajo que esto conlleva).

Hoy quiero compartirte una lectura bastante interesante para que podamos entender muchísimo más las implicaciones de tener una nueva versión en CSS.

Lectura: https://www.smashingmagazine.com/2020/03/css4-pros-cons-discussion/

Por favor, déjame saber en los comentarios qué piensas al respecto de esta lectura, si estás de acuerdo o si no, si te gustó, si crees que puedan existir otras alternativas diferentes a crear los diferentes niveles o si por el contrario te parece lo mejor… en fin! Me encantaría saber qué piensa la comunidad de Platzi sobre este tema tan apasionante 😄

**Conclusión: No habrá CSS4 pero si actualizaciones**.

La verdad si es algo confuso debido a que cuando estudias CSS, aprendes que CSS inició con versiones (CSS1 ,CSS2, CSS3) y por tanto parece natural pensar en CSS4. Sin embargo, me parece mejor que ya no existan más versiones de CSS y que se siga con la filosofía de la implementación de nuevos módulos que tengan su propio versionamiento interno. Me parece mas natural debido a la evolución no lineal que sufre y sufrirá nuestro querido lenguaje.

## CSS Subgrid

**Subgrid** en estos momentos esta en fase de experimentación, como podemos ver solo tiene compatibilidad con firefox, lo que no sería ideal que llevemos un tema de subgrid a un tema de producción precisamente por la falta de compatibilidad que tiene ahora, lo ideal es que miremos en la esquina superior derecha el tema de global y ese porcentaje ya nos va indicar si podemos utilizarlo y sino dependiendo de que tan alto esté ese porcentaje.

![img](https://www.google.com/s2/favicons?domain=https://caniuse.com//img/favicon-128.png)[Can I use... Support tables for HTML5, CSS3, etc](https://caniuse.com/)

## Native CSS Massonry Layout

Masonry, en español Mampostería. Se llama mampostería al sistema tradicional de construcción que consiste en erigir muros y paramentos mediante la colocación manual de los elementos o los materiales que los componen (denominados mampuestos), que pueden caracterizarse por estar sin labrar.

[Masonry Layout.](https://css-tricks.com/piecing-together-approaches-for-a-css-masonry-layout/)

## CSS feature queries: @supports

**`@supports`**

```css
@supports (grid-template-columns) and (not (subgrid)) {
	// Creas las columnas del padre dentro del hijo
}

//Si soportas grid-template-columns y no soportas subgrid, interpreta el contenido de las llaves

@supports (subgrid) {
	grid-template-columns: subgrid;
}
//Si soportas subgrid interpreta el contenido de las llaves
```

Esto fue un ejemplo que se me ocurrió, también se podría utilizar después con el **`grid-template-rows:massonry`** o con otras cosas nuevas de css 😃

[Using Feature Queries in CSS](https://hacks.mozilla.org/2016/08/using-feature-queries-in-css/)

## Nosotros y el futuro de la web: tips para seguir aprendiendo y mantenerse al día

[GitHub - platzi/CSS2020: Nosotros y el futuro de la web: Tips para seguir aprendiendo y mantenerse al día](https://github.com/platzi/CSS2020#28-nosotros-y-el-futuro-de-la-web-tips-para-seguir-aprendiendo-y-mantenerse-al-d%C3%ADa)

![img](https://www.google.com/s2/favicons?domain=https://static.canva.com/static/images/favicon.ico)[Slides - Nosotros y el futuro de la web: Tips para seguir aprendiendo y mantenerse al día](https://www.canva.com/design/DAEQqIapf1s/ROxFyghXOVkKAw8UV69JkA/view?utm_content=DAEQqIapf1s&amp;utm_campaign=designshare&amp;utm_medium=link&amp;utm_source=sharebutton)

**Websites**
[Smashing Magazine](https://www.smashingmagazine.com/)
[Medium](https://medium.com/)
[MDN web docs (CSS)](https://developer.mozilla.org/es/docs/Web/CSS)
[A book apart (El libro que menciono teff)](https://abookapart.com/products/the-new-css-layout)

