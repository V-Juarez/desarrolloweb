<h1>Bootstrap 4</h1>

<h3>Sacha Lifszyc</h3>

<h1>Tabla de Contenido</h1>

- [1. Introducción al curso](#1-introducción-al-curso)
  - [Repositorio del curso de bootstrap](#repositorio-del-curso-de-bootstrap)
  - [Bootstrap 4 ¿Qué trae de nuevo esta versión?](#bootstrap-4-qué-trae-de-nuevo-esta-versión)
  - [¿Que es un framework frontend?](#que-es-un-framework-frontend)
  - [Nuestro Proyecto: Hola Mundo de Bootstrap](#nuestro-proyecto-hola-mundo-de-bootstrap)
- [2. Creando el sitio web](#2-creando-el-sitio-web)
  - [La grilla de Bootstrap](#la-grilla-de-bootstrap)
  - [Reto: La grilla de Bootstrap](#reto-la-grilla-de-bootstrap)
  - [El footer](#el-footer)
  - [El header de nuestro sitio](#el-header-de-nuestro-sitio)
  - [Creando un carousel de imágenes](#creando-un-carousel-de-imágenes)
  - [Agregando texto informativo del evento](#agregando-texto-informativo-del-evento)
  - [Agregando botones](#agregando-botones)
  - [Las cards de Bootstrap 4](#las-cards-de-bootstrap-4)
  - [Pastillas de texto](#pastillas-de-texto)
  - [Agregando un contenedor de ancho completo](#agregando-un-contenedor-de-ancho-completo)
  - [Los formularios de Bootstrap 4](#los-formularios-de-bootstrap-4)
  - [Agregando un tooltip](#agregando-un-tooltip)
  - [Scrollspy: Conociendo la ubicación del usuario en el header](#scrollspy-conociendo-la-ubicación-del-usuario-en-el-header)
  - [Agregando un modal para comprar tickets](#agregando-un-modal-para-comprar-tickets)
  - [Un nuevo formulario para completar la compra](#un-nuevo-formulario-para-completar-la-compra)
- [3. Deploy a producción](#3-deploy-a-producción)
  - [Poniendo nuestro sitio en producción](#poniendo-nuestro-sitio-en-producción)
  - [Conclusiones del curso](#conclusiones-del-curso)

# 1. Introducción al curso

## Repositorio del curso de bootstrap

Bienvenidos de nuevo al **Curso de Bootstrap** le comparto el repositorio que vamos a estar utilizando a lo largo del curso.

Recuerden que si aún no tienen una cuenta en GitHub deben crearla, la necesitarán.

[Bootstrap | GitHub](https://github.com/platzi/bootstrap)

**Nota:** Los slides del curso los encontrarán en la siguiente clase en el sistema de archivos.

## Bootstrap 4 ¿Qué trae de nuevo esta versión?

Las novedades de Bootstrap

1. Cambios en el GRID
2. Glyphicons es sustituido por Font Awesome.
3. Adios a Less
4. No más soporte para Internet Explorer 8
5. Flexbox

Es la librería de frameworks frontend más popular. Veremos el potencial de su grilla, crearemos menú de navegación, títulos, botones, tarjetas, badges, etiquetas, formularios, modals y más!

[Bootstrap.pdf](https://drive.google.com/file/d/1plaVHpzvaRWHEmAfzXXtqq-s3mNVIIln/view?usp=sharing)

## ¿Que es un framework frontend?

Como desarrolladores debemos entender que la *reutilización* es una de los características que nos convierte en buenos desarrolladores, “no es necesario reinventar la rueda”.

**Frameworks Frontend:** Conocidos también como Frameworks CSS. Son una base para empezar un proyecto web, permite Flexibilidad en el Diseño. Nos ayuda a tener una Organización y Estructura del HTML, CSS y JS.

***¿Qué contiene un Framework Frontend?***
**Grilla:** Organiza el contenido de un sitio web, adaptando el contenido a cualquier dispositivo.
**Estilos para Fuentes & Componentes Visuales Pre-armados:** Se pueden utilizar para organizar información.

**¿Para qué nos ayuda un Framework Frontend?**

1. Ahorrar Tiempo.
2. Responsive Design, sitio web adaptable a cualquier dispositivo
3. No tenemos que preocuparnos por la compatibilidad con otros browsers

**Ejemplos de Frameworks Frontend**

- Bootstrap
- Foundation
- Bulma
- Tailwind

Ventajas de un framework frontend

- Ahorra tiempo
- Tiene componentes visuales
- Código adaptable a diferentes pantallas
- EL código anda en los diferentes navegadores

## Nuestro Proyecto: Hola Mundo de Bootstrap

Vamos a sitio web oficial de Bootstrap en [getbootstrap.com](http://getbootstrap.com/) . Cualquier duda que tengas sobre los componentes y su uso que no se respondan en este curso, lo puedes encontrar en la sección de *Documentation* del sitio de Bootstrap.

Es importante precisar que existen 2 formas para utiliza bootstrap:

**1. Mediante el CDN:** En este video, Sacha copia la plantilla de inicio que, contiene la referencia a los css y a 3 archivos javascript. Dentro de los cJS están:
**a. **La referencia a JQuery (Sí, utiliza JQuery para las funciones que css, por obvias razones no podría implementar)
**b.** Referencia a Popper.JS: Esta herramienta nos sirve para crear los popUps (esos mensajes flotantes que se muestran al posicionar el cursor en un lugar predeterminado)
**c. **Finalmente llama al JS de bootstrap.

Por otro lado, noten que cada referencia contiene un .min dentro de sus nombres (por ejemplo; bootstrap.min.css, …jquery-3.3.1.slim.min.js, etc) que no es más que la versión minificada(sin espacios) de
cada archivo de código.

**2. Descargando todos los archivos comprimidos:**
Otra de las opciones es descargando todos los archivos que componen Bootstrap, esta opción es interesante ya que te permite trabajar sin conexión a internet, cosa que no puedes hacer con laa opción anterior.

[Visual Studio Code](https://code.visualstudio.com/) es lo que deberías usar antes que cualquier otro editor. Viene con más funcionalidades y complementos, así como también menos errores 💪🏼

> Un CDN (Content Delivery Network) es una red de servidores que tienen almacenados archivos que puedes utilizar en tus proyectos, como por ejemplo el framework Bootstrap. Lo que hace tu computadora es conectarse al servidor más cercano para utilizar ese archivo sin tener que guardarlo en tu propio servidor. Un “.min” es generalmente un archivo de CSS “minificado”, es decir, un código al que se le quitaron los espacios, saltos de línea, comentarios, etc. para que pese menos y sea leído con mayor rapidez por el navegador. 

![img](https://www.google.com/s2/favicons?domain=http://getbootstrap.com//docs/4.1/assets/img/favicons/favicon-32x32.png)[Bootstrap · The most popular HTML, CSS, and JS library in the world.](http://getbootstrap.com/)

# 2. Creando el sitio web

## La grilla de Bootstrap

Los contenedores son el elemento de diseño más básico en Bootstrap y son necesarios al usar nuestro sistema de grillas predeterminado.

`.container`: ancho fijo
`.container-fluid`: ancho fluido

El sistema de grillas de Bootstrap usa una serie de contenedores, filas y columnas para diseñar y alinear el contenido. Está construido con flexbox y responde completamente.

<img src="https://i.ibb.co/wgDXymL/grilla.jpg" alt="grilla" border="0">

Recomiendo a todos la siguiente herramienta: [**Emmet**](https://emmet.io/). La [documentación](https://docs.emmet.io/) es muy clara, al aprender su uso la escritura de HTML será mas agil.

**Bootstrap** maneja un sistema de 12 grillas (o cuadriculas) que pueden gestionarse con las clases “.row” (para filas) o “.col” (para columnas).
En cada fila, puede haber un máximo de 12 columnas (en caso de que hayan mas de 12, la columna subsiguiente se ubicara en la siguiente fila).

**- Container** Nos crea un contenedor con un ancho fijo (ancho máximo) en cada uno de los tamaños de dispositivo.

**- Container-fluid**Nos crea un contenedor (igual que el anterior) pero sin un ancho fijo aparente, por lo que ocuparía todo el ancho de la pantalla.
**Breakpoints:** Son puntos de ruptura en el diseño web responsive en los que la pantalla cambia de layout.

## Reto: La grilla de Bootstrap

Tenemos un reto para tí, queremos que crees tu propia grilla. Te damos todas las instrucciones de cómo hacerlo en esta clase. Encuentra la respuesta a este desafío en el repositorio del curso.

**Reto 1:**
Valores de las clases de todos los elementos de la…
primera fila: col-4
segunda fila: col
tercera fila: col-6
cuarta fila: col
quinta fila: col-12 o col

**Reto 2:**
Valores de las clases de todos los elementos de la…
primera fila: col-6 col-md
segunda fila: col-12 col-md-6 col-lg

**Reto 3:**
Valores de las clases de todos los elementos de la…
primera fila: col-12 col-md-6 offset-md-6
segunda fila: col-8 offset-md-2
tercera fila: col-3 offset-9 offset-md-0

```html
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Retos Grilla Bootstrap</title>
    
    <style>
        
        .col{
            border: 1px solid white;
            height: 70px;
        }  
        
        
        .reto1 .col{
             background-color:darkseagreen;
        }
        
        .reto2 .col{
            background-color:burlywood;
        }
        
        .reto3 .col{
            background-color: firebrick;
            color: white;
        }
    </style>
  </head>
  
  
  <body>
    <br>
    <br>
    <div class="container reto1">
      <h2>Reto 1</h2>
       <div class="row">
            <div class="col"> </div>
            <div class="col"> </div>
            <div class="col"> </div>
       </div>
       
       <div class="row">
            <div class="col"> </div>
            <div class="col"> </div>
            <div class="col"> </div>
            <div class="col"> </div>
            <div class="col"> </div>
            <div class="col"> </div>
            <div class="col"> </div>
            <div class="col"> </div>
            <div class="col"> </div>
            <div class="col"> </div>
       </div>
       
       <div class="row">
            <div class="col"> </div>
            <div class="col"> </div>
       </div>
       
       <div class="row">
            <div class="col"> </div>
            <div class="col"> </div>
            <div class="col"> </div>
            <div class="col"> </div>
            <div class="col"> </div>
       </div>
        
        <div class="row">
            <div class="col"> </div>
       </div>
        
    </div>
    
    <br>
    <br>
    <br>
    
    <div class="container reto2">
      <h2>Reto 2</h2>
       <div class="row">
            <div class="col col-6 col-md ">Fila 1/Col 1</div>
            <div class="col col-6 col-md ">Fila 1/Col 2</div>
            <div class="col col-6 col-md ">Fila 1/Col 3</div>
            <div class="col col-6 col-md ">Fila 1/Col 4</div>
       </div>
       
       <div class="row">
            <div class="col col-12 col-md-6 col-lg">Fila 2/ Col 1</div>
            <div class="col col-12 col-md-6 col-lg">Fila 2/ Col 2</div>
            <div class="col col-12 col-md-6 col-lg">Fila 2/ Col 3</div>
            <div class="col col-12 col-md-6 col-lg">Fila 2/ Col 4</div>
            <div class="col col-12 col-md-6 col-lg">Fila 2/ Col 5</div>
            <div class="col col-12 col-md-6 col-lg">Fila 2/ Col 6</div>
            <div class="col col-12 col-md-6 col-lg">Fila 2/ Col 7</div>
            <div class="col col-12 col-md-6 col-lg">Fila 2/ Col 8</div>
            <div class="col col-12 col-md-6 col-lg">Fila 2/ Col 9</div>
            <div class="col col-12 col-md-6 col-lg">Fila 2/ Col 10</div>
       </div>
                
    </div>
    
    
    <br>
    <br>
    <br>
    
    <div class="container reto3">
      <h2>Reto 3</h2>
       <div class="row">
            <div class="col col-12 col-md-6 offset-md-6">Fila 1/Col 1</div>
       </div>
       
       <div class="row">
            <div class="col col-8 offset-md-2">Fila 2/ Col 1</div>
       </div>
               
        <div class="row">
            <div class="col col-3 offset-9 offset-md-0">Fila 3/ Col 1</div>
       </div>
                
    </div>
    
    <br>
    <br>
    <br>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>
```



## El footer

Vamos a crear el footer de nuestro website. Recuerdas que puedes consultar toda la documentaciónde Bootstrap en [getbootstrap.com](http://getbootstrap.com/) . Encuentra más información aquí: https://getbootstrap.com/docs/4.1/components/card/#header-and-footer

Gradient en el footer de Platzi, aquí se los comparto:

```css
#footer{
    background: linear-gradient(90deg, #1c3643, #273b47 25%, #1e5472);
}
```

## El header de nuestro sitio

Conocemos el componente de Navbar de Bootstrap para crear el encabezado de nuestro website. Conoce más de su manejo en https://getbootstrap.com/docs/4.1/components/navbar/

En bootstrap 5 ya no se usa ml-auto para llevar los elementos a la derecha. Ahora se usa ms-auto. En bootstrap 4 era l y r para left y right, Ahora es:
s = start
e = end

Propiedades de espaciado

- m - para las clases que establecen margin
- p - para las clases que establecen padding

Donde los lados es uno de:

- t- para las clases que establecen margin-top o padding-top
- b- para las clases que establecen margin-bottom o padding-bottom
- l- para las clases que establecen margin-left o padding-left
- r- para las clases que establecen margin-right o padding-right
- x- para las clases que establecen tanto -left y -right
- y- para las clases que establecen tanto -top y -bottom

en blanco - para las clases que establecen un margin o padding en los 4 lados del elemento
Donde el tamaño es uno de:

- 0- Para las clases que eliminen el margino padding configurándolo como.0
- 1- (por defecto) para las clases que establecen margino padding para $ spacer * .25
- 2- (por defecto) para las clases que establecen margino padding para $ spacer * .5
- 3- (por defecto) para las clases que establecen margino padding para $ spacer
- 4- (por defecto) para las clases que establecen margino padding para $ spacer * 1.5
- 5- (por defecto) para las clases que establecen margino padding para $ spacer * 3
- auto- Para las clases que configuran el margin auto.

Arreglar donde dice “Conf Hawaii” para que este centrado pueden usar este CSS

```css
.navbar-brand {
  display: flex;
}
```

![img](https://www.google.com/s2/favicons?domain=https://getbootstrap.com/docs/4.1/components/navbar//docs/4.1/assets/img/favicons/favicon-32x32.png)Navbar · Bootstraphttps://getbootstrap.com/docs/4.1/components/navbar/![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - platzi/bootstraphttps://github.com/platzi/bootstrap

## Creando un carousel de imágenes

El carousel nos permite ver un conjunto de imágenes y mostrarlas de manera automática. Encuentra más información en https://getbootstrap.com/docs/4.1/components/carousel/

para booststrap v5 se cancela la pausa con el hover en la imagen con la siguiente `clase: data-bs-pause=“false”`

Efectos que le pueden poner a las imágenes ademas de greyscale

```css
filter: url("filters.svg#filter-id");
filter: blur(5px);
filter: brightness(0.4);
filter: contrast(200%);
filter: drop-shadow(16px 16px 20px blue);
filter: grayscale(50%);
filter: hue-rotate(90deg);
filter: invert(75%);
filter: opacity(25%);
filter: saturate(30%);
filter: sepia(60%);
```

pueden combinarlos.

Para crear un carrusel de imágenes a traves de Bootstrap, podemos irnos a la documentación, a traves del link: https://getbootstrap.com/docs/4.1/components/carousel/
**object-fit:** Es una propiedad CSS que nos permite adaptar el ancho y alto de un elemento (como imagen por ejemplo) dependiendo del tamaño del contenedor. El valor por defecto es “fill” y puede tomar el valor de “cover”.
**data-pause=”false”**
Es un atributo que podemos asignar dentro de la etiqueta de inicio del carrusel, para evitar que las imágenes se pausen mietras hacemos hover con el mouse.

![img](https://www.google.com/s2/favicons?domain=https://getbootstrap.com/docs/4.1/components/carousel//docs/4.1/assets/img/favicons/favicon-32x32.png)[Carousel · Bootstrap](https://getbootstrap.com/docs/4.1/components/carousel/)

![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)[bootstrap/assets/images at gh-pages · platzi/bootstrap · GitHub](https://github.com/platzi/bootstrap/tree/gh-pages/assets/images)

## Agregando texto informativo del evento

Ahora vamos a incluir un texto informativo sobre nuestro carousel de imágenes. Encuentra más información de los botones en https://getbootstrap.com/docs/4.1/components/buttons/

Si hicieron todo igual, pero el texto no se ve, prueben agregando:

```css
    z-index: 1;
```

En la hoja de estilo en la sección **#carousel .overlay**. Esta propiedad sirve para controlar el orden de los elementos, mientras mayor es el valor, mayor es la prioridad.

**Relative:**
El elemento es posicionado de acuerdo al flujo normal del documento, y luego es desplazado con relación a sí mismo, con base en los valores de top, right, bottom, and left.
El desplazamiento no afecta la posición de ningún otro elemento.
Este valor crea un nuevo contexto de apilamiento, donde el valor de z-index no es auto.
**Absolute:**
El elemento es removido del flujo normal del documento, sin crearse espacio alguno para el elemento en el esquema de la página.
Es posicionado a su ancestro relativo más cercano.
Su posición final está determinada por los valores de top, right, bottom, y left.
Este valor crea un nuevo contexto de apilamiento cuando el valor de z-index no es auto.
Elementos absolutamente posicionados pueden tener margen, y no colapsan con ningún otro margen.

## Agregando botones

Si al pasar el mouse sobre el botón **Comprar tickets** el texto se pone de color negro en vez de blanco solo tiene que agregar **`!important`** a la propiedad color definida en la clase **`btn-platzi`**:

```css
.btn-platzi {
    background-color: #97c93e;
    color: white !important;
}
```

[Para ocultar elementos o visualizar elementos](https://getbootstrap.com/docs/4.1/utilities/display/#hiding-elements) en ciertos tamaños, hidden elements es la palabra clave en nuestra pagina de Bootstrap

Para los que quieren personalizar aun mas sus temas de bootstrap les recomiendo [bootswatch](https://bootswatch.com/)

![img](https://www.google.com/s2/favicons?domain=https://getbootstrap.com/docs/4.1/components/buttons//docs/4.1/assets/img/favicons/favicon-32x32.png)[Buttons · Bootstrap](https://getbootstrap.com/docs/4.1/components/buttons/)

## Las cards de Bootstrap 4

Conoceremos el feature de Cards de Bootstrap. Para más información consulta: https://getbootstrap.com/docs/4.1/components/card/. Tenemos un reto para ti: ya que sabes como crear las cards por speaker, queremos que cambies el numero de tarjetas que se muestran por el tipo de dispositivo.

Un tip para aquellos que no lo sabian si es que todavia no lo han mencionado. Para poder ahorrar tiempo en poner text generico(lorem ipsum), solamente al momento de escribir pongan “lorem” y la cantidad de palabras que quieran poner en display, seguido por la tecla TAB.

Ej. `<p>lorem20</p>`

![img](https://www.google.com/s2/favicons?domain=https://getbootstrap.com/docs/4.1/components/card//docs/4.1/assets/img/favicons/favicon-32x32.png)(Cards · Bootstrap)(https://getbootstrap.com/docs/4.1/components/card/)

## Pastillas de texto

A los Badges se les puede dar un poco más de padding para que “respiren más”, también se puede contrastar el color del texto con el del background para que luzca algo mejor.

<img src="https://i.ibb.co/CJTwWgC/badges.jpg" alt="badges" border="0">

Los badges no aparecen, colocar el siguente codigo en `link` en el `head`

```html
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
```

![img](https://www.google.com/s2/favicons?domain=https://getbootstrap.com/docs/4.1/components/carousel//docs/4.1/assets/img/favicons/favicon-32x32.png)[Carousel · Bootstrap](https://getbootstrap.com/docs/4.1/components/carousel/)

![img](https://www.google.com/s2/favicons?domain=https://getbootstrap.com/docs/4.1/components/badge//docs/4.1/assets/img/favicons/favicon-32x32.png)[Badges · Bootstrap](https://getbootstrap.com/docs/4.1/components/badge/)

## Agregando un contenedor de ancho completo

Le agregué mejor `align-self-center` a la segunda `col` para que me centre el contenido de la columna de forma vertical

```html
<div class="col-12 col-lg-6 pt-2 pb-2 align-self-center">
	<h2>Honolulu - Octubre 2025</h2>
	<p>Honolulu o Honolulú es la capital y localidad más grande del 		estado de Hawái, en los Estados Unidos. Honolulu es la más sureña de entre las principales ciudades estadounidenses. Aunque el nombre de Honolulu se refiere al área urbana en la costa sureste de la isla de Oahu, la ciudad y el condado de Honolulu han formado una ciudad-condado consolidada que cubre toda la isla (aproximadamente 600 km² de superficie).
	</p>
	<a href="https://es.wikipedia.org/wiki/Honolulu" class="btn btn-outline-light">Conoce más</a>
</div>
```

*Ahí también les dejo el texto y el link de wikipedia*

![img](https://www.google.com/s2/favicons?domain=https://getbootstrap.com/docs/4.1/layout/overview/#containers/docs/4.1/assets/img/favicons/favicon-32x32.png)[Overview · Bootstrap](https://getbootstrap.com/docs/4.1/layout/overview/#containers)

## Los formularios de Bootstrap 4

Aquí el **form con validaciones** :

Se pueden utilizar las clases en el form:

- **class=“was-validated”** - Para que se muestren en rojo los campos y el usuario observe que tiene que llenarlos
- **class=“needs-validation”** - Al momento en el que se lanza el submit del button se realiza la validación y le entrega el feedback al usuario de los campos correctos e incorrectos.

<img src="https://i.ibb.co/gTy2NpB/form.jpg" alt="form" border="0">

```html
<section id="conviertete-en-orador" class="pt-4 pb-4">
      <div class="container">
        <div class="row">
          <div class="col text-uppercase text-center">
            <small>Conviértete en un</small>
            <h2>Orador</h2>
          </div>
        </div>
        <div class"row">
          <div class="col text-center">
            Anótate como orador para darnos una charla ignite.
            Cuéntanos de qué quieres hablar!
          </div>
        </div>
        <div class="row">
          <div class="col col-md-8 offset-md-2 col-lg-8 offset-lg-2 pt-2">
            <form class="needs-validation" novalidate>
              <div class="form-row">
                <div class="form-group col-12 col-md-6">
                  <input type="text" class="form-control" placeholder="Nombre" required>
                  <div class="valid-feedback">
                    Correcto!!
                  </div>
                  <div class="invalid-feedback">
                    Por favor ingresa tu nombre.
                  </div>
                </div>
                <div class="form-group col-12 col-md-6">
                  <input type="text" class="form-control" placeholder="Apellido" required>
                  <div class="valid-feedback">
                    Correcto!!
                  </div>
                  <div class="invalid-feedback">
                    Por favor ingresa tu apellido.
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col">
                  <textarea class="form-control form-control-lg" placeholder="Sobre qué quieres hablar?" required></textarea>
                  <div class="valid-feedback">
                    Correcto!!
                  </div>
                  <div class="invalid-feedback">
                    Debes de escribir sobre qué quieres hablar.
                  </div>
                  <small class="form-text text-muted">
                    Recuerda incluir un título a tu charla.
                  </small>
                </div>
              </div>
              <div class="form-row">
                <div class="col">
                  <button type="submit" class="btn btn-platzi btn-block">Enviar</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
```

<img src="https://i.ibb.co/FDQzmfg/form1.jpg" alt="form1" border="0">

**Agregar el js** para que realice las validaciones:

```js
<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>
```

![img](https://www.google.com/s2/favicons?domain=https://getbootstrap.com/docs/4.1/components/forms//docs/4.1/assets/img/favicons/favicon-32x32.png)[Forms · Bootstrap](https://getbootstrap.com/docs/4.1/components/forms/)

## Agregando un tooltip

En esta clase aprenderemos a incluir abreviaciones, que es un componente que le permite al visitante de tu página tener una definición inmediata de una palabra. Para más información checa este website: https://getbootstrap.com/docs/4.1/content/typography/#abbreviations y también agregaremos tooltips un plugin de jQuery que ya viene incluído en Bootstrap: https://getbootstrap.com/docs/4.1/components/tooltips/
Reto: Incorpora otro tooltip en tu sitio.

**¿Que es un tooltip?**
Un tooltip (también llamada descripción emergente) es una herramienta de ayuda visual, que funciona al situar el cursor sobre algún elemento gráfico, mostrando una ayuda adicional para informar al usuario de la finalidad del elemento sobre el que se encuentra.

Los tooltip son una variación de los globos de ayuda y es un complemento muy usado en programación y diseño, dado que proporcionan información adicional sin necesidad de que el usuario la solicite de forma activa.

**Qué es un Ignite?**
Ignite es un evento de charlas ultrarrápidas de 5 minutos exactos, apoyadas por 20 transparencias programadas para avanzar cada 15 segundos. Están presentadas por gente que tiene una idea, una historia o una visión, y el valor de subirse al escenario para compartirlo con su comunidad. Ignite se celebra en la actualidad en más de 100 ciudades alrededor del mundo.

**¿Qué tiene de especial?**
La idea central de Ignite es “encender” (Ignite en inglés). Encender la curiosidad. Encender la inspiración. Encender las ideas. [Establecer conexiones entre asistentes, oradores y colaboradores](http://hemeroteca.innovaspain.com/detalle_noticia.php?id=3183)

Actualización para usarlo en Bootstrap 5:
Usar la etiqueta **abbr** de la siguiente manera:

```html
<abbr title="5 min talk" data-bs-toggle="tooltip">ignite</abbr>
```

En el archivo index.js colocar lo siguiente:

```js
var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
  return new bootstrap.Tooltip(tooltipTriggerEl)
})
```

**IMPORTANTE**

Si estas en el 2021 y no te funcional el tootip, prueba este codigo

`html`

```html
<abbr data-bs-toggle="tooltip"title="charlas de 5 minutos">charla ignite</abbr>.
```

`js`

```js
$(function () {
    $('[data-bs-toggle="tooltip"]').tooltip()
})
```

![img](https://www.google.com/s2/favicons?domain=https://getbootstrap.com/docs/4.1/content/typography/#abbreviations/docs/4.1/assets/img/favicons/favicon-32x32.png)[Typography · Bootstrap](https://getbootstrap.com/docs/4.1/content/typography/#abbreviations)

![img](https://www.google.com/s2/favicons?domain=https://getbootstrap.com/docs/4.1/components/tooltips//docs/4.1/assets/img/favicons/favicon-32x32.png)[Tooltips · Bootstrap](https://getbootstrap.com/docs/4.1/components/tooltips/)

## Scrollspy: Conociendo la ubicación del usuario en el header

El scrollspy le permite al usuario conocer la posición dentro del sitio. Revisa más sobre este componente en: [scrollspy](https://getbootstrap.com/docs/4.1/components/scrollspy/)

A LOS QUE NO LO FUNCIONA EL **sticky-top**, EN LAS NUEVAS VERSIONES DE BOOTSTRAP NO SE USA ASI, AHORA LO CAMBIARON POR **fixed-top**

Hay un problema cuando se cambia el id del nav a “navbar” y es que cuando la pantalla es pequeña, el botón para desplegar el menú del nav deja de funcionar.
Para solucionarlo solo debes cambiar de esto:

```html
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
	<span class="navbar-toggler-icon"></span>
</button>
```

A esto:

```html
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar">
	<span class="navbar-toggler-icon"></span>
</button>
```



![img](https://www.google.com/s2/favicons?domain=https://getbootstrap.com/docs/4.1/components/scrollspy//docs/4.1/assets/img/favicons/favicon-32x32.png)[Scrollspy · Bootstrap](https://getbootstrap.com/docs/4.1/components/scrollspy/)

## Agregando un modal para comprar tickets

En esta clase aprenderemos sobre los modals, que son básicamente mostrar un mensaje en una ventana emergente. Conoce más sobre los modals aquí: https://getbootstrap.com/docs/4.1/components/modal/

Plugins interesantes para Bootstrap 3 algunas ya se encuentran actualizados para Bootstrap 4:

**1. Fuel UX**
Fuel UX es una increíble colección de mejoras para Twitter Bootstrap. Todos los controles son limpios y ligeros, y se ajustan de forma natural al aspecto de Bootstrap. La colección incluye controles como datagrids, cajas de selección personalizadas, spinners, árboles, formularios en varios pasos y más.

**2. Jasny**
Jasny es otra colección de componentes para Bootstrap. Cuenta con controles como máscaras de entrada, botones de subida de archivos, iconos, estilos adicionales para otros componentes y demás. Existe una versión de Bootstrap con todos los cambios integrados que se puede descargar de forma separada o solo el plugin.

**3. Bootstrap Lightbox**
Bootstrap por defecto incluye un carrusel, pero se queda un pelín corto cuando necesitas mostrar la imagen en un lightbox. De eso es de lo que se ocupa Bootstrap Lightbox. Solamente debes añadir el código necesario y en los futuros carruseles que implementes en tu web tendrás la opción de ampliarlos mediante un lightbox responsive.

**4. Simple Lightbox**
Simple Lightbox es otro plugin para mostrar una vista previa de imágenes mediante una ventana modal, mucho más simple que el que te hemos mostrado antes. Para implementarlo solamente hay que añadir un atributo especial a la imagen.

**5. Bootstrap Image Gallery**
Bootstrap Image Gallery es una solución muy completa para crear galerías de imágenes en Bootstrap. Después de incluir los archivos necesarios en tu página, puedes conseguir una cuadrícula de imágenes que se abren en ventanas modales. La galería, de manera opcional, puede entrar en modo de pantalla completa.

**6. Bootbox.js**
Bootbox.js es una pequeña biblioteca JavaScript que automatiza el proceso de creación de los cuadros de diálogo en Bootstrap. Este plugin te lo pone bien sencillo, todo lo que tienes que hacer para mostrar un cuadro de diálogo en Bootstrap es llamar a una función. Es super útil para mostrar mensajes de alerta, de confirmación y de satisfacción.

**7. Bootstrap Modal**
Seguramente ya sabréis qué hace este plugin por su nombre. En efecto, Bootstrap Modal mejora la clase modal que hay en Bootstrap por defecto. Otorga la capacidad de cargar contenido en dichos modales a través de AJAX, cosa de lo que peca la opción por defecto.

**8. Bootstrap Growl**
Bootstrap Growl es un plugin que convierte las notificaciones de Bootstrap, en bonitas alertas Growl que se pueden personalizar al 100%. Puedes elegir la posición de la notificación respecto a la pantalla, las dimensiones, configurar una animación y el tiempo que tarda en desvanecerse.

**9. Bootstrap Notify**
Bootstrap Notify es una extensión muy fácil de utilizar para las notificaciones de Bootstrap. Al igual que Bootstrap Growl, con este plugin también puedes personalizar todos los aspectos de las notificaciones e incluso, hasta donde deben mostrarse.

**10. Bootstrap Form Helpers**
Bootstrap Form Helpers es una colección imprescindible para la mejora de formularios web. Viene con 12 plugins jQuery personalizados que te ofrecen de todo, desde nuevos controles para fecha y hora, listas de fuentes, campos para idioma y paises y demás.

**11. Bootstrap Tags**
Bootstrap Tags es un plugin ideal para la inserción de tags en formularios. Si pretendemos que un usuario deba introducir unos parámetros que deben ser recogidos por separado, Bootstrap Tags puede convertirse en tu mejor aliado para esa tarea. Además, el diseño de los tags puede personalizarse al 100% y no solo tiene soporte para tags, con este plugin también podrás crear popovers, placeholders y demás…

**12. Bootstrap Switch**
Una interfaz móvil no esta completa sin su control de estilo switch, y Bootstrap Switch te otorga el poder de poder llevarlo a cabo en tus páginas web siempre respetando el estilo de Bootstrap. Puedes personalizar el tamaño del control, así como los colores mediante la asignación de distintos nombre de clase al elemento.

**13. Bootstrap Markdown**
Bootstrap Markdown te permite la edición de Markdown en tus proyectos web de una manera fácil y sencilla. Solamente tendrás que agregar los archivos necesarios en tu página web, y ya podrás utilizarlo.

**14. Bootstrap Maxlength**
Bootstrap MaxLength es un pequeño plugin que detecta la propiedad maxlength de HTML en un campo de texto, y muestra un contador interactivo con los caracteres restantes. Es una gran compoenente que no está de más para añadirlo a los cuadros de texto de tus formularios.

**15. Bootstrap Select**
Bootstrap favorece los controles nativos del navegador. Sin embargo, en algunos proyectos es útil contar con controles personalizables, como los selects, y eso es lo que hace exáctamente Bootstrap Select. Este plugin te muestra un cuadro de selección bonito y personalizable que, de seguro, quedará muy bien en tu página. [**Fuente** ](https://programacion.net/articulo/50_plugins_para_bootstrap_que_deberias_estar_usando_ya_parte_1_1078)

Para implementar un modal debemos:

- Copiar el contenido HTML del modal como tal (mas no del botón). Es recomendable poner los modals fuera de cualquier otro elemento HTML (por ejemplo, al final de todas las secciones incluyendo el footer).
- Se recomienda cambiarle el nombre al id del modal, para una mejor legibilidad.
- Debemos asignar al ítem de navegación que abrirá el modal, las clases de **‘.data-toggle=”modal”’** y **‘”.data-target=”[#idDelContenidoHTMLDelModal]”’.**

debemos poner la palabara bs- es por BOOTSTRAP 5

```html
<data-bs-toggle="modal" data-bs-target="#modalCompra>
```

Los modals se crean con HTML, CSS y JavaScript. Se colocan sobre todo lo demás en el documento y eliminan el desplazamiento del `<body>` para que el contenido modal se desplace en su lugar.
Al hacer clic en el “fondo” modal, se cerrará automáticamente el modal.
Bootstrap solo admite una ventana modal a la vez. Los modales anidados no son compatibles, ya que creemos que son malas experiencias de usuario.
Uso de modalidades position: fixed, que a veces puede ser un poco particular sobre su representación. Siempre que sea posible, coloque su HTML modal en una posición de nivel superior para evitar posibles interferencias de otros elementos. Es probable que tenga problemas al anidar un .modaldentro de otro elemento fijo.
Una vez más, debido a que position: fixedhay algunas advertencias sobre el uso de modales en dispositivos móviles. Consulte nuestros documentos de soporte del navegador para más detalles.
Debido a cómo HTML5 define su semántica, el autofocusatributo HTML no tiene efecto en los modales de Bootstrap. Para lograr el mismo efecto, use algunos JavaScript personalizados:

![img](https://www.google.com/s2/favicons?domain=https://getbootstrap.com/docs/4.1/components/modal//docs/4.1/assets/img/favicons/favicon-32x32.png)[Modal · Bootstrap](https://getbootstrap.com/docs/4.1/components/modal/)

## Un nuevo formulario para completar la compra

Les comparto mi **modal form** :

```html
<div class="modal fade" id="modalCompra" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Adquirir Tickets</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="was-validated">
          <div class="form-row">
            <div class="col form-group">
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">@</span>
                </div>
                <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
                <div class="invalid-feedback">
                  Tu Username es incorrecto, ingrésalo nuevamente
                </div>
              </div>
            </div>
          </div>
          <div class="form-row">
            <div class="col form-group">
              <label class="text-platzi-modal" for="inputPassword5">Password</label>
              <input type="password" id="inputPassword5" class="form-control" aria-describedby="passwordHelpBlock" placeholder="Password">
              <div class="invalid-feedback">
                Tu passwaor es incorrecto
              </div>
            </div>
          </div>
          <div class="form-row">
            <div class="col form-group">
              <div class="form-check">
                <div class="custom-control custom-checkbox mb-3">
                  <input type="checkbox" class="custom-control-input" id="customControlValidation1" required>
                  <label class="custom-control-label" for="customControlValidation1">Acepto los términos y condicones del sorteo</label>
                  <div class="invalid-feedback">  Debes aceptar los términos y condiciones para acceder al sorteo.</div>
                </div>
              </div>
            </div>
          </div>
        </form>
        <div class="alert alert-success" role="alert">
          Recibirás un correo de confirmación si sales sorteado.
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-platzi">Comprar</button>
      </div>
    </div>
  </div>
</div>
```

<img src="https://i.ibb.co/9rk06FD/form-modal.jpg" alt="form-modal" border="0">

# 3. Deploy a producción

## Poniendo nuestro sitio en producción

Ahora vamos a poner nuestro sitio web estático a GitHub Pages. Para eso debes tener un usuario de GitHub y descargar GitHub for Windows.

![img](https://www.google.com/s2/favicons?domain=https://assets-cdn.github.com/favicon.ico)[Git for Windows · GitHub](https://github.com/git-for-windows)

**Pasos desde consola:**
Para poner nuestra pagina estática en producción, debemos crear un nuevo repositorio vamos a nuestro github y en la home le damos al botón verde New o en el headder, nuevo repositorio, a continuación github nos va a pedir unos datos, de los cuales el único dato requerido es el nombre del repositorio,un requerimiento que debemos tener instalado en windows en git for windows, la instalación es muy sencilla, hay que descargar el instalador y simplemente darle siguiente en cada una de las opciones, con git instalado en nuestra computadora, vamos a copiar la url que nos genera la pagina de github en la parte de HTTPS, la copiamos y nos dirigimos a nuestra consola, y lo que vamos a hacer es en la carpeta de nuestro repositorio, si aun no lo hicimos tenemos que correr el comando:

```bash
git init
```

de esta manera vamos a inicializar un nuevo repositorio local dentro de esta carpeta, cuando tengamos todo el código ya necesario para llevarlo a producción, vamos a tipear el comando:

```bash
git checkout -b
```

es decir vamos a crear una nueva rama llamada (se continua código de arriba):

```bash
git checkout -b gh-pages
```

le damos enter y a continuación tipeamos:

```bash
git remote add origin
```

y pegamos el enlace que habíamos copiado (se continua código de arriba):

```bash
git remote add origin https://github.com/nombre-proyecto.git
```

le damos enter y si tipeamos:

```bash
git remote
```

nos va a aparecer listado el origin, ahora lo que tenemos que tipear es:

```bash
git push origin gh-pages
```

deberíamos agregar después del push y antes del origin -u para hacer push de este nuevo branch hacia nuestro repositorio:

```bash
git push -u origin gh-pages
```

y ya estaría listo, en caso de que git no te deje realizar el push, es
porque antes deberíamos correr:

```bash
git add -A
```

o

```bash
git add 
```

y realizar un commit con:

```bash
git commit -m “mensaje que queramos”
```

a continuación refresquemos nuestro repositorio en la pagina de github y vemos que esta el código que teníamos en nuestro editor de texto, como el push lo hicimos en una rama llamada gh-pages, lo que sucede es que github crea por nosotros una url que nos va a dar en la parte de settings, abajo en la parte de github pages


## Conclusiones del curso

Curso finalizado. Reto muy especial. Para los dispositivos pequeños el menú de navegación superior no aparece si haces click en el menú hamburguesa, último reto, que resuelvas este bug y te dejamos la solución en el repositorio. ¡Èxitos!

