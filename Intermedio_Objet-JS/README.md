<h1>Intermedio de Programación Orientada a Objetos en JavaScript</h1>

<h3>Juan David Castro</h3>

<h1>Table of Content</h1>

- [1. Introducción](#1-introducción)
  - [¿Qué hay dentro de los objetos en JavaScript?](#qué-hay-dentro-de-los-objetos-en-javascript)
- [2. Profundicemos en los objetos](#2-profundicemos-en-los-objetos)
  - [Static: atributos y métodos estáticos en JavaScript](#static-atributos-y-métodos-estáticos-en-javascript)
  - [Métodos estáticos del prototipo Object](#métodos-estáticos-del-prototipo-object)
  - [Object.defineProperty](#objectdefineproperty)
- [3. Cómo copiar objetos en JavaScript](#3-cómo-copiar-objetos-en-javascript)
  - [Cómo funciona la memoria en JavaScript](#cómo-funciona-la-memoria-en-javascript)
  - [Shallow copy en JavaScript](#shallow-copy-en-javascript)
  - [Qué es JSON.parse y JSON.stringify](#qué-es-jsonparse-y-jsonstringify)
- [4. Recursividad en JavaScript](#4-recursividad-en-javascript)
  - [Qué es recursividad](#qué-es-recursividad)
  - [Deep copy con recursividad](#deep-copy-con-recursividad)
- [5. Abstracción y encapsulamiento sin prototipos](#5-abstracción-y-encapsulamiento-sin-prototipos)
  - [Abstracción con objetos literales y deep copy](#abstracción-con-objetos-literales-y-deep-copy)
  - [Factory pattern y RORO](#factory-pattern-y-roro)
  - [Module pattern y namespaces: propiedades privadas en JavaScript](#module-pattern-y-namespaces-propiedades-privadas-en-javascript)
  - [Getters y setters](#getters-y-setters)
- [6. Cómo identificar objetos](#6-cómo-identificar-objetos)
  - [Qué es duck typing](#qué-es-duck-typing)
  - [Duck typing en JavaScript](#duck-typing-en-javascript)
  - [Instance Of en JavaScript con instancias y prototipos](#instance-of-en-javascript-con-instancias-y-prototipos)
  - [Atributos y métodos privados en prototipos](#atributos-y-métodos-privados-en-prototipos)
  - [Creando métodos estáticos en JavaScript](#creando-métodos-estáticos-en-javascript)
- [7. Próximos pasos](#7-próximos-pasos)
  - [¿Quieres más cursos de POO en JavaScript?](#quieres-más-cursos-de-poo-en-javascript)

# 1. Introducción

## ¿Qué hay dentro de los objetos en JavaScript?

- Closures y scope
- Fundamentos de POO
- POO B&aacute;sico de JS

# 2. Profundicemos en los objetos

## Static: atributos y métodos estáticos en JavaScript

### Static: atributos y métodos estáticos en JavaScript

Hasta ahora habíamos aprendido que apara acceder a los métodos o atributos de una clase o prototipo teníamos que crear una instancia del prototipo(Objeto). Pero hay una forma de que podemos saltarnos tener que crear una instancia del prototipo para acceder a los métodos o atributos, esta es la forma **Static**

Para crear atributos estáticos los cuales podamos acceder sin crear un objeto o una instancia de este prototipo, solo hay que agregar al atributo la palabra `**static**`

**Métodos estáticos de Objetct**

```jsx
const objetito = {
  name: "Carlitos",
  email: "carlitosmazzaroli@gmail.com",
  age: 16
};
```

#### `Object.keys()`

Nos devuelve una lista con todos los keys(nombres claves) de nuestro objeto objetito

```jsx
Object.keys(objetito);
// (3) ["name", "email", "age"]
```

#### `Object.getOwnPropertyNames()`

Hace prácticamente lo mismo que Object.keys con pequeñas diferencias [DOCUMENTACION](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/getOwnPropertyNames)

```jsx
Object.getOwnPropertyNames(objetito);
// (3) ["name", "email", "age"]
```

#### `Object.entries()`

El método entries nos devolverá un arrays de arrays donde tendremos nuestra palabra clave con su respectivo valor por cada propiedad del prototipo [key, value]

```jsx
Object.entries(objetito);
// [
//     0: (2) ["name", "Carlitos"]
//     1: (2) ["email", "carlitosmazzaroli@gmail.com"]
//     2: (2) ["age", 16]
// ]
```

#### `Object.getOwnPropertyDescriptors(objetito)`

Nos devuelve todas las propiedades de los objetos, con sus keys y values, y otros atributos. Los atributos
**writable**, **configurable** y **enumerable**
es la forma que tiene JavaScript para limitar el acceso modificar o modificación de nuestros atributos o de nuestros objetos.

```jsx
Object.getOwnPropertyDescriptors(objetito);
// {
//     age:{
//         configurable: true
//         enumerable: true
//         value: 16
//         writable: true
//     }
//     email:{
//         configurable: true
//         enumerable: true
//         value: "carlitosmazzaroli@gmail.com"
//         writable: true
//     }
//     name:{
//         configurable: true
//         enumerable: true
//         value: "Carlitos"
//     }
// }
```

#### Encapsulamiento

Si recordamos del curso anterior, el objetivo del encapsulamiento es limitiar quien puede modificar, acceder o ejecutar nuestros metodos o atributos de la clase o prototipo.

Ahora con las propiedades **writable configurable enumerable** podemos limitar quien tiene acceso, modificar nuestros objetos.

Si quieres tener acceso a una variable o método sin tener que crear instancias de una clase usa la palabra static antes de crear tu variable o método.

```javascript
class saludar {
  static saludar = "hola";
  static metodoSaludar() {
    return "hola2";
  }
}
console.log(saludar.saludar); //hola
console.log(saludar.metodoSaludar()); //hola2
```

## Métodos estáticos del prototipo Object

#### Código Clase

```jsx
const carlos = {
  name: "carlito",
  age: 18,
  approvedCourses: ["Curso 1"],

  addCourse(newCourse) {
    console.log("This", this);
    console.log("This.approvedCourses", this.approvedCourses);
    this.approvedCourses.push(newCourse); // this hace referencia al objeto carlos
  }
};

// console.log(Object.keys(carlos));
// console.log(Object.getOwnPropertyNames(carlos));

// console.log(Object.entries(carlos));

// console.log(Object.entries(carlos)[1][0]); // Codigo probado en consola
// console.log(Object.entries(carlos)[1][1]); // Codigo probado en consola
// console.log(Object.entries(carlos)[3][1]); // Codigo probado en consola
// console.log(Object.entries(carlos)[3][1]("Curso 2")); // Error

console.log(Object.getOwnPropertyDescriptors(carlos));

Object.defineProperty(carlos, "pruebaNasa", {
  value: "extraterrestres",
  writable: true,
  enumerable: true,
  configurable: true
});
```

#### [`Object.defineProperty()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty)

El método estático Object.defineProperty() define una nueva propiedad sobre un objeto, o modifica una ya existente, y devuelve el objeto modificado.

#### [Sintaxis](https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty#sintaxis)

```javascript
Object.defineProperty(obj, prop, descriptor);
```

#### [Parámetros](https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty#parameters)

`obj`El objeto sobre el cual se define la propiedad.

`prop`El nombre de la propiedad a ser definida o modificada.

`descriptor`El descriptor de la propiedad que está siendo definida o modificada

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/favicon-48x48.97046865.png)Object - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object)

## Object.defineProperty

Con esta propiedad se pueden definir nuevas propiedades a nuestro objeto. Así mismo, se puede configurar ciertas características de la propiedad tales como:

- **Configurable:** Esta indica si la propiedad puede ser borrada o eliminada
- **Enumerable:** Indica si la propiedad puede ser mostrada en la enumeración de las mismas. Existen ciertos métodos que toman como referencia este valor para mostrar la propiedad
- **Writable:** Esta indica si la propiedad puede ser modificada con el operador de asignación (=)

```jsx
const person = {
  userName: "zajithcorro",
  age: 26,
  approvedCourses: ["Curso Profesional de Git y Github"],
  addApprovedCourse(course) {
    console.log(this);
    this.approvedCourses.push(course);
  }
};
Object.defineProperty(person, "mail", {
  value: "zajith@outlook.com",
  writable: false,
  enumerable: true,
  configurable: true
});

Object.defineProperty(person, "twitter", {
  value: "zajithcorro",
  writable: true,
  enumerable: false,
  configurable: true
});

Object.defineProperty(person, "platziPoints", {
  value: 7500,
  writable: true,
  enumerable: true,
  configurable: false
});

Object.defineProperty(person, "gender", {
  value: "male",
  writable: false,
  enumerable: false,
  configurable: false
});
```

Si queremos modificar un propiedad que tienen `writable: false` no permitirá que su valor sea modificado

```jsx
person.mail; // 'zajith@outlook.com'
person.mail = "zajith@gmail.com"; // 'zajith@gmail.com'
person.mail; // // 'zajith@outlook.com'
```

`Object.keys` solo muestra las propiedades que tienen `enumerable: true`. A diferencia de `Object.getOwnPropertyNames` que muestra todas las propiedades

```jsx
Object.keys(person); // [ 'userName', 'age', 'approvedCourses', 'addApprovedCourse', 'mail', 'platziPoints' ]
Object.getOwnPropertyNames(person); // [ 'userName', 'age', 'approvedCourses', 'addApprovedCourse', 'mail', 'twitter', 'platziPoints', 'gender' ]
```

Si queremos eliminar propiedad que tienen `configurable: false` no permitirá que sea borrada del objeto.

```jsx
delete person.platziPoints; // false
delete person.userName; // true
```

**Object.freeze()**

Este método _congela_ un objeto que sea pasado. Es decir:

- Impide que se le agreguen nuevas propiedades
- Impide que sean eliminas propiedades ya existentes
- Impide que sus las propiedades internas (`writable`, `enumerable` y `configurable`) sean modificadas

```jsx
const person = {
  userName: "zajithcorro",
  age: 26,
  approvedCourses: ["Curso Profesional de Git y Github"],
  addApprovedCourse(course) {
    console.log(this);
    this.approvedCourses.push(course);
  }
};

Object.freeze(person);

person.mail = "zajith@mail.com";
Object.keys(person); // [ 'userName', 'age', 'approvedCourses', 'addApprovedCourse' ]

person.age = 27;
person.age; // 26

delete person.userName; // false
```

**Object.seal()**

Este método _sella_ un objeto que sea pasada. Es decir:

- Impide que nuevas propiedades sean agregadas
- Cambia en todas las propiedades `configurable: false`, con lo que impide que sean borradas
- Las propiedades aún puede ser modificadas, ya que `writable` esta `true`

```jsx
const person = {
  userName: "zajithcorro",
  age: 26,
  approvedCourses: ["Curso Profesional de Git y Github"],
  addApprovedCourse(course) {
    console.log(this);
    this.approvedCourses.push(course);
  }
};

Object.seal(person);

person.mail = "zajith@mail.com";
Object.keys(person); // [ 'userName', 'age', 'approvedCourses', 'addApprovedCourse' ]

delete person.userName; // false

person.age = 27;
person.age; // 27
```

![img](https://i.imgur.com/ZWPW3Na.png)

# 3. Cómo copiar objetos en JavaScript

## Cómo funciona la memoria en JavaScript

🎳 Las variables son referencias a un espacio en memoria.

🎩 Los navegadores web usan dos tipos de memorias: Stack y Heap.

📁 La memoria Stack es muy rápida, pero sin tanto espacio. Aquí se guardan los valores primitivos (booleanos, strings, números…).

🌪 La memoria Heap es más lenta, pero permite guardar enormes cantidades de información _(son como los tornados: grandes, lentos y desordenados)_. En esta memoria guardamos los valores de los objetos `({...}`).

Entender cómo funciona la memoria en JavaScript no solo será útil para aprender POO, sino también para programación funcional. 😉

Las variables son una referencia a un espacio en memoria. Dependiendo del tipo de valor que sean serán ubicadas en alguna de las dos tipos de memoria:

- Stack - Mucho más rápida, pero sin tanto espacio
- Heap - Más lenta, pero con mucho más espacio

Las variables que no tienen un valor de tipo objeto, son almacenadas en la memoria `stack`. Las variables que tienen como valor un objeto, funcionan de una manera diferente:

- El valor (objeto) es guardada en la memoria `heap`
- En la memoria `stack` se guarda un apuntador (pointer) hacia la memoria `heap`

Es por esto que cuando nosotros asignamos una variable que tiene como valor un objeto, a una nueva variable, lo unico que hacemos es asignar el apuntador. Es así que al modificar el objeto, en cualquiera de las dos variables, los cambios se reflejan en las dos

```jsx
const person = {
  name: "Zajith",
  email: "zajith@mail.com"
};

const person2 = person;

person2.name = "Juanito";

person2; // { name: 'Juanito', email: 'zajith@mail.com' }
person; // { name: 'Juanito', email: 'zajith@mail.com' }
```

<h2>Stack</h2>

|   nombres    | Valores |          Objetos           |
| :----------: | :-----: | :------------------------: |
|  `let name`  | "Juan"  |                            |
|  `let age`   |   18    |                            |
| `let patito` | POINTER | `{...}` \| (Memoria heap ) |

Crear la variable se lo conoce como declarar.

```js
const patito
```

inicializar variables es cuando le asignamos algún valor

```js
patito = "feo";
```

## Shallow copy en JavaScript

Shallow Copy se refiere a la forma de crear un nuevo objeto a partir de las propiedades de otro. Esta copia solo se hace a un nivel alto, no se hace con objetos dentro de objetos (nested objects), lo que provoca que la modificación de una de sus propiedades, modifique el objeto principal.

```jsx
const person = {
  name: "Eduardo",
  email: "edudardo@mail.com",
  social: {
    facebook: "Eduardo Garcia",
    twiiter: "EduGar"
  }
};

const person2 = {};

for (prop in person) {
  person2[prop] = person[prop];
}

person2.name = "Eduardo Miguel";
person;
/* {
  name: 'Eduardo',
  email: 'edudardo@mail.com',
  social: { facebook: 'Eduardo Garcia', twiiter: 'EduGar' }
} */

person2;
/* {
  name: 'Eduardo Miguel',
  email: 'edudardo@mail.com',
  social: { facebook: 'Eduardo Garcia', twiiter: 'EduGar' }
} */

person2.social.facebook = "Eduardo Miguel Garcia";
person;
/* {
  name: 'Eduardo',
  email: 'edudardo@mail.com',
  social: { facebook: 'Eduardo Miguel Garcia', twiiter: 'EduGar' }
} */

person2;
/* {
  name: 'Eduardo Miguel',
  email: 'edudardo@mail.com',
  social: { facebook: 'Eduardo Miguel Garcia', twiiter: 'EduGar' }
} */
```

Existe un método del objeto `Object` que nos permite hacer esta copia directa, con el método `Object.assign()`. Sin embargo, este método sigue conservando el problema de los objetos internos.

```jsx
const person = {
  name: "Eduardo",
  email: "edudardo@mail.com",
  social: {
    facebook: "Eduardo Garcia",
    twiiter: "EduGar"
  }
};

const person2 = Object.assign({}, person);

person2.name = "Eduardo Miguel";
person;
/* {
  name: 'Eduardo',
  email: 'edudardo@mail.com',
  social: { facebook: 'Eduardo Garcia', twiiter: 'EduGar' }
} */

person2;
/* {
  name: 'Eduardo Miguel',
  email: 'edudardo@mail.com',
  social: { facebook: 'Eduardo Garcia', twiiter: 'EduGar' }
} */

person2.social.facebook = "Eduardo Miguel Garcia";
person;
/* {
  name: 'Eduardo',
  email: 'edudardo@mail.com',
  social: { facebook: 'Eduardo Miguel Garcia', twiiter: 'EduGar' }
} */

person2;
/* {
  name: 'Eduardo Miguel',
  email: 'edudardo@mail.com',
  social: { facebook: 'Eduardo Miguel Garcia', twiiter: 'EduGar' }
} */
```

De la misma manera existe otro método que nos permite hacer esta copia, el método `Object.create()`. A diferencia de los métodos anteriores, este método copia un objeto como el prototipo del nuevo objeto. Sin embargo, sigue conservando el mismo problema de los objetos internos.

```jsx
const person = {
  name: "Eduardo",
  email: "edudardo@mail.com",
  social: {
    facebook: "Eduardo Garcia",
    twiiter: "EduGar"
  }
};

const person2 = Object.create(person);

person2.name = "Eduardo Miguel";
person;
/* {
  name: 'Eduardo',
  email: 'edudardo@mail.com',
  social: { facebook: 'Eduardo Garcia', twiiter: 'EduGar' }
} */

person2;
/* {
  name: 'Eduardo Miguel',
  __proto__: {
    name: 'Eduardo Miguel',
    email: 'edudardo@mail.com',
    social: {
      facebook: 'Eduardo Garcia',
      twiiter: 'EduGar'
    }
  }
} */

person2.social.facebook = "Eduardo Miguel Garcia";
person;
/* {
  name: 'Eduardo',
  email: 'edudardo@mail.com',
  social: { facebook: 'Eduardo Miguel Garcia', twiiter: 'EduGar' }
} */

person2;
/* {
  name: 'Eduardo Miguel',
  __proto__: {
    name: 'Eduardo Miguel',
    email: 'edudardo@mail.com',
    social: {
      facebook: 'Eduardo Miguel Garcia',
      twiiter: 'EduGar'
    }
  }
} */
```

El **Shallow Copy** _(copia superficial)_ es una copia bit a bit de un objeto. Se crea un nuevo objeto que tiene una copia exacta de los valores del objeto original. Si alguno de los campos del objeto son referencias a otros objetos, solo se copian las direcciones de referencia, es decir, solo se copia la dirección de memoria.

![jqOlM.jpg](https://static.platzi.com/media/user_upload/jqOlM-029f1163-2db4-47ad-a1c4-45967791286c.jpg)

#### [Object.assign()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign)

El método Object.assign () copia todas las propiedades propias enumerables de uno o más objetos de origen a un objeto de destino. Devuelve el objeto de destino modificado.

Las propiedades del objeto de destino se sobrescriben con las propiedades de los orígenes si tienen la misma clave. Las propiedades de las fuentes posteriores sobrescriben a las anteriores.

#### [Object.create()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/create#specifications)

El método Object.create () crea un nuevo objeto, utilizando un objeto existente como prototipo del objeto recién creado.

#### [La diferencia entre Object.create y Object.assign](https://www.codecalls.com/javascript/difference-between-object-create-and-object-assign/)

La principal diferencia entre **Object.create()** y **Object.assign()** es que el método **Object.assign** crea un nuevo **Object**. utiliza el objeto proporcionado como prototipo del Objeto recién creado. Mientras que el método **Object.assign()** copia todas las propiedades de los objetos de origen al objeto de destino, que es el primer parámetro de esta función y devuelve este Objeto de destino.

Por lo tanto, vemos que mientras **Object.create()** define propiedades en nuestro **Object** recién creado. **Object.assign()** simplemente asigna el valor de los objetos de origen de destino a nuestro Objeto de destino.

#### Código Clase

```jsx
const obj1 = {
  a: "a",
  b: "b",
  c: {
    d: "d",
    e: "e"
  }
};

// Shallow Copy con for
const obj2 = {};
for (prop in obj1) {
  obj2[prop] = obj1[prop];
}

// Metodos de Object para hacer Shallow Copy
const obj3 = Object.assign({}, obj1);
const obj4 = Object.create(obj1);
```

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign/favicon-48x48.97046865.png)Object.assign() - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign)

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/create/favicon-48x48.97046865.png)Object.create() - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/create)

[![img](https://www.google.com/s2/favicons?domain=https://www.codecalls.com/wp-content/uploads/2019/11/cropped-android-chrome-512x512-1-32x32.png?x76789)The Difference Between Object.create and Object.assign - CodeCalls](https://codecalls.com/javascript/difference-between-object-create-and-object-assign)

## Qué es JSON.parse y JSON.stringify

Código Clase

```js
const obj1 = {
  a: "a",
  b: "b",
  c: {
    d: "d",
    e: "e"
  },
  editA() {
    this.a = "Abcd";
  }
};

const stringifiedComplexObj = JSON.stringify(obj1);
// "{\"a\":\"a\",\"b\":\"b\",\"c\":{\"d\":\"d\",\"e\":\"e\"},\"f\":[1,\"2\",3]}"

const obj2 = JSON.parse(stringifiedComplexObj);
// {a: "a", b: "b", c: {d: "d", e: "e"}}
```

## JSON.stringify()

El método JSON.stringify() convierte un objeto o valor de JavaScript en una cadena JSON, reemplazando opcionalmente valores si se especifica una función de reemplazo u opcionalmente incluyendo solo las propiedades especificadas si se especifica una matriz de reemplazo.

- Descripción
  - Los objetos Boolean, Number, and String se convierten a sus valores primitivos, de acuerdo con la conversión semántica tradicional.
  - Si durante la conversión se encuentra un undefined, una Function, o un Symbol se omite (cuando se encuentra en un objeto) o se censura a null (cuando se encuentra en un array). JSON.stringify() puede devolver undefined cuando se pasan valores “puros” como JSON.stringify(function(){}) o JSON.stringify(undefined).
  - Todas las propiedades que utilicen Symbol en los nombres de la clave se ignoran por completo, incluso si utilizan una función replacer.
  - Las instancias de Date implementan la función toJSON() devolviendo una cadena de texto (igual que date.toISOString()). Por lo que son tratadas como strings.
  - Los números Infinity y NaN, así como el valor null, se consideran null.
    El resto de instancias de Object (incluyendo Map, Set, WeakMap, y WeakSet) sólo tendrán serializadas sus propiedades enumerables.

JSON.stringify () convierte un valor en notación JSON que lo representa:

## JSON.parse()

El método JSON.parse() analiza una cadena de texto (string) como JSON, transformando opcionalmente el valor producido por el análisis.
Why JSON.parse(JSON.stringify()) is a bad practice to clone an object in JavaScript

- Puedes perder tipos de datos.
- JavaScript no te avisara cuando pierdas algún tipo de dato al usar JSON.stringify(), asi que GG mi rey
- Convierte tipos de datos no soportados en soportados, como infinity y NaN en null
- Los tipos de datos Date serán parseados como strings, no como Date
- No es tan rápido y eficiente.

---

```jsx
const obj1 = {
  a: "a",
  b: "b",
  c: {
    d: "d",
    e: "e"
  },
  editA() {
    this.a = "Abcd";
  }
};

const stringifiedComplexObj = JSON.stringify(obj1);
// "{\"a\":\"a\",\"b\":\"b\",\"c\":{\"d\":\"d\",\"e\":\"e\"},\"f\":[1,\"2\",3]}"

const obj2 = JSON.parse(stringifiedComplexObj);
// {a: "a", b: "b", c: {d: "d", e: "e"}}
```

#### [JSON.stringify()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify)

El método JSON.stringify() convierte un objeto o valor de JavaScript en una cadena JSON, reemplazando opcionalmente valores si se especifica una función de reemplazo u opcionalmente incluyendo solo las propiedades especificadas si se especifica una matriz de reemplazo.

- **Descripción**
  - Los objetos **Boolean**, **Number**, and **String** se convierten a sus valores primitivos, de acuerdo con la conversión semántica tradicional.
  - Si durante la conversión se encuentra un **undefined**, una **Function**, o un **Symbol** se omite (cuando se encuentra en un objeto) o se censura a **null** (cuando se encuentra en un array). **JSON.stringify()** puede devolver **undefined** cuando se pasan valores “puros” como **JSON.stringify(function(){}**) o **JSON.stringify(undefined)**.
  - Todas las propiedades que utilicen **Symbol** en los nombres de la clave se ignoran por completo, incluso si utilizan una función **replacer**.
  - Las instancias de **Date** implementan la función **toJSON()** devolviendo una cadena de texto (igual que **date.toISOString()**). Por lo que son tratadas como strings.
  - Los números **Infinity** y **NaN**, así como el valor **null**, se consideran **null**.
  - El resto de instancias de **Object** (incluyendo **Map**, **Set**, **WeakMap**, y **WeakSet**) sólo tendrán serializadas sus propiedades enumerables.

JSON.stringify () convierte un valor en notación JSON que lo representa:

#### [JSON.parse()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/parse)

El método JSON.parse() analiza una cadena de texto (string) como JSON, transformando opcionalmente el valor producido por el análisis.

#### [Why JSON.parse(JSON.stringify()) is a bad practice to clone an object in JavaScript](https://medium.com/@pmzubar/why-json-parse-json-stringify-is-a-bad-practice-to-clone-an-object-in-javascript-b28ac5e36521)

- Puedes perder tipos de datos.
- JavaScript no te avisara cuando pierdas algún tipo de dato al usar JSON.stringify(), asi que GG mi rey
- Convierte tipos de datos no soportados en soportados, como **`infinity`** y **`NaN`** en **`null`**
- Los tipos de datos **`Date`** serán parseados como **`strings`,** no como **`Date`**
- No es tan rápido y eficiente.

# 4. Recursividad en JavaScript

## Qué es recursividad

### ¿Por qué escribir programas recursivos?

- Son mas cercanos a la descripción matemática.
- Generalmente mas fáciles de analizar
- Se adaptan mejor a las estructuras de datos recursivas.
- Los algoritmos recursivos ofrecen soluciones estructuradas, modulares y elegantemente simples.

### Factible de utilizar recursividad

- Para simplificar el código.
- Cuando la estructura de datos es recursiva
  ejemplo : árboles.

### No factible utilizar recursividad

- Cuando los métodos usen arreglos largos.

- Cuando el método cambia de manera impredecible de campos.

- Cuando las iteraciones sean la mejor opción.

- Conclusiones

  - La recursividad consume mucha memoria y tiempo de ejecución.
  - La recursividad puede dar lugar a la redundancia (resolver el mismo problema más de una vez)
  - A veces es más sencillo encontrar una solución recursiva que una iterativa

  > La recursividad es muy bonita para desarrollar el pensamiento lógico y computacional.

  #### Qué es recursividad

  Es cuando una función se llama a sí misma. Tenemos que tener un caso base para que esta función se detenga.

  Ejemplo básico con TypeScript:

  ```typescript
  function recursiva(){
    if(/*validacion*/){
      // llamados recursivos
    } esle {
      // llamados normales, sin recursividad
    }
  }
  ```

```js
function recursiva(numerito: number): number {
  console.log(numerito);
  if (numerito < 5) {
    return recursiva(numerito + 1);
  } else {
    return 5;
  }
}

recursiva(0);

function recursiva(numbersArray: number[] | string[]): void {
  if (numbersArray.length !== 0) {
    const firstNum = numbersArray[0];
    console.log(`first number: ${firstNum}`);
    numbersArray.shift(); //\* elimina el primer elemento del array
    recursiva(numbersArray);
  }
}

recursiva([9, 8, 7, 6, 5, 4, 3, 2, 1]);
recursiva(["😎", "🤩", "😜", "🤑"]);
```

[Recursividad](https://infseg.com/informatica/recursividad-cuando-debo-utilizarla/)

[Recursividad.pdf](https://www.uv.mx/personal/ocastillo/files/2011/04/Recursividad.pdf)

[![img](https://www.google.com/s2/favicons?domain=//static.platzi.com/media/favicons/platzi_favicon.png)¿Qué es memoization? Técnicas de optimización en programación funcional](https://platzi.com/clases/2118-react-hooks/33472-que-es-memoization-tecnicas-de-optimizacion-en-pro/)

## Deep copy con recursividad

Funcion bucle compleja pero en recursividad.

```js
let array = [1, 23, 41, 52, 42, 5, 656, 6, 98];
let numeroArray = 0;

let fun = numerito => {
  if (numerito < array.length) {
    let valorArray = array[numerito];
    console.log(valorArray);
    return fun(numerito + 1);
  } else {
    console.log("fin");
  }
};

let y = fun(numeroArray);
```

---

**Deep copy**

Utilizaresmos recursividad para crear copia de los objetos y que no se rompan si tenemos objetso dentro de objetos dentro de objetos, o si tenemos metodos dentro de nuestros objetos.

Bien, deep copy en muchos aspectos es un gran algoritmo con manejo de datos, recursividad, etc etc, por lo tanto ire escribiendo fracciones de codigo y explicandolas, y cua do termine de explicar todo, pondre el algoritmo completo.

```jsx
const obj1 = {
  a: "a",
  b: "b",
  c: {
    d: "d",
    e: "e"
  },
  editA() {
    this.a = "AAAAA";
  }
};
//lo de aca arriba es el objeto a copiar

function isObject(subject) {
  return typeof subject == "object";
}
function esUnArray(subject) {
  return Array.isArray(subject);
}
//estas 2 funciones son funciones de validacion de datos, estan seran llamads y se les pasaran un parametro, la mayoria de datos se puede validar con typeof, ergo, los arrays son los unicos que tienen un metodo espacial = Array.isArray(objetoAsaberSiEsUnArray)

function deepCopy(subject) {
  let copySubject;
  //dentro de sta funcion sucedera todo,el copysubject guardara los datos, este esta esperando a saber si los datos son objetos,arrays u otras cosas como strings
  const subjectIsArray = esUnArray(subject);
  const subjectIsObject = isObject(subject);

  //con las constantes subjectIsArray,   subjectIsObject trabajere los datos,  estas son las encargadas de llamar a  las funciones que hicimos fuera de la funcion deepCopy.

  if (subjectIsArray) {
    copySubject = [];
  } else if (subjectIsObject) {
    copySubject = {};
  } else {
    return subject;
  }

  //por ultimo empezamos a trabajar con los datos ya validados, segun el dato que sea correspondiente, trbajaremos objeto,arrays u otros valores.
  //2da parte del algoritmo
  for (key in subject) {
    //Creamos un bucle for, este bucle (a in b)se puede ejecutar en una estructura de datos como arrays, objetos. Este loop signfica que el elemento a pasara por TODA la estructura de datos de b, y claro, dependieno la posicion de a,este tendra el valor de donde este parado encima. ejemplo:
    //let array = [52,42,56];
    //for(a in array) {
    //console.log(array[a]);
    //}
    const keyIsObject = isObject(subject[key]);
    //con keyIsObject VUELVO a validar si los datos DENTRO de la estructura de datos YA VALIDADA son objetos o datos.

    if (keyIsObject) {
      copySubject[key] = deepCopy(subject[key]);
      // si resulta que son objetos, entonces iremos copiando y pegando los datos en copySubject, y estos datos se iran copiando de manera identica y exitosa gracias la recursividad deepCopy(subject[key]);
    } else {
      if (subjectIsArray) {
        copySubject.push(subject[key]);
      } else {
        copySubject[key] = subject[key];
      }
    }
  }
  //estos 2 ultimos casos son mas sencillos ya que simplemente se basa en arrays u elementos que no sean ni arrays ni objetos

  return copySubject;
  // Y al final de todo, la funcion debe devolver algo,verdad? en este caso, quien es el que almaceno todos los datos de el objeto que copiamos? el copySubject, bien, ese es quien retornamos.
}
```

[![img](https://www.google.com/s2/favicons?domain=https://miro.medium.com/fit/c/152/152/1*sHhtYhaCe2Uc3IU0IgKwIQ.png)Deep and Shallow Copy in JavaScript | by Mayank Gupta | TechnoFunnel | Medium](https://medium.com/technofunnel/deep-and-shallow-copy-in-javascript-110f395330c5)

# 5. Abstracción y encapsulamiento sin prototipos

## Abstracción con objetos literales y deep copy

![img](https://i.imgur.com/39rqHLf.png)

#### [**Object.isSealed()**](https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/Object/isSealed)

El método **Object.isSealed()** si el objeto está sellado.
Devuelve true si el objeto está sellado, de lo contrario devuelve false. Un objeto está sellado si no es extensible y si todas sus propiedades no se pueden configurar y por lo tanto no removibles (pero no necesariamente no modificables).

#### [**Object.isFrozen()**](https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/Object/isFrozen)

El método **Object.isFrozen()** determina si un objeto está congelado.
Devuelve true si el objeto está sellado, de lo contrario devuelve false. Un objeto está sellado si no es extensible y si todas sus propiedades no se pueden configurar y por lo tanto no removibles (pero no necesariamente no modificables).
Un objeto está congelado si y solo si no es extendible, todas sus propiedades son no-configurables, y todos los datos de sus propiedades no tienen capacidad de escritura.

```js
Código Clase
/* Codigo para hacer un DeepCopy */
const studentBase = {
    name: undefined,
    email: undefined,
    age: undefined,
    approvedCourse: undefined,
    learningPaths: undefined,
    socialMedia: {
        twitter: undefined,
        instagram: undefined,
        facebook: undefined,
    }
};

const carlos = deepCopy(studentBase);
// Object.defineProperty(carlos, "name", {
//     value: "Carlitos",
//     configurable: false,
// });
Object.seal(carlos); // Es lo mismo que lo de arriba pero mas easy
carlos.name = "Carlitos";

Object.isSealed(carlos); // Nos muestra con True o False si todas las propiedades estan selladas
Object.isFrozen(carlos); // Nos muestra con True o False si todas las propiedades estan congeladas
```

## Factory pattern y RORO

**RORO** = Recibir un Objecto, y Retornamos otro Objecto.
Dentro de una función, recibimos un solo parámetro que es un objecto, el cual internamente contiene varias propiedades, con las cuales retornara otro nuevo objeto.

**RORO** es un patrón de diseño que consiste en crear una función que devuelve un objeto.
**RO** -> Recibir un objeto
**RO** -> Retornar un objeto

#### Código Clase

```jsx
/* Codigo Deep Copy */

// Requerimientos de parametros obligatorios
function requiredParam(param) {
  throw new Error(param + " Campo obligatorio");
}

// Fabrica de estudiantes
function createStudent({
  name = requiredParam("name"),
  email = requiredParam("email"),
  age,
  twitter,
  instagram,
  facebook,
  approvedCourses = [],
  learningPaths = []
} = {}) {
  return {
    name,
    age,
    email,
    approvedCourses,
    learningPaths,
    socialMedia: {
      twitter,
      instagram,
      facebook
    }
  };
}

const carlos = createStudent({
  name: "Carlito",
  age: 20,
  email: "carlito@mazzarolito.com",
  twitter: "carlitosmzz"
}); // {}
```

[![img](https://www.google.com/s2/favicons?domain=https://www.freecodecamp.org/news/elegant-patterns-in-modern-javascript-roro-be01e7669cbd//news/favicon.png)Elegant patterns in modern JavaScript: RORO](https://www.freecodecamp.org/news/elegant-patterns-in-modern-javascript-roro-be01e7669cbd/)

## Module pattern y namespaces: propiedades privadas en JavaScript

Definir o modificar varias propiedades a la vez 👀. Con la función Object.defineProperties() se puede pasar un objeto con las propiedades de cada key que quieren ser modificadas:

```javascript
// Con defineProperty
Object.defineProperty(public, "readName", {
  writable: false,
  configurable: false
});
Object.defineProperty(public, "changeName", {
  writable: false,
  configurable: false
});

// Con defineProperties
Object.defineProperties(public, {
  readName: {
    configurable: false,
    writable: false
  },
  changeName: {
    configurable: false,
    writable: false
  }
});
```

JavaScript no es un lenguaje fuertemente tipado, osea, que no tenemos que definir el tipo de nuestras variables. JavaScript entenderá que tipo de variable estamos usando.

En javascript no hay una palabra clave para definir una variable privada por lo que podemos crear una función que nos permita crear esta variable.

Por lo general, podemos usar el Object.defineProperty() para crear la variable publica o privada

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures/favicon-48x48.97046865.png)Closures - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures)

[![img](https://www.google.com/s2/favicons?domain=https://miro.medium.com/fit/c/152/152/1*sHhtYhaCe2Uc3IU0IgKwIQ.png)Module Pattern in JavaScript. JavaScript Developers need to… | by Mayank Gupta | JavaScript in Plain English](https://medium.com/technofunnel/data-hiding-with-javascript-module-pattern-62b71520bddd)

[![img](https://www.google.com/s2/favicons?domain=https://cdn.sstatic.net/Sites/stackoverflow/Img/favicon.ico?v=ec617d715196)How do I declare a namespace in JavaScript? - Stack Overflow](https://stackoverflow.com/questions/881515/how-do-i-declare-a-namespace-in-javascript)

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors/Cant_redefine_property/favicon-48x48.97046865.png)TypeError: can't redefine non-configurable property "x" - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors/Cant_redefine_property)

## Getters y setters

Los Getters y setters son métodos de acceso, lo que significa que generalmente son una interfaz publica para cambiar miembros de las clases privadas.

#### Getters y setters

```javascript
get name() {
  return private["_name"];
},
set name(newName) {
  newName.length !==0 ? private["_name"] = newName : console.warn("Name can't be empty");
}
```

La [**sintaxis**](https://platzi.com/clases/2419-javascript-poo-intermedio/39818-getters-y-setters/GETTERS) get vincula una propiedad de objeto a una función que se llamará cuando se busque esa propiedad.

La [sintaxis](https://platzi.com/clases/2419-javascript-poo-intermedio/39818-getters-y-setters/SETTERS) set vincula una propiedad de objeto a una función que se llamará cuando se intente establecer esa propiedad.

Utilizamos el método estático del prototipo object getOwnPropertyDescriptor().
Una función obtiene un valor de una propiedad se le llama getter y una que setea(establece) un valor de una propiedad se le llama setter
Para crearlos simplemente necesitan los keywords get y set

```js
const obj = {
    get prop(){
        return this.__prop__;
    },
    set prop() {
        this.__prop__ = value * 2 ;
    },
};
obj.prop = 12;

console.log(obj.prop); // 24
```

Se crea un objeto, con una única propiedad, tiene un getter y un setter. De esta manera cada vz que le establezcamos un alor a la propiedad, el valor se multiplicará por

2. se puede crear un accessor properties es e manera explćita usando

```js
Object.defineProperty()
Object.defineProperty(obj,//objeto target
'prop', //nombre de la propiedad)
 {
    enumerable:true,
    configurable:true,
    get prop(){
        return this.__prop__;
    },
    set prop(value){
        this.__prop__ = value * 2;
    },
};

obj.prop = 12;
var atr = Object.getOwnProperyDescriptor(obj,'prop');
console.log(atr); // {value: 24, writable: true, enumerable: true, configurable: true}


```

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/get/favicon-48x48.97046865.png)getter - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/get)

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/set/favicon-48x48.97046865.png)setter - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/set)

# 6. Cómo identificar objetos

## Qué es duck typing

> El duck typing es la forma de progamar donde identificamos a nuestros elementos dependiendo de los métodos y atributos que tengan por dentro.
>
> La realidad son las cosas que podemos percibir con nuestros sentidos en el presente, ya que lo pasado puede haber dejado de ser real y lo futuro es desconocido.

**Duck Typing** is a type system used in dynamic languages. For example, Python, Perl, Ruby, PHP, Javascript, etc. where the type or the class of an object is less important than the method it defines. Using Duck Typing, we do not check types at all. Instead, we check for the presence of a given method or attribute.

The name Duck Typing comes from the phrase:

> “If it looks like a duck and quacks like a duck, it’s a duck”

Es un término utilizado por los lenguajes dinámicos que no tienen un tipado fuerte.
La idea se que no necesita un type para invocar un método existente en un objeto; si se define un método, puede invocarlo.
El nombre proviene de la frase:

"Si camina como pato y patea como pato, entonces es un pato"
En otras palabras, tiene que cumplir con ciertos métodos y atributos para considerarse alguna cosa

si no cumple los requisitos, significa que no es el mismo objeto.

## Duck typing en JavaScript

Se trata de una estimación subjetiva de si el objeto es de un tipo u otro según las propiedades y métodos que lo compongan.

Sin embargo, en Javascript, puede ser una herramienta útil para determinar tanto el tipo de datos de un objeto concreto hasta para facilitar una pseudo implementación de interfaces cuando programamos en POO puro.

```js
function isObject(subject) {
  return typeof subject == "object";
}

function isArray(subject) {
  return Array.isArray(subject);
}

function deepCopy(subject) {
  let copySubject;

  const subjectIsObject = isObject(subject);
  const subjectIsArray = isArray(subject);

  if (subjectIsArray) {
    copySubject = [];
  } else if (subjectIsObject) {
    copySubject = {};
  } else {
    return subject;
  }

  for (key in subject) {
    const keyIsObject = isObject(subject[key]);

    if (keyIsObject) {
      copySubject[key] = deepCopy(subject[key]);
    } else {
      if (subjectIsArray) {
        copySubject.push(subject[key]);
      } else {
        copySubject[key] = subject[key];
      }
    }
  }

  return copySubject;
}

function requiredParam(param) {
  throw new Error(param + " es obligatorio");
}

function createLearningPath({ name = requiredParam("name"), courses = [] }) {
  const private = {
    _name: name,
    _courses: courses
  };

  const public = {
    get name() {
      return private["_name"];
    },
    set name(newName) {
      if (newName.length != 0) {
        private["_name"] = newName;
      } else {
        console.warn("Tu nombre debe tener al menos 1 caracter");
      }
    },
    get courses() {
      return private["_courses"];
    }
  };

  return public;
}

function createStudent({
  name = requiredParam("name"),
  email = requiredParam("email"),
  age,
  twitter,
  instagram,
  facebook,
  approvedCourses = [],
  learningPaths = []
} = {}) {
  const private = {
    _name: name,
    _learningPaths: learningPaths
  };

  const public = {
    email,
    age,
    approvedCourses,
    socialMedia: {
      twitter,
      instagram,
      facebook
    },
    get name() {
      return private["_name"];
    },
    set name(newName) {
      if (newName.length != 0) {
        private["_name"] = newName;
      } else {
        console.warn("Tu nombre debe tener al menos 1 caracter");
      }
    },
    get learningPaths() {
      return private["_learningPaths"];
    },
    set learningPaths(newLP) {
      if (!newLP.name) {
        console.warn("Tu LP no tiene la propiedad name");
        return;
      }

      if (!newLP.courses) {
        console.warn("Tu LP no tiene courses");
        return;
      }

      if (!isArray(newLP.courses)) {
        console.warn("Tu LP no es una lista (*de cursos)");
        return;
      }

      private["_learningPaths"].push(newLP);
    }
  };

  return public;
}

const juan = createStudent({ email: "juanito@frijoles.co", name: "Juanito" });
```

## Instance Of en JavaScript con instancias y prototipos

```js
function isObject(subject) {
  return typeof subject == "object";
}

function isArray(subject) {
  return Array.isArray(subject);
}

function deepCopy(subject) {
  let copySubject;

  const subjectIsObject = isObject(subject);
  const subjectIsArray = isArray(subject);

  if (subjectIsArray) {
    copySubject = [];
  } else if (subjectIsObject) {
    copySubject = {};
  } else {
    return subject;
  }

  for (key in subject) {
    const keyIsObject = isObject(subject[key]);

    if (keyIsObject) {
      copySubject[key] = deepCopy(subject[key]);
    } else {
      if (subjectIsArray) {
        copySubject.push(subject[key]);
      } else {
        copySubject[key] = subject[key];
      }
    }
  }

  return copySubject;
}

function requiredParam(param) {
  throw new Error(param + " es obligatorio");
}

function LearningPath({ name = requiredParam("name"), courses = [] }) {
  this.name = name;
  this.courses = courses;

  // const private = {
  //   "_name": name,
  //   "_courses": courses,
  // };

  // const public = {
  //   get name() {
  //     return private["_name"];
  //   },
  //   set name(newName) {
  //     if (newName.length != 0) {
  //       private["_name"] = newName;
  //     } else {
  //       console.warn("Tu nombre debe tener al menos 1 caracter");
  //     }
  //   },
  //   get courses() {
  //     return private["_courses"];
  //   },
  // };

  // return public;
}

function Student({
  name = requiredParam("name"),
  email = requiredParam("email"),
  age,
  twitter,
  instagram,
  facebook,
  approvedCourses = [],
  learningPaths = []
} = {}) {
  this.name = name;
  this.email = email;
  this.age = age;
  this.approvedCourses = approvedCourses;
  this.socialMedia = {
    twitter,
    instagram,
    facebook
  };

  if (isArray(learningPaths)) {
    this.learningPaths = [];

    for (learningPathIndex in learningPaths) {
      if (learningPaths[learningPathIndex] instanceof LearningPath) {
        this.learningPaths.push(learningPaths[learningPathIndex]);
      }
    }
  }

  // const private = {
  //   "_name": name,
  //   "_learningPaths": learningPaths,
  // };

  // const public = {
  //   email,
  //   age,
  //   approvedCourses,
  //   socialMedia: {
  //     twitter,
  //     instagram,
  //     facebook,
  //   },
  //   get name() {
  //     return private["_name"];
  //   },
  //   set name(newName) {
  //     if (newName.length != 0) {
  //       private["_name"] = newName;
  //     } else {
  //       console.warn("Tu nombre debe tener al menos 1 caracter");
  //     }
  //   },
  //   get learningPaths() {
  //     return private["_learningPaths"];
  //   },
  //   set learningPaths(newLP) {
  //     if (!newLP.name) {
  //       console.warn("Tu LP no tiene la propiedad name");
  //       return;
  //     }

  //     if (!newLP.courses) {
  //       console.warn("Tu LP no tiene courses");
  //       return;
  //     }

  //     if (!isArray(newLP.courses)) {
  //       console.warn("Tu LP no es una lista (*de cursos)");
  //       return;
  //     }

  //     private["_learningPaths"].push(newLP);
  //   },
  // };

  // return public;
}

const escuelaWeb = new LearningPath({ name: "Escuela de WebDev" });
const escuelaData = new LearningPath({ name: "Escuela de Data Science" });
const juan = new Student({
  email: "juanito@frijoles.co",
  name: "Juanito",
  learningPaths: [escuelaWeb, escuelaData]
});
```

También pudimos haber usado el for ... of en lugar del for ... in para no hacer el código tan verboso.

For … in devuelve los indices o las llaves del array u objeto que esta siendo iterado.
For … of devuelve ya directamente los valores.

Personalmente, por esa misma razón casi siempre me gusta dejar exclusivamente el for ... in para objetos y el for ... of para arrays.

Aun asi, al final va a depender del resultado que queramos obtener.

![carbon (7).png](https://static.platzi.com/media/user_upload/carbon%20%287%29-ffafc572-c5f5-4352-bccc-5806dc3ce8b5.jpg)

El operador instanceof verifica si un objeto en su cadena de prototipos contiene la propiedad prototype de un constructor

objeto isntacneof constructor

En la documentación de los recursos me percaté que existen dos maneras de usar el new.

La primera en forma de Operador.\* El cual permite a los desarrolladores crear una instancia de un tipo de objeto definido por el usuario o de uno de los tipos de objeto integrados que tiene una función constructora.

```js
function Car(make, model, year) {
  this.make = make;
  this.model = model;
  this.year = year;
}
const car1 = new Car("Honda", "Civic", "2000");

console.log(car1.make); // Honda

// sintaxis

new constructor[[arguments]]();
```

. Una clase o función que especifica el tipo de instancia del objeto.
arguments. Una lista de valores con los que se llamará al constructor.
La palabra new hace lo siguiente:

Crea un objeto avascript simple y en blanco;
vincula (establee el constructor de) este objeto a otro bojeto;
Pasa el objeto recién creado del paso 1 como contexto this;
Devuelve this si la función no devuelve un objeto.
La creación de un objeto definido por el usuario requiere dos pasos:

Defina el tipo de objto escribiendo una función
Crea una instancia de objeto new

new.target. Esta propidad nos permite detectar si una función o constructor fue llamado usando el operador new. En constructores y funciones instanciadas con el operador new, new.target devuelve una referencia al constructor o función. En llamadas a funciones normales, new.target es undefined

## Atributos y métodos privados en prototipos

Para asignar los métodos y atributos privados en Clases (ahora en ES21) seran con un # El propio JavaScript aplica la encapsulación de privacidad de estas características de clase.

```js
class ClassWithPrivateField {
  #privateField;
}

Ahora vamos a ver un Ejemplo
Metodos Privados

Vamos a crear una clase llamada People y vamos a tener varios metodos.

class People {
  showName() {
    console.log("My name is David")
  }
  showAge() {
    console.log("David is 21")
  }
}

Para acceder a los metodos dentro de las clases, primero necesitamos instanciar la clase.
class People {
  showName() {
    console.log("My name is David")
  }
  showAge() {
    console.log("David is 21")
  }
}

const people = new People()

people.showName()
people.showAge()
```

Podemos ver My name is David y David is 21 en la consola.
Si queremos hacer showAge(), un método privado dentro de la clase People, por lo que fuera del alcance de la clase no es accesible.
Simplemente agregamos # a showAge() algo asi #showAge()
class People {

```js
 showName() {
    console.log("My name is David")
  }
  #showAge() {
    console.log("David is 21")
  }
}

const people = new People()

people.showName()
people.showAge()
```

Podemos ver el resultado en nuestra consola. Un error es decir people.showAge que no es una función. Esto se debe a #showAge() que ahora es un método privado dentro de la clase People y solo se puede acceder a través de un método público dentro de la clase People.

## Creando métodos estáticos en JavaScript

Reto:

```js
static isObject(subject) {
    if (Array.isArray(subject)) {
      return false;
    }
    return typeof subject === "object";
  }
```

Dolución al reto.

Ademas de la comprobación del Array, agregué una comprobación para null, ya que existe un bug en ECMAScript que indica que null es un tipo objeto.

```js
typeof null; // object
```

Y para evitar errores a futuro hice esto.

```js
SuperObject.isObject = function(subject) {
  if (subject === null || Array.isArray(subject)) return false;
  return typeof subject === "object";
};
```

# 7. Próximos pasos

## ¿Quieres más cursos de POO en JavaScript?

Nunca pares de Aprender!
