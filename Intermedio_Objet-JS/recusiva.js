/*
function recursiva() {
  if (validation) {
    llamdos recursivos
  } else {
    llamdos normales, sin recursividad
  }
}
*/

const numeritos = [0, 1, 2, 4, 5, 76, 8, 3, 2, 34, 6, 2, 13, 421, 1234, 3];

/*
let numerito = 0;
for (let index = 0; index < numeritos.length; index++) {
  numerito = numeritos[index];
  console.log({index, numerito});
}
*/
function recursiva(numbersArray) {
  if (numbersArray.length != 0) {
    const firstNum = numbersArray[0];
    console.log(firstNum);
    numbersArray.shift();
    return recursiva(numbersArray);
  }
}
