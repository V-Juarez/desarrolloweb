// const obj1 = {
//   a: "a",
//   b: "b",
//   c: {
//     d: "d",
//     e: "e"
//   },
//   editA() {
//     this.a = "AAAAA";
//   }
// };

function isObject(subject) {
  return typeof subject == "object";
}

function isArray(subject) {
  return Array.isArray(subject);
}

function deepCopy(subject) {
  let copySubject;

  const subjectIsObject = isObject(subject);
  const subjectIsArray = isArray(subject);

  if (subjectIsArray) {
    copySubject = [];
  } else if (subjectIsObject) {
    copySubject = {};
  } else {
    return subject;
  }

  for (key in subject) {
    const keyIsObject = isObject(subject[key]);

    if (keyIsObject) {
      // copySubject[key] = deepCopy(subject[key]);
    } else {
      if (subjectIsArray) {
        copySubject.push(subject[key]);
      } else {
        copySubject[key] = subject[key];
      }
    }
  }

  return copySubject;
}

// const studentBase = {
//   name: undefined,
//   email: undefined,
//   age: undefined,
//   aprovedCourses: undefined,
//   learningPaths: undefined,
//   socialMedia: {
//     twitter: undefined,
//     instragram: undefined,
//     facebook: undefined
//   }
// };

// const juan = deepCopy(studentBase);
// Object.seal(juan);
// juan.name = "Juanito";
// Object.isSealed(juan);
// Object.isFrozen(juan);

// Object.defineProperty(juan, "name", {
//   value: "Juanito",
//   configurable: false,
// })

function requiredParam(param) {
  throw new Error(param + " es obligatorio");
}

function createLearnigPath({
  name = requiredParam("name"),
  courses = [],
}) {
  const private = {
    "_name": name,
    "_courses": courses,
  };

  const public = {
    get name() {
      return private["_name"];
    },

    set name(newName){
      if (newName.length != 0){
        private["_name"] = newName;
      } else {
        console.warn("Tu nombre debe tener al menos 1 caracter");
      }
    },

    get courses() {
      return private["_courses"];
    },
  };

  return public;
}

function createStudent({
  name = requiredParam('name'),
  email = requiredParam('email'),
  age,
  Twitter,
  facebook,
  instagram,
  approvedCourses = [],
  learningPaths = [],
} = {}) {
  const private = {
    "_name": name,
    "_learningPaths": learningPaths,

  };

  const public = {
    age,
    socialMedia: {
      twitter,
      instagran,
      facebook,
    },
    // readName() {
    //   return private["_name"];
    // },

    // changeName(newName) {
    //   private["name"] = newName;
    // },

    // getter y setter
    get name() {
      return private["_name"];
    },

    set name(newName) {
      if (newName.length != 0) {
        private["_name"] = newName;
      } else {
        console.warn("Tu nombre debe tener al menos 1 caracter");
      }
    },
    
    get learningPaths() {
      return private["_learningPaths"];
    },
    set learningPaths(newLP) {
      if (!newLP.name) {
        console.warn("Tu LP no tiene la propiedad name");
        return;
      }
    };

    if (!isArray(newLP.courses)) {
      console.warn("Tu LP no es una lista (*de cursos)");
      return;
    },
      
      private["_learningPaths"].push(newLP);
  }

  return public;
  // Object.defineProperty(public, "readName", {
  //   writable: false,
  //   configurable: false,
  // });

  // return public;
};

// const juan = createStudent({
//   name: "Juanito",
//   age: 20,
//   email: "juanito@frijolitos.com",
//   twitter: "fjuandc",
// });

const juan = createStudent({ email: 'juanito@frijoles.co', name: 'Juanito' });