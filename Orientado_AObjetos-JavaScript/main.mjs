function videoPlay(id) {
  const urlSecreta = "https://holadesdedondequieras.com" + id;
  console.log("Reproduciendo desde la urla " + urlSecreta)
}


function videoStop(id) {
  console.log("Pausamos la url " + urlSecreta)
}

export class PlatziClass {

  constructor({
    name,
    videoID,
  }) {
    this.name = name;
    this.videoID = videoID;
  }

  reproducir() {
    videoPlay(this.videoID);
  }

  pausar() {
    videoStop(this.videoID);
  }
}





class Course {

  constructor({
    name,
    classes = [],
  }) {
    this._name = name;
    this.courses = courses;
  }

  get name() {
    return this._name;
  }


  set name(nuevoNombrecito) {
    if (nuevoNombrecito === "Curso Malito de Progamacion Basica") {
      console.error("web... no");
    } else {
      this._name = nuevoNombrecito;
    }
  }

  // changeName(nuevoNombrecito) {
  //   this._name = nuevoNombrecito
  // }
}

const cursoProgBasica = new Course({
  name: "Curso Gratis de Programacion Basica",
});

cursoProgBasica.name


const cursoDefinitivoHTML = new Course({
  name: "Curso Definitivo de HTML y CSS",
});

const cursoPracticoHTML = new Course({
  name: "Curso Practico de HTML Y CSS",
});

class LearningPath {
  constructor({
    name,
    courses = [],
  }) {
    this.name = name;
    this.courses = courses;
  }
}

const escuelaWeb = new LearningPath({
  name: "Escuela de Desarrollo Web",
  courses: [
    cursoJS,
    cursoReact,
    cursoNextJS,
  ],
});

const escuelaData = new LearningPath({
  name: "Escuela de Data Science",
  courses: [
    cursoPython,
    CursoMatematicas,
    CursoExcel,
  ],
});
class Student {
  constructor({
    name,
    email,
    username,
    twitter = undefined,
    instagram = undefined,
    facebook = undefined,
    approvedCourses = [],
    learningPaths = [],
  }) {
    this.name = name;
    this.email = email;
    this.username = username;
    this.socialMedia = {
      twitter,
      instagram,
      facebook,
    }
    this.approvedCourses = approvedCourses;
    this.learningPaths  = learningPaths;
  }
}

const juan2 = new Student({
  name: "JuandDC",
  username: "juandc",
  email: "juanito@juanito.com",
  twitter: "fjuandc",
});

const juan2 = new Student({
  name: "Miguelito",
  username: "miguelitofeliz",
  email: "miguel_@miguelito.com",
  instagram: "miguelito",
});