<h1>Programación Orientada a Objetos con JavaScript</h1>

<h3>Juan David Castro</h3>

<h1>Tabla de Contenido</h1>

- [1. JavaScript Orientado a Objetos](#1-javascript-orientado-a-objetos)
  - [JavaScript: orientado a objetos, basado en prototipos](#javascript-orientado-a-objetos-basado-en-prototipos)
  - [Qué es programación orientada a objetos](#qué-es-programación-orientada-a-objetos)
  - [Bonus: fútbol y programación orientada a objetos](#bonus-fútbol-y-programación-orientada-a-objetos)
- [2. Objetos, clases y prototipos](#2-objetos-clases-y-prototipos)
  - [Qué es un objeto en JavaScript](#qué-es-un-objeto-en-javascript)
  - [Objetos literales y prototipos en JavaScript](#objetos-literales-y-prototipos-en-javascript)
  - [Clases en JavaScript](#clases-en-javascript)
  - [Ventajas de la programación orientada a objetos](#ventajas-de-la-programación-orientada-a-objetos)
- [3. Pilares de la POO: abstracción y encapsulamiento](#3-pilares-de-la-poo-abstracción-y-encapsulamiento)
  - [Qué es abstracción](#qué-es-abstracción)
  - [Abstracción en JavaScript](#abstracción-en-javascript)
  - [Qué es encapsulamiento](#qué-es-encapsulamiento)
  - [Getters y setters en JavaScript](#getters-y-setters-en-javascript)
  - [Módulos de ECMAScript 6](#módulos-de-ecmascript-6)
- [4. Pilares de la POO: herencia y polimorfismo](#4-pilares-de-la-poo-herencia-y-polimorfismo)
  - [Qué es herencia](#qué-es-herencia)
  - [Herencia en JavaScript](#herencia-en-javascript)
  - [Qué es polimorfismo](#qué-es-polimorfismo)
  - [Polimorfismo en JavaScript](#polimorfismo-en-javascript)
- [5. Próximos pasos](#5-próximos-pasos)
  - [¿Quieres más cursos de POO en JavaScript?](#quieres-más-cursos-de-poo-en-javascript)


# 1. JavaScript Orientado a Objetos ❤️

## JavaScript: orientado a objetos, basado en prototipos

📌 JavaScript es un lenguaje de programación orientado a objetos, aunque **basado en prototipos** porque los objetos están (internamente) construidos con prototipos.

**What is Object Oriented Programming (OOP)?**
**OOP** describes a way to write programs. This way focuses on data: stored as object properties, and actions: stored as object methods. The notion behind this way of writing code is that it more closely reflects how we think in reality. In OOP, everything is an object, and any actions we need to perform on data (logic, modifications, e.c.t) are written as methods of an object.

[curso-basico-de-programacion-orientada-a-objetos-con-javascript.pdf](https://drive.google.com/file/d/1-WnUkZzx4HQvgyZKuLbDpWPi8fdnzSpJ/view?usp=sharing)

## Qué es programación orientada a objetos

### **Programación imperativa:**

Los primeros lenguajes de programación y, por extensión, también los primeros programas informáticos, se basaban completamente en este enfoque, que prevé una secuencia regularizada de órdenes o instrucciones determinadas. Este paradigma de programación es la base, por ejemplo, de los veteranos lenguajes Pascal y C, así como de todos los lenguajes ensambladores, entre otros. En la programación imperativa, el centro de atención radica, entre otras cosas, en trabajar lo más cerca posible del sistema. Como consecuencia, el código de programación resultante es fácil de comprender y, a la vez, muy abarcable.

<img src="https://i.ibb.co/qm93617/1-imperativa.jpg" alt="1-imperativa" border="0">

### **Programacion estructurada:**

La principal modificación del principio básico radica en que, en lugar de instrucciones de salto absolutas (instrucciones que provocan que el procesamiento no continúe con la siguiente instrucción, sino en otro lugar) este paradigma de programación de software prevé el uso de bucles y estructuras de control.

<img src="https://i.ibb.co/82Sk3nG/2-estructurada.jpg" alt="2-estructurada" border="0">

### **Programación procedimental:**

El paradigma de programación procedimental amplía el enfoque imperativo con la posibilidad de desglosar algoritmos en porciones manejables. Estos se denominan como procedimientos, dependiendo del lenguaje de programación, o también como subprogramas, rutinas o funciones. El sentido y el propósito de esta distribución es hacer que el código de programa sea más claro y evitar las repeticiones innecesarias de código.



### **Programación modular:**

El código fuente se divide específicamente en bloques parciales lógicos independientes los unos de los otros para proporcionar más transparencia y facilitar el proceso de debugging (resolución de errores). Los bloques parciales individuales, denominados módulos, se pueden probar por separado antes de vincularlos posteriormente a una aplicación conjunta

<img src="https://i.ibb.co/r7tdjPf/3-modular.jpg" alt="3-modular" border="0">

### **Programación declarativa:**

Radica en la descripción del resultado final que se busca. Por lo tanto, en primera línea se encuentra el “qué” del resultado y no el “cómo” de los pasos que llevan a la solución, como es el caso en la programación imperativa. Esto provoca que el código de la programación declarativa sea más difícil de comprender debido al alto grado de abstracción, aunque resulta muy corto y preciso.

<img src="https://i.ibb.co/vkr8bss/4-declaratividad.jpg" alt="4-declaratividad" border="0">

### **Programación funcional:**

Un programa de programación funcional consta de llamadas de función concatenadas en las que cada parte del programa se interpreta como una función. En este sentido, las funciones dentro de la programación funcional pueden adoptar distintas “estructuras”. Por ejemplo, se pueden vincular entre sí como datos o se pueden utilizar en forma de parámetros. Asimismo, se pueden utilizar como resultados de función. En contraposición, el paradigma se ocupa de que no haya asignaciones independientes de valores.

<img src="https://i.ibb.co/PM4S83d/5-function.jpg" alt="5-function" border="0">

### **Programación lógica:**

El paradigma de software lógico, denominado también como programación predicativa, se basa en la lógica matemática. En lugar de una sucesión de instrucciones, un software programado según este principio contiene un conjuntode principios que se pueden entender como una recopilación de hechos y suposiciones. Todas las solicitudes al programa se procesan de forma que el intérprete recurre a estos principios y les aplica reglas definidas previamente para alcanzar el resultado deseado.

<img src="https://i.ibb.co/PZKmfxx/6-logica.jpg" alt="6-logica" border="0">

### Qué es programación orientada a objetos

La Programación Orientada a Objetos (POO, en español; OOP, según sus siglas en inglés) es **un paradigma de programación** que viene a innovar la forma de obtener resultados. Los objetos se utilizan como metáfora para emular las entidades reales del negocio a modelar.

### Origen

Los conceptos de la POO tienen origen en Simula 67, un lenguaje diseñado para hacer simulaciones, creado por Ole-Johan Dahl y Kristen Nygaard, del Centro de Cómputo Noruego en Oslo. En este centro se trabajaba en simulaciones de naves, que fueron confundidas por la explosión combinatoria de cómo las diversas cualidades de diferentes naves podían afectar unas a las otras. La idea surgió al agrupar los diversos tipos de naves en diversas clases de objetos, siendo responsable cada clase de objetos de definir sus “propios” datos y comportamientos. Fueron refinados más tarde en Smalltalk, desarrollado en Simula en Xerox PARC (cuya primera versión fue escrita sobre Basic) pero diseñado para ser un sistema completamente dinámico en el cual los objetos se podrían crear y modificar “sobre la marcha” (en tiempo de ejecución) en lugar de tener un sistema basado en programas estáticos.

### POO en JavaScript

JavaScript (abreviado comúnmente JS) es un lenguaje de programación interpretado, dialecto del estándar ECMAScript. Se define como orientado a objetos, basado en prototipos, imperativo, débilmente tipado y dinámico.

Los paradigmas son distintas maneras de programar, los más comunes son:

- Estructurado
- Orientado a objetos
- Funcional

Un paradigma **NO** es mejor que otro, depende de la situación y el contexto.

Cada lenguaje puede trabajar con 1 o varios paradigmas.

Ventajas de POO ✅:

- Orden
- Todo está conectado
- Reutilizar código -> Los moldes son las clases, que usaremos para instanciar objetos

Los objetos tienen **métodos** (funciones) y **atributos** (características).

**Paradigmas**

- Estructurado
- Orientado a Objetos ✅
- Funcional

Ventajas de POO:

1. **Encapsulamientos de datos**: Podemos agrupar datos que tienen algo en común, o que pueden ser agrupados.
2. **Reutilizacion de codigo**: Podemos reusar código repetitivo encapsulando, métodos y atributos en un objeto.

> 📌 Los **objetos** son usados para almacenar colecciones de **atributos**(datos) y **métodos**(funciones).
>
> 📌 La Programación Orientada a Objetos nos permiten tener orden, ligar información y reutilizar el código mediante las **clases ** que son un molde(una base de atributos y métodos) del cual podremos **instanciar** nuevos objetos con la misma base y una personalización.
>
> Nota. En JavaScript, la POO se basa en prototipos, no en clases.

### Paradigma

Es esa teoría que suministra la base y el modelo para resolver problemas. Hay que tener en cuenta que ningún paradigma es necesariamente mejor que otro. Solo hay que saber cual aplicar para la lógica de tu negocio. Ahora bueno…

### ¿Qué es la POO?

Resumiendo es una forma de abordar tus problemas abstrayendo los elementos en forma de objetos.

#### Algunas de sus ventajas?

Todo nuevo paradigma surge para resolver nuevas necesidades que los viejos paradigmas no nos permiten de manera sencilla (siendo justos, es para hacernos la vida mas sencilla).
Dentro de sus ventajas tenemos:

- El orden: Al separar nuestros elementos en Objetos será mas fácil mantener nuestro código y hacerlo escalable.
- Reutilización del código: DRY (Don’t Repeat Yourself), Nuestras piezas de código no deberían de repetirse ya que esto nos dificultará la evolución futura (y su mantenimiento).

### ¿Qué Elementos podemos encontrar en la POO?

Hasta ahora hemos escuchado de estos 4 ( y los seguirás escuchando muy a menudo):

- Clases: Son bloques de código que nos permiten definir métodos y atributos (base) para nuestros objetos.
- Atributos: Son las características de nuestros objetos (Altura, peso, Si es estudiante (o no)
- Métodos: Son funciones que guardamos en nuestros objetos para actualizar la información de forma segura.

Así vamos, sigamos con la siguiente clase 😄

Ah, para hacer las 10000 Galletas con nuestro nuevo molde, solamente nos tomaría menos de una semana (sin ningún tipo de descanso 😐) Para ser exactos 83.33 horas 😄

## Bonus: fútbol y programación orientada a objetos

**Conceptos**:

1. **Abstracción**: Es cuando hacemos un análisis a nuestros elementos para obtener las características que tienen en común y así empezar a definir nuestras clases.
2. **Clase**: Representación de métodos y atributos que representan a una entidad.
3. **Objeto**: Instancia de una clase.
4. **Herencia**: Crear nuevas clases con características similares a partir de otras.
5. **Encapsulamiento**: Esconder datos que solo pueden ser accedidos por la clase.
6. **Polimorfismo**: Un objeto puede comportarse de muchas formas, ejemplo: En herencia, las clases hijas pueden comportarse como su clase padre y viceversa.

Podemos tomar como ejemplo objetos de la vida real, por ejemplo un lápiz, una pluma o un borrador. Cada uno de ellos tendrá datos únicos de los cuales podremos construir sus clases

**Las clases** es la forma en que se van obtener información la información de cada objetos, a estos se les llama atributos. A partir de estas clases podemos crear objetos, a esto se le llama Instanciar de una clase.

- **Atributos**: Son las propiedades que tendrán cada uno de los objetos creados con nuestras clases, por ejemplo en la clase lápiz tiene como propiedades o atributos un color, textura, tamaño, forma de la punta, etc. Para el siguiente ejemplo tomamos una clase Usuario y le ponemos como atributo “nombre”
- **Metodos**: Son simples funciones que existen dentro de la clase que determinan las acciones, por ejemplo en el caso del borrador tiene una función de borrar, para el lápiz tiene la función de escribir al igual que la pluma. Para el siguiente ejemplo de codigo ejecutaremos un metodo para saludar

### En la POO hay 4 pilares que debemos comprender

- **Herencia**: Con la herencia puedes crear una clase que sea hijo o una copia de otra clase, al heredar una clase se tiene todos los metodos y atributos de la clase padre, podras modificarlas solo en caso de ser necesario. Funciona como una jerarquia de lo mas general a lo mas especifico.
- **Encapsulamiento**: Esta significa restringir u ocultar el acceso a los datos dentro de la misma clase al “mundo exterior”, usualmente solo se modifican en la misma clase. Existen tres clases de encapsulamiento.
- **Abstracción**: La abstracción son los datos necesarios de una clase, por ejemplo si se elabora el menu de un restaurante, es necesario el nombre del platillo y precio, otros datos como el color favorito del chef no son necesarios. Esto debe ir de lo mas general a lo especifico
- **Polimorfismo**: Es la habilidad de tener diferentes comportamientos o formas basados en que subclase se esta utilizando, relacionado a la herencia

# 2. Objetos, clases y prototipos

## Qué es un objeto en JavaScript

**¿Qué es “this” (este)?**
Es posible que hayas notado algo un poco extraño en nuestros métodos. Mira esto, por ejemplo:

```js
saludo: function() {
  alert('¡Hola!, Soy '+ this.nombre.pila + '.');
}
```

Probablemente te estés preguntando qué es “this”. La palabra clave this se refiere al objeto actual en el que se está escribiendo el código, por lo que en este caso this es equivalente a la persona. Entonces, ¿por qué no escribir persona en su lugar? Como verás en el artículo JavaScript orientado a objetos para principiantes cuando comenzaremos a crear constructores, etc., this es muy útil: siempre asegurará que se usen los valores correctos cuando cambie el contexto de un miembro (por ejemplo, dos diferentes instancias de objetos persona) pueden tener diferentes nombres, pero querráx usar su propio nombre al decir su saludo).

Vamos a ilustrar lo que queremos decir con un par de objetos persona simplificados:

```js
var persona1 = {
  nombre: 'Chris',
  saludo: function() {
    alert('¡Hola!, Soy '+ this.nombre + '.');
  }
}

var persona2 = {
  nombre: 'Brian',
  saludo: function() {
    alert('¡Hola!, Soy '+ this.nombre + '.');
  }
}
```

En este caso, persona1.saludo() mostrará “¡Hola!, Soy Chris”; persona2.saludo() por otro lado mostrará “¡Hola!, Soy Brian”, aunque el código del método es exactamente el mismo en cada caso. Como dijimos antes, this es igual al objeto en el que está el código; esto no es muy útil cuando se escriben objetos literales a mano, pero realmente se vuelve útil cuando se generan objetos dinámicamente (por ejemplo, usando constructores) Todo se aclarará más adelante.

Un objeto en JavaScript es una instancia del prototipo Object que es creado de forma “nativa” por Js… jajaja creo que esa definición empieza a estar en un ámbito “existencial”.

Un objeto en JavaScript es una colección de propiedades. Una propiedad en JavaScript es simplemente una asociación entre un nombre (llave) y un valor. Cuando este valor es una función, podemos decir que se trata de un método.

Los objetos en JavaScript, al ser un lenguaje basado en prototipos, heredan propiedades de otros objetos por medio del prototipo.

Podemos construir objetos de dos formas: De la “nada” (o también llamado objeto literal) o *clonando* un objeto ya existente. En la mayoría de lenguajes que soportan prototipos existe una clase raíz (por lo general llamada Object) que tiene las propiedades mínimas necesarias para la creación de objetos.

**Conceptos:**
**Objeto**: Instanciación de una Clase, Prototipo, o Objeto Prototipo.
**Prototipo**: Agrupación de métodos donde un objeto específico tiene acceso.
**Objeto Prototipo**: Es un objeto omnipresente preestablecido, dando por resultado que todo objeto instanciado en javascript será heredado directa o indirectamente del Objeto Prototipo y mediante el atributo `__proto__` vamos se podrá acceder a su respectivo prototipo.
En imagen podemos ver esta herencia o también llamada Prototype Chain( [ver enlace](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Object_prototypes) );

<img src="https://i.ibb.co/4KZcNJw/prototype-object.jpg" alt="prototype-object" border="0">

[Documentación oficial MDN sobre objetos en JS](https://developer.mozilla.org/es/docs/Web/JavaScript/Guide/Working_with_Objects)
**Arrays en JS**

```javascript
const students_platzirank = [
  'Juan',
  'Juanita',
  'Nath',
  'Nora',
  'Luisa'
];
```

**Array asociativo en PHP**

```php
$students_platzirank = [
  'Juan' => 100,
  'Juanita' => 300,
  'Nath' => 700,
  'Nora' => 150,
  'Luisa' => 0
];
```

**Diccionarios en Python**

```python
students_platzirank = {
  'Juan': 100,
  'Juanita': 300,
  'Nath': 700,
  'Nora': 150,
  'Luisa': 0
}
```

**Objetos literales en JS**

```javascript
const students_platzirank = {
  'Juan': 100,
  'Juanita': 300,
  'Nath': 700,
  'Nora': 150,
  'Luisa': 0
};
```

`objetos_literales !== instancias` Peeeeero sí son instancias del prototipo Object de JS 😅

**Clases y objetos en PHP**

```php
class Student {
  public $name = 'Nombre';
  public $age = 18;
  public $points = 750;
}

$juanita = new Student;
```

**Clases y objetos en Python**

```python
class Student:
  name = 'Nombre'
  age = 18
  points = 750

juanita = Student()
```

**Clases y objetos en JavaScript**

```javascript
function Student(){
  this.name = 'Nombre'
  this.age = 18
  this.points = 750
}

const juanita = new Student()
juanita.hasOwnProperty("name") //true
const objetito = {}
objetito.hasOwnProperty("name") // false
```

> De hecho, los objetos literales y lo arrays son instancias del prototipo Object y Array, respectivamente.

## Definiciones Claves

- **this**: Esta palabra reservada hace referencia al objeto actual donde se está ejecutando. Lo utilizamos **Por ejemplo**: Para crear funciones reutilizables que se puedan adaptar al contexto donde se están ejecutando. Ejemplos [aquí](https://platzi.com/comentario/2667373/)
- **new**: Es un operador, el cual nos permite crear instancias de un objeto definido por el usuario o de los objetos integrados que tienen una función constructora (Object, Array, String)
- **prototype**: Es la propiedad donde definimos **todas nuestras propiedades y métodos** que queremos que sean heredados. Esta es un repositorio, el cual también es un objeto.
- **Prototipos**: Es el mecanismo que utilizamos para que los objetos en JS hereden características entre sí.
- ** **Proto****: Es la propiedad que guarda los atributos y métodos definidos en prototype. Es importante recalcar que cuando se crea un objeto y se guardan valores acá, ambos siempre hacen referencia al mismo objeto.

Y para responder qué es un objeto?
Todo en JS es un objeto 😂.
Se podría resumir como una colección de valores, organizados de manera clave-valor.

>  Un **Objeto en JavaScript** es un grupo, sección, conjunto, colección, que también llaman ente, entidad, individuo, sujeto, criatura, unidad o cosa de información descrita en propiedades que puede ser atributos (información) o métodos (acciones para manejar y operar dicha información), que usualmente viene de una herencia del prototipo Object (nativo en JS) por medio de una/alguna clase, que es el molde para un/muchos objeto(s).

## Objetos literales y prototipos en JavaScript

**Prototipos (Prototypes)**
JavaScript es un lenguaje basado en prototipos, por lo tanto entender el objeto prototype es uno de los conceptos más importantes que los profesionales de JavaScript necesitan saber. Este artículo te dará una breve descripción del objeto prototype a través de varios ejemplos. Antes de leer este artículo, deberás tener un entendimiento básico de la referencia this en JavaScript.

**Objeto prototype**
En honor a la claridad, vamos a examinar el siguiente ejemplo:

```js
function Punto2D(x, y) {
  this.x = x;
  this.y = y;
}
```

Al declararse la función Punto2D, una propiedad predeterminada llamada prototype será creada para ella (ten en cuenta que, en JavaScript, una función es también un objeto). La propiedad prototype es un objeto que contiene una propiedad constructor y su valor es la función Punto2D: Punto2D.prototype.constructor = Punto2D. Y cuando tú llamas a Punto2D con la palabra reservada new, los objetos recién creados heredarán todas las propiedades de Punto2D.prototype. Para verificar esto, puedes agregar un método llamado mover en Punto2D.prototype de la siguiente manera:

```js
Punto2D.prototype.mover  = function(dx, dy) {
  this.x += dx;
  this.y += dy;
}

var p1 = new Point2D(1, 2);
p1.mover (3, 4);
console.log(p1.x); // 4
console.log(p1.y); // 6
```

El Point2D.prototype es llamado objeto prototipo o prototipo del objeto p1 y de cualquier otro objeto creado con la sintaxis new Point2D(…) . Puedes agregar tantas propiedades al objeto Point2D.prototype como quieras. Lo que suele hacerse es declarar métodos en Point2D.prototype y otras propiedades se declararán en la función constructora.

Los objetos integrados en JavaScript se construyen de manera similar. Por ejemplo:

El prototipo de los objetos creados con la sintaxis new Object() o {} es Object.prototype
El prototipo de los arreglos creados con la sintaxis new Array() o [] es Array.prototype
Y es igual con otros objetos integrados como Date y RegExp.
Object.prototype es heredado por todos los objetos y no tiene prototipo (su prototipo es null)

**Cadena de prototipos**

El mecanismo de la cadena del prototipo es simple: cuando accede a una propiedad p en el objeto obj, el motor de JavaScript buscará esta propiedad dentro del objeto obj. Si el motor falla en la búsqueda, continúa buscando en el prototipo de objeto obj y así sucesivamente hasta llegar a Object.prototype. Si finalizada la búsqueda no se ha encontrado nada, el resultado será undefined. Por ejemplo:

```js
var obj1 = {
  a: 1,
  b: 2
};

var obj2 = Object.create(obj1);
obj2.a = 2;

console.log(obj2.a); // 2
console.log(obj2.b); // 2
console.log(obj2.c); // undefined
```

En el fragmento de código anterior, la declaración var obj2 = Object.create(obj1) creará el objeto obj2 con el objeto prototipo obj1. En otras palabras, obj1 se convierte en el prototipo de obj2 en lugar de Object.prototype por defecto. Como puedes ver, bno es una propiedad de obj2, pero puedes acceder a ella a través de la cadena de prototipos. Sin embargo, para la propiedad c, se obtiene un valor undefined porque no se puede encontrar en obj1 y en Object.prototype.

> **El método Object.create() crea un objeto nuevo, utilizando un objeto existente como el prototipo del nuevo objeto creado.**

```js
const person = {
  isHuman: false,
  printIntroduction: function() {
    console.log(`My name is ${this.name}. Am I human? ${this.isHuman}`);
  }
};

const me = Object.create(person);

me.name = 'Matthew'; // "name" is a property set on "me", but not on "person"
me.isHuman = true; // inherited properties can be overwritten

me.printIntroduction();
// expected output: "My name is Matthew. Am I human? true"
```

### **¿Qué es un literal?*

La definición de literal alude a algo textual, por ejemplo, si declaramos una variable de la siguiente manera:

```js
let colorDelSol = "Amarillo";
```

Podemos decir la variable colorDelSol tiene asignada un string literal ya que se asigna el valor textualmente.

Exactamente lo mismo ocurre con los objetos literales, por ejemplo:

```js
let perro = {
   nombre:"Scott",
   color:"Cafe",
   edad: 5,
   macho: true
};
```

Donde:

El nombre del objeto es perro y sus claves/valores se describen en la siguiente tabla:

Clave Valor Tipo de dato
nombre Scott string
color Cafe string
edad 5 int
macho true bolean

Los tipos de datos que puede almacenar un objeto pueden ser strings, enteros, boleanos, inclusive otros objetos.

**Acceder a valores de un objeto**
Existen 2 maneras simples para poder acceder a los valores de un objeto:

Notación de punto
Consiste en escribir el nombre del objeto seguido de un punto y el nombre de la propiedad a la cual se quiere acceder: objeto.clave

```js
let perro = {
   nombre:"Scott",
   color:"Cafe",
   edad: 5,
   macho: true
};
```

console. info (perro.nombre); // Scott
console. info(perro.edad); // 5
Notación de corchetes / llaves cuadradas o brackets
Consiste en escribir el nombre del objeto anteponiendo entre corchetes la clave a la que se quiere acceder: objeto[clave]

```js
let perro = {
   nombre:"Scott",
   color:"Cafe",
   edad: 5,
   macho: true
};


console.info(perro['nombre']); // Scott
console.info(perro['edad']);  // 5
```

Ambas maneras retornan los mismos resultados, por lo tanto se pueden considerar homónimas, pero en la práctica se acostumbra a usar más la notación de punto.

**Métodos en un objeto**
Un objeto no solo se limita a la agrupación de claves valores, es posible también incorporar métodos de la siguiente manera:

```js
let perro = {
   nombre:"Scott",
   color:"Cafe",
   edad: 5,
   macho: true,
   ladrar: function(){
     return(`${this.nombre} puede ladrar`)
   }

};

console.log(perro.ladrar()); // Scott puede ladrar
```

Inserción de nuevos campos a un objeto
Para agregar nuevos campos al objeto solo basta con mencionar el nombre del objeto seguido de la propiedad nueva y el valor nuevo a insertar: perro.tamaño = “Grande”;

```js
let perro = {
   nombre:"Scott",
   color:"Cafe",
   edad: 5,
   macho: true,
   ladrar: function(){
     return(`${this.nombre} puede ladrar`)
   },

};

perro.tamaño = "Grande";
console.log(perro);
/*
[object Object] {
  color: "Cafe",
  edad: 5,
  ladrar: function(){
     return(`${this.nombre} puede ladrar`)
   },
  macho: true,
  nombre: "Scott",
  tamaño: "Grande"
}
*/
```

Con ellos conseguiremos agregar un nuevo valor al objeto.

Actualizar campos del objeto
Basta con sobre escribir una propiedad ya existente, de la siguiente manera:

```js
let perro = {
   nombre:"Scott",
   color:"Cafe",
   edad: 5,
   macho: true,
   ladrar: function(){
     return(`${this.nombre} puede ladrar`)
   },

};

perro.edad = 8;
console.log(perro);
/*
[object Object] {
  color: "Cafe",
  edad: 8,
  ladrar: function(){
     return(`${this.nombre} puede ladrar`)
   },
  macho: true,
  nombre: "Scott"
}
*/
```

Eliminación de campos
Para borrar una propiedad de un objeto se utiliza la palabra reservada delete de la siguiente manera:
delete nombre_del_objeto.clave;
Por ejemplo:

```js
let perro = {
   nombre:"Scott",
   color:"Cafe",
   edad: 5,
   macho: true,
   ladrar: function(){
     return(`${this.nombre} puede ladrar`)
   }

};

delete perro.color;
console.log(perro);
/*
let perro = {
   nombre:"Scott",
   color:"Cafe",
   edad: 5,
   macho: true,
   ladrar: function(){
     return(`${this.nombre} puede ladrar`)
   },

};
*/
```

## Clases en JavaScript

En ES2016, ahora podemos usar la palabra clave Class , así como los métodos mencionados anteriormente para manipular el prototype. Las clases de JavaScript son atractivas para los desarrolladores con experiencia en programación orientada a objetos, pero esencialmente hace lo mismo que el anterior.

```js
class Rectangle {
  constructor(height, width) {
    this.height = height
    this.width = width
  }

  get area() {
    return this.calcArea()
  }

  calcArea() {
    return this.height * this.width
  }
}

const square = new Rectangle(10, 10)

console.log(square.area) // 100
```

Esto es básicamente lo mismo que:

```js
function Rectangle(height, width) {
  this.height = height
  this.width = width
}

Rectangle.prototype.calcArea = function calcArea() {
  return this.height * this.width
}
```

Los métodos getter y setter en las clases vinculan una propiedad Object a una función que será llamada cuando se busque esa propiedad. Esto es solo azúcar sintáctico para ayudar a que sea más fácil buscar o establecer propiedades.

> Classes are only syntactical sugar for constructor functions. Everything still works the same way!

<img src="https://i.ibb.co/9tLdPzh/clases.gif" alt="clases" border="0">

**Código de la clase:**

```javascript
class Student {
  constructor({ name, age, approvedCourses, email }) {
    this.name = name;
    this.age = age;
    this.email = email;
    this.approvedCourses = approvedCourses;
  }

  approveCourse(course) {
    this.approvedCourses.push(course);
  }
}

const JUAN = new Student({
  name: 'Juan David Castro',
  age: 18,
  email: 'juan@platzi.com',
  approvedCourses: [
    'Cómo Conseguir Trabajo en Programación',
    'Curso Básico de Programación Orientada a Objetos con JavaScript',
  ],
});

console.log(JUAN;
```

## Ventajas de la programación orientada a objetos

Reusabilidad. Cuando hemos diseñado adecuadamente las clases, se pueden usar en distintas partes del programa y en numerosos proyectos.

Mantenibilidad. Debido a las sencillez para abstraer el problema, los programas orientados a objetos son más sencillos de leer y comprender, pues nos permiten ocultar detalles de implementación dejando visibles sólo aquellos detalles más relevantes.

Modificabilidad. La facilidad de añadir, suprimir o modificar nuevos objetos nos permite hacer modificaciones de una forma muy sencilla.

Fiabilidad. Al dividir el problema en partes más pequeñas podemos probarlas de manera independiente y aislar mucho más fácilmente los posibles errores que puedan surgir.

**La programación orientada a objetos presenta también algunas desventajas como pueden ser:**

Cambio en la forma de pensar de la programación tradicional a la orientada a objetos.

La ejecución de programas orientados a objetos es más lenta.

La necesidad de utilizar bibliotecas de clases obliga a su aprendizaje y entrenamiento.

```js
class Course {
  constructor({
    id,
    name,
    teacher,
    lessons = [],
  }) {
    this.id = id;
    this.name = name;
    this.teacher = teacher;
    this.lessons = lessons;
  }
}

class LearningPath {
  constructor({
    id,
    name,
    courses = [],
  }) {
    this.id = id;
    this.name = name;
    this.courses = courses;
  }

  addCourse(course) {
    this.courses.push(course);
  }

  replaceCourse(oldCourse, newCourse) {
    const oldCourseIndex = this.courses.findIndex(course => course.id === oldCourse.id);

    if (oldCourseIndex !== -1) {
      this.courses[oldCourseIndex] = newCourse;
    }

    return this.courses;
  }

  deleteCourse(oldCourse) {
    const courseIndex = this.courses.findIndex(course => course.id === oldCourse.id);
    this.courses.splice(courseIndex, 1);

    return this.courses;
  }
}

const reactNativeLearningPath = new LearningPath({
  id: 'react-native',
  name: 'Desarrollo de Apps con React Native',
  courses: [
    new Course({ 
      id: 'basico-javascript', 
      name: 'Curso Básico de JavaScript', 
      teacher: 'Diego De Granda',
    }),
    new Course({
      id: 'ecmascript-6',
      name: 'Curso de ECMAScript 6+',
      teacher: 'Orlando Naipes',
    }),
    // etc...
  ]
})
```

Todas las clases en archivos diferentes, esto con la finalidad de que el codigo sea mas facil de leer y no se sature en un solo archivo… Se los comparto:

- **Cambio 1:**
  Primero vamos a tu html y agregas esta propiedad al script para que cargue el js. `type="module"`, quedando de la siguiente manera:
  `<script type="module" src="./src/Ventajas/main.js"></script>`
- **Paso 2:**
  Dividimos el codigo en pedacitos mas pequeños que armaremos, para ello creamos 4 archivos, los cuales seran: `Student`, `LearningPath`, `Course` y `main`, y su estructura es:

```js
Student,js
export default class Student {
  constructor({
    name,
    username,
    points,
    approvedCourses = [],
    learningPaths = [],
    twitter = undefined,
    facebook = undefined,
    instagram = undefined,
  }) {
    this.name = name;
    this.username = username;
    this.points = points;
    this.approvedCourses = approvedCourses;
    this.learningPaths = learningPaths;
    this.socialMedia = {
      twitter,
      facebook,
      instagram,
    };
  }
}
```

El siguiente es `Course.js`:

```js
export default class Student {
  constructor({
    name,
    username,
    points,
    approvedCourses = [],
    learningPaths = [],
    twitter = undefined,
    facebook = undefined,
    instagram = undefined,
  }) {
    this.name = name;
    this.username = username;
    this.points = points;
    this.approvedCourses = approvedCourses;
    this.learningPaths = learningPaths;
    this.socialMedia = {
      twitter,
      facebook,
      instagram,
    };
  }
}
```

Agregamos a `LearningPath.js`:

```js
export default class LearningPath {
  constructor({ id, name, description, category, courses = [] }) {
    this.id = id;
    this.name = name;
    this.category = category;
    this.description = description;
    this.courses = courses;
  }
  addCourse(newCourse) {
    this.courses.push(newCourse);
  }
}
```

Esto lo debemos agregar y trabajar en el `main.js`:

```js
import Course from "./Course.js";
import LearningPath from "./LearningPath.js";
import Student from "./Student.js";

const inglesBasico = new Course({
  id: 1,
  name: "Curso Ingles basico",
  teacher: "Juan Perez",
  lessons: 23,
});
console.log(inglesBasico);

const rutaIngles = new LearningPath({
  id: 1,
  name: "Escuela Ingles",
  description: "Ganate tu primer B1",
  category: "Ingles",
  courses: [inglesBasico],
});

console.log(rutaIngles);

const edgar = new Student({
  name: "Edgar Gc",
  username: "edgargc026",
  points: 2000,
  learningPaths: [rutaIngles],
  approvedCourses: ["Curso Definitivo HTML y CSS", "Curso Practio HTML y CSS"],
});

console.log(edgar);
```

# 3. Pilares de la POO: abstracción y encapsulamiento

## Qué es abstracción

Podemos reducir la complejidad y permitir una implementación y diseño eficiente de los datos.

**Ventajas de uso**:

- Evitamos codigo duplicado, es decir, reusamos codigo.
- Podemos crear múltiples instancias con una sola abstracción.
- Al encapsular datos, los estamos protegiendo
- Evitamos código a bajo nivel.
- Podemos cambiar implementaciones en la clase, sin perjudicar su funcionamiento.
  🥵

> 📌 La **abstracción**es un proceso u operación mental que tiene como objetivo **aislar las propiedades y funciones esenciales** en una clase que sirve de molde para crear otros objetos(instancias) que comparten las mismas propiedades y funciones y se pueden personalizar.

**Abstracción** es cuando separamos los datos de un objeto para generar un molde.

Abstracción es:

- Enfocarnos en la información relevante.
- Separar la información central de los detalles secundarios.

## Abstracción en JavaScript

Diagrama de clases **básico**, de Platzi.

![UML Class - Animation Matrix.png](https://static.platzi.com/media/user_upload/UML%20Class%20-%20Animation%20Matrix-65e16e66-4a1a-453d-aa15-ccadb2adde8b.jpg)

cursos.

```javascript
class Comments {
    constructor({user, comment}){
        this.user = user;
        this.comment = comment;
    }
}
const primerComentario = new Comments({user: 'Fernando', comment: 'Este es el primer comentario'});

class Classes {
    constructor({name, time, comentarios = []}){
        this.name = name; 
        this.time = time;
        this.comentarios = comentarios;
    }
}
const primeraClaseProgramacionBasica = new Classes({name: 'Que es la programación', comentarios: [primerComentario]});

class Course {
    constructor({name, classes = []}){
        this.name = name;
        this.classes = classes;
    }
}
const cursoProgramacionBasica = new Course({name: 'Curso Gratis de programación Básica', classes: primeraClaseProgramacionBasica});
const cursoDefinitivoHTMLyCSS = new Course({name: 'Curso definitivo de HTML y CSS'});
const cursoPracticoHTMLyCSS = new Course({name: 'Curso práctico de HTML y CSS'});
```

## Qué es encapsulamiento

**Encapsulación**
La encapsulación es el empaquetamiento de datos y funciones en un componente (por ejemplo, una clase) y para luego controlar el acceso a ese componente para hacer un ejecto de “caja negra” fuera del objeto. Debido a esto, un usuario de esa clase solo necesita conocer su interfaz (es decir, los datos y las funciones expuestas fuera de la clase), no la implementación oculta.

¿Qué es encapsulamiento?
Es guardar, proteger, guardar o limitar el acceso de cierto atributos y/o propiedades en nuestros prototipos y objetos.

Cuando hablamos de **encapsulamiento **hablamos de:

- Esconder métodos y atributos 👻
- No permitir la alteración de métodos y atributos ❌

**Encapsulamiento en JavaScript**

- No permitir la alteración de métodos y atributos ❌

*Formas de aplicar encapsulamiento en JavaScript*

- Getters y setters 🖐
- Namespaces 🙂
- Object.defineProperties 🎈
- Módulo de ES6 🤝

Es la forma de proteger, encapsular, guardar, limitar, esconder el acceso de ciertos atributos y propiedades de nuestros objetos.

1. Esto nos permite crear métodos y atributos privados. Solamente los métodos y atributos que se encuentren dentro de la clase podrán acceder a estos datos privados.

2. Así, nosotros podemos prevenir la sobreescritura o alteración de métodos y atributos, de las clases, de los objetos, o los prototipos incluso, o sea las instancias creadas.

   Pero… en JavaScript, el encapsulamiento no es tan posible. Porque en JS todo es público. Y aunque nos lo puede permitir, debemos tener un dominio más profundo del prototipo Object para lograrlo… otros métodos son usando getters & setters, Namespaces, Object.defineProperties, módulos de ES6.

## Getters y setters en JavaScript

En ES2020 se introdujo la sintaxis campos privados en las clases. Se hace uso de un numeral como prefijo del nombre de la variable.

¿Cúal sería la ventaja de usar esto? Que no existe la posibilidad de que alguien modifique la variable privada desde la instancia a menos de que use el setter que le dimos.

Con el ejemplo en esta clase, quedaría así:

```javascript
class Course {
  #name;

  constructor({
    name,
    classes = []
  }) {
    this.#name = name;
    this.classes = classes;
  }

  get name() {
    return this.#name;
  }

  set name(nuevoNombrecito) {
    if (nuevoNombrecito === 'Curso Malito de Programación Básica') {
      console.error('Web... no');
    } else {
      this.#name = nuevoNombrecito;
    }
  }
}
```

**Qué son los getters y setters**
Una función que obtiene un valor de una propiedad se llama getter y una que establece el valor de una propiedad se llama setter.

Esta característica a sido implementada en ES2015, pudiendo modificar el funcionamiento normal de establecer u obtener el valor de una propiedad, a estas se les conoce como accessor properties.

**Funcionamiento**
En ocasiones queremos valores basados en otros valores, para esto los data accessors son bastante útiles.

Para crearlos usamos los keywords get y set

```
const obj = {
  get prop() {
    return this.__prop__;
  },
  set prop(value) {
    this.__prop__ = value * 2;
  },
};

obj.prop = 12;

console.log(obj.prop); //24
```

Creamos un objeto, con una única propiedad, que tiene un getter y un setter. de esta manera cada vez que establezcamos un valor para prop se multiplicará por dos.

Nota: utilice prop por convención, pero no implica que es un valor especial, este es un valor normal.

Otra manera de crear un accessor properties es de manera explícita usando Object.defineProperty

```javascript
const obj = {};

Object.defineProperty(obj, //objeto target
  'prop', //nombre propiedad
  {
    enumerable: true,
    configurable: true,
    get prop() { //getter
      return this.__prop__;
    },
    set prop(value) { //setter
      this.__prop__ = value * 2;
    },
  });
obj.prop = 12;

var atr = Object.getOwnPropertyDescriptor(obj, 'prop')
console.log(atr); 
```

La ventaja que tenemos de esta manera, es que podemos establecer los atributos que queremos tenga la propiedad.

“prototypes” y logre esto:

```javascript
"use strict";

function Student(name, age, nationality) {
  this._name = name;
  this._age = age;
  this.nationality = nationality;
}

Student.prototype = {
  get name() {
    return this._name;
  },
  set name(newName) {
    this._name = newName;
  },

  get age() {
    return this._age;
  },

  set age(newAge) {
    this._age = newAge;
  },
};

let edgar = new Student("Edgar", 25, "Mexico");
edgar.name = "Juan";
edgar.age = 30
console.log(edgar);
```

Funcionando al 100 :3

## Módulos de ECMAScript 6

módulos en JavaScript depende de las declaraciones ***Export\*** e ***Import\***.

Para modular, solo basta que el archivo tenga la extensión ***.js\***, pero el motor V8 que es el que corre en los navegadores, recomienda usar la extensión ***.mjs\***

- Esto es util porque queda claro cuál archivo es modulo y cual no.
- Asegura que los archivos de modulo sean tratados como tal por la extensión **Babel** o **Node.js**

> ¿Como Exportar?
> hay dos formas de exportar:

1. Colocar en frente de cada elemento la palabra ***export\***
2. Exportar en una sola declaración al final del archivo modulo las características que se quieren exportar, ejemplo:

```javascript
export {PlatziClass, Student, LearningPaths};
```

> ¿Como Importar?
> La importación obviamente se hace en el script que queremos usar dichos elementos.

```javascript
import {PlatziClass, Student, LearningPaths} from '/ruta';
```

> ¿Como aplicar el modulo en HTML?

```javascript
<script type="module" src="main.js"></script>
```

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)Code Runner - Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=formulahendry.code-runner)

# 4. Pilares de la POO: herencia y polimorfismo

## Qué es herencia

Cuando observemos 👀 en nuestras clases o en el proceso de abstracción que hay clases que pueden compartir atributos, métodos (o simplemente porque la lógica del negocio nos lo indica). Podemos abstraer aun más nuestras clases y crear así las Suuuuuuper clases. Estas serán moldes que nos permitirán “heredar” atributos y métodos a nuestras sub-clases.
No lo olviden, DRY(Don’t Repeat Yourself).
Mejor utiliza una SUUUUPEEER Class.

![franky.png](https://static.platzi.com/media/user_upload/franky-80a140ba-8efc-4050-9321-83ce6fb06ff8.jpg)

> La herencia nos permite crear “clases madre”, la cual servirá de molde para clases hijas, que compartirán sus métodos y atributos.
> Usamos la palabra reservada `extends`.

**HERENCIA**
JavaScript en un lenguaje orientado a objetos basado en prototipos, en lugar de estar basado en clases. Debido a esta básica diferencia, es menos evidente entender cómo JavaScript nos permite crear herencia entre objetos, y heredar las propiedades y sus valores.

> Nos permite crear Clases Madre/Padre, moldes generales, prototipos super-prototipos, para poder crear prototipos (clases) hijas. Así, se puede crear una Clase molde Madre, y a partir de esta clase, se puede definir y extender más clases que heredan todos los atributos y métodos de su mamá, Clase molde Hija.

**Don’t repeat yourself** es una filosofía que promueve la reducción de duplicación en programación, esto nos va a inculcar que no tengamos líneas de código duplicadas.

Toda pieza de información nunca debería ser duplicada debido a que incrementa la dificultad en los cambios y evolución

## Herencia en JavaScript

Para hacer la herencia usando la sintaxis de prototipos podemos hacer lo siguiente:
 
Suponiendo que ya tenemos creada nuestra superclase (Student). Vamos a crear una clase (FreeStudent) que va a pasar los parámetros de inicialización al constructor de la superclase, para esto hacemos uso de la función `call()`.

```javascript
function FreeStudent(props) {
  Student.call(this, props);
}
```

Le pasamos como primer atributo el contexto de ejecución de nuestra nueva “clase” y como segundo parámetro los props, que son estas propiedades que recibiremos de inicialización.
 
Después de esto, clonamos el prototipo de nuestra superclase en el prototipo de nuestra subclase:

```javascript
FreeStudent.prototype = Object.create(Student.prototype);
```

Por último, le agregamos cualquier función extra que deseemos agregar a la subclase:

```javascript
FreeStudent.prototype.approveCourse = function (newCourse) {
  if (newCourse.isFree) {
    this.approvedCourses.push(newCourse);
  } else {
    console.warn(`Lo sentimos, ${this.name}, sólo puedes tomar cursos gratis`);
  }
}
```

La palabra clave super es usada para acceder y llamar funciones del padre de un objeto.

Las expresiones super.prop y super[expr] son válidas en cualquier definición de método tanto para clases como para objetos literales (en-US).

**Sintaxis**
// llama al método constructor del objeto padre.
super([arguments]);

// llama cualquier otro método del objeto padre.
super.functionOnParent([arguments]);
**Descripción**
Cuando es usado en un constructor, la palabra clave super aparece sola lo cual invoca el constructor del objeto padre. En este caso debe usarse antes de que la palabra clave this sea usada. La palabra clave super también puede utilizarse para llamar otras funciones del objeto padre.
**
Ejemplo**
Usando super en clases
Este fragmento de código se toma del ejemplo de clases (demo en vivo). Aquí se llama a super() para evitar la duplicación de las partes del constructor que son comunes entre Rectangle y Square.

```javascript
class Rectangle {
  constructor(height, width) {
    this.name = 'Rectangle';
    this.height = height;
    this.width = width;
  }
  sayName() {
    console.log('Hi, I am a ', this.name + '.');
  }
  get area() {
    return this.height * this.width;
  }
  set area(value) {
    this.height = this.width = Math.sqrt(value);
  }
}

class Square extends Rectangle {
  constructor(length) {
    this.height; // ReferenceError, super necesita ser llamado primero!

    // Aquí, llama al constructor de la clase padre con las longitudes
    // previstas para el ancho y la altura de Rectangle
    super(length, length);

    // Nota: En las clases derivadas, se debe llamar a super() antes de
    // poder usar 'this'. Salir de esto provocará un error de referencia.
    this.name = 'Square';
  }
}
```

## Qué es polimorfismo

Es como la herencia reloaded. Es como ‘La Herencia 2.0’. Es un pilar de la OOP. Lo que es importante es lo que se puede hacer con este: Permite a nuestras subclases cambiar o anular los comportamientos de los métodos y atributos del prototipo madre, de la clase madre. Aunque herede las propiedades, el polimorfismo permite cambiar su comportamiento.

Tipos:

1. Polimorfismo de Sobrecarga: ocurre cuando existen métodos con el mismo nombre y funcionalidad similar en clases totalmente independientes entre ellas.
2. Polimorfismo Paramétrico: El polimorfismo paramétrico es la capacidad para definir varias funciones utilizando el mismo nombre, pero usando parámetros diferentes (nombre y/o tipo).
3. Polimorfismo de Inclusión (JS): La habilidad para redefinir por completo el método de una superclase en una subclase.

Un ejemplo claro del Polimorfismo es el de la imagen, todas las figuras heredan de la super-Clase Figura.
Todas ellas pueden Dibujarse, pero cada una lo hará diferente dependiendo de la figura que sea.

![Herencia - Figuras.png](https://static.platzi.com/media/user_upload/Herencia%20-%20Figuras-84af0bbf-3587-4b6d-a5cd-9cecef206149.jpg)

## Polimorfismo en JavaScript

El polimorfismo es uno de los principios de la programación orientada a objetos (OOP). Es la práctica de diseñar objetos para compartir comportamientos y poder anular los comportamientos compartidos con otros específicos. El polimorfismo se aprovecha de la herencia para que esto suceda.

En OOP se considera que todo está modelado como un objeto. Esta abstracción puede reducirse a tuercas y tornillos para un automóvil, o tan amplia como simplemente un tipo de automóvil con un año, marca y modelo.

Para tener un escenario de automóvil polimórfico sería el tipo de automóvil base, y luego habría subclases que heredarían del automóvil y proporcionarían sus propios comportamientos además de los comportamientos básicos que tendría un automóvil. Por ejemplo, una subclase podría ser TowTruck que aún tendría un año de fabricación y modelo, pero también podría tener algunos comportamientos y propiedades adicionales que podrían ser tan básicos como una bandera para IsTowing tan complicados como los detalles específicos de la elevación.

Volviendo al ejemplo de las personas y los empleados, todos los empleados son personas, pero todas las personas no son empleados. Lo que quiere decir que las personas serán la súper clase y el empleado la subclase. Las personas pueden tener edades y pesos, pero no tienen salarios. Los empleados son personas por lo que inherentemente tendrán una edad y peso, pero también porque son empleados tendrán un salario.

Así que para facilitar esto, primero escribiremos la súper clase (Persona)

```js
function Person(age,weight){
 this.age = age;
 this.weight = weight;
}
```

Y le daremos a la persona la posibilidad de compartir su información.

```js
Person.prototype.getInfo = function(){
 return "I am " + this.age + " years old " +
    "and weighs " + this.weight +" kilo.";
};
```

A continuación deseamos tener una subclase de Persona, Empleado.

```js
function Employee(age,weight,salary){
 this.age = age;
 this.weight = weight;
 this.salary = salary;
}
Employee.prototype = new Person();
```

Y anularemos el comportamiento de getInfo definiendo uno que sea más adecuado para un empleado

```js
Employee.prototype.getInfo = function(){
 return "I am " + this.age + " years old " +
    "and weighs " + this.weight +" kilo " +
    "and earns " + this.salary + " dollar.";  
};
```

Se pueden usar de forma similar a su código original.

```js
var person = new Person(50,90);
var employee = new Employee(43,80,50000);

console.log(person.getInfo());
console.log(employee.getInfo());
```

Sin embargo, aquí no se gana mucho con la herencia, ya que el constructor de Employee es muy similar al de una persona, y la única función en el prototipo está siendo anulada. El poder en el diseño polimórfico es compartir comportamientos.

# 5. Próximos pasos

## ¿Quieres más cursos de POO en JavaScript?

Nunca pares de Aprender!