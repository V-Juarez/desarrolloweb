const natalia = {
  name: "Natalia",
  age: 20,
  cursosAprobados: [
    "Curso Definitivo de HTML y CSS",
    "Curso Practico de HTML y CSS",
  ],
  aprobarCurso(nuevoCursito) {
    this.cursosAprobados.push(nuevoCursito);
  },
};

function Student(name, age, cursosAprobados) {
  this.name = name;
  this.age = age;
  this.cursosAprobados = cursosAprobados;
  // this.aprobarCurso = function (nuevoCursito) {
  //   this.cursosAprobados.push(cursosAprobados);
  // }
}



Student.prototype.aprobarCurso = function (nuevoCursito) {
  this.cursosAprobados.push(nuevoCursito);
}
// Hacer que Natalia aprueve otros cursos 
// natalia.cursosAprobados.push("Curso de Responsive Design");

const juanita = new Student(
  "Juanita Alejandra",
  15,
   [
    "Curso de Bash",
    "Curso de JavaScript",
    "Curso Unreal Enged",
  ],
);

// prototipos con la sintaxis de clases

class Student2 {
  
  constructor({
    name, 
    age, 
    cursosAprobados = [],
    email,
    facebook,
    twitter,
    Instagram,
    }) {
    this.name = name;
    this.age = age;
    this.cursosAprobados = cursosAprobados;
    this.email = email;
  }

  aprobarCurso(nuevoCursito) {
    this.cursosAprobados.push(nuevoCursito);
  }
}

// const miguel = new Student2(
//   "Miguel",
//   28,
//   [
//     "Cursos Creacion de startups",
//     "Cursos de Pynton Basico",
//     "Cursos de Pynton Intermedio",
//   ],
// );
const miguel = new Student2({
  email: "miguelito@platzi.com",
  name: "Miguel",
  age: 28,
  cursosAprobados: [
    "Cursos Creacion de startups",
    "Cursos de Pynton Basico",
    "Cursos de Pynton Intermedio",
  ],
});