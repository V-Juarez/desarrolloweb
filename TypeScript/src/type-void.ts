// Void 

// Tipo explicito
console.log('Tipo explicito');

function showInfo(user: any) {
  console.log('User info', user.id, user.username, user.firstName);
}

showInfo({id: 1, username: 'luixaviles', firstName: 'Luis'});


// // Tipo Inferido
console.log('Tipo enfefido');

function showFormattedInfo(user: any) {
  console.log('User info', `
    id: ${user.id}
    username: ${user.username}
    firstName: ${user.firstName}
  `);
}

showFormattedInfo({id: 1, username: 'luixaviles', firstName: 'Luis'});

// Tipo void, como tipo de dato en variable
let unusable: void;
unusable = null;
unusable = undefined;

// Tipo: Never

function handleError(code: number, message: string): never{

  // Process your code here
  // Generate a message
  throw new Error(`${message}. Code: ${code}`);
}

try {
  handleError(404, 'Not Found');
} catch (error) {

}

function sumNumbers(limit: number): never {
  let sum = 0;
  while(true) {
    sum++;
  }
  // sum return 
}

sumNumbers(10);
// ciclo infinito, programa nunca termina