let user: object;
user = {};  // Object

user = {
  id: 1,
  username: 'paparazzi',
  firstName: 'Pablo',
  isPro: true
}

console.log('User', user);

// Object vs object(Clase JS vs tipo TS)
const myObj = {
  id: 1,
  username: 'paparazzi',
  firstName: 'Pablo',
  isPro: true
};

const isInstance = myObj instanceof Object; // clase Objet JavaScript
console.log('isInstance', isInstance);
console.log('User.username', myObj.username);