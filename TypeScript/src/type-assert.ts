export {};

// <tipo> Angle Bracket syntax

let username: any;
username = 'luixaviles';

// Tenemos una cadenal, TS confia en mi!
let message: string = (<string>username).length > 5 ? 
                      `welcome ${username}`:
                      'Username is too short';
console.log('message', message);

let usernameWithId: any = 'luixaviles 1';
// Como obtener el usename?
username = (<string>usernameWithId).substring(0, 10);
console.log('Username only', username);

// Sintaxis "as"
message = (username as string).length > 5 ? 
          `welcome ${username}`:
          'Username is too short';

let helloUser: any;
helloUser = 'Hello paparazzi';
username = (helloUser as string).substring(6);
console.log('username', username);