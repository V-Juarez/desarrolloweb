console.log('Hola platzi, Student');


// Number
// Explicito 
let phone: number;
phone = 1;
phone = 234546;
// phone = 'hola'; Error


// Inferido 
let phoneNumber = 234521456;
phoneNumber = 12354;
// phoneNumber = 'true'; error por tipo

let hex: number = 0xf00d;
let binary: number = 0b1010;
let octal: number = 0o744;

// boolean 
// tipado explicito
let isPro: boolean;
isPro = true;
// esPro =1; 

// Inferido 
let isUserPro = false;
isUserPro = true;
// isUserPro = 10; Errpr!

// Strings
let username: string = 'luixaviles';
username = "Luis";
// username = true Error: tipo string

// Template String
// Uso de back-tik ``
let userInfo: string;
userInfo = `
  User Info:
  username: ${username}
  firstname: ${username + ' Aviles'}
  phone: ${phone}
  isPro: ${isPro}
`;

console.log('userInfo', userInfo);