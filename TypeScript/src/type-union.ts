export {};

// 10, '10',

// let idUser: number | string;
// idUser = 10; 
// idUser = '10'; 

// // Buscar unsername dado un ID
// function getUsenameById(id: number | string) {
//   // Logica de negocios, find the user
//   return 'luixaviles';
// }

// getUsenameById(20);
// getUsenameById('20');

type IdUser = number | string;
type Username = string;
let idUser: IdUser;
idUser = 10; 
idUser = '10'; 

// Buscar unsername dado un ID
function getUsenameById(id: IdUser): Username {
  // Logica de negocios, find the user
  return 'luixaviles';
}

getUsenameById(20);
getUsenameById('20');

// Tipos literaless
// 100x100, 500x500, 1000x1000
type SqureSize = '100x100' | '500x500' | '1000x1000';

let smallPicture: SqureSize = '100x100';
let mediumPicture: SqureSize = '500x500';