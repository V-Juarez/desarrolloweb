<h1>Fundamentos de TypeScript</h1>

<h3>Luis Aviles</h3>

<h1>Tabla de Contenido</h1>
- [1. Introducción a TypeScript](#1-introducción-a-typescript)
  - [El lenguaje de programación TypeScript](#el-lenguaje-de-programación-typescript)
  - [Instalación de herramientas](#instalación-de-herramientas)
  - [Instalación de herramientas en Windows](#instalación-de-herramientas-en-windows)
  - [Navegación y refactorización](#navegación-y-refactorización)
- [2. Entorno de Desarrollo](#2-entorno-de-desarrollo)
  - [El compilador de TypeScript](#el-compilador-de-typescript)
  - [El archivo de configuración de TypeScript](#el-archivo-de-configuración-de-typescript)
  - [Mi primer proyecto TypeScript](#mi-primer-proyecto-typescript)
- [3. Tipos en TypeScript](#3-tipos-en-typescript)
  - [Tipado en TypeScript](#tipado-en-typescript)
  - [Number, Boolean y String](#number-boolean-y-string)
  - [Any](#any)
  - [Void y never](#void-y-never)
  - [null y undefined](#null-y-undefined)
  - [Object](#object)
  - [Array](#array)
  - [Tupla](#tupla)
  - [Enum](#enum)
  - [Unión de Tipos, Alias y Tipos Literales](#unión-de-tipos-alias-y-tipos-literales)
  - [Aserciones de tipo](#aserciones-de-tipo)
  - [Funciones en TypeScript](#funciones-en-typescript)
  - [Resumen](#resumen)
- [4. Tipos Personalizados](#4-tipos-personalizados)
  - [Interfaces](#interfaces)
  - [Interfaces: propiedades opcionales](#interfaces-propiedades-opcionales)
  - [Extensión de interfaces](#extensión-de-interfaces)
  - [Clases](#clases)
  - [Clases públicas y privadas](#clases-públicas-y-privadas)
  - [Métodos Get y Set](#métodos-get-y-set)
  - [Herencia de clases y propiedades estáticas](#herencia-de-clases-y-propiedades-estáticas)
  - [Resumen](#resumen-1)
- [5. Módulos](#5-módulos)
  - [Principios de responsabilidad única](#principios-de-responsabilidad-única)
  - [Resolviendo módulos](#resolviendo-módulos)
  - [Webpack y agrupación de Módulos](#webpack-y-agrupación-de-módulos)
- [6. Cierre](#6-cierre)
  - [Cierre](#cierre)


# 1. Introducción a TypeScript

## El lenguaje de programación TypeScript

Es un superconjunto tipado de javascript, que compila a javascript.

**Lenguaje de programación tipado:** Posee un conjunto de tipos para poder usarlos con las variables, pudiendo personalizarlos o extenderlos.

**Lenguaje de alto nivel:** Entendible por humanos y posee un alto nivel de abstracción del código máquina.

**Genera como resultado código JavaScript**: Emite código javascript compatible con browsers y otras herramientas de javascript.

**Código abierto.**

**Desarrollo desde cualquier sistema.**

**El código puede ejecutarse en cualquier navegador o plataforma que soporte javascript.**

<h3>Porque usar TypeScript</h3>

- Programación orientada a objetos
- Potenciar tu código JavaScript
- Mayor productividad
- Poderoso sistema de tipos
- Compila a ES5, ES6 y más
- Proyecto muy activo/Open source
- Actualizaciones periódicas
- Comunidad creciente
- Puede prevenir cerca del 15% de bugs
- Puede usar TypeScript para backend**__**

### TypeScript

**¿Qué es TypeScrip?** Según su definición forma es un superconjunto tipado de JavaScript, que compila a JavaScript. Pero esto no nos dice mucho, desmenuzando un poco su definición tenemos que es un lenguaje de:

- Programación tipado, esto quiere decir que posee un conjunto de tipos para definir las variables.
- Alto nivel, lo que quiere decir que es entendible por humanos y posee un alto nivel de abstracción del código máquina.
- Programación que genera código JavaScript, sí, al final del día emite código JavaScript el cual es compatible con los navegadores.
- Código abierto.
- Desarrollo para cualquier sistema.
- Programación que puede ejecutarse en cualquier navegador o plataforma que soporte JavaScript.

**¿Por qué deberíamos utilizarlo?** Porque obtendremos un JavaScript con superpoderes. Esto quiere decir que tenemos programación orientada a objetos, podemos potenciar nuestro código de JavaScript ya que soporta ES5, ES6 y más, es un proyecto muy activo ya que es open source por lo que también tenemos actualizaciones periódicas y una comunidad creciente.

💡 Con TypeScript podemos prevenir cerca del 15% de bugs de un proyecto a diferencia que si lo hicieramos con JavaScript.

<img src="https://i.ibb.co/gdy3vXv/type-Script.jpg" alt="type-Script" border="0">

[Slides-TypeScript.pdf](https://drive.google.com/file/d/1gdK5-sUPTrxGeMpJ5tYPA0oQ-xhGvhLE/view?usp=sharing)

[Note | TypeScript](https://www.notion.so/TypeScript-9ca37bd954e443eda4c6f64ee0324f8c)

## Instalación de herramientas

[Version node]( https://www.npmjs.com/package/n)

## Instalación con NVM

Una alternativa a la instalación de Node.js a través de `apt` es utilizar una herramienta llamada `nvm`, que significa “Node.js Version Manager”. En vez de funcionar en el nivel del sistema operativo, `nvm` funciona en el nivel de un directorio independiente dentro de su directorio de inicio. Esto significa que puede instalar varias versiones autónomas de Node.js sin que afecte a todo el sistema.

Controlar su entorno con `nvm` le permite acceder a las versiones más recientes de Node.js, además de conservar y administrar versiones anteriores. Sin embargo, es una herramienta distinta de `apt` y las versiones de Node.js que administra con ella con distintas de las que maneja con `apt`.

Para descargar la secuencia de comandos de instalación de `nvm` de la [página de GitHub del proyecto](https://github.com/creationix/nvm), puede utilizar `curl`. Tenga en cuenta que el número de versión puede diferir del que se resalta aquí:

```bash
curl -sL https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh -o install_nvm.sh
```

Inspeccione la secuencia de comandos de instalación con `nano`:

```bash
nano install_nvm.sh
```

Ejecute la secuencia de comandos con `bash`:

```bash
bash install_nvm.sh
```

Instalará el software en un subdirectorio de su directorio de inicio en `~/.nvm`. También agregará las líneas necesarias a su archivo `~/.profile` para utilizarlo.

Para obtener acceso a la funcionalidad `nvm`, deberá cerrar sesión e iniciarla de nuevo u obtener el archivo `~/.profile` para que su sesión actual registre los cambios:

```bash
source ~/.profile
```

Con `nvm` instalado, puede instalar versiones aisladas de Node.js. Para obtener información sobre las versiones de Node.js disponibles, escriba lo siguiente:

```bash
nvm ls-remote
```

```
Output...
         v8.11.1   (Latest LTS: Carbon)
         v9.0.0
         v9.1.0
         v9.2.0
         v9.2.1
         v9.3.0
         v9.4.0
         v9.5.0
         v9.6.0
         v9.6.1
         v9.7.0
         v9.7.1
         v9.8.0
         v9.9.0
        v9.10.0
        v9.10.1
        v9.11.0
        v9.11.1
        v10.0.0  
```

Como puede ver, la versión LTS actual en el momento en que se redactó este artículo era la 8.11.1. Puede instalarla escribiendo lo siguiente:

```bash
nvm install 8.11.1
```

 Normalmente, `nvm` aplicará un cambio para utilizar la versión más reciente instalada. Puede indicar a `nvm` que utilice la versión que acaba de descargar escribiendo lo siguiente:

```bash
nvm use 8.11.1
```

 Cuando instale Node.js utilizando `nvm`, el ejecutable se llamará `node`. Puede ver la versión que el shell utiliza actualmente escribiendo lo siguiente:

```bash
node -v
```

 Si dispone de varias versiones de Node.js, puede ver cuál está instalada escribiendo lo siguiente:

```bash
nvm ls
```

 Si desea establecer como predeterminada una de las versiones, escriba lo siguiente:

```bash
nvm alias default 8.11.1
```

 Esta versión se seleccionará de forma automática cuando se genere una nueva sesión. También puede hacer referencia a ella con el alias, como se muestra:

```bash
nvm use default
```

Cada versión de Node.js hará un seguimiento de sus propios paquetes y cuenta con `npm` para administrarlos.

También puede contar con paquetes de instalación de `npm` en el directorio `/node_modules` del proyecto de Node.js. Utilice la siguiente sintaxis para instalar el módulo `express`:

```bash
npm install express
```

 Si desea instalar el módulo de manera general para que otros programas que utilizan la misma versión de Node.js puedan emplearlo, puede agregar el indicador `-g`:

```bash
npm install -g express
```

Con esto, el paquete se instalará aquí:

```
~/.nvm/versions/node/node_version/lib/node_modules/express
```

Instalar el módulo de forma general le permitirá ejecutar comandos de la línea de comandos, pero deberá vincular el paquete a su esfera local para poder solicitarlo desde un programa:

```bash
npm link express
```

Puede obtener más información sobre las opciones disponibles con `nvm` escribiendo lo siguiente:

```bash
nvm help
```

Instalar [nvm | Linux](https://www.digitalocean.com/community/tutorials/como-instalar-node-js-en-ubuntu-18-04-es)

![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)[GitHub - nvm-sh/nvm: Node Version Manager - POSIX-compliant bash script to manage multiple active node.js versions](https://github.com/nvm-sh/nvm)

## Instalación de herramientas en Windows

Para poder realizar este curso es necesario que tengas instalada una versión de **Node.js** en tu equipo. Lo único que tienes que hacer es descargar el instalador desde la [página oficial de Node.js](https://nodejs.org/es/). Te recomiendo que descargues la versión marcada con LTS para que tengas mayor soporte de actualizaciones a lo largo del tiempo:

![Node Installer](https://i.imgur.com/xBGY9I6.png)

Una vez hayas descargado el instalador solo tienes que ejecutarlo y dar clic en instalar con todas las opciones por defecto:

![Installer](https://i.imgur.com/G7AK0CE.png)

Para verificar que tienes instalado correctamente Node.js solo tienes que abrir una ventana del símbolo de sistema en Windows y teclear el comando `node -v` que mostrará tu versión de Node.js instalada.

Si te sale un resultado como el siguiente significa que todo está bien instalado:

![Node version](https://i.imgur.com/bw8hEAz.png)

## Instalación de NVM en Windows

El repositorio de **NVM** original solo funciona con Linux y MacOS, pero existe una alternativa para Windows que podemos usar. Solo tienes que entrar a este [enlace](https://github.com/coreybutler/nvm-windows/releases) donde encontrarás la última versión de NVM para Windows.

Recuerda descargar el archivo comprimido llamado *nvm-setup.zip*:

![Download NVM](https://i.imgur.com/ErKmUp0.png)

Uno vez que se termine de descargar el archivo lo tienes que descomprimir y te aparecerá un icono como el siguiente que es el instalador de NVM:

![Icon NVM](https://i.imgur.com/2AM0Muj.png)

Al igual que con el instalador de Node,js solo necesitas aceptar todas las configuraciones por defecto y dar clic en instalar:

![NVM install](https://i.imgur.com/eCm4MUk.png)

Para comprobar que NVM está instalado correctamente puedes abrir una ventana del símbolo del sistema y listar las versiones de Node.js que tienes instaladas con `nvm ls`:

![NVM list](https://i.imgur.com/kKaOCbr.png)

## Navegación y refactorización

**Proyecto referencia:**

```bash
git clone https://github.com/codex-team/editor.js
```

### Typescript en Visual Studio Code

El editor Visual Studio Code viene configurado para aprovechar al máximo TypeScript.
Entre las features se encuentran:

- IntelliSense
- Snippets
- JSDocs
- Formateo
- Refactorización
- Arreglos rápidos.

# 2. Entorno de Desarrollo

## El compilador de TypeScript

### Instalación de TypeScript

Con el siguiente comando lo instalaremos de manera global:

```bash
npm install -g typescript
```

### Consultar la versión del compilador de TS:

```bash
tsc -v
```

### Compilar nuestros ficheros .ts

```bash
tsc your_file.ts
```

### Ejecutar el `archivo.js`

```bash
# node archivo.js
node hello.js
```

### Compilar de manera ‘automática’ nuestros ficheros .ts

```bash
tsc --watch your_file.ts
```

No hay que tenerle miedo al TypeScript, ya que este nos creará un archivo: your_file.js.
Es decir un archivo .js compatible con todo.

Si no quisieran instalar typescript de manera global, pueden ejecutar:

```bash
npx tsc file.ts
npx tsc --watch file.ts
```

## El archivo de configuración de TypeScript

```
tsconfig.json
```

- Especifica la raiz de un proyecto TS
- Permite configurar opciones para el compilador

Para crear este archivo en cualquier proyecto:

```bash
tsc --init
```

El archivo base es este:

```json
{
	"extends": "./config/base"
	"compilerOptions":{
		"target": "es6",
		"module": "commonjs"
		"strict": true,
		"removeComments": true
	},
	"include":[
		"src/**/*.ts"
	],
	"exclude": [
		"node_modules",
		"**/*.test.ts"
	]
}
```

Una vez que este archivo es generado, ejecutamos:

```json
tsc // Busa la configuracion dentro del proyecto
tsc --project platzi // Especifica el directorio donde esta la configuracion
tsc file.ts // Omite la configuracion
```

Para ver todas las configuraciones que podemos hacer:

[TSConfig Reference - Docs on every TSConfig option](https://www.typescriptlang.org/tsconfig)

[Documentacion | TypeScript](https://www.typescriptlang.org/tsconfig)

## Mi primer proyecto TypeScript

- Ir al archivo tsconfig.json
- Descomentar la linea “outDir”:"./"
- Agregar a la linea anterior el directorio dist: `"outDir": "./dist",`
- Compilar con `tsc`
- Invocar al motor de Node acceder al directorio dist y al archivo para ejecutarlo: `node dist/hello.js`

# 3. Tipos en TypeScript

## Tipado en TypeScript

**Tipado en TypeScript**
Explicito: Define una sintaxis para la creación de variables con tipo de dato
nomVariable : Tipo de dato
Inferido: TypeScript tiene la habilidad de deducir el tipo en funcion de un valor.

**Tipo de datos primitivos**

```tsx
Number > Boolean > String > Array
Tuple > Enum > Any > Void
Null > Undefined > Never >Object
```

Tipado de TS:

- Explícito: Se define explícitamente una sintaxis (el orden, la formación y la combinación de las reglas) del tipo de la variables a usar.

```tsx
miVariableExplicita : string = 'Esta variable es explícita' // el `:` permite especificar el tipo del dato
```

- Inferido: TS tiene la habilidad de deducir el tipo de variable que se usa a partir del valor asignado a dicha variable

```
miVariableInferida = "Esta variable será un string"  // TS deduce el tipo de miVariableInferida y el valor, a partir de la inicialización de la misma
```

**Tipos de datos primitivos:**

- Number
- Boolean
- String
- Array
- Tuple
- Enum
- Any
- Void
- Null
- Undefined
- Never
- Object

> Cuando escribas tu código JavaScript en TypeScript y lo transpiles luego, debes acostumbrarte a no hacer «retoques» a mano en el JavaScript. Si tienes que cambiar o retocar cualquier cosa, por nimia que te parezca, hazlo en el TypeScript original y vuelve a transpilar, siguiendo las reglas de TypeScript. De otro modo, podrías estar infringiéndolas y encontrarte cometiendo los mismos errores una y otra vez. Cuando escribimos un código en TypeScript y, para poder usarlo, debemos transpilarlo a JavaScript, si hemos cometido errores de sintaxis el transpilador nos lanzará mensajes de error que nos ayudarán a escribir un código limpio y bien organizado.

## Number, Boolean y String

### Tipos de datos

Dentro de los tipos explícitos que maneja TypeScript, tenemos:

1. **Number**. Este tipo de dato incluye valores numéricos, hexadecimales, binarios y octales.

   ```tsx
   // Explícito
   let phone: number
   phone = 3315015804
   
   let hex: number = 0xf00d
   let binary: number = 0b1010
   let octal: number = 0o744
   
   // Implícito
   let phoneNumber = 3315015804
   ```

2. **Boolean**. Es el tipo de dato más simple en TypeScript ya que solo acepta dos tipos de valores que son `true` o `false`.

   ```tsx
   // Explícito
   let isTrue: boolean = true
   
   // Implícito
   let isFalse = false
   ```

3. **String**. Es el tipo de dato para trabanar con datos textuales o cadenas, para definir este tipo de dato se puede usar comilla dobles `""` o simples `''`.

   ```tsx
   // Explícito
   let userName: string = 'Alejandra'
   
   // Implícito
   let lastName = 'Camacho'
   ```

   Dentro del tipo de dato *string* también podemos crear *templates* de la siguiente forma:

   ```tsx
   // Template string
   // Uso de back-tick ``
   let userInfo: string = `
     User info:
     firstName: ${firstName}
     lastName: ${lastName}
     phone: ${phone}
     isValid: ${isTrue}
   ```

Usar los comandos tsc --watch y node dist/main constantemente. Sugiero agregar un package.json con el comando **npm init -y** en tu directorio del proyecto.

Una vez que genere el package.json agregar los siguientes comandos dentro de **scripts**

```jso
  "scripts": {
    "watch": "tsc --watch",
    "start": "node dist/main"
  },
```

Deja el archivo de referencia:

```json
{
  "name": "typeScript",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "watch": "tsc --watch",
    "start": "node dist/main"
  },
  "keywords": [
    "typeScript",
    "javaScript"
  ],
  "author": "Tu Nombre",
  "license": "MIT"
}
```

Una vez que tienes el archivo abrir una terminal agregar el comando:

```bash
npm run watch
```

Para validar los cambios en consola, en otra terminal agregar el comando

```bash
npm start
```

## Any

> Podriamos decir que el uso de **any** es quitarle el super poder de tipado a TypeScript.

**Tipo: Any**

- Usado para capturar valores dinámicos
- Los valores pueren cambiar de tipo en el tiempo:
  – APIs externas
  – Librerías de terceros

<img src="https://i.ibb.co/4thzpB3/any.jpg" alt="any" border="0">

## Void y never

**Tipo Void:** Representa la ausencia de tipo. usado en funciones que no retornan nada.
**Tipo Never:** Representa funciones que lanzan excepciones o nunca retornan un valor.

Código de la clase:

```tsx
// type void for functions
// Explicit type

function showInfo(user: any): any {
  console.log(`User Info ${user.id} ${user.username} ${user.firstname}`);
  //   return 'hola';
}

showInfo({id: 1, username: 'Antúnez Durán', firstname: 'Francisco Javier'});

// Inferred type
function showFormattedInfo(user: any) {
  console.log(`User Info,
        id: ${user.id}
        username: ${user.username}
        firstname: ${user.firstname}`);
}

showFormattedInfo({id: 1, username: 'Antúnez Durán', firstname: 'Francisco Javier'});

// Type void as variable data type
let unusable: void;
// unusable = null; --> colocar "strict": false en tsconfig.json para poder hacer uso
unusable = undefined;

// Type never
function handleError(code: number, message: string): never {
  // Process your code
  // Generate a message
  throw new Error(`${message}. Code: ${code}`);
}

try {
  console.log('La funcion handleError no devuelve nada bajo esta linea');
  handleError(404, 'Not found');
} catch (error) {}

function sumNumbers(limit: number): never {
  let sum = 0;
  while (true) {
    sum++;
  }
  // return sum;
}

sumNumbers(10); // --> Llamada a un bucle infinito no acabaria nunca, typescript no compila, al verlo.
```

## null y undefined

- En TypeScript, ambos “valores” tienen sus respectivos tipos:
  - null
  - undefined
- Null y Undefined se pueden asumir como subtipos de los otros tipos de datos.
- Significa que se pueden asignar null y undefied a una variable de tipo string, por ejemplo.

<h5>Opción --strictNullChecks</h5>

- Solo permite asignar null y undefined a una variable de tipo any o a sus respectivos
- Ayua a evitar errores comunes en programación de apps en el ámbito JavaScript

Generará un reporte de los errores que encuentre, hacemos `tsc nombreDelArchivo.ts --strictNullChecks`

```tsx
//  ------------------ NULL ------------------
// Explicita
let nullVariable: null;
nullVariable = null;
// nullVariable = 1; // --> Error

// Inferido
let otherVariable = null;   // --> any
otherVariable = 'test';

console.log('nullVariable : ', nullVariable);
console.log('otherVariable : ', otherVariable);

//  ----------------- UNDEFINED -----------------
let undefinedVariable: undefined = undefined;
// undefinedVariable = 'test'; // --> Error

let otherUndefined = undefined;     // --> any
otherUndefined = 1;

console.log('undefinedVariable : ', undefinedVariable);
console.log('otherUndefined : ', otherUndefined);

//  ------- NULL y UNDEFINED: como subtipos -------

/*
 * tsc --watch src/type-null-undefined.ts --strictNullChecks
 * Podemos ver las lineas del flag --strictNullChecks: para ver el numero de linea del error
 * Archivo tsconfig.json "strictNullChecks": true
 */
let albumName: string;
// albumName = null;
// albumName = undefined;
```

Se puede utlizar **null** y **undefined ** como tipos
Estos hacen que sus valores solo sean null y undefined respectivamente.
Son subtipos en any.

Flag del compilador de typescript --strictNullChecks. Solo permite asignar null y undefined a una variable de tipo **any** o sus tipos respectivos. Ayuda a evitar errores comunes.

![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)[GitHub - luixaviles/platzi-typescript at 05-null-undefined](https://github.com/luixaviles/platzi-typescript/tree/05-null-undefined)

## Object

puede ser de utilidad cuando uno quiere evitar mutar una variable. En el ejemplo voy a guardar user en una variable auxiliar newUser, luego voy a actualizar el nombre del primer usuario y finalmente voy a mostrar sus resultados.

caso 1) Object

```tsx
const user = { 
  id: 1, 
  name: 'Harry' 
}

const newUser = user
user.name = 'Henry'; 
console.log('user', user); // user { id: 1, name: 'Henry' }
console.log('newUser', newUser); // newUser { id: 1, name: 'Henry' }
```

caso 2): object

```tsx
let user: object;
user = {}
user = { 
  id: 1, 
  name: 'Harry' 
}

const newUser = user
// user.name = 'Henry' // Error
user = {...user, name: 'Henry'} 
console.log('user', user); // user { id: 1, name: 'Henry' }
console.log('newUser', newUser); // newUser { id: 1, name: 'Harry' }
```

Como podemos observar debido a que en el caso 1 es posible acceder a la propiedad name se puede llegar a mutar la variable lo que puede causar errores, mientras que para el caso 2 debido a que no es posible acceder a las propiedades del objeto nos vemos forzados a buscar otra alternativa que causa que las variables no muten.

**Diferencias entre object y Object.**
Object: instancia de la clase Object de Javascript
object: tipo para valores no primitivos. Con este tipo no se puede acceder a las propiedades del objeto.

- **Object**

  Es un tipo de dato no primitivo.

  **Declarar a una variable con el tipo object no es lo mismo que crear un Object nativo de JS**

  Si tenemos un objeto declarado con el object de TS no podremos acceder a sus atributos mientras que si lo hacemos regularmente como en vanilla JS si podremos hacerlo.

  Por lo tanto declarar una variable como object de TS nos puede llegar a servir para una situacion en la que no querramos que el objeto pueda mutar.

  **Sintaxis:**

  ```tsx
  //Explicito
  let user: object = { id: 234, name: "Federico"}
  
  //Implicito
  //Este objeto sera creado como un objeto nativo de JS
  let user = { id: 234, name: "Federico"}
  ```

## Array

Dos notaciones: [] y Array`<Type>`
ejemplo:

```tsx
const names = string[];
const months = Array<string>;
```

Se puede acceder con indices.

```tsx
names[0];
months[5];
```

se puede usar la prop **length**
se puede usar el method **push**
Supongo que todos los de Array. Buscar en MDN.

1. **Array**. Al igual que en JavaScript, TypeScript permite definir un arreglo para contener un conjunto de valores, para definir los arreglos se pueden usar nos notaciones `[]` y `Array<type>`.

   ```tsx
   // Corchetes []
   const nickname: string[] = ['Syaan', 'Matt', 'Lou']
   const idNumber: number[] = [1, 2, 3, 4, 5]
   
   // Array<type>
   const pictureTitles: Array<string> = ['Favorite Sunset', 'Vacation Time', 'Landscape']
   ```

## Tupla

**Tupla**: Permite expresar un arreglo con un numero **fijo** de elementos. Los tipos de datos son conocidos.
Asi se crean las tuplas.

```tsx
let userInfo = [number,string];
userInfo = [1,'danijazzero'];
```

Se accede con indices.

```tsx
userInfo[0]; // esto es 1
userInfo[1]; // esto es 'danijazzero'
```

Definimos arreglo de tuplas

```tsx
let usersInfo = [number,string][] = [];
//usamos push para agregar users a usersInfo
```

- En TS, las tuplas permiten expresar un arreglo con un número fijo de elementos.
- La sintaxís es la misma que en un array `[]` pero los tipos de datos deben ser distintos

> Una tupla sirve para agrupar, como si fueran un único valor, varios valores que, por su naturaleza, deben ir juntos.

Tupla, array con número fijo de elementos, los tipos de dato son diferentes. se accede por indices. A los incies les puedo agregar métodos de cadenas.
Para hacer un array de tuplas se usan [] dsp de el tipado. Se pueden usar métodos para modificar las tuplas utilizando los índices (de la tupla & del índica a modificar)

**Hay que declarar el arreglo vacío:**

```tsx
let array:[number, string][] = [];
```

```tsx
// Tipo tuple
// Tuplas: permiten expresar un arreglo con un numero fijo de elementos

export {}; // -> user ya fue declarado en otro archivo

// [1, 'user']
let user: [number, string]; // -> user ya fue declarado en otro archivo
user = [1, 'luixaviles'];

console.log('user : ', user);
console.log('username : ', user[1]);
console.log('username.length : ', user[1].length);
console.log('id : ', user[0]);

// Tuplas con varios valores
// id, username, isPro
let userInfo: [number, string, boolean];
userInfo = [2, 'paparazzi', true];
console.log('userInfo : ', userInfo);

// Arreglo de Tuplas
let array: [number, string][] = [];
array.push([1, 'luixaviles']);
array.push([2, 'paparazzi']);
array.push([3, 'lensQueen']);   // indice: 2
console.log('array : ', array);

// Uso de funciones array
// lensQueen --> lensQueen001
array[2][1] = array[2][1].concat('001');  // --> concatena
console.log('array : ', array);
```

[Documentación oficial de Tuplas](https://www.typescriptlang.org/docs/handbook/2/objects.html#tuple-types)

## Enum

La manera de **extender** un enum en TypeScript es simplemente asignando nuevos valores al enum que queremos extender, es decir:

```tsx
enum Color {
    Red,
    Green,
    Blue
}

enum Color {
    DarkRed = 3,
    DarkGreen,
    DarkBlue
}
```

Tomando en cuenta que es necesario *reinicializar* el primer elemento del segundo enum en continuación del primero.

Esto se puede lograr ya que una vez compilado nuestro código a JavaScript un enum no deja de ser un objeto literal, es por eso que se pueden asignar nuevos valores.

- Los enumerados permiten definir un conjunto de **constantes con nombre**
- Tienen la ventaja de adapartse al contexto de la aplicación

[Documentación oficial de enums en TS](https://www.typescriptlang.org/docs/handbook/enums.html)

[Enums are open ended - TypeScript Deep Dive](https://basarat.gitbook.io/typescript/type-system/enums#enums-are-open-ended)

[Number Enums and Strings - TypeScript Deep Dive](https://basarat.gitbook.io/typescript/type-system/enums#number-enums-and-strings)

## Unión de Tipos, Alias y Tipos Literales

Union de tipos. permite definir una variable con multiples tipos de datos. Se usa el pipe para unirlos | .

```tsx
let idUser = number | string;
idUser = 10;
idUser = '10';
```

tambien sirve para los argumentos de una funcion.

Alias de tipos: Permite crear alias como nuevo nombre para un tipo. Usa la palabra reservada type.

```tsx
type IdUser = number | string; 
let idUser : IdUser; 
idUser = 10;
idUser = '10';
```

TIpos literales: Una variable con un tipo literal puede ocntener unicamente uina cadena del conjunto. Se usan cadenas como ‘tipos’, combinados con el simbolo pipe | entre ellos. *Yo lo veo como una forma de enum*.

```tsx
type SquareSize = '100x100' | '500x500' | '1000x1000'
```

Este editor es una basura desde hace años. Si por error paso a la siguiente clase o refresco la pagina, pierdo todo lo que había escrito.

```tsx
enum PicturesSizes {
    small = '75x75',
    medium = '240x180',
    large = '500x375',
    extraLarge = '1024x768',
    superLarge = '3072x2304'
}

const picturaSmall: PictureOrientation = PictureOrientation.Landscape;

type PhotoSizesType = '75x75' | '240x180' | '500x375' | '1024x768' | '3072x2304'

const medium : PhotoSizesType= '240x180';
```

Una función en Typescript puede recibir como parámetro diferentes tipos predefinidos usando **Union Types**

```ts
function whatTime(hour: number | string, minute: number | string) : string {
  return hour+':'+minute;
}

whatTime(1,30) //'1:30'
whatTime('1',30) //'1:30'
whatTime(1,'30') //'1:30'
whatTime('1','30') //'1:30'
```

Usando **Alias** podemos reducir la cantidad de código en los tipos predefinidos.

```ts
type Hours = number | string;
type Minutes = number | string;

function whatTime(hour: Hours, minute: Minutes) : string {
  return hour+':'+minute;
}
```

<h5>Unión de Tipos</h5>

- En TypeScript se puede definir una variable con múltiples tipos de datos: `Union Type`
- Se usa el símbolo de pipe `|` entre los tipos

<h5>Alias de Tipos</h5>

- TypeScript permite crear un alias como nuevo nombre para un tipo
- El alias se puede aplicar también a un conjunto de combinación de tipos
- Se usa la palabra reservada `type`

<h5>Tipos Literales</h5>

- Una variable con un tipo literal puede contener únicamente una cadena del conjunto
- Se usan cadenas como “tipos”, combinados con el símbolo de pipe | entre ellos

```typescript
export {}

//* Por ejemplo, tenemos usuarios que tienen un ID numérico o de tipo string 10, '10'
let idUser: number | string //* Aceptará strings y number
idUser = 10
idUser = '10'

//* Buscar username dado un ID

function getUserNameById(id: number | string) {
  //* Lógica de negocio, find the user
  return id
}

//* No da errores 😄
getUserNameById(10)
getUserNameById('10')

//--------------------------------------------------------------------------------------------

//* Alias de tipos de TS
type IdUser = number | string
type UserName = string
let userID: IdUser
idUser = 10
idUser = '10'

//* Buscar username dado un ID

function getUserName(id: IdUser): UserName {
  //* Lógica de negocio, find the user
  return 'Mike'
}

//----------------------------------------------------------------------------------------------------

//* Tipos literales
//* Por ejemplo, sólo aceptamos fotos de 100x100, 500x500 y 1000x1000
type SquareSize = '100x100' | '500x500' | '1000x1000'
//! let smallPicture: SquareSize = "200x200" //!Esto da un error, no está incluido ese valor en SquareSize
let smallPicture: SquareSize = "100x100"
let mediumPicture: SquareSize = "500x500"
let bigPicture: SquareSize = "1000x1000"

console.log('small Picture: ', smallPicture)
console.log('medium Picture: ', mediumPicture)
console.log('big Picture: ', bigPicture)
```

## Aserciones de tipo

- Cuando el programador puede conocer más que typescript sobre el valor de una variable
- Es un mensaje al compilador: “Confia en mi, se lo que hago”
- Se parece al casting de tipos en otros lenguajes de programación
- Usa dos sintaxis: `<Angle Bracket>` y (variable as tipo)

```tsx
export{}
//<tipo> // Angle Bracket sintax
let username:any;
username = 'luixaviles';

//tenemos una cadena, TS confia en mi!
  //Queremos utilizar la funcion length pero la variable esta declarada con any
    //Entonces lo que hacemos es utilizar <tipoDato> para indicarle que es un string
       //Y poder utilizar length para conocer la longitud del string
let message:string = (<string>username).length > 5? 
                      `Welcome ${username}`:
                      'Username es muy corto';
console.log(message);

let usernameWithId: any  = 'luixviles 1';
//Como obtener el username

username = (<string>usernameWithId).substring(0,10);
console.log(username);

//Sintaxis as
  //Es otra forma de insertar tipos en ts
    //En lugar de los <> podemos utilizar as dentro de los parentisis para especificarle
      // Un tipo de dato a la variable

message = (username as string).length > 5? 
                      `Welcome ${username}`:
                      'Username es muy corto';
console.log(message);

let helloUser: any;
helloUser = "hello paparazzi";
username = (helloUser as string).substring(6);
```

Aserciones de tipos: Mecanismo de conversión de tipos de datos. Se parece al casting de tipos en otros lenguajes de programación.
Usa dos sintaxis.

```tsx
//Angle Bracket: <Type>
let username : any;
username = (<string>'danijazzero').toUpperCase()
///as: variable as type
username = (username as string).toLowerCase()
```

> La prioridad de usar [**as** por encima de **<>**](https://www.typescriptlang.org/docs/handbook/release-notes/typescript-1-6.html#new-tsx-file-extension-and-as-operator) es desde la versión 1.6 de Typescript debido a que había ambigüedad en archivos **.jsx**

Se puede obligar el uso del método con **as** desde el archivo **ts.config.js**

```json
{
  rules: {
    "no-angle-bracket-type-assertion": true
  }
}
```

![img](https://www.google.com/s2/favicons?domain=https://combo.staticflickr.com/pw/favicon.ico)[All sizes | Pelis (y series) decorando el auditorio de Platzi | Flickr - Photo Sharing!](https://www.flickr.com/photos/atalaya/46640526094/sizes/l/)

## Funciones en TypeScript

- Los parámetros en las funciones son tipados
- Se pueden definir parámetros opcionales
- El tipo de retorno puede ser un tipo básico, enum, alias, tipo literal o una combinación de ellos
- Se puede definir que tipo de datos devolvera la funcion

```typescript
//*Crear una fotografía
//*Usando TS, definimos tipos para parámetros

type SquareSize = '100x100' | '500x500' | '1000x1000'

function createPicture(title:string, date:string, size: SquareSize) {
  //*Se crea la fotografía
  console.log('Create picture using: ', title, date, size)
}

createPicture('My picture', '2021/10/22', '1000x1000')
//!createPicture('Disney Vacation', '2018-03-12') //!Esto da un error porque necesita 3 parámetros

//*Parámetros opcionales
//*Solo hace falta escribir '?' al lado del parámetro
function createPhoto(title:string, date:string, size?: SquareSize) {
  //*Se crea la fotografía
  console.log('Create photo using: ', title, date, size)
}

createPhoto('Foto1', '2020-10-10') //! El tercer parámetro es undefined


//* Flat array function
let createPic = (title:string, date:string, size:SquareSize):object => {
  return { title, date, size }
}

const picture = createPic('Platzi session', '2020-03-10', '100x100')
console.log('Picture: ', picture)
```

### Funciones

```tsx
/* DEFINIR TIPOS PARA LAS FUNCIONE */
function Funcion(param1: string, param2: number){}

/* PARAMETROS OPCIONES CON ? */
function Funcion(param1?: string, param2?: number){}

/* PRIMERO LOS OBLIGATORIOS Y LUEGO LOS OPCIONALES */
function Funcion(param1: string, param2?: number){}
```

<h3>Array Function</h3>

```tsx
let funcion = (param1: string, param2: number) : object => 
{
	return {}
}

let funcion = (param1: string, param2: number) : void => 
{}
```

## Resumen

![Screen Shot 2020-07-18 at 12.16.38.png](https://static.platzi.com/media/user_upload/Screen%20Shot%202020-07-18%20at%2012.16.38-ea16d15f-18a3-4476-adc4-49d669c279d0.jpg)

Como dato extra, cuando una función recibe otra función, el tipo de esa función se define de esta forma.

```tsx
function apply(items: number[], fn:(item: number) => number): number{
    ///....
}
```

El valor de retorno se define con =>

Usar never cuando vamos a lanzar excepciones en la función.
Al momento de usar la función, ponerlo en un try catch.

null y undefined son subtipos de void. A su vez, null y undefined son subtipos de :

- number
- boolean
- string
- array
- tuple
- enum
- any
- void
- object

# 4. Tipos Personalizados

## Interfaces

Se constituyen como forma de contratos para el codigo que vaya a implementarla.

Es una forma de definir que propiedades y metodos deben implementar si o si las propiedades que se declaren de este tipo de interfas

```tsx
// INTERFACES
/* Las interfaces una forma poderosa de definir 'contratos' tanto para tu proyecto, como para el codigo externo */

// Funcion para mostrar una Fotografia
export {}

enum PhotoOrientation {
    Landscape,
    Portrait,
    Square,
    Panorama
}
interface Picture {
    title: string,
    date: string,
    orientation: PhotoOrientation
}
function showPicture(picture: Picture) {
    console.log(`[title: ${picture.title}, date: ${picture.date}, orientation: ${picture.orientation}]`);
}
let myPic = {
    title: 'Test title',
    date: '2020-03',
    orientation: PhotoOrientation.Landscape
}
showPicture(myPic);
showPicture({
    title: 'Test Title',
    date: '2020-03',
    orientation: PhotoOrientation.Portrait,
    // extra: 'test'   // --> Error
}); // objecto anonimo
```

> Una interfaz es como un molde para un objeto. Si el objeto no encaja en el molde, te va a dar error.
>
> Una interface para defir los parámetros y el tipado para invocarlo en las funciones.

Interfaces, contrato para el código. Palabra reservada interface, defir los parámetros y el tipado para invocarlo en las funciones.
Objeto anónimo: No defino una variable pero le envio la configuración necesaria.

- **Interface ** : Permite definir tipos personalizados (un contrato) para que se adapte a un contexto para el problema que se resuelve. Las interfaces permiten una forma poderosa de definir contratos dentro del proyecto y así también para un código externo. No se puede violar éste contrato por lo que daría un error.

  En nuestro caso, necesitamos definir entidades para cada fotografía dentro del App. Y las interfaces juegan un papel importante para que la fotografía, dentro del App como en el código, se pueda mantener bajo éste contrato.

```tsx
export {}

enum PhotoOrientation {
    Landscape,
    Portrait,
    Square,
    Panorama,
};

interface Picture {
    title: string,
    date: string,
    orientation: PhotoOrientation
};

let makePictures = (picture: Picture) => {
    console.log({ picture });
}
let myObjPicture = { title: 'Andrés', date:'10-07-2020', orientation: PhotoOrientation.Square };

makePictures(myObjPicture);
```

## Interfaces: propiedades opcionales

Propiedades opcionales : No todas las propiedades de una interfaz podrian ser requeridas. Usamos el simbolo ‘?’ luego del nombre de la propiedad.

```tsx
interface PictureConfig {
	title: string;
	date?: string;
	orientation: PhotoOrientation
}
```

Propiedades de solo lectura: Algunas propiedades de la interfaz podrian no ser modificables una vez creado el objeto. Esto es posible usando **readonly** antes del nombre de la propiedad.

```tsx
interface User {
	readonly id: number;
	username: string;
	readonly isPro:boolean;
}
```

```tsx
interface PictureConfig {
    title?: string;
    date?: string;
    orientation?: PhotoOrientation
}
function generatePicture(config: PictureConfig) {
    const pic = { title: 'Default', date: '2020-03' };
    if (config.title) {
        pic.title = config.title;
    }
    if (config.date) {
        pic.date = config.date;
    }
    return pic;
}
let picture = generatePicture({});
console.log('picture : ', picture);
picture = generatePicture({title: 'Travel Pic'});
console.log('picture : ', picture);
picture = generatePicture({title: 'Travel Pic', date: '2012-05'});
console.log('picture : ', picture);

// Interfaz: usuario
interface User {
    readonly id: number; // solo lectura
    username: string;
    isPro: boolean
}
let user: User;
user = { id: 10, username: 'luixaviles', isPro: true }
console.log('user : ', user);
user.username = 'paparazzi';
// user.id = 20; // --> Error
console.log('user : ', user);
```

## Extensión de interfaces

La herencia es un mecanismo para poder reutilzar código dentro de la programación orientada a objetos. TS provee esto con las interfaces. Las interfaces pueden extenderse de otras. Esto permite copiar los miembros ya definidos en una interfaz a otra, ganando flexibilidad y reusabilidad de componentes.

```typescript
export {}

enum PhotoOrientation{
  Landscape,
  Portrait,
  Square,
  Panorama
}

//*Herencia de interfaces

interface Entity {
  id: number,
  title: string,
}

interface Album  extends Entity{
  //*Copia de los atributos de Entity
  description: string
}

interface Picture extends Entity {
  orientation: PhotoOrientation
}

const album: Album = {
  id: 5,
  title: 'Meetups',
  description: 'Community events around the world'
}

const picture: Picture = {
  id: 10,
  title: 'Family',
  orientation: PhotoOrientation.Square
}

let newPicture = {} as Picture
newPicture.id = 2
newPicture.title = 'Moon'

console.table({album})
console.table({picture})
console.table({newPicture})
```

> **Herencia**
> Extención de interfaces, Los miembros definidos a una interfaz pueden ser copiados para flexibilidad y ahorro de componentes.
>
> ```tsx
> interface NameObj extends NameOb
> ```
>
> j. Cambiar el estado de los objetos.

<h3>Extender Interfaces</h3>

Las interfaces pueden extenderse unas de otras. Esto permite copiar los miembros ya definidos en una interfaz a otra, ganando flexibilidad y reusabilidad de componentes.

```tsx
enum PhotoOrientation {
	Landscape,
	Portrairt,
	Square,
	Panorama
}

interface Entity{
	id: number;
	title: string;
}

interface Album extends Entity{
	description: string;
}

interface Picture extends Entity{
	orientation: PhotoOrientation;
}

const album: Album = {
	id: 1,
	title: 'Meetups,
	description; 'community events around the world'
};
```

## Clases

- Las clases y a la POO, se puede conectar las diferentes entidades y se puede relacionar
- Una clase es la asbtracción de un conjunto de objetos
  ejemplo:
  Para definir una entidad usuario, se hace uso de una clase: Se debe definir una clase que
  dentro tenga una función que permita crear objetos a partir de esta Clase y poder interactuar con ellos.

```tsx
export {}

enum PhotoOrientation {
    Landscape,
    Portrait,
    Square,
    Panorama,
};

class Picture {
    // properties
    id: number;
    title: string;
    orientation: PhotoOrientation;
    //constructor
    constructor( id: number, title: string, orientation: PhotoOrientation ) {
        this.id = id;
        this.title = title;
        this.orientation = orientation;
    }
    //Performance
    toString() {
        return `[id: ${this.id}, title: ${this.title}, orientation: ${this.orientation}]`
    }
}

class Album {
    id: string;
    title: string;
    pictures: Picture[];

    constructor( id: string, title: string ) {
        this.id = id;
        this.title = title;
        this.pictures = [];
    }    

    addPicture(picture: Picture) {
        this.pictures.push(picture);
    }
}

let album: Album = new Album('stories-1', 'Photos of mine');
const newPic: Picture = new Picture(2, 'my new pic!', PhotoOrientation.Panorama);
const new2Pic: Picture = new Picture(3, 'my 2nd new pic!', PhotoOrientation.Portrait);
const new3Pic: Picture = new Picture(4, 'my 3rd new pic!', PhotoOrientation.Portrait);
album.addPicture(newPic);
album.addPicture(new2Pic);
album.addPicture(new3Pic);
console.log('album -> ', album);
```

Podemos hacer `console.log del objeto`
También podemos hacer `console.table` de ese mismo objeto
Y como tenemos un array en el objeto, podemos hacer también `console.table` de ese array dentro del objeto.

![logTable.jpg](https://static.platzi.com/media/user_upload/logTable-88fc0d51-5fb6-4147-b2c9-f34ddf6a8d95.jpg)

## Clases públicas y privadas

Para solucionar el “error” al colocar # sólo hay que cambiar el target en el tsconfig.json:

![carbon (1).png](https://static.platzi.com/media/user_upload/carbon%20%281%29-7bcec79e-57cd-468d-b5e3-1e90de602416.jpg)

Clases - miembros públicos: define un modificador de acceso publico por defecto para los miembros de la clase. También es posible marcar un miembro como publico usando la palabra reservada **public**
Clases - miembros privados: define una manera propia de declarar o marcar un miembro como privado usando la palabra reservada **private**

```tsx
class Person{
	private id:number;
	private name: string;
	public constructor(){}
	public getName(){
		return this.name
	}
}
```

Miembros privados ECMAScript: soporta (a partir de la versión 3.8) la nueva sintaxis JavaScript para miembros privados: #atributo. Esta caracteristica puede ofrecer mejores garantias de aislamiento en miembros privados

```tsx
class Person{
	#id:number;
	#name: string;
	public constructor(){}
	public getName(){
		return this.#name
	}
}
```

Para que VSCode no nos tire errores por usar esta sintaxis, instalar JavaScript and TypeScript Nightly

Usar la sintaxis **#** para miembros privados es mejor porque nos garantiza encapsular nuestros miembros privados. a diferencia de usar **private**

> Un pequeño tip sobre la pronunciación de la palabra ***private\***
> Es común para nosotros latino pronunciarla como *priveit*. Diría que un 95% de latinos la pronuncian así. Pero la correcta pronunciación sería algo como *praivt* pudiendo poner también una **e** en la última sílaba *praivet*

Aún agregando la extensión sugerida, el error sigue apareciendo. Debemos agregar una propiedad a nuestro archivo tsconfig.json:
`{ "compilerOptions": { ... "target": "es5" } }`

- TS define un modificador de acceso publico por defecto para los miembros de clase ( public )
- TS define un modificador de acceso privado para los miembros que no se deseen ser leídos por fuera de la clase ( private )

- con TS se tiene una sintaxis para métodos privados. Usando el signo # antes del nombre del miembro de clase: p.e. #atributo
  Esto se hace debido a que ofrece garantías de aislamiento en miembros privados.
  ( Private identifiers are only available when targeting ECMAScript 2015 and higher. )

```tsx
export {}

enum PhotoOrientation {
    Landscape,
    Portrait,
    Square,
    Panorama,
};

class Picture {
    // properties
    #id: number;
    #title: string;
    #orientation: PhotoOrientation;
    //constructor
    public constructor( id: number, title: string, orientation: PhotoOrientation ) {
        this.#id = id;
        this.#title = title;
        this.#orientation = orientation;
    }
    //Performance
    private toString() {
        return `[id: ${this.#id}, title: ${this.#title}, orientation: ${this.#orientation}]`
    }
}

class Album {
    #id: string;
    #title: string;
    #pictures: Picture[];

    public constructor( id: string, title: string ) {
        this.#id = id;
        this.#title = title;
        this.#pictures = [];
    }    

    private addPicture(picture: Picture) {
        this.#pictures.push(picture);
    }
}

let album: Album = new Album('stories-1', 'Photos of mine');
/** Don't care about commented code below. The classes has private members, the output will be empty  */
// const newPic: Picture = new Picture(2, 'my new pic!', PhotoOrientation.Panorama);
// const new2Pic: Picture = new Picture(3, 'my 2nd new pic!', PhotoOrientation.Portrait);
// const new3Pic: Picture = new Picture(4, 'my 3rd new pic!', PhotoOrientation.Portrait);
// album.addPicture(newPic); there's an error due to private class props
// album.addPicture(new2Pic); there's an error due to private class props
// album.addPicture(new3Pic);  there's an error due to private class props
console.log('album -> ', album);
```

## Métodos Get y Set

TypeScript soporta getters/setters como una forma de interceptar accesos a un miembro de un objeto. Esto le da una forma de tener un control más fino sobre cómo se accede a un miembro en cada objeto.

Convirtamos una clase simple para usar get y set. Primero, empecemos con un ejemplo sin getters y setters.

```tsx
class Employee {
  fullName: string;
}

let employee = new Employee();
employee.fullName = "Bob Smith";

if (employee.fullName) {
  console.log(employee.fullName);
}
```

Aunque permitir que la gente establezca directamente el nombre completo al azar es bastante útil, también podemos querer imponer algunas restricciones cuando se establece el nombre completo.

En esta versión, añadimos un configurador que comprueba la longitud del nuevo Nombre para asegurarse de que es compatible con la longitud máxima de nuestro campo de base de datos de respaldo. Si no lo es, arrojamos un error notificando al código del cliente que algo salió mal.

Para preservar la funcionalidad existente, también añadimos un simple programa que recupera el nombre completo sin modificar.

```tsx
const fullNameMaxLength = 10;

class Employee {
  private _fullName: string;

  get fullName(): string {
    return this._fullName;
  }

  set fullName(newName: string) {
    if (newName && newName.length > fullNameMaxLength) {
      throw new Error("fullName has a max length of " + fullNameMaxLength);
    }

    this._fullName = newName;
  }
}

let employee = new Employee();
employee.fullName = "Bob Smith";

if (employee.fullName) {
  console.log(employee.fullName);
}
```

Para probarnos a nosotros mismos que nuestro accesorio está comprobando ahora la longitud de los valores, podemos intentar asignar un nombre de más de 10 caracteres y verificar que obtenemos un error.

Un par de cosas a tener en cuenta sobre los accesorios:

Primero, los accessores requieren que el compilador dé como resultado ECMAScript 5 o superior. La bajada de nivel a ECMAScript 3 no está soportada. Segundo, los accesorios con un get y sin set se infieren automáticamente para ser de solo lectura. Esto es útil cuando se genera un archivo.d.ts a partir de su código, porque los usuarios de su propiedad pueden ver que no pueden cambiarlo.

_(underscore) Es una simple convención que indica una variable privada.

```tsx
   private _id: number;
   private _title: string;
   private _orientation: PhotoOrientation;
```

**Codigo clase**

```tsx
export {};
console.clear();

enum PhotoOrientation {
  Landscape = 'Landscape',
  Portrait = 'Portrait',
  Square = 'Square',
  Panorama = 'Panorama',
}

// get y set
class Picture {
  private _id: number;
  private _title: string;
  private _orientation: PhotoOrientation;

  constructor(id: number, title: string, orientation: PhotoOrientation) {
    this._id = id;
    this._title = title;
    this._orientation = orientation;
  }

  get id(): number {
    return this._id;
  }
  set id(id: number) {
    this._id = id;
  }

  get title(): string {
    return this._title;
  }
  set title(title: string) {
    this._title = title;
  }

  get orientation(): PhotoOrientation {
    return this._orientation;
  }
  set orientation(orientation: PhotoOrientation) {
    this._orientation = orientation;
  }

  // Comportamiento
  public toString() {
    return `[id: ${this.id}, title: ${this.title}, orientation: ${this.orientation}]`;
  }
}

class Album {
  #id: number;
  #title: string;
  #pictures: Picture[] = [];

  constructor(id: number, title: string) {
    this.#id = id;
    this.#title = title;
    // this.pictures = [];
  }

  get id(): number {
    return this.#id;
  }
//   set id(id: number) {
//     this.#id = id;
//   }

  get title(): string {
    return this.#title;
  }
  set title(title: string) {
    this.#title = title;
  }

  addPicture(picture: Picture) {
    this.#pictures.push(picture);
    // this.pictures.push({...picture});
  }
}

const picture: Picture = new Picture(100, 'cool', PhotoOrientation.Square);
const picture1 = new Picture(201, 'korn', PhotoOrientation.Square);
const album: Album = new Album(534, 'Family');
console.log(picture);
console.log(picture1);
album.addPicture(picture);
album.addPicture(picture1);

console.log('album', album);

// Accediendo a los miembros publicos
// picture.id = 100;
picture.title = 'Another title';
console.log('album', album);
// console.log(album.id);

picture.title = 'New title';
```

## Herencia de clases y propiedades estáticas

**sbtract**: cuando requeires que una clase no sea instanciada
**protected**: atributo de los miembros para que puedan ser accedidos desde su propia clase y también que puedan ser solo accedidos a quienes heredan esa clase con implements(herencia)
**implements**: herencia claseA extends claseB { … }
**static**: para poder acceder a métodos/propiedades de una clase sin la necesidad de la instancia(new ClaseA()) sino const a = ClaseA.propiedad

> Por convención los nombres de las clases siempre deben ir con inicial mayúscula.

Herencia de clases y miembros protegidos
Typescript soporta este patrón común en el mundo de la POO
Implementa la habilidad de extender codigo de clases existentes a través de la **herencia**.
Utilizamos la palabra **extends** para heredar
Se heredan solo los atributos **public** o **protected**
tenemos acceso al constructor de la clase padre **super()**

```tsx
class Person {
	protected id:number;
	protected name:string;
	constructor(id:number, name:string){
	
	}
}
class Student extends Person {
	private active:boolean
	constructor(id:number, name:string, active:boolean){
		super(id,name)
		this.active = active
	}
}
```

Clases abstractas: Las clases abstractas son la base de donde otras clases podrian derivarse. A diferencia de una interfaz, una clase abstracta puede implementar funciones para sus instancias.
La palabra reservada es **abstract**

```tsx
abstract class myClass{}
```

Recordar que las clases abstractas no se pueden instanciar
Propiedades estaticas y de solo lectura
Las clases por lo general definen atributos y métodos aplicables a las instancias de las mismas. A través de la palabra reservada **static ** se puede definir un miembro visible a nivel de clase
Al igual que las interfaces, podemos usar la palabra reservada **readonly** para marcar el miembro de una clase como solo lectura

```tsx
class Person {
	static personQuantity: number = 0
	protected readonly id: number
}
```

a las propiedades static se las accede a través de la clase

```tsx
console.log(Person.personQuantity)
```

a las propiedades readonly no se las puede modificar.

```tsx
export {};

enum PhotoOrientation {
    Landscape,
    Portrait,
    Square,
    Panorama
}
// SUPERclase
abstract class Item {
    protected readonly _id: number;
    protected _title: string;

    constructor(id: number, title: string) {
        this._id = id;
        this._title = title;
    }

    get id() {
        return this._id;
    }
    // set id(id: number) {
    //     this._id = id;
    // }
    get title() {
        return this._title;
    }
    set title(title: string) {
        this._title = title;
    }
}


// get y set

class Picture extends Item{
    public static photoOrientation = PhotoOrientation;
    // Propiedades
    private _orientation: PhotoOrientation;

    public constructor(id: number, 
                title: string, 
                orientation: PhotoOrientation) {
        super(id, title);
        this.orientation = orientation;
    }
    get orientation() {
        return this._orientation;
    }
    set orientation(o: PhotoOrientation) {
        this._orientation = o;
    }

    // Comportamiento
    public toString() {
        return `[id: ${this.id}, 
                 title: ${this.title}, 
                 orientation: ${this.orientation}]`;
    }
}

class Album extends Item{
    private pictures: Picture[];

    public constructor(id: number, title: string) {
        super(id, title);// constructor de SUPER clase
        this.pictures = [];
    }
    public addPicture(picture: Picture) {
        this.pictures.push(picture);
    }
}

const album: Album = new Album(1, 'Personal Pictures');
const picture: Picture = new Picture(1, 'Platzi session', PhotoOrientation.Square);
album.addPicture(picture);
console.log('album', album);

// Accediendo a los miembros publicos
console.log('picture.id', picture.id); // get id()
// picture.id = 100; // private, set id(100);
picture.title = 'Another title'; // private
album.title = 'Personal Activities'; //private
console.log('album', album);

// const item = new Item(1, 'Test title');
// console.log('item', item);

// Probar el miembro estatico
console.log('PhotoOrientation', Picture.photoOrientation.Landscape);
```

## Resumen

Esta clase es muy importante para entender Angular 💚

> ¿Angular? Para entender los módulos de JS, para usar React, para usar Vue, para tener código modularizado y limpio, para todo.

**Codigo de la clase**

*photo-app.ts*

```tsx
export enum PhotoOrientation {
	Landscape,
	Portrait,
	Square,
	Panorama
}

export class Item {
	constructor(public readonly id: number, protected title: string) {}
}

export class User {
	private album: Album[];

	constructor(private id: number, private username: string, private firstName: string, private isPro: boolean) {
		this.album = [];
	}

	addAlbum(album: Album) {
		this.album.push(album);
	}
}

export class Album extends Item {
	private pictures: Picture[];

	public constructor(id: number, title: string) {
		super(id, title);
		this.pictures = [];
	}
	public addPicture(picture: Picture) {
		this.pictures.push(picture);
	}
}

export class Picture extends Item {
	public constructor(id: number, title: string, private _date: string, private _orientation: PhotoOrientation) {
		super(id, title);
	}
	public toString() {
		return `[id: ${this.id},
                 title: ${this.title},
                 orientation: ${this._orientation}]`;
	}
}
```

*main.ts*

```tsx
import { User, Album, Picture, PhotoOrientation } from './photo-app';

const user = new User(1, 'Erickowski', 'Erick', true);
const album = new Album(10, 'Platzi Album');
const picture = new Picture(1, 'Foto', '2020-08', PhotoOrientation.Landscape);

// Creamos relaciones
user.addAlbum(album);
album.addPicture(picture);

console.log('user', user);
```

Módulos en TypeScript: Los módulos en typescript proveen un mecanismo para una mejor organización del código y promueven su reutilización
A partir de ECMAScript 2015 los módulos son parte nativa del lenguaje Javascript

Importando y exportando modulos: Generalmente se define un modulo con la idea de agrupar codigo relacionado. Podemos tomar criterios en torno a la funcionalidad, features, utilitarios, modelos, etc.

Los miembros de modulo interactúan con el uso de las palabras reservadas **import** y **export**.

> Como estamos manejando objetos y arrays, recomiendo usar `console.table()`
>
> ```tsx
> console.table(user)
> console.table(album)
> console.table(picture)
> ```

**Módulos**

Mecanismo para una mejor organización.
Código relacionado será un módulo.
`import y export.`

```bash
tsc --init
```

crear src para almacenar `js,` en el `archivo json outDir / dist`, en el archivo principal contentrá toda la definición del código. Se usa export antes de las clases para decirle que voy a exportar y a cada hoja importar lo deseado.

# 5. Módulos

## Principios de responsabilidad única

Principio de responsabilidad unica. Idealmente un archivo deberia tener un proposito o responsabilidad unica: definir una clase, una interfaz, un enumerado, etc.
Esto mejora la legibilidad de codigo, facilita la lectura, testing y favorece su mantenimiento.

Utilizamos archivos separados y la utilizacion de import, export para lograr un poco mas de mantenibiilidad. Podemos usar tambien carpetas para separar nuestros archivos.

Para observar una carpeta entera usamos:

```tsx
tsc --project myFolder --watch
```

- **Como defender el Principio de Responsabilidad Única** :
  Para defender el Principio de Responsabilidad Única en nuestro código podemos apoyarnos en mejorar dos aspectos del mismo. Estos son la cohesión y el acoplamiento, de forma que:

1. Mantener una alta cohesión, es decir, mantener ‘unidas’ funcionalidades que estén relacionadas entre sí y mantener fuera aquello que no esté relacionado. El objetivo es aumentar la cohesión entre las cosas que cambian por las mismas razones.
2. Mantener un bajo acoplamiento, es decir, reducir al máximo posible el grado de la relación de un clase o módulo con el resto, para favorecer crear código más fácilmente mantenible, extensible y testeable. El objetivo es disminuir el acoplamiento entre aquellas cosas que cambian de forma diferente.

Estos dos aspectos favorecerá un código con funcionalidades más concretas y mejor especificadas.
***Reúne las cosas que cambian por las mismas razones. Separa las cosas que [cambian por diferentes razones.](https://medium.com/@mpijierro/principios-solid-principio-de-responsabilidad-única-13eb4d5537c1)***

## Resolviendo módulos

Resolviendo Modulos: Typescript resuelve la ubicacion de modulos observando referencias relativas y no relativas.
Posteriormente intenta localizar el modulo usando una estrategia de resolucion de modulos.

```bash
tsc --moduleResolution node
tsc --moduleResolution classic
```

diferencias
node: Modulos CommonJs o UMD, mas opciones de configuración
classic: Modulos AMD, System, ES2015, poco configurable

en tsconfig.json

```json
"moduleResolution": "node|classic"
"traceResolution":true
```

Existen los path alias para que no tengamos que lidiar con esa mano de puntos y slash en nuestros proyectos.
Esta configuración la pueden poner en el ts.config.json dentro de compilerOptions y así pueden acceder al shortcut @item para llegar a esa carpeta en específico que están buscando.
Para el ejemplo yo tengo una carpeta item y dentro el archivo index.ts.

```json
"paths": { 
      "@item": ["item/index.ts"],
    }
```

y así se podría importar

```json
import { Item }  from  '@item'
```

```json
// Json
{
  "compilerOptions": {
    /* Basic Options */
    "target": "ES2015",                          /* Specify ECMAScript target version: 'ES3' (default), 'ES5', 'ES2015', 'ES2016', 'ES2017', 'ES2018', 'ES2019', 'ES2020', or 'ESNEXT'. */
    "module": "commonjs",                     /* Specify module code generation: 'none', 'commonjs', 'amd', 'system', 'umd', 'es2015', 'es2020', or 'ESNext'. */
    "outDir": "./dist",                        /* Redirect output structure to the directory. */
    "strict": true,                           /* Enable all strict type-checking options. */
    "moduleResolution":"node",
    "traceResolution": true,
    "esModuleInterop": true,                  /* Enables emit interoperability between CommonJS and ES Modules via creation of namespace objects for all imports. Implies 'allowSyntheticDefaultImports'. */
    "forceConsistentCasingInFileNames": true  /* Disallow inconsistently-cased references to the same file. */
  }
}

```

Documentación de Module Resolution:

> [Module-resolution](https://www.typescriptlang.org/docs/handbook/module-resolution.html)

## Webpack y agrupación de Módulos



```js
module.exports = {
    mode: 'production',
    entry: './src/main.ts',
    devtool: 'inline-source-map',
    resolve: {
        extensions: ['.ts', '.js'],
    },
    output: {
        filename: 'bundle.js',
    }
}
```

```bash
# Instalar webpack
npm install typescript webpack webpack-cli --save-dev

# Ts
npm install ts-loader --save-dev
```



```js
Una forma mas corta de indicar que se agreguen a la parte de dependencias de Development esos módulos es con el comando.

npm i -D typescript webpack webpack-cli
En mi opinión considero que instalar de manera global el compilador en ocasiones puede derivar en errores de actulizacion, lo que yo recomiendo es siempre instalar el modulo typescript en el entorno de desarrollo.

Asi siempre te aseguras de usar la version de TS mas reciente y actualizada.

Después agregar un script al package.json para correr desde los node_modules el compilador de esta forma.

Example package.js

{
  "name": "Curso_Bases_Typescript",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "start": "tsc",
    "dev": "tsc --watch"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "dependencies": {},
  "devDependencies": {
    "typescript": "^3.9.7",
    "webpack": "^4.44.1",
    "webpack-cli": "^3.3.12"
  }
}
```

Para quienes no saben de expresiones regulares

- // son los delimitadores de la expresión.
- \ se utiliza para escapar algún carácter especial, en este caso el punto.
- $ hace referencia al final de la coincidencia.
  Con esto tomando todos los archivos con extensión ts.

# 6. Cierre


## Cierre

![m2.png](https://static.platzi.com/media/user_upload/m2-bed6ef91-c48a-44e1-affd-c1b591a0ee34.jpg)

