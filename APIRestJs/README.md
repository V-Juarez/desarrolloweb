<h1>Consumo de API REST con JavaScript</h1>

<h3>Juan David</h3>

<h2>Tabla de Contenido</h2>

- [Conceptos fundamentales](#conceptos-fundamentales)
  - [¿Qué es una API REST?](#qué-es-una-api-rest)
  - [Flujo de comunicación entre usuarios, frontend y backend](#flujo-de-comunicación-entre-usuarios-frontend-y-backend)
- [Primeros pasos con fetch](#primeros-pasos-con-fetch)
  - [Consume tu primera API REST](#consume-tu-primera-api-rest)
  - [Endpoints y query parameters](#endpoints-y-query-parameters)
  - [¿Qué son los HTTP Status Codes?](#qué-son-los-http-status-codes)
  - [¿Qué es una API KEY?](#qué-es-una-api-key)
- [Proyecto](#proyecto)
  - [Maquetación del proyecto](#maquetación-del-proyecto)
  - [¿Qué son los Métodos HTTP?](#qué-son-los-métodos-http)
  - [GET: leyendo michis favoritos](#get-leyendo-michis-favoritos)
  - [POST: guardando michis favoritos](#post-guardando-michis-favoritos)
  - [Consultas a la API para escribir HTML dinámico](#consultas-a-la-api-para-escribir-html-dinámico)
  - [DELETE: borrando michis favoritos](#delete-borrando-michis-favoritos)
- [Headers en peticiones HTTP](#headers-en-peticiones-http)
  - [¿Qué son los Headers HTTP?](#qué-son-los-headers-http)
  - [Header de autorización](#header-de-autorización)
  - [Header de Content-Type](#header-de-content-type)
  - [FormData: publicando imágenes de michis](#formdata-publicando-imágenes-de-michis)
- [Bonus](#bonus)
  - [Axios: librerías de JavaScript para consumir APIs](#axios-librerías-de-javascript-para-consumir-apis)
  - [CORS, caché, redirect y tu propio clon de fetch](#cors-caché-redirect-y-tu-propio-clon-de-fetch)
  - [GraphQL, Web Sockets y Web 3.0: el mundo más allá de REST](#graphql-web-sockets-y-web-30-el-mundo-más-allá-de-rest)
- [Próximos pasos](#próximos-pasos)
  - [Toma el Curso Práctico de Consumo de API](#toma-el-curso-práctico-de-consumo-de-api)

# Conceptos fundamentales

## ¿Qué es una API REST?

![Infografía](https://i.ibb.co/MSW3V2W/dark-800-2200-px-1.png)

![Captura de Pantalla 2022-05-03 a la(s) 8.23.18 a.m..png](https://static.platzi.com/media/user_upload/Captura%20de%20Pantalla%202022-05-03%20a%20la%28s%29%208.23.18%20a.m.-17df73fb-d66f-47d1-ad20-c2c04ac7afe9.jpg)

## Flujo de comunicación entre usuarios, frontend y backend

![flujo.png](https://static.platzi.com/media/user_upload/flujo-ddef2aa7-0d4f-48b0-9da4-d90fcef9fa9d.jpg)

**\*\*Server Side Rendering - SSR\*\***

Las SSRs son páginas que necesitan traer diferentes archivos HTML del servidor cada vez que el usuario navega por nuestra aplicación, es decir, casi todas las veces que damos click en un link o botón. Aunque estas páginas tienen un tiempo de carga muy corto, la carga debe repetirse.

**\*\*Single Page Applications - SPA\*\***

Las SPAs son páginas que siempre cargan el mismo archivo HTML. Este, a su vez, carga un archivo gigante de JavaScript con toda la lógica de nuestra apliacación (por ejemplo, usando React.js).

Estas páginas tienen una carga inicial muy lenta, ya que no podremos ver la información importante hasta que termine de cargar el archivo de JavaScript. Pero una vez termina la carga inicial, las SPAs son muy rápidas, incluso al navegar por diferentes secciones de nuestra aplicación.

Como el archivo de JavaScript tiene todo el código de nuestra aplicación, el tiempo de navegación pasa de segundos a milisegundos. No necesitamos hacer más requests al servidor. Pero en mucho casos debemos esperar algunos segundos para que termine la carga inicial y podamos utilizar la aplicación.

**¿Cómo las hacemos dinámicas y interactivas?** Pues las APIs del DOM que nos permiten escuchar las interacciones del usuario y la API Fetch son herramientas poderosísimas que combinadas nos dan una alternativa al tener que recargar toda la página HTML cada vez que queremos algún recurso (server side rendering)

Aquí un mini video de cómo sería acceder del Home de Platzi al curso de Javascript sin tener que recargar la página de nuevo 😋

![Un gif](https://i.ibb.co/J5vphNP/Response-del-servidor-con-el-HTML-del-home-de-Platzi-2.gif)

# Primeros pasos con fetch

## Consume tu primera API REST

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - public-apis/public-apis: A collective list of free APIs](https://github.com/public-apis/public-apis)

[![img](https://www.google.com/s2/favicons?domain=https://devhints.io/js-fetch./assets/favicon.png)fetch() cheatsheet](https://devhints.io/js-fetch)

[![img](https://www.google.com/s2/favicons?domain=https://api.thecatapi.com/favicon.ico)Search & Pagination - Home](https://docs.thecatapi.com/1.0/pagination)

## Endpoints y query parameters

![Some image](https://i.imgur.com/gAYHTI0.png)

DOM con JavaScript 😅

```js
const app = document.getElementById("app");
const btn = document.querySelector("button");

const URL_API = "https://api.thecatapi.com/v1/images/search?limit=3";

const reset = () => {
  while (app.firstChild) {
    app.removeChild(app.firstChild);
  }
};

const getData = async () => {
  reset();
  const data = await (await fetch(URL_API)).json();
  data.map(item => {
    const img = document.createElement("img");
    const figure = document.createElement("figure");
    img.src = item.url;
    figure.appendChild(img);
    return app.append(figure);
  });
};
getData();

btn.onclick = () => getData();
```

HTML

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Gatitos Aleatorios</title>
    <link rel="stylesheet" href="./styles.css" />
  </head>
  <body>
    <header>
      <h1>Gatitos aleatorios</h1>
    </header>
    <div class="container">
      <img id="img-cat" alt="Gatitos aleatorios" />
      <button type="button" id="btn-button">Cambiar imagen</button>
    </div>
    <script src="./main.js"></script>
  </body>
</html>
```

CSS

```css
* {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}
html {
  font-size: 62.5%;
}
header {
  display: flex;
  justify-content: center;
}
h1 {
  font-size: 4rem;
}
.container {
  display: flex;
  flex-direction: column;
  justify-content: center;
  place-items: center;
  padding: 20px;
}
img {
  width: auto;
  height: 500px;
  border-radius: 30px;
}
button {
  width: 100%;
  min-width: 300px;
  padding: 10px;
  margin-top: 40px;
  font-size: 16px;
  border-radius: 15px;
  background: #66bb6a;
  cursor: pointer;
  transition: all;
  border: 5px solid #187a1d;
}
button:hover {
  border: 5px solid rgb(228, 164, 94);
  background-color: rgb(227, 141, 67);
}
@media screen and (min-width: 1024px) {
  button {
    width: 20%;
  }
}
```

JAVASCRIPT

```js
//para mandar a llamar una url  se hacen los siguientes pasos

const URL = "https://api.thecatapi.com/v1/images/search";

//mostrar una imagen con boton

async function mycat() {
  const res = await fetch(URL);
  const data = await res.json();
  const img = document.getElementById("img-cat");
  img.src = data[0].url;
}
const myButton = document.getElementById("btn-button");
myButton.onclick = mycat;
```

![Captura gatitos.PNG](https://static.platzi.com/media/user_upload/Captura%20gatitos-384217b2-162a-4251-8e29-769272617308.jpg)

[![img](https://www.google.com/s2/favicons?domain=https://api.thecatapi.com/favicon.ico)Search & Pagination - Home](https://docs.thecatapi.com/1.0/pagination)

## ¿Qué son los HTTP Status Codes?

**HTTP status codes**

- **1XX** Respuestas Afirmativas
- **2XX** Respuestas satisfactorias
- **3XX** Re-direcciones
- **4XX** Error del cliente
- **5XX** Error de servidor

![HTTP 409](https://http.cat/409)

**HTTP 418 I’m a teapot** 🫖 Soy una tetera! en español! Fue una broma del día de los Inocentes! indica que el servidor se reusa a hacer cafe porque es permanentemente una tetera! Fue creado en 1998! pero tiene toda una estructura! Segun mdn web docs, algunos websites lo usan para solicitudes que no desean manejar como por ejemplo, consultas automáticas.

![tetera.png](https://static.platzi.com/media/user_upload/tetera-92614e66-0d04-4551-b504-b3c88e2149b3.jpg)

> Comprensión de los tipos de código de estado HTTP
> Los códigos de estado HTTP se dividen en 5 «tipos». Se trata de agrupaciones de respuestas que tienen significados similares o relacionados. Saber qué son puede ayudarte a determinar rápidamente la sustancia general de un código de estado antes de que vayas a buscar su significado específico.
>
> Las cinco clases incluyen:
>
> 100s: Códigos informativos que indican que la solicitud iniciada por el navegador continúa.
> 200s: Los códigos con éxito regresaron cuando la solicitud del navegador fue recibida, entendida y procesada por el servidor.
> 300s: Códigos de redireccionamiento devueltos cuando un nuevo recurso ha sido sustituido por el recurso solicitado.
> 400s: Códigos de error del cliente que indican que hubo un problema con la solicitud.
> 500s: Códigos de error del servidor que indican que la solicitud fue aceptada, pero que un error en el servidor impidió que se cumpliera.

[![img](https://www.google.com/s2/favicons?domain=https://httpstatusdogs.com/apple-touch-icon.png)HTTP Status Dogs](https://httpstatusdogs.com/)

## ¿Qué es una API KEY?

![img](https://i.imgur.com/Zo66y0B.png)

## API KEY

Son una, no la única, de las formas en que el backend puede identificar quien está haciendo cada solicitud.

Debemos entender dos conceptos importantes, la Autenticación y la Autorización.

## Autenticación

Consiste en identificar quien es cada quien. No sabe que permisos tiene fulano, No sabe que puede o no hacer fulano, Solamente sabe que él es fulano, que ella es pamela o que esa es una persona sin identificar.

## Autorización

Es la que nos dice que permisos tiene cada quien, es decir, si fulano quiere ir a la nevera para comerse un pastel, es la que dice, espérate fulano, tienes permisos para abrir la nevera?, a listo ábrela, tienes permisos de comerte el pastel?, a bueno comételo.

Y además por poner un ejemplo más real, si fulano trata de ver las fotos privadas de pamela, la autorización va a decir, ok quien eres?, la autenticación ya te dijo que eras fulano, a listo perfecto, autenticación me pasas un token, listo ya sé que tu eres fulano, y luego empieza a revisar los permisos, como no los tiene se lo va a prohibir,

Obviamente estos trabajan en conjunto para prohibir o permitir a toda la información que tenemos en nuestra aplicación, y ahí es donde entran las API KEYs.

Estas API Keys son una de las formas en que el backend puede indentificar quien es cada quien.

El backend necesita saber quien esta haciendo cada solicitud, para proteger la información privada de las personas, pero también en muchos casos, para limitar la cantidad o las solicitudes que le hacemos a la aplicación.

Para que nosotros podamos enviarle esta API KEY en cada solicitud que hagamos al backend podemos utilizar varias formas:

- Query parameter: `?apy_key=ABC123`
- Authorization Header: `X-API-Key: ABC123`

## Alternativas

- Authorization: Basic
- Authorization: Barer Token
- OAuth 2.0 (es de las mejores y más complicadas formas de autenticar en la modernidad)
- Access Key + Secret Key

En este caso estamos haciendo una `Application-based authentication` es decir estamos autenticando nuestra aplicación, estamos autenticando a nuestro frontend para que pueda hacer solicitudes al backend, pero hay aplicaciones donde no solamente necesitamos una Application-based authentication, también hay apps que necesitamos usar esta con una `User-based authentication`.

Autenticación

![img](https://us.cdn.eltribuno.com/052022/1652399987200.jpg?&cw=320)

Autorización

![img](https://pbs.twimg.com/media/EqfvFlYXYAQkFC-.jpg)

> Según Google…
>
> El código de error HTTP 418 Soy una tetera indica que el servidor se rehusa a preparar café porque es una tetera. Este error es una referencia al Hyper Text Coffee Pot Control Protocol, creado como parte de una broma del April Fools’ de 1998.

```js
const API_KEY = "xxxxxx-yyyyyyyy-zzzzzzzzz";

const URL = [
  "https://api.thecatapi.com/v1/images/search",
  "?limit=5",
  "&order=Asc",
  `&api_key=${API_KEY}`
].join("");
```

[The Cat API - Cats as a Service.](https://thecatapi.com/)

[![img](https://www.google.com/s2/favicons?domain=https://www.instagram.com/p/CVeEpPivyoG//static/images/ico/apple-touch-icon-76x76-precomposed.png/666282be8229.png)Login • Instagram](https://www.instagram.com/p/CVeEpPivyoG/)

[4 Most Used REST API Authentication Methods](https://blog.restcase.com/4-most-used-rest-api-authentication-methods/)

# Proyecto

## Maquetación del proyecto

```html
<body>
  <h1>Cats API App</h1>

  <section id="randomCats">
    <h2>Random Cats</h2>
    <article>
      <img alt="Foto de gatito 1" style="width:600px" />
      <button>Save to favorites</button>
    </article>
    <article>
      <img alt="Foto de gatito 2" style="width:600px" />
      <button>Save to favorites</button>
    </article>
    <article>
      <img alt="Foto de gatito 3" style="width:600px" />
      <button>Save to favorites</button>
    </article>

    <button onclick="pickACat()">Pick another CAThino</button>
  </section>

  <section id="favoriteCats">
    <h2>favorites Cats</h2>
    <article>
      <img alt="Foto de gatito fav" style="width:600px" />
      <button>Remove from favorites</button>
    </article>
  </section>

  <script src="main.js"></script>
</body>
```

`js`

```js
const API_URL =
  "https://api.thecatapi.com/v1/images/search?limit=3&page=1&api_key=your-api-key";

const pickACat = async (url = API_URL) => {
  const response = await fetch(url);
  const data = await response.json();
  const imgTags = document.querySelectorAll("img");
  imgTags.forEach((element, key) => (element.src = data[key].url));
};

pickACat();
```

![Screenshot 2022-06-07 at 20-29-14 Document.png](https://static.platzi.com/media/user_upload/Screenshot%202022-06-07%20at%2020-29-14%20Document-aec1577b-4d0f-4e38-83de-e621eb88b2f1.jpg)

## ¿Qué son los Métodos HTTP?

### HTTP

Un protocolo especifica reglas en la comunicación entre dos entes, en este caso entre dos computadoras.

HTTP (Hyper Text Transfer Protocol) fue creado específicamente para la web.

#### Verbos

Una de las cosas que especifica el protocolo HTTP son los verbos:

- **GET**: solicitar datos o algún recurso.
- **HEAD**: traer headers (como una peticion GET pero sin contenidos). Es util cuando vamos a utilizar APIs, para comprobar si lo que vamos a enviar esta correcto y puede ser procesado.
- **POST**: enviar datos a un recurso para la creación.
- **PUT**: reemplazar por completo un recurso.
- **PATCH**: reemplazar parcialmente un recurso.
- **DELETE**: eliminar un recurso.

La infografia del compañero Juan sebastian explica todo super bien!

![img](https://i.imgur.com/yx0LXkP.png)

**Métodos HTTP:**

- **GET** Lee datos del server (Solo Lectura)

- **HEAD** Recupera datos de los headers (Solo Lectura)

- **POST** Envía datos al server

- **PUT/PATCH** Salva datos en el server

- **DELETE** Borra datos del server

* [x] [POST vs. PUT](https://twitter.com/Rapid_API/status/1529122521827708928?t=nLNBmOCXdfEFOUuucNK3jg&s=19)

* [x] [PUT vs. PATCH](https://twitter.com/Rapid_API/status/1529153009124532226?t=he3zUvyFv7bexzkvQSgScA&s=19)

## GET: leyendo michis favoritos

Un aporte si también intentan hacer la petición con la API pero sin tener favoritos guardados es va a arrojar un 404 ya que aún no hay favortios.

También otra manera de darle manejos a los errores si usamos async await y es agregando el Try catch

Otra cosa es que también podemos usar el innerText ya que como no estamos usando etiquietas HTML y solo estamos insertando texto nos puede llegar a ser de útilidad, pero todo depende de como quieras aplicarlo

Solo recuerden Nunca dejen de aprender 💚

![API.png](https://static.platzi.com/media/user_upload/API-b2274bd0-fd11-424e-951b-6e4dfd116931.jpg)

```js
const HTTP = {
  OK: 200,
  CREATED: 201,
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  FORBIDDEN: 403,
  NOT_FOUND: 404,
  INTERNAL_SERVER_ERROR: 500,
  SERVICE_UNAVAILABLE: 503,
  GATEWAY_TIMEOUT: 504
};
```

## POST: guardando michis favoritos

```js
async function saveFavouriteMichis() {
  const res = await fetch(API_URL_FAVORITES, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      image_id: "sdfs"
    })
  });

  const data = await res.json();
  console.log("save");
  console.log(res);
  if (res.status !== 200) {
    spandError.innerHTML = "Hubo un error: " + res.status + data.message;
  }
}
```

html

```html
<article>
        <img id="img1" width="200" alt="Foto gatito aleatorio" />
        <button id="btn1" onClick="saveFavouriteMichis()">
          Guardar michi en favoritos
        </button>
      </article>

      <article>
        <img id="img2" width="200" alt="Foto gatito aleatorio" />
        <button id="btn2" onClick="saveFavouriteMichis()">
          Guardar michi en favoritos
        </button>
      </article>

      <button onClick="loadRandomMichis()">Recargar</button>
    </section>
```

## Consultas a la API para escribir HTML dinámico

```js
async function loadRandomMichis() {
  const res = await fetch(API_URL_RANDOM);
  const data = await res.json();
  console.log("Random");
  console.log(data);

  if (res.status !== 200) {
    spanError.innerHTML = "Hubo un error: " + res.status;
  } else {
    const img1 = document.getElementById("img1");
    const img2 = document.getElementById("img2");
    const btn1 = document.getElementById("btn1");
    const btn2 = document.getElementById("btn2");

    img1.src = data[0].url;
    img2.src = data[1].url;

    btn1.onclick = () => saveFavouriteMichi(data[0].id);
    btn2.onclick = () => saveFavouriteMichi(data[1].id);
  }
}
```

## DELETE: borrando michis favoritos

![apiDogs2.png](https://static.platzi.com/media/user_upload/apiDogs2-c1bb92b9-1d72-4002-bd3d-02dad4e03960.jpg)

html

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Gatitos Aleatorios</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- <link href="css/style.css" rel="stylesheet" /> -->
  </head>
  <body>
    <h1>Gatitos App</h1>

    <span id="error"></span>

    <section id="randomMichis">
      <h2>Michis Aleatorios</h2>

      <article>
        <img id="img1" width="200" alt="Foto gatito aleatorio" />
        <button id="btn1">
          Guardar michi en favoritos
        </button>
      </article>

      <article>
        <img id="img2" width="200" alt="Foto gatito aleatorio" />
        <button id="btn2">
          Guardar michi en favoritos
        </button>
      </article>

      <button onclick="loadRandomMichis()">Recargar</button>
    </section>

    <!-- section 2 -->
    <section id="favoriteMichis">
      <h2>Michis Favoritos</h2>

      <article>
        <img id="img1" width="200" alt="Foto gatito aleatorio" />
        <button>Sacar al michi de favoritos</button>
      </article>
    </section>

    <script src="index.js"></script>
    <!-- <script src="main.js"></script> -->
  </body>
</html>
```

js

```js
const API_URL_RANDOM =
  "https://api.thecatapi.com/v1/images/search?limit=2&api_key=c08d415f-dea7-4a38-bb28-7b2188202e46";
const API_URL_FAVOTITES =
  "https://api.thecatapi.com/v1/favourites?api_key=c08d415f-dea7-4a38-bb28-7b2188202e46";
const API_URL_FAVOTITES_DELETE = id =>
  `https://api.thecatapi.com/v1/favourites/${id}?api_key=c08d415f-dea7-4a38-bb28-7b2188202e46`;

const spanError = document.getElementById("error");

async function loadRandomMichis() {
  const res = await fetch(API_URL_RANDOM);
  const data = await res.json();
  console.log("Random");
  console.log(data);

  if (res.status !== 200) {
    spanError.innerHTML = "Hubo un error: " + res.status;
  } else {
    const img1 = document.getElementById("img1");
    const img2 = document.getElementById("img2");
    const btn1 = document.getElementById("btn1");
    const btn2 = document.getElementById("btn2");

    img1.src = data[0].url;
    img2.src = data[1].url;

    btn1.onclick = () => saveFavouriteMichi(data[0].id);
    btn2.onclick = () => saveFavouriteMichi(data[1].id);
  }
}

async function loadFavouriteMichis() {
  const res = await fetch(API_URL_FAVOTITES);
  const data = await res.json();
  console.log("Favoritos");
  console.log(data);

  if (res.status !== 200) {
    spanError.innerHTML = "Hubo un error: " + res.status + data.message;
  } else {
    const section = document.getElementById("favoriteMichis");
    section.innerHTML = "";

    const h2 = document.createElement("h2");
    const h2Text = document.createTextNode("Michis favoritos");
    h2.appendChild(h2Text);
    section.appendChild(h2);

    data.forEach(michi => {
      const article = document.createElement("article");
      const img = document.createElement("img");
      const btn = document.createElement("button");
      const btnText = document.createTextNode("Sacar al michi de favoritos");

      img.src = michi.image.url;
      img.width = 150;
      btn.appendChild(btnText);
      btn.onclick = () => deleteFavouriteMichi(michi.id);
      article.appendChild(img);
      article.appendChild(btn);
      section.appendChild(article);
    });
  }
}

async function saveFavouriteMichi(id) {
  const res = await fetch(API_URL_FAVOTITES, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      image_id: id
    })
  });
  const data = await res.json();

  console.log("Save");
  console.log(res);

  if (res.status !== 200) {
    spanError.innerHTML = "Hubo un error: " + res.status + data.message;
  } else {
    console.log("Michi guardado en favoritos");
    loadFavouriteMichis();
  }
}

async function deleteFavouriteMichi(id) {
  const res = await fetch(API_URL_FAVOTITES_DELETE(id), {
    method: "DELETE"
  });
  const data = await res.json();

  if (res.status !== 200) {
    spanError.innerHTML = "Hubo un error: " + res.status + data.message;
  } else {
    console.log("Michi eliminado de favoritos");
    loadFavouriteMichis();
  }
}

loadRandomMichis();
loadFavouriteMichis();
```

# Headers en peticiones HTTP

## ¿Qué son los Headers HTTP?

![ayudita a lso compas.png](https://static.platzi.com/media/user_upload/ayudita%20a%20lso%20compas-1131ceda-8144-4dbe-8e8e-b371d82b9c91.jpg)

## ¿Qué son los Headers HTTP?

Los **headers HTTP** son parámetros que se envían en una transacción HTTP, que contienen información del estado de la transacción en curso.

Cuando un cliente solicita información a un servidor, este puede pasarle información adicional en el header de la solicitud. información del tipo de datos que se esperan recibir, del tipo de datos que envían, información de autenticación etc.

De la misma forma el servidor puede incluir estos headers en las respuestas para mostrar información del estado de la solicitud.(HTTP Status Codes)

Estos pueden ser separados en varios grupos: ([fuente](https://rapidapi.com/learn/rest-apis/rest-api-for-experts/different-types-of-http-headers?utm_source=google&utm_medium=cpc&utm_campaign=DSA&gclid=Cj0KCQjwyMiTBhDKARIsAAJ-9VtRyyo-mBrfeW6xZM5jJyQug_VtCsrpPCa_AlylsLPWyRWmQ62bnUcaAjGlEALw_wcB))

### Request Headers

Pasan información de la solicitud. Contienen información sobre el recurso solicitado e información del cliente que la solicita.

URL a la que se le hace la solicitud, detalles de autenticación o políticas de cache

Estos pueden ser:

- Accept: Informan al servidor el tipo de datos que el cliente puede entender

  ```jsx
  Accept: text / html;
  Accept: application / xhtml + xml;
  Accept: image / png;
  ```

- Accept-Encoding: Envían información sobre el tipo de codificación que el cliente puede entender

  ```jsx
  Accept-Encoding: gzip
  Accept-Encoding: gzip, compress
  ```

- Authorization: sirve para pasar credenciales que le servirán al servidor determinar si el cliente tiene acceso a ciertos recursos

  ```jsx
  Authorization: Basic YWxhZGRpbjpvcGVuc2VzYW1l
  Authorization: Bearer eyYWxhZGRpbjpvcGVuc2VzYW1l
  ```

- Accept-Language: Permite saber al servidor que tipo lenguaje es entendido por el cliente logrando entender que configuración local es viable enviar. Por ejemplo: los horarios, fechas, medidas, etc.

  ```jsx
  Accept-Language: fr-CH
  Accept-Language: en-US
  ```

- Cache-Control: contiene información sobre el control de la cache por parte del cliente y del servidor.

  ```jsx
  Cache-Control: stale-while-revalidate=60
  Cache-Control: no-cache
  ```

### Response Headers

Así como los request headers contienen información del cliente, los response headers contienen información del servidor al que se le hace la petición.

En realidad todos los headers enviados en un respuesta del servidor pueden ser llamados de esta manera.

- Age: Contienen información del tiempo que un objeto estuvo en caché. Se representa en segundos. Si es 0(cero) significa que la solicitud se obtuvo del servidor de origen. Sino se calcula como la diferencia entre el Date del proxy y el date enviado por la respuesta original.

  ```jsx
  Age: 24;
  ```

- Server: Describen el software usado por el servidor que manejó la solicitud. Es decir el que generó la respuesta.
  Hay que tener en cuenta que hay que evitar demasiado detalle en estas respuesta ya que sino se estaría enviando información que podrían utilizar los atacantes(por ejemplo la versión del sistema operativo que utiliza el servidor). Se utiliza por ejemplo para exponer la versión de apache utilizada,

  ```jsx
  Server: Apache/2.4.1 (Unix)
  ```

- Location: indica la URL a la que redirigir una página. Solo proporciona un significado cuando se sirve con una respuesta de estado 3xx (redireccionamiento) o 201 (creado).

### Representation Headers

Contienen información acerca del body de la solicitud, enviado en una respuesta o (en un POST)

- Content-type: Indica el tipo de contenido (formato de archivo) es enviado en una solicitud.

  ```jsx
  ejemplo: Content-Type: text/html; charset=UTF-8
  ```

- Content-Enconding: Contienen la información necesaria para decodificar un archivo a su formato original.

  ```jsx
  Content-Encoding: compress
  Content-Encoding: gzip
  ```

- Content-Languaje: Indica el lenguaje para los cuales es más relevante el contenido de una página, de modo que los usuarios puedan diferenciarlos según su propio idioma preferido.

  ```jsx
  Content-Language: en-US
  Content-Language: en-CA
  ```

- Content-Location: Indican un URL o dirección alternativa para la respuesta. A diferencia de Location (en Request Headers). Este indica la url directa que puede ser utilizada para acceder al recurso. Mientras Location esta asociada la respuesta en si, content-location esta asociada a los datos devueltos.
  Por ejemplo: Si una api puede devolver datos en los formatos JSON, XML o CSV y su ruta se encuentra en https://ejemplo.com/documents/archivo.
  El sitio podría retornan distintas url dependiendo del parámetro Accept pasado en la solicitud.

| Request header                      | Response header                       |
| ----------------------------------- | ------------------------------------- |
| Accept: application/json, text/json | Content-Location: /documents/foo.json |
| Accept: application/xml, text/xml   | Content-Location: /documents/foo.xml  |
| Accept: text/plain, text/\*         | Content-Location: /documents/foo.txt  |

### Payload Headers

contiene información acerca de la carga de la respuesta. Lenght, Enconding, Range.

- Content-Lenght: Indica el tamaño del body del mensaje en bytes

  ```jsx
  Content-Length: 3495
  ```

- Content-Range: Indica la posición a la que pertenece una porción del mensaje respecto al body.

  ```jsx
  Content-Range: <unit> <range-start>-<range-end>/<size>
  Content-Range: bytes 200-1000/67589
  ```

[![img](https://www.google.com/s2/favicons?domain=https://apipheny.io/wp-content/uploads/2020/01/apipheny-favicon-70x70.png)API Headers - What Are They? Examples & More Explained\] - Apipheny](https://apipheny.io/api-headers/)

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/favicon-48x48.cbbd161b.png)HTTP headers - HTTP | MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers)

## Header de autorización

En lugar de estar repitiendo la API key en cada función que la necesita, simplemente podemos guardarla en una constante y a esta pasarla en los headers:

```js
const API_KEY = "0f11e73d-a405-45a7-b298-3066aa412dda"
//
//
        headers: {
            'Content-Type': 'application/json',
            'x-api-key': API_KEY,
        },
```

función guardar ha quedado de la siguiente manera

```js
const saveImageFavorites = async id => {
  console.log("start" + id);
  console.log("el id es " + id);
  const res = await fetch(API_URL_DELATE_FAVORITES, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "x-api-key": "93281cee-4d7f-4c6d-abc9-62cc90323eb1"
    },
    body: JSON.stringify({
      image_id: id
    })
  });
  const data = await res.json();
  console.log("save");
  console.log(res);

  const element = document.getElementById("start" + id);
  element.classList.add("start-active");

  if (res.status !== 200) {
    console.log("Error: " + res.status + " " + data.message);
  } else {
    getImagesFavorites();
  }
};
```

## Header de Content-Type

Uno de los headers que determinaremos al enviar datos es el Content Type, es decir, que tipo de dato será lo que enviaremos, para que el backend pueda decir: Ah! Me están enviando un tipo de dato X, entonces debo leer el body de esta manera.

Tenemos muchísimos tipos de content types, empecemos a citarlos y agruparlos por categorías para que sea más fácil la lectura:

-Application: {

```html
application/json, application/xml, application/zip,
application/x-www-form-urlencoded: "para enviar datos de formularios HTML"
```

}

Envío de archivos de audio literalmente
-Audio: {

```html
audio/mpeg, audio/x-ms-wma, audio/vnd.rn-realaudio, audio/w-wav
```

}

-Image: {

```html
image/gif, image/jpeg, image/png, image/x-icon, image/svg+xml
```

}

Video: {

```html
video/mpeg, video/mp4, video/quicktime, video/webm
```

}

Multipart: {

```html
multipart/mixed, multipart/alternative, multipart/related, multipart/form-data:
"sirve para enviar datos de formularios, nos ahorra tener que hacer un
querySelector a cada input y su value, al usar este tipo de dato podemos agrupar
todos esos datos en uno solo"
```

}

Text: {

```html
text/css, text/csv, text/html, text/plain, text/xml
```

}

VND: {

```
application/vnd.ms-excel,
application/vnd.ms-powerpoint,
application/msword
```

}

Diferencia entre **application/xml** y **text/xml**

```html
<?xml version="1.0" encoding="UTF-8"?>
```

esta sentencia puede ser ignorada cuando se usa text/xml. por lo que deberías pasar esa información en el propio header y generar muchos inconvenientes que pueden ser solucionados al usar application/xml.

Sin embargo, text/xml pareciera ser mejor para seguir la semántica de lectura humana.(siempre recordando enviar el encoding en el header)

Si un documento XML es legible para usuarios, es preferible text/xml a application/xml. Los agentes de usuario MIME que no tienen soporte explícito para text/xml lo tratarán como texto sin formato.
Si el conjunto de caracteres predeterminado (US-ASCII) para text/xml no es soportado, application/xml proporciona una alternativa

En resumen, application/xml es el preferido por sus ventajas, pero text/xml tiene sus respectivas aplicaciones.

Más info: https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types/favicon-48x48.cbbd161b.png)Common MIME types - HTTP | MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types)

## FormData: publicando imágenes de michis

Código que usé:

```js
if (res.status !== 201) {
  spanError.innerHTML = `Hubo un error al subir michi: ${res.status} ${data.message}`;
} else {
  console.log("Foto de michi cargada :)");
  console.log({ data });
  console.log(data.url);
  saveFavouriteMichi(data.id); //para agregar el michi cargado a favoritos.
}
```

aprovecho para mostrarles a mi perro Ponky!
HTML

```html
<section id="uploadingDog">
  <h2>Sube la foto de tu perro</h2>
  <form id="uploadingForm">
    <label for="file" class="custom-file-upload"
      >Selecciona la foto<i class="fa-solid fa-camera"></i
    ></label>
    <input id="file" type="file" name="file" onchange="previewImage()" />
    <img id="preview" />
    <button id="submitDog" type="button" onclick="uploadDogPhoto()">
      Subir foto de tu perro<i class="fa-solid fa-paper-plane"></i>
    </button>
  </form>
</section>
```

JS

```js
const previewImage = () => {
  const file = document.getElementById("file").files;
  console.log(file);
  if (file.length > 0) {
    const fileReader = new FileReader();

    fileReader.onload = function(e) {
      document.getElementById("preview").setAttribute("src", e.target.result);
    };
    fileReader.readAsDataURL(file[0]);
  }
};
```

CSS

```css
#submitDog,
.custom-file-upload {
  width: 260px;
  height: 50px;
  border-radius: 20px;
  border: none;
  font-size: 20px;
  cursor: pointer;
  background-color: #db3a34;
  opacity: 0.6;
  color: #fff;
  transition: 0.5s;
  margin: 0;
}

.custom-file-upload:hover,
#submitDog:hover {
  opacity: 1;
}

#submitDog i,
.custom-file-upload i {
  margin-left: 12px;
}

.custom-file-upload {
  border-radius: 20px 20px 0 0;
  background-color: #f0f0f0;
  color: #000;
  line-height: 50px;
}

#submitDog {
  border-radius: 0 0 20px 20px;
  margin-bottom: 30px;
}

#uploadingForm {
  display: inline-flex;
  flex-direction: column;
  justify-content: space-between;
}

#file {
  display: none;
}

#preview {
  width: 260px;
  height: 260px;
  object-fit: cover;
  box-shadow: 0 40px 60px -6px black;
}
```

```JS
async function uploadMichiPhoto(){
        const form = document.getElementById('uploadingForm');
        const formData = new FormData(form);
        console.log(formData.get('file'));
        const res = await fetch(API_URL_UPLOAD,{
            method: 'POST',
            headers:{
                //'Content-Type':'multipart/form-data',
                'X-API-KEY':'ABC123',
            },
            body:formData
        })
        const data = await res.json();
        if(res.status !== 201){
            spanError.innerHTML = "Hubo un error: " +res.status + data.message;
            console.log({data})
        }else{
            console.log('Foto de michi subida');
            console.log({data})
            console.log(data.url)
            saveFavoritesCat(data.id);
        }
    }
```

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/es/docs/Web/API/FormData/favicon-48x48.cbbd161b.png)FormData - Referencia de la API Web | MDN](https://developer.mozilla.org/es/docs/Web/API/FormData)

[![img](https://www.google.com/s2/favicons?domain=https://muffinman.io/blog/uploading-files-using-fetch-multipart-form-data//favicon/blue/icon-32.ico)Uploading files using 'fetch' and 'FormData' · Muffin Man](https://muffinman.io/blog/uploading-files-using-fetch-multipart-form-data/)

# Bonus

## Axios: librerías de JavaScript para consumir APIs

ctualmente la API de fetch ya está disponible en Node.js a partir de la v17 🤭

> **Can I use Fetch in Node.js now?**
>
> Fetch is already available as an experimental feature in Node v17. If you’re interested in trying it out before the main release, you’ll need to first download and upgrade your Node.js version to 17.5.

Acá pueden leer más sobre ello: [The Fetch API is finally coming to Node.js](https://blog.logrocket.com/fetch-api-node-js/)

HTML

```html
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8" />
    <meta name="Description" content="Extended Description" />
    <meta name="robots" content="index,follow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Consumo de APIs</title>

    <!-- Utilizando el CDN de la biblioteca Axios -->
    <script defer src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script defer src="./app.js"></script>

    <style>
      .container {
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        margin-bottom: 48px;
      }
      img {
        display: block;
        background-repeat: no-repeat;
        background-position: center;
        background-size: cover;
      }
      .random-img {
        width: 400px;
        height: auto;
      }
      button {
        margin: 16px 0;
      }
      .random-cats-sec,
      .favorites-cats-sec {
        padding: 25px 25px;
        border: solid 1px black;
        border-radius: 25px;
      }
      .error-app {
        color: red;
      }
    </style>
  </head>

  <body>
    <div class="container">
      <h1>Gatos App</h1>

      <span class="error-app" id="error"></span>

      <section class="random-cats-sec" id="random-cats">
        <h2>Gatos Aleatorios</h2>
      </section>
      <button id="reload">Recargar</button>

      <section class="favorites-cats-sec" id="favorites-cats">
        <h2>Gatos Favoritos</h2>
      </section>
    </div>
  </body>
</html>
```

.
JS

```js
// El mismo proyecto de API Rest pero usando Axios

const API_KEY = "tuapikey" - tuapikey - tuapikey;

// Creando una instancia de axios para reutilizarla
const api = axios.create({
  baseURL: "https://api.thecatapi.com/v1/",
  headers: { "X-API-KEY": API_KEY }
});

const URL_RANDOM_RES = "/images/search?limit=2";
const URL_FAVORITES_RES = "/favourites";

const randomCats = document.querySelector("#random-cats");
const favoritesCats = document.querySelector("#favorites-cats");
const buttonReload = document.querySelector("#reload");
const errorNode = document.querySelector("#error");

// Función para adicionar imagen a una sección
function addImageNode({ nodeSection, imageId, imageUrl, attId, buttonText }) {
  const article = document.createElement("article");
  const img = document.createElement("img");
  const button = document.createElement("button");
  img.className = "random-img";
  img.setAttribute(attId, imageId);
  img.src = imageUrl;
  const btnText = document.createTextNode(buttonText);
  button.appendChild(btnText);
  article.appendChild(img);
  article.appendChild(button);
  nodeSection.appendChild(article);
}

// Cargando imágenes aleatorias
async function loadRandomCats() {
  try {
    const { data, status } = await api.get(URL_RANDOM_RES);
    if (status !== 200)
      throw new Error(`Error de petición HTTP en Random: ${status}`);
    data.forEach(el => {
      addImageNode({
        nodeSection: randomCats,
        imageId: el.id,
        imageUrl: el.url,
        attId: "data-imageid",
        buttonText: "Guardar Favorito"
      });
    });
  } catch (error) {
    errorNode.innerText = `Error: ${error.message}`;
  }
}

// Cargando imágenes de favoritos
async function loadFavoritesCats() {
  try {
    const { data, status } = await api.get(URL_FAVORITES_RES);
    if (status !== 200)
      throw new Error(`Error de petición HTTP en Favoritos: ${status}`);
    data.forEach(el => {
      addImageNode({
        nodeSection: favoritesCats,
        imageId: el.id, // no es id de la imagen sino id de favorito
        imageUrl: el.image.url,
        attId: "data-id",
        buttonText: "Eliminar Favorito"
      });
    });
  } catch (error) {
    errorNode.innerText = `Error: ${error.message}`;
  }
}

// Salvando imagenes a favoritos
async function saveFavoritesCat(id) {
  try {
    // Llamamos al objeto axios con método que necesitamos, en este caso POST
    // response = { data, status }
    const { data, status } = await api.post(URL_FAVORITES_RES, {
      // Pasamos los parámetros o querystring
      image_id: id
    });
    if (status !== 200)
      throw new Error(`Error de petición POST HTTP en Favoritos: ${status}`);
    return data.id;
  } catch (error) {
    errorNode.innerText = `Error: ${error.message}`;
  }
}

// Eliminando imágenes de favoritos
async function deleteFavoritesCat(id) {
  try {
    const { status } = await api.delete(`${URL_FAVORITES_RES}/${id}`);
    if (status !== 200)
      throw new Error(`Error de petición DELETE HTTP en Favoritos: ${status}`);
  } catch (error) {
    errorNode.innerText = `Error: ${error.message}`;
  }
}

// Evento recargar imagenes random
buttonReload.addEventListener("click", () => {
  while (randomCats.lastChild.nodeName === "ARTICLE") {
    randomCats.lastChild.remove();
  }
  loadRandomCats();
});

// Evento guardar imagen en favorito
randomCats.addEventListener("click", e => {
  const target = e.target;
  if (target && target.nodeName === "BUTTON") {
    const img = target.previousSibling;
    const imageId = img.dataset.imageid;
    saveFavoritesCat(imageId).then(id => {
      alert("Hemos guardado la imagen a favoritos!");
      addImageNode({
        nodeSection: favoritesCats,
        imageId: id, // no es id de la imagen sino id de favorito
        imageUrl: img.src,
        attId: "data-id",
        buttonText: "Eliminar Favorito"
      });
    });
  }
});

// Evento eliminar imágenes de favorito
favoritesCats.addEventListener("click", e => {
  const target = e.target;
  if (target && target.nodeName === "BUTTON") {
    const img = target.previousSibling;
    const id = img.dataset.id;
    deleteFavoritesCat(id);
    alert("Hemos eliminado la imagen a favoritos!");
    const parent = target.parentNode;
    if (parent.nodeName === "ARTICLE") parent.remove();
  }
});

loadRandomCats();

loadFavoritesCats();
```

https://platzi.com/comentario/3594423/)

Forma de crer instancia de axios actualmente:

```js
const instance = axios.create({
  baseURL: "https://some-domain.com/api/",
  timeout: 1000,
  headers: { "X-Custom-Header": "foobar" }
});
```

https://axios-http.com/es/docs/instance

## CORS, caché, redirect y tu propio clon de fetch

![5f7ibr.jpg](https://static.platzi.com/media/user_upload/5f7ibr-2258abcf-452c-409c-a9e9-7b6c6b3debc2.jpg)

![img](https://preview.redd.it/bpehxczwvr241.png?auto=webp&s=a1444c46d2f224b61678491b47751fdc0d7f2243)

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/API/Request/cache/favicon-48x48.cbbd161b.png)Request.cache - Web APIs | MDN](https://developer.mozilla.org/en-US/docs/Web/API/Request/cache)

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/API/fetch/favicon-48x48.cbbd161b.png)fetch() - Web APIs | MDN](https://developer.mozilla.org/en-US/docs/Web/API/fetch)

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/HTTP/Caching#freshness/favicon-48x48.cbbd161b.png)HTTP caching - HTTP | MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP/Caching#freshness)

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/API/Headers/favicon-48x48.cbbd161b.png)Headers - Web APIs | MDN](https://developer.mozilla.org/en-US/docs/Web/API/Headers)

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/es/docs/Web/HTTP/Caching#actualizaci%C3%B3n/favicon-48x48.cbbd161b.png)HTTP caching - HTTP | MDN](https://developer.mozilla.org/es/docs/Web/HTTP/Caching#actualización)

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/API/Response/favicon-48x48.cbbd161b.png)Response - Web APIs | MDN](https://developer.mozilla.org/en-US/docs/Web/API/Response)

## GraphQL, Web Sockets y Web 3.0: el mundo más allá de REST

DNS (Domain Name System) lo usamos para poder comunicarnos con otros sitios a travez de un nombre de dominio como wwwplatzicom, sin ello sería atravez de la IP “000.00.000.000” del servidor donde se encuentra esa página y como es muy larga y difícil de recordar se uso DNS en la web2.

ENS (Ethereum Naming Service) es la sustitución del DNS pero en la web3, ahora como cada persona tiene su wallet en el mundo del crypto pues la dirección seria algo a si “e32fre43f584bnf2784b3” lo cual se repite la historia como con la IP, es muy difícil de recordarla.

Al comprar el dominio ENS seria de esta forma “badman.eth”. Alfín otra forma de recordarla más fácil.

Ethereum Name Service (ENS)
“Domain Name System”, es decir, “Sistema de nombres de dominio”(DNS)
