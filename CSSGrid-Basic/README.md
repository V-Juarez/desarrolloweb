<h1>CSS Grid Básico</h1>

<h3>Estefany Salas</h3>

<h1>Tabla de Contenido</h1>

- [1. Introducción a Grid](#1-introducción-a-grid)
  - [¿Qué es CSS Grid Layout?](#qué-es-css-grid-layout)
  - [Conceptos para comenzar](#conceptos-para-comenzar)
- [2. Propiedades de Grid](#2-propiedades-de-grid)
  - [Propiedades del contenedor](#propiedades-del-contenedor)
  - [Propiedades de alineación](#propiedades-de-alineación)
  - [Propiedades de ubicación](#propiedades-de-ubicación)
- [3. Power ups de Grid](#3-power-ups-de-grid)
  - [Funciones especiales](#funciones-especiales)
  - [Keywords especiales](#keywords-especiales)
- [4. Proyecto](#4-proyecto)
  - [Proyecto](#proyecto)
  - [Hagamos nuestra primera sección](#hagamos-nuestra-primera-sección)
  - [Creando la grilla con área](#creando-la-grilla-con-área)
  - [Armando el listado](#armando-el-listado)
- [5. Bonus](#5-bonus)
  - [¿Cómo hacer nuestro proyecto responsivo?](#cómo-hacer-nuestro-proyecto-responsivo)
- [6. Próximos pasos](#6-próximos-pasos)
  - [Más cursos de CSS Grid](#más-cursos-de-css-grid)

# 1. Introducción a Grid

## ¿Qué es CSS Grid Layout?

> **# css-grid-basico** Classes for my CSS Grid Básico course on Platzi, here you'll find all the exercises that I'll be explaining.

CSS Grid layout contiene funciones de diseño dirigidas a los desarrolladores de aplicaciones web. El CSS grid se puede utilizar para lograr muchos diseños diferentes. También se destaca por permitir dividir una página en áreas o regiones principales, por definir la relación en términos de tamaño, posición y capas entre partes de un control construido a partir de primitivas HTML.

Al igual que las tablas, el grid layout permite a un autor alinear elementos en columnas y filas. Sin embargo, con CSS grid son posibles muchos más diseños y de forma más sencilla que con las tablas. Por ejemplo, los elementos secundarios de un contenedor de cuadrícula podrían posicionarse para que se solapen y se superpongan, de forma similar a los elementos posicionados en CSS.

La guía de referencia más pedagógica:
**[CSS Tricks: GSS Grid Layout](https://css-tricks.com/snippets/css/complete-guide-grid/)**

## Conceptos para comenzar

- Container: Es un contenedor donde se almacenan los elementos
- Item: Son los elementos que estan en el contenedor y se convierten en Grid Item pueden ser (botones,links,imagenes y videos)
- Lineas : Son los elementos que dividen las filas y columnas de una Grilla

- Celda: Es la unidad minima que tenemos en una Grilla, esta delimitada por 4 lineas. Ocupa 1 columna, 1 Fila
- Track: Los track son un grupo de celdas que estan en una misma fila o una misma columna
- Area: Pueden usar varias filas o varias columnas al mismo tiempo.

Recordemos que al trabajar en con CSS Grid nos referimos en inglés a líneas y columnas:

- Línea / row
- Columna / column

Un gran material de apoyo para poder complementar este curso, es css-tricks y su guía sobre css grid: [Guia de css-grid](https://css-tricks.com/snippets/css/complete-guide-grid/)

![img](https://lenguajecss.com/css/maquetacion-y-colocacion/grid-css/grid-css-conceptos.png)

Elementos principales del Grid CSS.

- **Contenedor.** El elemento padre contenedor que definirá la cuadrícula o rejilla.
- **ítem.** Cada uno de los hijos que contiene la cuadrícula (elemento contenedor).
- **Celda (grid cell).** Cada uno de los cuadros que conforman la cuadrícula, esta sería la unidad mínima.
- **Area (grid area).** Región o conjunto de celdas de la cuadrícula.
- **Banda (grid track).** Banda horizontal o vertical de celdas de la cuadrícula.
- **Línea (grid line).** Separador horizontal o vertical de las celdas de la cuadrícula.

Para trabajar con el concepto de cuadriculas Grid CSS se necesita un escenario similar al siguiente en nuestro código HTML.

```html
<!-- contenedor -->
<section>
  <!-- ítems del grid -->
  <article>Item 1</article>
  <article>Item 2</article>
  <article>Item 3</article>
</section>
```

Si queremos activar la cuadrícula grid hay que habilitarla modificando el valor por defecto de la propiedad `display` y especificar el valor `grid` o `inline-grid`.

```css
section {
  display: grid;
}
```

Una vez definido esto podemos empezar a trabajar con nuestras cuadrículas.

# 2. Propiedades de Grid

## Propiedades del contenedor

Se puede establecer un tamaño fijo para la cuadrícula. Para ello podemos hacer uso de las propiedades CSS `grid-template-columns` y `grid-template-rows`, que sirven para indicar las dimensiones de cada celda de la cuadrícula, diferenciando entre columnas y filas.

Esto en código sería lo siguiente:

```css
section {
  display: grid;
  grid-template-columns: 50px 300px;
  grid-template-rows: 200px 75px;
}
```

El código anterior se traduce como que tendremos una cuadrícula con dos columnas (la primera con 50px de ancho y la segunda con 300px) y con dos filas (la primera con 200px de alto y la segunda con 75px).

![2.png](https://static.platzi.com/media/user_upload/2-eaec82d5-e3da-457b-9626-23619dd03fdc.jpg)
Representación gráfica de la cuadrícula generada.

Dependiendo del número de ítems definidos en nuestro HTML, podremos tener una cuadrícula de 2x2 elementos (4 ítems), 2x3 elementos (6 ítems) y así sucesivamente. Si el número de ítems es impar, la última celda de la cuadrícula se quedará vacía.

Es posible que al definir estas propiedades necesitemos más de una fila o columna con el mismo valor por lo que podemos utilizar la expresión `repeat()` para indicar repetición de valores, indicando el número de veces que se repiten y el tamaño a repetir.

```css
section {
  display: grid;
  grid-template-columns: 100px repeat(2, 50px) 200px;
  grid-template-rows: repeat(2, 50px 100px);
}
```

El código anterior crea una cuadricula con cuatro columnas: la primera de 100px de ancho, la segunda y tercera de 50px de ancho y la cuarta de 200px de ancho. Por otro lado se tendrían 4 filas: la primera de 50px de alto y la segunda de 100px de alto, la tercera y cuarta repiten este patrón. En el caso de tener más ítems hijos, el patrón se seguiría repitiendo.

Esto equivaldría al siguiente código CSS.

```css
section {
	  display: grid;
	  grid-template-columns: 100px 50px 50px 200px;
	  grid-template-rows: 50px 100px 50px 100px;;
```

![reto1_css.png](https://static.platzi.com/media/user_upload/reto1_css-1c59cf63-22d2-4bd4-aa13-5d52d98d74d6.jpg)

• `Display grid:` para decirle al contenedor que tenga esa propiedad y así aplicarle estilos de grid

• `Grid-template:` define cuantas y de qué tamaño son las filas o columnas

• `Gaps:` el espacio entre cada celda

• `Grid-auto:` aplica el cambio para todas las columnas o filas

**Propiedades de Alineacion**

`html`

```html
<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="./style.css" />
  </head>
  <body>
    <div class="contenedor">
      <div class="item item-1">1</div>
      <div class="item item-2">2</div>
      <div class="item item-3">3</div>
      <div class="item item-4">4</div>
      <div class="item item-3">5</div>
      <div class="item item-4">6</div>
    </div>
  </body>
</html>
```

`css`

```css
.contenedor {
  border: 5px solid #e1bee7;
  background-color: #fff1ff;
  display: grid;
  grid-gap: 15px;
  grid-template-columns: 150px 150px 150px;
  grid-auto-rows: 150px;
  place-content: center;
  place-items: end;
  height: 600px;
}

.item {
  border: 5px solid #00bcd4;
  font-size: 2rem;
}
```

**Propiedades de Contenedor**

`html`

```html
<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="./style.css" />
  </head>
  <body>
    <div class="contenedor">
      <div class="item item-1">1</div>
      <div class="item item-2">2</div>
      <div class="item item-3">3</div>
      <div class="item item-4">4</div>
      <div class="item item-3">5</div>
      <div class="item item-4">6</div>
      <div class="item item-3">7</div>
      <div class="item item-4">8</div>
    </div>
  </body>
</html>
```

`css`

```css
.contenedor {
  border: 5px solid #e1bee7;
  background-color: #fff1ff;
  display: grid;
  grid-template-columns: [perro dog] 50px [gato] 100px [raton] 200px;
  grid-template-rows: 400px 50px;
  grid-gap: 15px;
  grid-auto-flow: column;
  grid-auto-columns: 50px;
}

.item {
  border: 5px solid #00bcd4;
  font-size: 2rem;
}
```

![](https://static.platzi.com/ui/assets/image/isotipoPlatzi093f27a2fb00922bb105.png)[CSS Grid](https://platzi.com/tutoriales/1229-css-grid-layout/6653-todo-lo-que-necesitas-saber-de-css-grid/)

## Propiedades de alineación

> Propiedades para la alineación de los items (elementos):

- Justify-items
- Align-items
- Place-items

> Propiedades para la alineación del container(El Contenedor):

- Justify-content
- Align-content
- Place-content

> Propiedades para la alineación de un solo item individual

- Justify-self
- Align-self
- Place-self

place-content: shorthand de alineamientos de las celdas (items) como un todo con respecto a su contenedor.
place-items: shorthand de alineamiento del contenido de cada celda con respecto su celda.
place-self:shorthand de alineamiento del contenido de una celda con respecto a su celda.

**RETO**
![reto2.png](https://static.platzi.com/media/user_upload/reto2-65774f00-8bbf-4ef2-bb3a-971e77e69c97.jpg)

**HTML**
![reto2-html.png](https://static.platzi.com/media/user_upload/reto2-html-44aa93db-2862-49ad-8a58-cc62d562795e.jpg)

**CSS**
![reto2-css.png](https://static.platzi.com/media/user_upload/reto2-css-f7b1e081-2c25-459a-8890-62e498af54bc.jpg)

**Propiedades básicas de Grid:**
• Display Grid
• Grid-template
• Gaps
• Grid-Auto

```css
display: grid;
grid-template-rows: 100px100px100px;
/filas3/grid-auto-columns: 100px;
/tamañocolumnas/grid-auto-flow: column;
/ordenadoencolumnas//row-gap: 15px;
/gap: 20px;
```

**Propiedades de alineación de los ítems**

> Justify-items: Alineación de manera horizontal en el espacio que haya

> Align-items: Alineación vertical

> Place items: Mezcla de las 2 Justify y Align

**Valores**:

```css
start | end | center | stretch;
```

**Propiedades de alineación del contenedor**

> Justify-content: Alineación de manera horizontal en el espacio que haya

> Align-content: Alineación vertical

> Place-content: Mezcla de las 2 Justify y Align

**Valores:**

```css
start | end | center | stretch |space-around | space-between |space-evenly;
```

**Propiedades de alineación del ítem**

> Justify-self: Alineación de manera horizontal en el espacio que haya

> Align- self: Alineación vertical

> Place- self: Mezcla de las 2 Justify y Align

**Valores:**

```css
start | end | center | stretch;
```

## Propiedades de ubicación

![grd.PNG](https://static.platzi.com/media/user_upload/grd-6bc3afa4-42fd-43ea-b57a-f8224a2f7ad7.jpg)

😃 🐱

![Captura de pantalla 2021-11-02 204233.png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%202021-11-02%20204233-9bf69ed3-a93e-454d-84ef-15ce675ffa70.jpg)

Código

![gatos.png](https://static.platzi.com/media/user_upload/gatos-c3526107-ccf2-40b0-9c55-a18f9ffac0af.jpg)

# 3. Power ups de Grid

## Funciones especiales

> Funciones especiales

- minmax: ayuda a declarar el tamaño minimo y maximo para el ancho y alto de una celda, sin depender del contenido que tengamos en ella
- repeat : se usa cuando todas las columnas o filas tienen el msmo ancho y evitar repetir el tamaño de las columnas.

#### `minmax`

Nos ayuda a determinar el tamaño maximo para el tamaño de una celda

- La funcion recibe el tamaño minimo y el tamaño maximo de las filas o columnas

```css
grid-template-rows: minmax(30px, 100px);
```

#### `repeat`

Nos permite definir el tamaño de las columnas si tener que escribirlo por el numero de columnas de nuestra grilla,

- La funcion recibe, el numero de columnas o filas y su tamaño

```css
grid-template-columns: repeat(3, 100px);
```

```embeddedjs
div.contenedor>div.item.item-${$}*9
```

## Keywords especiales

- fr : Es una unidad de medida especial de css grid para darle ancho o alto a filas y columnas 1fr representa una fraccion del total de columnas o filas.
- min-content : Ajusta el ancho de la celda lo mínimo posible sin romper su contenido.
- max-content : Ajusta el ancho de la celda lo máximo posible para mostrar su contenido.
- auto-fill : Agrega columnas “fantasma” que rellenan el espacio sobrante del contenedor.
- auto-fit : Ensancha las columnas para que ocupen todo el espacio del contenedor.

**auto-fill y auto-fit** ayudan a la grilla a ocupar el 100% del espacio disponible.

> - `min-content` hará que nuestro contenido haga salto de linea en cada oportunidad que tenga, siendo tan ancho como la palabra mas larga.
>
> - `max-content` no tendrá salto de linea, ocupando todo el ancho que pueda, incluso causando overflow.

> **auto-fit y auto-fill**
>
> - **auto-fit:** intenta crear nuevas columnas siempre que cuente con el espacio, sin embargo, si el contenido es vacío hace que las columnas que si tienen contenido se expandan (siempre que sea posible ocupando el espacio disponible)
>
> - **auto-fill:** intenta crear nuevas columnas siempre que cuente con el espacio, sin embargo, si el contenido es vacío hace que ellas aunque no existan, ocupen el espacio como si existieran

# 4. Proyecto

## Proyecto

Tip de maquetación con grid:
En una posible grilla ver cual es el elemento más pequeño que tiene que estar en la grilla.
A partir del elemento más pequeño determinar el número de filas y columnas a necesitar para maquetar todos los elementos de dicha sección de la web.

link externo y no lo tengan en local 😊💚
dish1
https://i.postimg.cc/qR0pRyfB/dish1.png
dish2
https://i.postimg.cc/Wp5sNd5j/dish2.png
dish3
https://i.postimg.cc/hPwgmHF5/dish3.png
dish4
https://i.postimg.cc/MG08hHnC/dish4.png
hero
https://i.postimg.cc/zD21m9fG/hero.png
plate1
https://i.postimg.cc/15g1yJgc/plate1.png
plate2
https://i.postimg.cc/cH1P2my3/plate2.png
plate3
https://i.postimg.cc/rs3bd2dG/plate3.png
plate4
https://i.postimg.cc/Fsk8MzqT/plate4.png
plate5
https://i.postimg.cc/rw63Mc2M/plate5.png
video
https://i.postimg.cc/k5qHRc5H/video.png

## Hagamos nuestra primera sección

😄

![code4.png](https://static.platzi.com/media/user_upload/code4-93550506-e551-459e-8006-0d2ed173edd9.jpg)

![code3.png](https://static.platzi.com/media/user_upload/code3-97bf9473-15d4-4b81-ac5e-cc702d419fd3.jpg)

![code2.png](https://static.platzi.com/media/user_upload/code2-7f8741a1-7b5a-4342-82eb-3b8e3116f3e7.jpg)

## Creando la grilla con área

![grilla.jpg](https://static.platzi.com/media/user_upload/grilla-7e93c131-7dc9-4ed7-a6b6-19e35613d091.jpg)

HTML
![html1.jpg](https://static.platzi.com/media/user_upload/html1-cfe04d6e-6fdb-4a42-8bdd-65603c4f0ff3.jpg)

CSS
![css2.jpg](https://static.platzi.com/media/user_upload/css2-493cf2d1-955e-42aa-ad08-86427753cc65.jpg)/

## Armando el listado

![aporte.PNG](https://static.platzi.com/media/user_upload/aporte-4e15290d-f03f-4274-83bd-7192c6e04ebf.jpg)

![menu.jpg](https://static.platzi.com/media/user_upload/menu-0b633dba-28c3-4cd0-a41d-2a023b4f4fa1.jpg)

HTML
![html3.jpg](https://static.platzi.com/media/user_upload/html3-ca0a7583-ba48-4338-8c95-7b4caa77ea14.jpg)

CSS
![css3.jpg](https://static.platzi.com/media/user_upload/css3-3e696d38-147c-4d55-bcae-39075f8af8fc.jpg)

**Resultado Final**

![resultado.jpg](https://static.platzi.com/media/user_upload/resultado-8b0f1c28-3c7a-432d-b607-3f9cc5ed6bc3.jpg)

# 5. Bonus

## ¿Cómo hacer nuestro proyecto responsivo?

**parte visual**

![grid.jpg](https://static.platzi.com/media/user_upload/grid-1c2edab0-1228-4856-a2a4-668782fb3b84.jpg)

![grid2.jpg](https://static.platzi.com/media/user_upload/grid2-bb447947-c2b7-4282-a765-6f579770c644.jpg)

![grid3.jpg](https://static.platzi.com/media/user_upload/grid3-155dff98-70f1-49f7-90fc-243ddc68e344.jpg)

# 6. Próximos pasos

## Más cursos de CSS Grid

**Nunca pares de Aprender! 📚**
