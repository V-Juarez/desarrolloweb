<h1>WebPack Pr&aacutectico</h1>

<h3>Oscar Barajas</h3>

<h1>Tabla de Contenido</h1>

- [1. Introducción](#1-introducción)
  - [¿Por qué usar Webpack?](#por-qué-usar-webpack)
  - [Presentación del proyecto en React.js: PlatziStore](#presentación-del-proyecto-en-reactjs-platzistore)
- [2. Webpack en el Backend](#2-webpack-en-el-backend)
  - [Instalación y configuración de Express.js](#instalación-y-configuración-de-expressjs)
  - [Configuración de Webpack para Express.js](#configuración-de-webpack-para-expressjs)
  - [Configuración de TypeScript](#configuración-de-typescript)
  - [Preparar la API Rest con Express.js](#preparar-la-api-rest-con-expressjs)
- [3. Webpack en el Frontend](#3-webpack-en-el-frontend)
  - [Migracion de Webpack 4 a Webpack 5](#migracion-de-webpack-4-a-webpack-5)
  - [Configuración inicial de Webpack para PlatziStore](#configuración-inicial-de-webpack-para-platzistore)
  - [Cómo integrar la API de Platzi Store](#cómo-integrar-la-api-de-platzi-store)
  - [Integrando la API de Platzi Store](#integrando-la-api-de-platzi-store)
  - [Webpack Alias](#webpack-alias)
  - [Manejo de assets en Webpack](#manejo-de-assets-en-webpack)
  - [Optimización de imágenes con Webpack y CDN](#optimización-de-imágenes-con-webpack-y-cdn)
  - [Integración con TypeScript](#integración-con-typescript)
  - [Hot Reload](#hot-reload)
  - [Lazy Loading](#lazy-loading)
  - [Code Splitting en desarrollo](#code-splitting-en-desarrollo)
  - [Code Splitting en producción](#code-splitting-en-producción)
- [4. Despliegue](#4-despliegue)
  - [Deploy de React en Netlify](#deploy-de-react-en-netlify)
  - [Deploy de Express en Heroku](#deploy-de-express-en-heroku)
  - [Comprar y conectar un dominio](#comprar-y-conectar-un-dominio)
  - [Próximos pasos con Webpack](#próximos-pasos-con-webpack)

# 1. Introducción

## ¿Por qué usar Webpack?

¿Cual es la diferencia de configurar mi proyecto con Webpack y Create React App?
Pues tendremos el control total del proyecto,
Create React App es una librería que nos ayuda a crear aplicaciones robustas, pero desconocemos la configuración. Que NO utilizaremos.
EN CAMBIO con WEBPACK:

- Controlaremos como trabajar en modo de desarrollo.
- Como trabajar en Staging.
- Como trabajar en Modo de Producción
- Añadir configuraciones que se alineen al modelo de negocio de la aplicación
  **Beneficios:**
- Al tener una configuración 100% personalizada,
  ayuda a tener control **TOTAL**
- Teniendo total control ayuda a llevar nuestra aplicación a un siguiente nivel (una nueva implementacion, una nueva tecnología).
  Todo esto con el fin de que soporte grandes cargas al momento de llevar la aplicación al producción.

Trabajaremos el proyecto PlatziStore el cual lo vamos a migrar de webpack 4 a webpack 5 (la más actual),
a su ves:

- Añadiremos Algunas configuraciones con TypeScript.
- Lo prepararemos para el modo producción y modo desarrollo.
- Trabajaremos con una API que vamos a crear y **preparar** por medio de un servidor con Express
- Pondremos en práctica los alias.
- Pondremos en práctica los CDNs (Content Delivery Networks)
- Optimizaremos nuestras imágenes
- Trabajaremos con el modo: Hot reload, Lazy loading y Code Splitting
  **Tendremos 2 proyectos que se unificaran.**
  Haremos el Deploy en plataformas distintas.


## Presentación del proyecto en React.js: PlatziStore

Crear una tienda online, con react y configurar con Typescript. Subir lo a Heroku.

# 2. Webpack en el Backend

## Instalación y configuración de Express.js

```sh
mkdir webpack-express && cd webpack-express
```

para inicializar el proyecto:

```sh
git init && npm init -y
```

luego instalamos las dependencias:
Express:

```sh
npm install express -S 
```

Webpack y webpack-cli:

```sh
npm install webpack webpack-cli -D
```

Babel( Transpilar el código de cualquier estandar de ecma a navegadores para su compatibilidad):

```sh
npm install @babel/core @babel/preset-env babel-loader
```

Configuracion del proyecto:

- creamos la carpeta src, y el archivo dentro de src > index.js

Para escuchar en nuestro server:

```bash
node src/index.js
```

`url: localhost:3000` ó el puerto que pusieron.

Actualmente no es necesario ingresar el flag **-S** para guardar como dependencias de produccion, ya que npm por defecto las guarda de esta forma.

Pero si es necesario el flag **-D** para aquellas que son solo de desarrollo.

Aun que npm pareca algo sencillo, tiene mucho por dentro, si te animas puedes aprender mas de él en el curso de [npm](https://platzi.com/clases/npm/)

**#nuncaparesdeaprender**

Hay un pequeño trick que puedes usar para que puedas abrir tu navegador por defecto desde la terminal, y es que cuando muestres el puerto que esta escuchando le agreges una url completa de la siguiente forma:

```js
app.listen(port,()=>{
    console.log(`Server listen at http://localhost:${port}`);
})
```

Al poner el " **http…** " tu terminal reconocerá que se trata de una url, y solo bastará con darle click y automaticamente se te abrirá en tu navegador.

> Las dependencias de Babel se deberían salvar también como desarrollo:
> npm install @babel/core @babel/preset-env babel-loader -D

## Configuración de Webpack para Express.js

- Creamos en la raíz de nuestro proyecto el archivo: webpack.config.js
- En webpack.config.js:

```js
const path = require('path');
// path: la ubicacion del proyecto

module.exports = {
    //module.export: la info de nuestra configuracion webpack
    name: 'express-server',
    //name: nombre del proyecto
    entry: './src/index.js',
    // entry: punto de entrada de nuestra aplicacion
    target: 'node',
    output: {
        //output: donde guardamos el recurso que fue modularizado por webpack
        path: path.resolve(__dirname, 'dist'),
        //path: ubicacion del guardado
        //__dirname: donde esta el proyecto en el Sistema Operativo
        // 'dist'(distribution): nombre del folder, si no existe entonces creara dist
        filename: 'index.js',
        //filename: como llamar a la aplicacion cuando se prepare para produccion
    },
    resolve: {
        // extensions: con que extensiones trabajaremos
        extensions: ['.js']
    },
    module: {
        rules: [
            //rules: reglas con las que trabajaremos en el proyecto
            // se asigna a cada loader(optimizador), con que tipo de archivo trabajara y con que reglas
            {
                test: /\.(js)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            }
        ]
    }
}
```

Scripts del package.json:

```json
    "start": "node dist/index.js",
    "build": "webpack --mode production"
```

En “dev” hay dos formas que funcionan igual:

```json
"dev": "webpack --mode development && npm run start",
// ó
"dev": "webpack --mode development && node dist/index.js",
```

BABEL creamos en la raiz el archivo .babelrc:

```js
{
    "presets": [
        "@babel/preset-env"
    ]
} 
```

y corremos 

```sh
npm run dev
```



## Configuración de TypeScript

Typescript npm:

```sh
npm install typescript ts-loader @types/express -D
```

- Cambiamos la extensión de src/index.js a index.ts:
  Adaptamos Index a typescript:

```ts
import * as express from 'express';
import { Request, Response } from 'express';

const app = express();
const port = 3000;

app.get('/', (req: Request, res: Response) => {
    res.send('Hello Typescript')
});

app.listen(port, () => {
    console.log(`App listening on port: ${port}`);
})
```

- Creamos en la raiz el archivo .env (variables de entorno).
  .env :

```js
POST=3005
NODE_ENV=development
```

- En Webpack.config.js

```js
const path = require('path');
// path: la ubicacion del proyecto
const NODE_ENV = process.env.NODE_ENV;
const PORT = process.env.PORT;
...
module.exports = {
    name: 'express-server',
// ACTUALIZAMOS LA EXTENSIÓN DE INDEX
    entry: './src/index.ts',
    target: 'node',
// AÑADIMOS: mode
    mode: NODE_ENV,
// en resolve:
 extensions: ['.ts','.js']
// en rules añadimos
{
        test: /\.ts$/,
        use: [
            'ts-loader',
         ]
 }
```

- Creamos el archivo tsconfig.json:

```js
{
    "compilerOptions": {
        "sourceMap": true,
    }
}
```

Por ultimo instalamos:

```sh
npm install webpack-node-externals -D
```

Añadimos en webpack.config.js:

```js
const path = require('path');
const nodeExternals = require('webpack-node-externals');
....
module.exports = {
    mode: NODE_ENV,
    externals: [nodeExternals()],
}
```

y 

```sh
npm run build
```



## Preparar la API Rest con Express.js

1. Ingresar al siguiente link

```sh
https://gist.github.com/gndx/b7ecca73a219712708d858b77dea1f86
```

1. Copiar el recurso del initial state y pegarlo en el index.ts
2. Vamos a exponer esta data y retornaremos la data en formato JSON:

```js
app.get('/api/v1', (req: Request, res: Response) => {
    res.json(initialState)
})
```

1. Lanzamos el comando npm run dev y verificamos que este retornando la data en

```sh
 localhost:3000/api/v1
```

![img](https://www.google.com/s2/favicons?domain=https://gist.github.com/fluidicon.png)[InitialState-webpack.js · GitHub](https://gist.github.com/gndx/b7ecca73a219712708d858b77dea1f86)

# 3. Webpack en el Frontend

## Migracion de Webpack 4 a Webpack 5

Después de tener ya el proyecto en nuestro entorno de desarrollo.

- Actualizamos.
  Webpack y plugins:

```sh
npm install webpack@latest webpack-cli@latest babel-loader@latest html-webpack-plugin@latest mini-css-extract-plugin@latest
```

React:

```sh
npm install react@latest react-dom@latest react-router-dom@latest react-redux@latest redux@latest 
```

Luego 

```sh
 npm install
```

**Importante:** en package.json:
Cambiamos:

```json
"start": "webpack-dev-server --open --mode development",
```

Cambió, desde webpack 5 ya es:

```json
"start": "webpack serve --open --mode development",
```

y 

```sh
npm run start
```

Se generó al compilar el proyecto debido a que no se instauro la configuración de babel para soportar jsx. Lo solucioné actualizando todo lo relacionado a babel y creando el fichero de configuración de `.babelrc`

```js
{
  "presets": [
    "@babel/preset-env",
    [
      "@babel/preset-react",
      {
        "runtime": "automatic"
      }
    ]
  ]
}
```

![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)[Proyecto - GitHub - gndx/platzi-store at clase/10](https://github.com/gndx/platzi-store/tree/clase/10)

## Configuración inicial de Webpack para PlatziStore

- Creamos el archivo webpack.config.dev.js
- Copiamos todo el código de webpack.config.js y lo pegamos a webpack.config.dev.js
  Actualizamos en webpack.config.dev.js el apartado devServer:

```js
  devServer: {
    historyApiFallback: true,
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 3005,
  },
```

Instalamos las siguientes dependencias:

```sh
npm install terser-webpack-plugin clean-webpack-plugin css-minimizer-webpack-plugin -D
```

En webpack.config.js añadimos:

```js
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
....
const CSSMinimizerPlugin = require('css-minimizer-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
....
plugins: [
	......
	new CleanWebpackPlugin
],
optimization: {
    minimize: true,
    minimizer: [
      new CSSMinimizerPlugin(),
      new TerserPlugin(),
    ]
  },
```

Y en **package.json** actualizamos:

```sh
 "build": "webpack --mode production --config webpack.config.js",
 "start": "webpack serve --open --mode development  --config webpack.config.dev.js",
```

y 

```sh
npm run start 
# y/o 
npm run build
```

Error en css de `postcss` ejecutar 

```sh
npm install -D postcss postcss-cli
```

## Cómo integrar la API de Platzi Store

Para el manejo de peticiones asíncronas con `Async/Await` usamos el plugin `@babel/plugin-transform-runtime` .

```sh
npm i @babel/plugin-transform-runtime -D
```

![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)[https://us-central1-gndx-fake-api.cloudfunctions.net/api](https://us-central1-gndx-fake-api.cloudfunctions.net/api)

- Crearemos un hook para trabajar directamente con el llamado de esta API
  1.- Creamos una carpeta en src llamada hooks.
  2.- Creamos un archivo dentro de hooks > llamado useInitialState.js
  3.- Instalamos axios con: **npm install axios -S**
  4.- Instalamos: npm install @babel/plugin-transform-runtime -D
  5.- Actualizamos **.babelrc**

```js
{
  "presets": [
    "@babel/preset-env",
    "@babel/preset-react"
  ],
  "plugins": [
    "babel-plugin-transform-class-properties",
    "@babel/plugin-transform-runtime"
  ]
}
```

App.jsx:

```js
import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Home from '../containers/Home';
import Checkout from '../containers/Checkout';
import Layout from '../components/Layout';
import NotFound from '../containers/NotFound';

import useInitialState from '../hooks/useInitialState';

const App = () => {
  const initialState = useInitialState();
  const isEmpty = Object.keys(initialState.products).length;
  return (
    <>
    {isEmpty > 0 ? (
        <BrowserRouter>
          <Layout>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/checkout" component={Checkout} />
              <Route component={NotFound} />
            </Switch>
          </Layout>
        </BrowserRouter>
      ) : <h1>Loading ...</h1>}
    </>
  )
}

export default App;
```

useInitialState.js :

```js
import { useEffect, useState } from 'react';
import axios from 'axios';

const API = 'https://us-central1-gndx-fake-api.cloudfunctions.net/api';

const useInitialState = () => {
    const [products, setProducts] = useState([]);

    useEffect(async () => {
        const response = await axios(API);
        setProducts(response.data);
    }, []);

    return {
        products,
    }
}

export default useInitialState;
```

## Integrando la API de Platzi Store

- Creamos una carpeta dentro de **src** llamada **context**.
- Dentro de context creamos el archivo **AppContext.js**
  AppContext.js:

```jsx
import React from 'react';

const AppContext = React.createContext({});

export default AppContext; 
```

En App.jsx importamos AppContext:

```jsx
import useInitialState from '../hooks/useInitialState';
import AppContext from '../context/AppContext';
```

Y lo añadimos a nuestro template:

```js
const App = () => {
  const initialState = useInitialState();
  const isEmpty = Object.keys(initialState.products).length;
  return (
    <>
    {isEmpty > 0 ? (
	// START
        <AppContext.Provider value={initialState}>
        <BrowserRouter>
          <Layout>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/checkout" component={Checkout} />
              <Route component={NotFound} />
            </Switch>
          </Layout>
        </BrowserRouter>
        </AppContext.Provider>
	// END
      ) : <h1>Loading ...</h1>}
    </>
  )
}
```

Y Aqui el código de Products.jsx: [link](https://justpaste.it/3jwc9)

## Webpack Alias

Los Alias nos permiten otorgar **nombres paths específicos evitando los paths largos**
Para crear un alias debes agregar la siguiente configuración a webpack.

- En webpack.config.js Y webpack.config.dev.js:

```js
  resolve: {
    extensions: ['.js', '.jsx'],
    // añadimos alias
    alias: {
      '@components': path.resolve(__dirname, 'src/components/'),
      '@containers': path.resolve(__dirname, 'src/containers/'),
      '@context': path.resolve(__dirname, 'src/context/'),
      '@hooks': path.resolve(__dirname, 'src/hooks/'),
      '@routes': path.resolve(__dirname, 'src/routes/'),
      '@styles': path.resolve(__dirname, 'src/styles'),
    },
  }, 
```

En cada importacion de nuestros archivos, componentes, hooks, index, etc; basta con hacer la siguiente referencia.
Ejemplo:

```js
import Home from '@containers/Home';
```

En lugar de :

```js
import Home from '../containers/Home';
```

Esto resulta beneficioso porque la ruta puede cambiar mediante nuestra aplicación va creciendo, y solo basta cambiar el alias y no así archivo por archivo.

instale `eslint-import-resolver-webpack eslint-import-resolver-alias` y seguí su guía en github, pero no me ha funcionado para quitar los syntax errors de eslint

otras cosas que he probado es la guía de este link:

- https://medium.com/@justintulk/solve-module-import-aliasing-for-webpack-jest-and-vscode-74007ce4adc9
- .eslintrc

```json
...
  "import/resolver": {
    "webpack": {
      "config": "./webpack/webpack.dev.config.js"
    }
  }
```

## Manejo de assets en Webpack

Puedes usar una forma de importar las imágenes haciendo un import de las mismas y generando una variable.
No es necesario instalar ninguna dependencia, webpack ya lo tiene incluido.

- Creamos la carpeta assets dentro de src y dentro de assets descargamos la siguiente imagen:
  imagen: https://i.ibb.co/fr7JzwW/logo-gndx.png
- Agregamos un nuevo alias en webpack.config.dev.js, porque primero debemos probarla en dev(desarrollo) antes de llevarlo a producción:

```jsx
      '@assets': path.resolve(__dirname, 'src/assets'),
```

y añadimos en rules una nueva regla para los assets(es importante que en type se escriba **asset** y no assets):

```js
      {
        test: /\.(png|jpg)$/,
        type: 'asset/resource',
      },
```

En src > components > Header.jsx:

- importamos el logo

```jsx
import logo from '@assets/logo-gndx.png';
```

- Agregamos el logo en nuestro h1:

```jsx
    <h1 className="Header-title">
      <Link to="/">
        <img src={logo} alt="logo" width="32" />
        Platzi Store
      </Link>
    </h1>
```

Por último copiamos el alias y la nueva regla de webpack.config.dev.js en webpack.config.js.
npm run build => para probar.

## Optimización de imágenes con Webpack y CDN

Acá estamos solo optimizando imágenes sin pardina `.png` usando `imagemin-optipng`, pero si quieremos optimizar otras imagenes pueden instalar estos plugins:

#### Sin perdida => modo Lossless

- **.png**

```sh
npm install imagemin-optipng -D
```

- **.jpeg**

```sh
npm install imagemin-jpegtran -D
```

- **.svg**

```sh
npm install imagemin-svgo -D
```

- **.gif**

```sh
npm install imagemin-gifsicle -D
```

#### Con perdida => modo Lossy

- **.png**

```sh
npm install imagemin-pngquant -D
```

- **.jpeg**

```sh
npm install imagemin-mozjpeg -D
```

Para saber mas sobre la optimizacion de imagenes con webpack [Optimize Image](https://webpack.js.org/plugins/image-minimizer-webpack-plugin/) 😄

1.- instalamos los recursos:

```sh
npm install image-minimizer-webpack-plugin imagemin-optipng -D 
```

En webpack.config.dev.js y luego en webpack.config.js:

```jsx
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// Debajo de minicssextractplugin o por esta área
const ImageMinimizerPlugin = require('image-minimizer-webpack-plugin');
```

y luego en plugins:

```js
   new ImageMinimizerPlugin({
      minimizerOptions: {
        plugins: [
          ['optipng', { optimizationLevel: 5 }],
        ],
      },
    }),
```

**IMPORTANTE**: En rules habiamos agregado una regla para las imagenes.

```js
      {
        test: /\.(png|jpg)$/,
        type: 'asset/resource',
      },
```

y debemos modificarla de la siguiente manera:

```js
      {
        test: /\.(png|jpg)$/,
        type: 'asset',
      },
```

y 

```sh
npm run build && npm run start
```

 para verificar.

![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)[GitHub - webpack-contrib/image-minimizer-webpack-plugin: Webpack loader and plugin to compress images using imagemin](https://github.com/webpack-contrib/image-minimizer-webpack-plugin)

## Integración con TypeScript

Añadimos las dependencias:

```sh
npm install typescript ts-loader @types/react @types/react-dom @types/webpack
```

Cuando estamos trabajando con typescript, tenemos que agregar un archivo de su configuración llamado **tsconfig.json**:
tsconfig.json.

```js
{
    "compilerOptions": {
        "outDir": "./dist",
        "target": "es5",
        "module": "es6",
        "jsx": "react",
        "noImplicitAny": true,
        "allowSyntheticDefaultImports": true,
	"moduleResolution": "node",
	"baseUrl": "./",
	"paths": {
		"@components": [
			"src/components/*"	
		],
	}
    }
}
```

y añadimos una nueva regla en webpack.config.dev.js:
Y después en webpack.config.js

```js
{
	test: /\.tsx?$/,
	use: 'ts-loader',
	exclude: '/node_modules/',
},
```

Creamos en components > el archivo **Title.tsx**:

```jsx
import React from 'react';

type TitleProps = {
    title: string,
}

const Title = ({ title }: TitleProps) => <span>{title}</span>;

export default Title;
```

Lo importamos en components/Header.jsx:

```jsx
import Title from '@components/Title';
```

Y en la parte del logo, la agregamos:

```jsx
      <Link to="/">
        <img src={logo} alt="logo" width="32" />
        <Title title="Platzi Store" />
      </Link>
```

**IMPORTANTE**: añadir en webpack.config.js y webpack.config.dev.js > resolve:
La extensión .tsx

```js
    extensions: ['.tsx', '.js', '.jsx'],
```

y

```sh
npm run build && npm run start
```

 para verificar

## Hot Reload

- Nos permite **injectar** el código sin necesidad de compilar el proceso completo, solo hace un cambio en vez de estar preparando el proyecto completo.
  1.- Instalamos la dependencia:

```sh
npm install react-hot-loader 
```

2.- en .babelrc actualizamos los plugins:

```js
  "plugins": [
    "babel-plugin-transform-class-properties",
    "@babel/plugin-transform-runtime",
    "react-hot-loader/babel"
  ]
```

3.- En routes > App.jsx.
importamos hot:

```jsx
import { hot } from 'react-hot-loader/root';
```

y en la última linea(en la que exportamos), modificamos de la siguiente manera:

```jsx
export default hot(App); 
```

4.- Y solo en **webpack.config.dev.js**, ya que no necesitamos el hot reload para producción:
Modificamos el entry:

```jsx
module.exports = {
  entry: ['react-hot-loader/patch', './src/index.js'],
  .....
}
```

En devServer añadimos hot:

```json
  devServer: {
    historyApiFallback: true,
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 3005,
    hot: true,
  }, 
```

ejecutar  

```sh
npm run start
```

## Lazy Loading

### Lazy Loading

Este es una funcionalidad muy importante y funcional al momento de optimizar nuestra aplicación o página web.
Aqui esta una clase del curso de Optimizacion web que habla más a profundidad acerca del Lazy Loading.
Optimización Web que se los recomiendo mucho, ya que se puede ver como podemos, por ejemplo, ahorrar dinero optimizando nuestra web app, landing page, etc.


----------- **¿Qué hicimos en esta clase ?** ------------
En routes > App.jsx:
Actualizamos nuestra importacion de react(la primera).

```jsx
import React, { Suspense } from 'react';
```

Después de los imports agregamos el siguiente código:

```js
import .....
const AsyncCheckoutContainer = React.lazy(() => {
  import("@containers/Checkout")
});
```

Y el template debe quedar así:

```jsx
  return (
    <>
    {isEmpty > 0 ? (
        <Suspense fallback={<div>Loading...</div>}>
        <AppContext.Provider value={initialState}>
        <BrowserRouter>
          <Layout>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/checkout" component={AsyncCheckoutContainer} />
              <Route component={NotFound} />
            </Switch>
          </Layout>
        </BrowserRouter>
        </AppContext.Provider>
        </Suspense>
      ) : <h1>Loading ...</h1>}
    </>
  )
```

En webpack.config.dev.js deshabilitar hot-reload temporalmente:
Debe quedar algo así

```js
module.exports = {
  entry: ['./src/index.js'], 
  .....
}
```

Y en devServer comentamos hot:

```js
  devServer: {
    historyApiFallback: true,
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 3005,
    // hot: true,
  },
```

añadimos después de plugins:

```js
plugins: [
....
],
 optimization: {
   splitChunks: {
     chunks: 'all',
   },
 },
```

y en el output(en el comienzo de nuestro objeto):
**(Cambiamos el filename)**

```js
    filename: '[name].bundle.js',
```

y npm run start para probar.

## Code Splitting en desarrollo

## Code Splitting en desarrollo

Existe una clase del curso de Optimización web.
Podemos ver que el code splitting es dividir el código, es beneficioso el uso que se le quiera dar, ya que se puede implementar de diferentes formas.

-------------- **¿Qué hicimos en esta clase?** ------------
1.- Creamos una carpeta **Header** dentro de src.
2.- Creamos dentro de **Header**:
\- Un Archivo llamado index.js
\- Una carpeta llamada components.
\- Dentro de esa carpeta components se crea el archivo **App.jsx**
3.- en src/Header/components/App.jsx:

```jsx
import React from 'react';

const App = () => {
    return (
        <h1>Hello React From Header</h1>
    );
}

export default App;
```

4.- en src/Header/index.js:

```jsx
import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';


ReactDOM.render(<App />, document.getElementById('header'));
```

5.- **en public > index.html**:
Añadimos e div con id : header

```html
  <body>
    <div id="header"></div>
    <div id="app"></div>
  </body>
```

- En **webpack.config.dev.js**:
  Modificamos entry y output:

```js
module.exports = {
  entry: {
    home: './src/index.js',
    header: './src/Header/index,js',
  },
 .....
},
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].bundle.js',
    chunkFilename: '[name].bundle.js',
  },
```

```sh
npm run start
```

## Code Splitting en producción

1.- En **webpack.config.js** modificamos entry y output:

```js
module.exports = {
  entry: {
    home: './src/index.js',
    header: './src/Header/index.js',
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].bundle.js',
    chunkFilename: '[name].bundle.js',
  },
}
```

Y en optimization:

```sh
optimization: {
    minimize: true,
    minimizer: [
      new CSSMinimizerPlugin(),
      new TerserPlugin(),
    ],
    splitChunks: {
      chunks: 'all',
      cacheGroups: {
        default: false,
        commons: {
          test: /[\\/]node_modules[\\/](react|react-dom)[\\/]/,
          chunks: 'all',
          name: 'commons',
          filename: 'assets/common.[chunkhash].js',
          reuseExistingChunk: true,
          enforce: true,
          priority: 20,
        },
        vendors: {
          test: /[\\/]node_modules[\\/]/,
          chunks: 'all',
          name: 'vendors',
          filename: 'assets/vendor.[chunkhash].js',
          reuseExistingChunk: true,
          enforce: true,
          priority: 10,
        },
      },
    },
  },
```

construir el proyeto

```sh
 npm run build
```

![img](https://www.google.com/s2/favicons?domain=https://webpack.js.org/plugins/split-chunks-plugin//favicon.f326220248556af65f41.ico)[SplitChunksPlugin | webpack](https://webpack.js.org/plugins/split-chunks-plugin/)

# 4. Despliegue

## Deploy de React en Netlify

Hey! Es posible que te encuentres con este error cuando visites tu deploy en netlify e intentes recargar la pagina en una url difrente a la inicial o “/”

![img](https://res.cloudinary.com/practicaldev/image/fetch/s--wnl5nbWu--/c_imagga_scale,f_auto,fl_progressive,h_420,q_auto,w_1000/https://dev-to-uploads.s3.amazonaws.com/i/7kwc5kbz97o2ui9utneu.png)

Al parecer netlify Al cagar la pagina ira a buscar esa direccion en nuestro proyecto a ver si encuentra algun html , Ya que estamos construyendo un SPA sobre un unico index.html no encontrara nada por ende arrojara este error , puedes leer mas al respecto --> [Aca](https://answers.netlify.com/t/support-guide-i-ve-deployed-my-site-but-i-still-see-page-not-found/125/2)

Para solucionarlo primero deberas de asegurarte de tener un “publish directory” correcto en netlify , para eso vas a tu proyecto --> Site settings --> Build & Deploy , en el primer apartado “Build Settings --> publish directory” Debera estar el nombre del directorio donde webpack genera el build de tu aplicacion

Luego de esto crearas un archivo netlify.toml en la raiz de tu proyecto y le asignaras lo siguiente

```toml
[[redirects]]
  from = "/*"
  to = "/index.html"
  status = 200
```

De esta forma netlify siempre cargara tu index.html y tu aplicacion podra ser recargada en cualquier ruta , de nuevo puedes leer mas acerca de esto [Aca](https://answers.netlify.com/t/support-guide-i-ve-deployed-my-site-but-i-still-see-page-not-found/125/2) :3

## Deploy de Express en Heroku

1.- En src > index.ts:
Cambiar la constante port

```js
const port = process.env.PORT || 3005;
```

2.- Seguir los comandos de git para actualizar cambios.

```sh
git add .
git commit -m "UPDATE PORT"
```

En caso de no tener un repositorio en Github(necesario), debemos crear uno, luego:

```sh
git remote add origin url-de-tu-repositorio
git push origin master
```

3.- Crear una cuenta en Heroku: https://www.heroku.com/
4.- Create app con heroku
5.- Conectar con Github
6.- Buscar el repo que creamos y click en **Connect**
6.1.- Click en deploy branch (debe estar en master o la rama principal de tu repositorio)
LISTO!

![img](https://www.google.com/s2/favicons?domain=https://www.herokucdn.com/favicon.ico)[Cloud Application Platform | Heroku](https://www.heroku.com/)

## Comprar y conectar un dominio

Cloudflare

![img](https://www.google.com/s2/favicons?domain=https://www.namecheap.com/assets/img/nc-icon/favicon.ico)[Buy a domain name - Register cheap domain names from $0.99 - Namecheap](https://www.namecheap.com/)


## Próximos pasos con Webpack
