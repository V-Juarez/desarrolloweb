<h1>API REST con JavaScript<h1>

<h2>Juan David</h2>

<h1>Tabla de Contenido</h1>

- [1. Presentación del proyecto: PlatziMovies](#1-presentación-del-proyecto-platzimovies)
  - [TheMovieDB: análisis de su API](#themoviedb-análisis-de-su-api)
  - [Bocetos en papel y diseño en Figma](#bocetos-en-papel-y-diseño-en-figma)
- [2. Configuración inicial y maquetación del proyecto](#2-configuración-inicial-y-maquetación-del-proyecto)
  - [Configuración del entorno de desarrollo](#configuración-del-entorno-de-desarrollo)
  - [Maquetación del proyecto: HTML y CSS](#maquetación-del-proyecto-html-y-css)
  - [Consumiendo la API](#consumiendo-la-api)
  - [Lista de películas en tendencia](#lista-de-películas-en-tendencia)
  - [Lista de categorías](#lista-de-categorías)
- [3. Migración a Axios](#3-migración-a-axios)
  - [Navegación](#navegación)
  - [Location y hash navigation](#location-y-hash-navigation)
  - [Mostrando y ocultando secciones](#mostrando-y-ocultando-secciones)
  - [Error: carga duplicada de datos](#error-carga-duplicada-de-datos)
- [4. Views](#4-views)
  - [Filtrando películas por categoría](#filtrando-películas-por-categoría)
  - [Retos: scrollTop y DRY](#retos-scrolltop-y-dry)
  - [Buscador de películas](#buscador-de-películas)
  - [Retos: historial de navegación y página de tendencias](#retos-historial-de-navegación-y-página-de-tendencias)
  - [Endpoint de detalles de una película](#endpoint-de-detalles-de-una-película)
  - [Lista de películas recomendadas](#lista-de-películas-recomendadas)

# 1. Presentación del proyecto: PlatziMovies

## TheMovieDB: análisis de su API

[Rapid API](https://rapidapi.com/hub)

[![img](https://www.google.com/s2/favicons?domain=https://developers.themoviedb.org/3/getting-started/introduction/favicon.ico)API Docs](https://developers.themoviedb.org/3/getting-started/introduction)

## Bocetos en papel y diseño en Figma

![cap2.png](https://static.platzi.com/media/user_upload/cap2-ead95d45-8bfb-4857-8701-0ddffcf58d32.jpg)

![2022-06-09_22h38_06.png](https://static.platzi.com/media/user_upload/2022-06-09_22h38_06-0771fc80-df45-472d-bf19-ead229984263.jpg)

![Home.jpg](https://static.platzi.com/media/user_upload/Home-5f5a978b-83d2-4eaf-ad7b-7dc197f56d82.jpg)

![Explore.png](https://static.platzi.com/media/user_upload/Explore-b19f3378-79e9-4b14-bfd6-04e74e5abaac.jpg)

![Results.png](https://static.platzi.com/media/user_upload/Results-2ee2dff5-c53e-4ab1-a89b-1703616464ff.jpg)

[Material.io](https://material.io/)

# 2. Configuración inicial y maquetación del proyecto

## Configuración del entorno de desarrollo

En el HTML decimos que nuestro js va a ser de tipo módulo.

```js
<script type="module" src="./src/main.js"></script>
```

Nuestra api-key

```js
export const API_KEY = "soy la secreta api-key";
```

La importamos en el main.js

```js
import { API_KEY } from "./key.js";
```

## Maquetación del proyecto: HTML y CSS

[![img](https://www.google.com/s2/favicons?domain=https://developers.themoviedb.org/3/getting-started/introduction/favicon.ico)API Docs](https://developers.themoviedb.org/3/getting-started/introduction)

[![img](https://www.google.com/s2/favicons?domain=https://developers.themoviedb.org/3/trending/get-trending/favicon.ico)API Docs](https://developers.themoviedb.org/3/trending/get-trending)

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - platzi/curso-api-rest-javascript-practico at 1d85ea74cc83341581455b040d895fb620268155](https://github.com/platzi/curso-api-rest-javascript-practico/tree/1d85ea74cc83341581455b040d895fb620268155)

# 3. Consumiendo la API

## Lista de películas en tendencia

Para los que quieren generar la API key:

1. entren a TMDB, luego hacen log in.
2. A la derecha arriba le dan click a su avatar
3. en settings o configuracion, se van a API
4. ingresan los campos que les piden
5. copian y pegan el API Key que se les genera en el archivo secrets.js

```js
async function getTrendingMoviesPreview() {
  const res = await fetch(
    "https://api.themoviedb.org/3/trending/movie/day?api_key=" + API_KEY
  );
  const data = await res.json();

  const movies = data.results;
  movies.forEach(movie => {
    const trendingPreviewMoviesContainer = document.querySelector(
      "#trendingPreview .trendingPreview-movieList"
    );

    const movieContainer = document.createElement("div");
    movieContainer.classList.add("movie-container");

    const movieImg = document.createElement("img");
    movieImg.classList.add("movie-img");
    movieImg.setAttribute("alt", movie.title);
    movieImg.setAttribute(
      "src",
      "https://image.tmdb.org/t/p/w300" + movie.poster_path
    );

    movieContainer.appendChild(movieImg);
    trendingPreviewMoviesContainer.appendChild(movieContainer);
  });
}

getTrendingMoviesPreview();
```

[![img](https://www.google.com/s2/favicons?domain=https://developers.themoviedb.org/3/getting-started/introduction/favicon.ico)API Docs](https://developers.themoviedb.org/3/getting-started/introduction)

[![img](https://www.google.com/s2/favicons?domain=https://developers.themoviedb.org/3/trending/get-trending/favicon.ico)API Docs](https://developers.themoviedb.org/3/trending/get-trending)

## Lista de categorías

```js
async function getCategegoriesPreview() {
  const res = await fetch(
    "https://api.themoviedb.org/3/genre/movie/list?api_key=" + API_KEY
  );
  const data = await res.json();

  const categories = data.genres;
  categories.forEach(category => {
    const previewCategoriesContainer = document.querySelector(
      "#categoriesPreview .categoriesPreview-list"
    );

    const categoryContainer = document.createElement("div");
    categoryContainer.classList.add("category-container");

    const categoryTitle = document.createElement("h3");
    categoryTitle.classList.add("category-title");
    categoryTitle.setAttribute("id", "id" + category.id);
    const categoryTitleText = document.createTextNode(category.name);

    categoryTitle.appendChild(categoryTitleText);
    categoryContainer.appendChild(categoryTitle);
    previewCategoriesContainer.appendChild(categoryContainer);
  });
}

getTrendingMoviesPreview();
getCategegoriesPreview();
```

## Migración a Axios

```js
const api = axios.create({
  baseURL: "https://api.themoviedb.org/3/",
  headers: {
    "Content-Type": "application/json;charset=utf-8"
  },
  params: {
    api_key: API_KEY
  }
});

async function getTrendingMoviesPreview() {
  const { data } = await api("trending/movie/day");
  const movies = data.results;

  movies.forEach(movie => {
    const trendingPreviewMoviesContainer = document.querySelector(
      "#trendingPreview .trendingPreview-movieList"
    );

    const movieContainer = document.createElement("div");
    movieContainer.classList.add("movie-container");

    const movieImg = document.createElement("img");
    movieImg.classList.add("movie-img");
    movieImg.setAttribute("alt", movie.title);
    movieImg.setAttribute(
      "src",
      "https://image.tmdb.org/t/p/w300" + movie.poster_path
    );

    movieContainer.appendChild(movieImg);
    trendingPreviewMoviesContainer.appendChild(movieContainer);
  });
}

async function getCategegoriesPreview() {
  const { data } = await api("genre/movie/list");

  const categories = data.genres;
  categories.forEach(category => {
    const previewCategoriesContainer = document.querySelector(
      "#categoriesPreview .categoriesPreview-list"
    );

    const categoryContainer = document.createElement("div");
    categoryContainer.classList.add("category-container");

    const categoryTitle = document.createElement("h3");
    categoryTitle.classList.add("category-title");
    categoryTitle.setAttribute("id", "id" + category.id);
    const categoryTitleText = document.createTextNode(category.name);

    categoryTitle.appendChild(categoryTitleText);
    categoryContainer.appendChild(categoryTitle);
    previewCategoriesContainer.appendChild(categoryContainer);
  });
}

getTrendingMoviesPreview();
getCategegoriesPreview();
```

# 4. Navegación

## Location y hash navigation

## Mostrando y ocultando secciones

## Error: carga duplicada de datos

# 5. Views

## Filtrando películas por categoría

## Retos: scrollTop y DRY

## Buscador de películas

## Retos: historial de navegación y página de tendencias

## Endpoint de detalles de una película

## Lista de películas recomendadas
