<h1>Sistemas de Diseño</h1>

<h3>Rulótico González</h3>

<h1>Tabla de Contenido</h1>

- [1. ¿Qué es un Sistema de Diseño?](#1-qué-es-un-sistema-de-diseño)
  - [Introducción y presentación](#introducción-y-presentación)
  - [¿Qué es un Sistema de Diseño?](#qué-es-un-sistema-de-diseño)
  - [Bonus: Utilizando Notion](#bonus-utilizando-notion)
- [2. Principios del Sistema de Diseño](#2-principios-del-sistema-de-diseño)
- [Principios del Sistema de Diseño](#principios-del-sistema-de-diseño)
- [3. Niveles de Sistematización](#3-niveles-de-sistematización)
  - [Niveles de Sistematización](#niveles-de-sistematización)
  - [Bonus: Utilizando Coggle](#bonus-utilizando-coggle)
- [4. Paradigmas de Sistemas de Diseño](#4-paradigmas-de-sistemas-de-diseño)
  - [Paradigmas: Diseño atómico, diseño procedural y DRY](#paradigmas-diseño-atómico-diseño-procedural-y-dry)
  - [Bonus: Utilizando Adobe XD](#bonus-utilizando-adobe-xd)
- [5. ¿Qué es un componente?](#5-qué-es-un-componente)
  - [¿Qué es un componente?](#qué-es-un-componente)
- [6. Foundations](#6-foundations)
  - [Foundations](#foundations)
- [7. Tipografía](#7-tipografía)
  - [Tipografía](#tipografía)
- [8. Colores](#8-colores)
  - [Bonus: Cómo evitar colores constrastantes](#bonus-cómo-evitar-colores-constrastantes)
  - [Paleta de colores](#paleta-de-colores)
- [9. Reglas de espaciado](#9-reglas-de-espaciado)
  - [Reglas de espaciado](#reglas-de-espaciado)
- [10. Animación](#10-animación)
  - [Animación](#animación)
  - [Consejos para empezar a animar usando After Effects](#consejos-para-empezar-a-animar-usando-after-effects)
- [11. Voz y tono](#11-voz-y-tono)
  - [Voz y tono](#voz-y-tono)
  - [12. Icon System](#12-icon-system)
  - [Iconografía](#iconografía)
- [13. Hitos](#13-hitos)
  - [Hitos](#hitos)
- [14. Bonus: UI Kit](#14-bonus-ui-kit)
  - [Bonus: UI Kit](#bonus-ui-kit)
- [15. Un sistema basado en personas](#15-un-sistema-basado-en-personas)
- [Un sistema basado en personas](#un-sistema-basado-en-personas)
- [16. Iteremos](#16-iteremos)
  - [Iteremos](#iteremos)
- [17. Cierre](#17-cierre)
  - [Cierre](#cierre)

# 1. ¿Qué es un Sistema de Diseño?

## Introducción y presentación

En este curso aprenderás a crear sistemas de diseño desde cero así como todas las herramientas que tienes para dialogar y trabajar junto con otros equipos a la hora de crear o iterar productos de software.

Nuestro profesor **Rulótico González** es un diseñador interactivo, en otras palabras, es diseñador y programador al mismo tiempo. Como diseñador y programador transitó en el diálogo y los conflictos entre estas profesiones, fue así como encontró los sistemas de diseño, creando mejores procesos y estructuras para dialogar.

> Lo de Unicornio es porque no existen 🦄😛

<img src="https://notion-emojis.s3-us-west-2.amazonaws.com/v0/svg-twitter/1f21a.svg" weith="200" height="100" alt="🈚" style="zoom:25%;" />[Design System](https://www.notion.so/Design-System-9141c3678ecf4a29a48df9f60ca98e1b)

## ¿Qué es un Sistema de Diseño?

Los sistemas de diseño están en todas partes, su función tiene que ver con la planeación y la construcción de productos *(no necesariamente digitales)*.

Hace muchos años, un hombre llamado Malcom McLean transportaba mercancías en sus barcos, pero la descarga de todos estos objetos era muy compleja, todo tenía tamaños y pesos distintos. Para resolver este problema, McLean trabajo con un grupo de ingenieros y crearon los *containers* o contenedores, cajas de metal que agilizan este proceso gracias la estandarización de todos los sistemas y medios de transporte de estos contenedores.

Esto mismo sucede cuando trabajamos con software, buscamos una manera de agilizar nuestros desarrollo estandarizando todos estos procesos. Los sistemas de diseño son conjuntos de reglas para un producto que se modifica dependiendo de las necesidades del mismo, son una decisión de negocio. Cualquier persona del equipo de producto debe estar involucrada en este proyecto y poder consultar toda esta documentación.

Para crear un sistema de diseño podemos usar diferentes herramientas como Bear, Notion o Evernote, lo importante es que podamos escribir y organizar estos sistemas.

Partes del Design System:

- **Principios**: marcan la pauta y las reglas de nuestro producto
- **Sistema**: Cual va a ser el flujo de creación (tiempos y forma)
- **Foundations**: Bases de todo lo visual
- **Componentes**: Partes que forman un todo

> Un **Sistema de Diseño** es un conjunto de reglas para crear productos digitales de forma *escalable* y *repetible* que se *modifica* dependiendo de las necesidades del mismo.

Los Design Systems o Sistemas de Diseño son un conjunto de reglas para un producto que se modifica o puede mutar dependiendo de las necesidades del mismo.

Ahora bien básicamente con los Design Systems lo que se busca generar estándares dentro de los procesos de Desarrollo/Diseño

 ***"El sistema de diseño no es una decision creativa, es una decision de negocios "***

sistemas de diseño:

- [bear](https://bear.app/)
- [notion](https://www.notion.so/)
- [evernote](https://evernote.com/)

- [How design systems can help build and prototype better digital products](https://uxdesign.cc/how-design-systems-can-help-build-and-prototype-better-digital-products-426f1de7cefc)

[La mejor aplicación para tomar notas | Organiza tus notas con Evernote](https://evernote.com/intl/es)

![img](https://www.google.com/s2/favicons?domain=https://www.notion.so//images/favicon.ico)[Notion – The all-in-one workspace for your notes, tasks, wikis, and databases.](https://www.notion.so/)

![img](https://www.google.com/s2/favicons?domain=https://www.notion.so/Design-system-1bdbab2c4c354f80944c1fb93d3743ff/images/favicon.ico)[Notion – The all-in-one workspace for your notes, tasks, wikis, and databases.](https://www.notion.so/Design-system-1bdbab2c4c354f80944c1fb93d3743ff)

## Bonus: Utilizando Notion

**Notion** sirve para cualquier cosa, incluso para propuestas de matrimonio 😂😎💍.

⚡Pueden ver todas las notas de la clase en este [Notion](https://www.notion.so/simon98/Design-System-a2fa420384a941d387f943b274a40db1), además le agregué recursos extras, espero les guste.

✔También escribí el siguiente tutorial [8 atajados de teclado para ser un máster en Notion](https://platzi.com/tutoriales/1420-sistemas-diseno/4832-8-atajados-de-teclado-para-ser-un-master-en-notion/).


# 2. Principios del Sistema de Diseño

## Principios del Sistema de Diseño

Los principios del diseño son la base fundamental de los paradigmas y metodologías que aplicaremos en nuestro sistema. Todas las decisiones que tomemos deben tener en cuenta y fundamentarse en estos principios, así evitamos cometer errores o generar conflictos, ya que todos los caminos serian correctos.

Principios del sistema de diseño:

- **Accesibilidad**: Nuestro producto debería ser usable para cualquier usuario, debemos construir productos **perceptibles**, **operables**, **entendibles** y **robustos**.
- **Consistencia**: Todo el equipo deben tener los mismos objetivos y deben hablarle de la misma forma a los usuarios, sea visualmente o por escrito.
- **Reusabilidad**: Cada hora que invirtamos trabajando en algún componente debe servir para construir otros componentes y evitar que tu o alguien más tenga que reconstruir nuestro trabajo.
- **Shareable**: Debemos construir y trabajar con herramientas que nos permitan compartir todo nuestro trabajo.

Podemos añadir otros principios que se adecuen correctamente a nuestra empresa, pero debemos tener cuidado de que todos estos principios sean compatibles entre sí.

En nuestro caso, añadiremos los principios de ***User Control\*** *(el usuario debe tener la sensación de control sobre el producto)*, ***Forgiveness\*** *(debemos permitir que el usuario pueda cambiar de opinión o volver a empezar algún proceso)* y ***Perceived stability\*** *(aún con plataformas robustas y complejas, el usuario debe percibir los procesos tan simples y familiares como sea posible)*.

Podemos complementar todo lo que aprendimos sobre sistemas de diseño con este artículo de Marcin Treder, el creador de [UXPin](https://www.uxpin.com/):

- [Design System Sprint 4: Design Principles](https://medium.com/@marcintreder/design-system-sprint-4-design-principles-8efb22d8a208)

**Primeros principios:**

**Accesibilidad: **Todos los usuarios puedan utilizar la plataforma. Debe ser perceptible, operable (Debe poder ser utilizada), Entendible (En este punto tener en cuenta el lenguaje, más si el producto se exporta a otros países). Robusto (Debe darle oportunidad a todos los tipos de usuarios, por ejemplo a los daltonicos).

**Consistencia:** Siempre hablarle de una misma manera al usuario, de manera escrita, visual, en procesos. Esto permitirá al usuario menos esfuerzo utilizar nuestro producto.

**Reusable:** Es importante que cada hora de trabajo que inviertamos tanto en diseño, desarrollo, etc. Sea reutilizable.

**Shareable:** Toda la documentación debe ser compartido.

La plataforma puede ser restrictiva ( Guiar siempre al usuario para cumplir un objetivo), permisiva ( perdona ciertos errores) Forgiveness (Perdonar al usuario si hace algo mal, darle la posibilidad de volver atrás, etc)

Los principios lo tenemos que decidir nosotros, es como crear una personalidad. Un buen punto de partida para definir los principios pueden ser las 10 reglas Heurísticas.

# 3. Niveles de Sistematización

## Niveles de Sistematización

El ***Design System Workflow\*** nos ayuda a entender mucho mejor cómo entra, por dónde sale y por cuáles puntos pasa nuestro trabajo cuando trabajamos con sistemas de diseño.

En este *workflow* o flujo de trabajo comenzamos diseñando y documentando para después hacer deploy *(un término de programación para los momentos en que pasamos nuestro trabajo a producción, una versión funcionando en vivo)*, en esta etapa de deploy debemos tener muy claro para cuál plataforma o entorno estamos trabajando, así tendremos mucho más claro todo lo que vamos a necesitar construir *(por ejemplo, cuando construimos páginas web trabajamos en archivos `.css` con los estilos de nuestros productos)*.

Pero el trabajo no termina aquí, después de hacer deploy entramos a la etapa de testing para evaluar la efectividad de estos estilos y seguirlos mejorando.

También vamos a aprender sobre ***Building Design Systems\***, donde construimos herramientas para que todos puedan usar e implementar las guías y los estilos que estamos trabajando. Para organizarnos y sistematizar estos procesos podemos implementar el modelo solitario, dónde tú, el diseñador haces todo el trabajo para que alguien más lo consuma, pero existen otros modelos como el centralizado o el confederado donde trabajamos con otras personas o incluso otros equipos y pedimos feedback dependiendo de la organización de nuestras empresas.

Los sistemas de diseño tiene niveles y cada nivel abarcan diferentes partes del equipo.
Un sistema de diseño es un equipo que convierte lo que se esta pensando se convierta en software. No importa todo lo que tenemos en ui kit, wireframe, prototipo, si al final no es creado en software.
En un sistema de diseño intervienen todo tipo de perfiles (Ux, User research, frontend, backend, etc).

**TIPOS DE MODELO organizativos**

**• Solitario:** tú construyes, los demás consumen
**• Centralizado:** tú consultas a los demás departamentos y construyes un SD
**• Confederado:** varios equipos dentro de varios departamentos y todos construyen el sistema.

> Un sistema de diseño NO es el uso de las tecnologías aplicadas a los procesos del diseño, son equipos que transforman diseños, pensamientos e ideas en productos reales de software.
>
> **#EPIC!**

Normalmente los diseñadores trabajamos diseñando pantallas para los clientes de las agencia o para los productos internos de una compañía 🏥🏨🏦, pero no entendemos el cómo, el por qué o el para qué de lo que estamos haciendo 🤔, y cuando trabajamos con plataformas o productos más grandes y robustos nos encontramos con procesos mucho más complejos como recibir feedback, trabajar con versiones diferentes, iteraciones, etc 😕😰.

🎉 Gracias a los sistemas de diseño vamos a organizar y sistematizar estos procesos y hacernos la vida más fácil y ágil 😎👌.

<h3>Niveles</h3> 

<h4>SD Design Workflow</h4>

Es tener una nocion particular de como entra algo, como sale, y porque puntos pasa.

Ej.  diseña - documenta -deploy- css o mobile - test - iteration

Un sistema de diseño va hacia algun lugar puede ir por ejemplo a una pagina web o a un dispositivo mobile. El test nos sirve para medir la efectividad, hay que poner alguna forma de calificar el exito, ej. cuanta gente lo uso, cuanta gente le dio like.

<h4>Building Design Systems</h4>

Tu diseñas pero en algún momento se construye, esto es un paso para construir herramientas para que otro haga algo que tu estas haciendo. Como lo sistematizas?

Puedes usar modelo solitario. Tu les das a los demás… viii, y los demás consumen.

O usar modelo centralizado, tienes un equipo, puedes consultar a otros departamentos del proyecto.

O modelo confederado, tienes varios equipos dentro de departamentos diferentes, y luego desarrollan el sistema hacia adentro despues lanzan la info hacia ceo o cliente, pero tu eres el lider como departamento de diseño y guias al resto.

> Los Systems Design Teams construyen productos a partir de pensamientos e ideas.

![img](https://www.google.com/s2/favicons?domain=https://static.coggle.it/img/favicon64.png?_v=1540053867)[Simple Collaborative Mind Maps & Flow Charts - Coggle](https://coggle.it/)

![img](https://www.google.com/s2/favicons?domain=https://www.notion.so/Design-system-1bdbab2c4c354f80944c1fb93d3743ff/images/favicon.ico)[Notion – The all-in-one workspace for your notes, tasks, wikis, and databases.](https://www.notion.so/Design-system-1bdbab2c4c354f80944c1fb93d3743ff)

## Bonus: Utilizando Coggle

**Crear mapas mentales:**

[MindMeister](https://www.mindmeister.com/es)
[Ayoa](https://www.ayoa.com/)
[Milanote](https://milanote.com/?utm_source=CBTmindmap&utm_medium=referral&dpm=39488)
[MindGenius](https://www.mindgenius.com/mindgenius-20/)
[Miro](https://miro.com/)

![img](https://www.google.com/s2/favicons?domain=https://static.coggle.it/img/favicon64.png?_v=1547042475)[Simple Collaborative Mind Maps & Flow Charts - Coggle](https://coggle.it)

# 4. Paradigmas de Sistemas de Diseño

## Paradigmas: Diseño atómico, diseño procedural y DRY

Existen varios paradigmas de diseño que pueden guiarte en la construcción de tu sistema de diseño.

El paradigma **procedural** es la construcción de objetos variados para posteriormente construir nuestros sistemas a partir de estos elementos, es la construcción de funciones que en el futuro se construirán a sí mismas. En vez de construir todos los objetos de nuestro sistema, vamos a trabajar los elementos y reglas que se resolverán después. Por ejemplo, construir las mangas, el cuello u otros elementos para al obtener el resultado de un abrigo.

Otro paradigma que debemos entender es el **diseño atómico** para organizar los elementos de nuestros diseños y software. Los elementos independientes más sencillos de nuestro sistema *(labels, inputs, botones, títulos, etc)* los conocemos como **átomos**, pero cuando juntamos átomos construimos **moléculas**, elementos sencillos unidos entre sí *(un buscador por ejemplo, el conjunto de label + input + botón)*, y formamos **organismos** cuando juntamos moléculas *(por ejemplo, una barra de navegación con logos, enlaces y un buscador)*.

Con la suma de estos organismos generamos **templates**, la forma más básica de nuestras plataformas qué podemos utilizar en diferentes partes de nuestros diseños aplicando pequeños cambios en los átomos, y cuando aplicamos estos cambios para las partes especificas de nuestro sitio obtenemos **páginas**, el resultado final de toda la organización de elementos independientes hasta formas conjuntos de organismos complicados y armónicos entre sí.

Por último, vamos a entender el paradigma **dry** *(Don’t Repeat Yourself)* para construir elementos reciclables y no gastar tiempo volviendo a construir los mismos objetos una y otra vez.

**Procedural:** Permite la construcción de funciones que a futuro se van a reconstruir a sí mismas. Se basa en objetos ya hechos para obtener resultados diferentes. Por ejemplo, cuando se usa código se utiliza el concepto procedural.

**Atómico:** Le da una organización a los elementos: átomos, moléculas, organismos, templates, páginas. Es decir, ir desde los elementos más sencillos como las etiquetas más básicas, hasta los elementos mayores que corresponden a la unión de varios átomos, moléculas, organismos y templates para obtener una página.

**DRY:** Don’t Repeat Yourself. No repetir los mismos elementos.

<img src="https://i.ibb.co/3YGT6VM/atomico.png" alt="atomico" border="0">

Este artículo del blog de Platzi también explica muy bien cómo funciona y por qué usar **Atomic Design**:

- [⚛ ¿Por qué usar Atomic Design? ⚛](https://platzi.com/blog/por-que-atomic-design/)

## Bonus: Utilizando Adobe XD

Estoy más cerca de hacer un sistema económico-social que lo pete organizado entorno a una plataforma de educación. He aquí alguno de sus principios:

- Compromisocrático = Democracia ponderada. Todos los hombres tenemos dignidad pero no todos los hombre somos iguales. El sistema recompensa el compromiso.
- Curation Rewards (Porque el obrero es digno de un salario): Ganas Influence Points cuando haces like push.
- Emision de Moneda descentralizada
- Cantidad de Moneda Finita pero infinitesimalmente divisible => Moneda deflaccionaria (mejor que la actual moneda fiat inflaccionaria)
- Minado activado por generación de recursos: Invierte el principio actual de “emisión de moneda -> generación de recursos” siendo el nuevo principio "generación de recursos -> emisión de moneda: Por ejemplo: primero generas electricidad con tus paneles solares => recibes la recompensa en cripto correspondiente.
  -Separación del poder legislativo del económico mediante dos sistemas de puntos: Influence Points (para gobernanza compromisocrática) y Moneda (una cripto de emisión descentralizada que siga los principios de este manifiesto)

# 5. ¿Qué es un componente?

## ¿Qué es un componente?

Los componentes forman parte de un todo, así como las velas de un pastel, el piso de los edificios o las partes de un motor cada una con su funcionalidad. Lo mismo pasa con las interfaces, vamos a construir diferentes elementos y herramientas para que los usuarios logren cumplir sus objetivos.

Todos los componentes tienen una entrada y una salida, el usuario realiza una acción y los elementos deben responder de alguna forma *(feedback)*, esta es la forma de comunicamos con los usuarios, trabajamos haciendo conversaciones para informar que todo esta funcionando correctamente y qué pasos deben seguir a continuación. Este proceso de comunicación lo conocemos como **interacción**.

Partes de un componente:

- Nombre *(así evitamos diferentes definiciones y establecemos los objetivos y funciones de nuestros componentes)*
- Descripción y solución *(en qué problema estamos trabajando y cómo deberíamos implementar estas soluciones)*
- *Behavior* *(el comportamiento de nuestros componentes dentro del sistema)*
- States *(las variaciones y distintos comportamientos que pueden tener nuestros componentes dependiendo de su contexto)*.

> En el diseño “antiguo” *(el diseño gráfico)* los afiches o las impresiones que construimos son estáticas, no tienen ningún tipo de interacción, pero todo esto cambia en el diseño digital, nuestra prioridad y nuestro trabajo más importante es comunicarnos con los usuarios para darles feedback sobre las acciones que están realizando, así también recibimos feedback por parte del los usuarios y podemos construir productos de máxima calidad.

Un componente forma parte de un todo. Un componente tiene una entrada y una salida. La entrada seria el punto donde el usuario empieza a utilizar el componente y la salida seria el feedback que da el componente en base a la acción del usuario. Ej: Tenemos un componente que tiene la funcionalidad de Buscar un usuario de nuestro sistema. La entrada es cuando se coloca los datos del usuario que queremos buscar y la salida es cuando el componente nos arroja resultado de la búsqueda.

La conversación entre el componente y el usuario (Entrada y salida) se llama interacción.Los componente deben tener:
\* **Nombre** único
\* **Descripción** (Que funcionalidad y que problema soluciona)
\* **Behavior**: Es un comportamiento que tiene el componente dentro del sistema. (Un ejemplo puede ser, como es el comportamiento en mobile)
\* **States**: Es como se ve el componente cuando se inicia la pagina (Prendido, apagado, desplegado, oculto, con datos por defecto, sin datos por defecto, etc)

**Martín Coronel**
Diseño UX/UI

[Web](http://martincoronel.com/)
[Facebook](https://www.facebook.com/mcoronel01/)
[Behance](https://www.behance.net/martincoronel)

![img](https://www.google.com/s2/favicons?domain=https://www.notion.so/Design-system-1bdbab2c4c354f80944c1fb93d3743ff/images/favicon.ico)[Notion – The all-in-one workspace for your notes, tasks, wikis, and databases.](https://www.notion.so/Design-system-1bdbab2c4c354f80944c1fb93d3743ff)

# 6. Foundations

## Foundations

Los sistemas de diseño son un conjunto de reglas que organizamos con nuestros equipos, y los fundations son las partes más básicas que podemos configurar en nuestro sistema. Vamos a repasar cada una de estas bases mientras escribimos la documentación en Notion:

- Tipografía
- Colores
- *Layout* y *spaces* *(son las formas y espacios fundamentales que utilizamos para ordenar los elementos de nuestro sistema)*
- Iconografía
- *Styles (nuestra marca puede presentarse con estilos juguetones o realistas, lo importante es definir tan claro como sea posible qué intentamos transmitir)*
- Tono *(con qué personalidad o de qué forma debemos hablar con nuestra audiencia)*

Fundations son los elementos configurables con el cual vamos a construir distintos elementos de nuestro producto.

**Tipografía**: Definir los tamaños en especifico y familia tipografica en cada caso de uso.

**Colores:** Se definen también desde le principio para que el resultado sea el planeado y no se vaya tomando decisiones en el camino

**Layout- spaces:** Layout determina como van a estar distribuido nuestros elementos en la pagina. (Ej: Sidebar a la derecha, Tirulos, descripción de la pagina, etc) y ademas debemos tener en cuenta el espaciado entre los elementos.

**Iconografía:** Definir las reglas de la iconografia en nuestro sistema nos ayudara a mantener la consistencia. Tener en cuenta el tipo de iconos (Outline, relleno, emojis, etc(

**Style:** (nuestra marca puede presentarse con estilos juguetones o realistas, lo importante es definir tan claro como sea posible qué intentamos transmitir)
Tono: Que tipo de personalidad tiene nuestro producto, como habla, que características y que NO debería decir.

> Documentar lleva su tiempo pero a la larga nos hará fácil de trabajar en los diferentes etapas de un sistema(wiframes, sketch y employ).

# 7. Tipografía

## Tipografía

Cuando trabajamos con tipografía para software debemos tener en cuenta las implicaciones que estas pueden generar si trabajamos para dispositivos móviles o los requerimientos técnicos de alguna pantalla en particular. Un buen lugar para encontrar tipografías listas para el desarrollo y diseño de nuestros productos es [Google Fonts](https://fonts.google.com/).

Para definir y clasificar las características y peculiaridades en los elementos de nuestro diseño podemos basarnos las etiquetas de HTML para títulos y encabezados *(`H1`, `H2`, `H3`, `H4`, `H5` y `H6`)*, párrafos *(`p`)* y párrafos más pequeños *(`small`)*. Podemos definir que los títulos se trabajen en negrita y con tamaños de fuente más grandes, lo importante es que estas reglas se acomoden al sistema de diseño que estamos trabajando.

Este articulo sobre cómo trabajar las tipografias del blog de Platzi me gusto mucho para reforzar lo que aprendimos en clase:

- [⚓️ Guía para elegir y trabajar con tipografía](https://platzi.com/blog/tipografia/)

![img](https://mir-s3-cdn-cf.behance.net/project_modules/1400/81820772905057.5c0907428637f.gif)

![img](https://www.google.com/s2/favicons?domain=https://www.gstatic.com/images/branding/product/ico/google_fonts_lodp.ico)[Google Fonts](https://fonts.google.com)

😄 [WhatFont](https://chrome.google.com/webstore/detail/whatfont/jabopobgcpjmedljpbcaablpmlmfcogm)

[FontFaceNinja](https://chrome.google.com/webstore/detail/fontface-ninja/eljapbgkmlngdpckoiiibecpemleclhh)

# 8. Colores

## Bonus: Cómo evitar colores constrastantes

Cuando utilizamos colores demasiado contrastantes podemos generar efectos visuales algo molestos, problemas en el *render* o lineas blancas y negras en los bordes de los elementos. Para solucionar estos problemas visuales debemos elegir nuestros colores con mucho más cuidado, moviendo los colores hacia alguna tonalidad para evitar colores demasiado contrarios.

Un consejo muy repetitivo( o bueno eso es lo que he percibido en los cursos de la carrera de diseño) es tratar de inclinar el color deseado hacia el blanco o negro, tal cuál como nos muestra rulótico. Hay varias paletas de colores en [colorhunt.com](http://colorhunt.com/) o en [colors.com](http://colors.com/). Además si poseen Adobe Creative Cloud, existe también la version Adobe Color CC.

> En vez de utilizar colores drásticos y contrastantes *(blanco y negro, azul fuerte y rojo fuerte)* podemos inclinar nuestros colores hacia algún lugar un lograr efectos visuales mucho más profesionales 👍🎉.
>
> > No olvidar a los usuarios que tienen dificultades visuales (que no pueda ver uno o varios colores), para estos usuarios si se debe usar colores contrastantes

La elección de colores deberá variar de acuerdo a la identidad de marca, a la intención y el objetivo de la empresa u organización al que va dirigido el contenido. Hay que evitar basar esa gama de colores en tus preferencias personales.

![img](https://miro.medium.com/max/1600/1*RmOZAD_SS5gORQUdhZdMzg.gif)

[Colorable](https://colorable.jxnblk.com/)

## Paleta de colores

Los colores también deben llevar algún tipo de clasificación ya que definen el estilo visual de nuestro sistema. La paleta de colores define el estilo de tu sistema de diseño y ayuda a definir el sistema de diseño, diferenciando la identidad de la marca y del resto de efectos y elementos visuales.

Muchos sistemas de diseño clasifican sus colores según sus productos *(un color para ventas, otro para servicio al cliente, etc)*, otros utilizamos Material Design y clasificamos los colores como *actions colors*, _secondary colors _ y otros niveles de clasificación. En realidad no importa, podemos tomar cualquier otro paradigma de fundamentos de color para nuestros sistemas, lo importante es clasificar el uso de estos colores y asegurarnos de que todo el equipo los entiende.

Para escoger estos colores debemos tener en cuenta los colores de la marca, los colores principales *(colores de acción o call to actions)*, colores secundarios, los grises y los colores de fondo. El trabajo se puede complicar un poco cuando tenemos en cuenta todos estos colores, pero el resultado será mucho mejor, los colores funcionaran correctamente entre todos.

**COLORES PARA PALETAS DE COLORES**

Primarios: Rojo - Azul - Amarillo

- **Monocromático**: Un color con diferente opacidad
- **Análoga**: Un primario + un secundario + un terciario
- **Complementaria**: Primario + Inverso Secundario
- **Triádica**: En triangulo (Tres del mismo tipo)
- **Tétrada**: En rectángulo (2 primarios + 2 secundarios)

Estos artículos explican muy bien cómo deben funcionar los colores y también tienen algunos trucos o guías para elegirlos más profesionalmente, son excelentes complementos para la clase 👌✌️:

- [Teoría del Color en el diseño de interfaces](https://platzi.com/blog/color-en-interfaces/)
- [Cómo hacer un diseño efectivo con jerarquía visual - Blog de Platzi](https://platzi.com/blog/jerarquia-visual/)
- [My struggle with colors](https://uxdesign.cc/my-struggle-with-colors-52156c664b87)

# 9. Reglas de espaciado
## Reglas de espaciado

Las reglas de espaciado son muy útiles cuando trabajamos nuestros sistemas de diseño para anticipar la visualización de nuestras interfaces en diferentes dispositivos.

- **Ritmo**: Qué tan seguido aparecen elementos arriba o debajo de otros.
- **Padding**: Es el espacio dentro de nuestros elementos, nos ayuda a enfocar y transmitir los estilos de nuestra marca.
- **Margin**: Es el espacio entre elementos pero hacia afuera, nos permite generar ritmo visual o para separar elementos de secciones diferentes.
- **Border**: Trabajamos con el borde de los elementos, recuerda que las dimensiones o medidas de nuestros objetos se verán afectadas al aplicar o no aplicar estos bordes.
- **Layout**: Vamos a definir la forma general en que combinamos las columnas, headers y barras de navegación de nuestras plataformas, gracias a estas reglas podemos garantizar que nos adaptamos a todos los tamaños y dispositivos.

# 10. Animación
## Animación

Los 12 principios de la animación son un conjunto de reglas creadas por Disney para la animación de personajes, pero muchos de estos principios son muy útiles para ciertos aspectos de la animación de elementos de nuestras interfaces:

- **Anticipación**: Vamos a a preparar a los usuarios para la acción que viene a continuación, por ejemplo, antes de desconectar la señal de un teléfono podemos animar el icono del avión y transmitir que no señal durante algún tiempo.
- **Estirar y encoger**: Nos ayuda a generar drama sobre algún elemento, por ejemplo, para dramatizar la animación de un botón cuando el usuario ha cometido un error o alguna acción incorrecta.
- **Entradas y salidas lentas**: Nos permite introducir o remover elementos de la plataforma, tal vez conozcas estas animaciones como *Fade In* y *Fade Out*.
- **Acciones secundarias**: Todas las microinteracciones trabajan con este principio, estas animaciones nos ayudan a transmitir o dar información adicional como respuesta a alguna acción de los usuarios.
- **Timing**: Mientras más detalles añadimos a las animaciones, más rápidas o lentas se pueden percibir. Podemos utilizar estos efectos visuales para transmitir apuro o tranquilidad mientras la plataforma esta cargando.
- **Exageración**: Así como las acciones secundarias, podemos exagerar los movimientos de nuestros elementos para transmitir alguna sensación, por ejemplo, cuando el usuario quiere eliminar su cuenta.

También vamos a utilizar [Animate.css](https://daneden.github.io/animate.css/), una librería para trabajar con todo tipo de animaciones sin mucho trabajo, también nos facilita el proceso de documentación de las reglas de animación de nuestro sistema.

**Alan Becker** *(el creador del famoso vídeo Animation vs Animator)* explica los 12 principios de animación con ejemplos muy prácticos: [12 Principles of Animation by Alan Becker](https://www.youtube.com/watch?v=uDqjIdI4bF4) 😮 😎 😏 👏.

Web [Animate.css](https://daneden.github.io/animate.css/)

[animista](http://animista.net/play/basic)

## Consejos para empezar a animar usando After Effects

Como parte de la clase de animación te quiero contar algunos detalles básicos sobre la animación para que puedas animar tú mismo en cualquier programa.

Para esta sección vamos a ocupar After Effects, puedes bajarlo de Adobe.

Lo primero que tienes que notar en cualquier programa de animación es el timeline (línea de tiempo), este controla el tiempo de animación y te dará la pauta de que es lo que quieres comunicar cuando animas algo en particular.

<img src="https://i.ibb.co/S73qj05/AE1.jpg" alt="AE1" border="0">

**Frames**

Una animación es una secuencia de imágenes. Las imágenes pasando a un determinado tiempo son los frames, normalmente cuando ves la televisión la velocidad de los frames es de 29.67 frames por segundo. Eso significa que pasan muchas cosas en un solo segundo, mi recomendación es que en web utilices animaciones dentro de los 15 frames por segundo, si usas más frames la animación tendrá más información que comunicar y podrás ver más fluido cada movimiento.

After Effects es una de las herramientas más completas para poder hacer animaciones,si quieres saber más de estos temas, próximamente puedo desarrollar otro tutorial para crear micro-interacciones. Por ahora me gustaría que notaras como experimentar con herramientas muy sencillas. También puedes utilizar otras herramientas de animación, todas tienen el mismo principio: elementos, frames.

<img src="https://i.ibb.co/mBz6py1/AE2.jpg" alt="AE2" border="0">

**Nodos de animación**

Cuando tu animas un elemento se crean nodos de animación estos nodos indican el cambio de ese elemento.

<img src="https://i.ibb.co/hDRHFRC/AE3.jpg" alt="AE3" border="0">

**Curvas de animación**

Las curvas de animación nos ayudan a controlar qué tan rápido se mueve de un lado a otro y justo son estas las que nos dejan imprimir personalidad a nuestras animaciones.

<img src="https://i.ibb.co/XCf96tR/AE4.jpg" alt="AE4" border="0">

Estos son los conceptos más básicos que necesitas para comenzar a usar After Effects. No tengas miedo de esta herramienta, experimenta o prueba otras similares que te permitan expresar tus ideas con movimiento.

# 11. Voz y tono
## Voz y tono

Los textos o *copies* que acompañan nuestros diseños también hace parte de la forma en que te comunicas con tus usuarios. Al incluir la voz y el tono de nuestros productos en el sistema de diseño conseguimos que todo el equipo tenga una guía sobre la personalidad de la marca, recuerda que toda esta documentación debe contribuir a la consistencia y comunicación con nuestros equipos.

Para definir la voz y el tono de nuestros sistemas debemos tener en cuenta los siguientes aspectos:

- **Buzzwords**: las palabras más usadas de nuestro producto
- **Phrases**: vamos a construir una librería de frases clave de nuestro producto
- **Objetivo**: el objetivo o la misión de nuestro producto en términos de voz y tono ayuda a que el equipo tenga orientación a la hora de crear nuevos textos o frases
- **Características**: definimos la personalidad de la marca, así tendremos mucho más contexto y podremos crear mejores *copies* para nuestra marca.

> El término **buzzwords** es utilizado para indicar aquellos términos o frases que pueden ser identificadas como expresiones “de moda” en un determinado contexto de tiempo y temática. Términos como “modelo de negocios”, “nuevas tecnologías” o “globalización”, por ejemplo, han sido buzzwords en diferentes años y distintos contextos en lo referido al rubro o temática. Para que una expresión pueda ser considerada buzzword, debe, además de estar de moda, ser una expresión que atraiga la atención del público cuando es mencionada.
>
> En este caso serán palabras que nos podrán identificar y nos ayudaran a salir de lo cotidiano como el típico siguiente en los botones, o el aceptar al que estamos acostumbrados.

[Design System](https://atlassian.design/)

- [Creating a copy system along with your design system](https://uxdesign.cc/creating-a-copy-system-along-with-your-design-system-58afd89c3c12)
- [Designing The Words: Why Copy Is A Design Issue](https://www.smashingmagazine.com/2013/09/why-copy-is-a-design-issue/)
- [UX Writing](https://uxplanet.org/copywriting-for-interfaces-types-of-copy-in-web-and-mobile-ui-6326f92865f8)

# 12. Icon System
## Iconografía

Los iconos son muy útiles para comunicar o resaltar características y funcionalidades de nuestros productos, podemos construir nuestras propias librerías de iconos personalizados o utilizar librerías como [Font Awesome](https://fontawesome.com/) y completar los iconos que hagan falta si no tenemos un equipo tan grande.

Para definir las reglas y la documentación de una iconografía consistente debemos tener en cuenta los siguientes aspectos:

- **Grid**: Las medidas y lineamientos que deben seguir todos los iconos
- **Shapes**: Las formas o figuras que podemos utilizar
- **Size**: Qué tamaño deben tener nuestros iconos para estar alineados con la tipografía y el resto de la plataforma
- **Styles**: Cómo deben estar construidos visualmente nuestros iconos, podemos utilizar colores planos o podemos trabajar en iconos mucho más realistas, etc, lo importante es estar alineados con las reglas de nuestro sistema

Iconografía

Puedes tomar 2 caminos:

1. Usar una librería
2. Crear tu iconografía. Para esto hay que tomar en cuenta lo siguiente:
   - Grid: Que todos los iconos tengan el mismo tamaño y espaciado.
   - Shapes: Formas que tendrán.
   - Tamaños: Estandariza el tamaño de los íconos.
   - Styles: ¿Qué estilos van a tener?

Ya sabes, ***documenta todo esto\***, **Sean claros y concisos!**

# 13. Hitos
## Hitos

Los **hitos** son características muy particulares o incluso personales de nuestros productos, las mejores marcas incluso pueden definirse y marcar su presencia sin necesidad de utilizar logos o nombres, tal es el caso de las animaciones de carga de Google o Slack.

> **Hitos** son los elementos identificador de un producto, Aveces las marcas tienen elementos que los representan muy bien sin necesidad de que este el logo presente.
>
> Los hitos en nuestro producto pueden ser Loaders, icons, animaciones, etc.

Son cosas muy en particular de una marca. Lograr hitos dentro de tu producto es una **gran forma de personalización**. es difícil porque requiere de mucho trabajo y análisis para poder comunicar esto.

Son una representación de todos los conceptos que queremos comunicar.

# 14. Bonus: UI Kit
## Bonus: UI Kit

Los **UI Kits** son parte fundamental del sistema de diseño. Son librerías donde se alojan todos los elementos y componentes diseñados del producto. **Un buen UI Kit es escalable y flexible a múltiples cambios**.

**UI kit:** es una librería completa para diseñadores, permite reutilizar elementos una y otra vez. Adobe XD trae algunos ui kit bastantes completos para utilizar.

Los ui kit son muy cambiantes, es recomendable tener un modelo de versionamiento,

Es recomendable definir colores en cada elemento que se no seas mas fácil cambiar el color a todos los elementos a la misma vez.

[Behance](https://www.behance.net/gallery/55462459/Wires-wireframe-kits-for-Adobe-XD)

# 15. Un sistema basado en personas
## Un sistema basado en personas

Un sistema de diseño se asegura que todo lo que estás diseñando se convierta en software, debemos asegurarnos de que todos los elementos estén conectados para hacer nuestro trabajo más eficiente.

Los sistemas de diseño nos ayudan a construir software en equipo, involucrando al equipo de desarrollo, de marketing, de finanzas, etc, todos aquellos que contribuyen a la hora de crear un nuevo software y ayudan a que el trabajo fluya y sea más eficiente.

Todos los elementos de un sistema de diseño tienen el objetivo de hacer más eficiente el **trabajo en equipo**

El design system se trata de herramientas para **personas**, no importa qué herramientas elijas, es importante que le sirva a los miembros de tu equipo para trabajar alineados hacia el mismo objetivo y de manera mucho más eficiente.

>  case - computer aided software engineering
>
> Eso es un Design System, algo que no ayuda a contruir otra cosa. La gran diferencia es que el DS ayuda a encontrar un flujo entre el producto, el equipo y lo que estamos produciendo. En el DS el proucto no es lo más importante sino el equipo y todas las herramientas que le damos a las personas, por eso el DS está basado en personas y no en tecnología, lo importante es que el equipo lo entienda y convierta algo que se está pensando en algo real, y esto solo se consigue si están alineados en la dirección y visión del producto!

<img src="https://www.notion.so/image/https%3A%2F%2Fs3-us-west-2.amazonaws.com%2Fsecure.notion-static.com%2Ff8a632a3-ff2f-4f2d-99c1-8483b7d339aa%2Flogo-platzi.png?table=block&id=1bdbab2c-4c35-4f80-944c-1fb93d3743ff&spaceId=89058248-453d-4803-aa98-4b0aa16aef46&width=250&userId=82e08c58-7a33-40b6-b802-65a476926a7f&cache=v2" alt="img" style="zoom: 25%;" />[Notion – The all-in-one workspace for your notes, tasks, wikis, and databases.](https://www.notion.so/Design-system-1bdbab2c4c354f80944c1fb93d3743ff)

# 16. Iteremos
## Iteremos

El proceso de creación de software no es tan definitivo como la creación de otros tipos de producto, son productos vivos que siguen mutando y desarrollándose en el tiempo. Los sistemas de diseño también pueden prever la forma de iterar, es decir, mejorar todos los días las construcciones y la forma de construir del equipo, lo más importante es que entre todos nos pongamos de acuerdo de manera fácil, rápida y efectiva.

Para organizar el proceso de iteraciones debemos documentar todos los cambios y problemas que nos encontramos para que todo el equipo este actualizado y logremos entender el trabajo de la misma forma. Vamos a seguir los siguientes pasos:

- Darle un nombre a la iteración
- Describir por qué estamos iterando, ¿qué problema vamos a resolver?
- ¿Qué solución encontramos a estos problemas?
- Cambios de estimación de conflictos, la duración y la dificultad estimada para trabajar esta iteración.

> En las iteraciones, seria bueno registrar la fecha.

>  Las juntas de más de 10 minutos más que juntas son un convivio. –_Rulótico_
>
> Una regla es 2 minutos por cada persona de la reunión. Una reunión de dos personas = 4 minutos.

Muchas veces vamos a recibir feedback, detras de un análisis tenemos que actualizar nuestro Design System para mejorar la calidad de nuestro producto.

Siempre preguntarse ¿Realmente vale la pena iterar en este caso?

A la hora de iterar debemos definir “Nombre de la iteración”, “¿Por qué estamos iterando?”, “Solución”

Debemos tener una plantilla para poder definir una iteración.
Es importante que todos estén de acuerdo con las iteracciones. De manera tapida y efectiva.

La iteración en un Sistema de Diseño se traduce en el feedback que se obtiene sobre un producto realizado para mejorarlo o solucionar los problemas que surgieron, ese va y viene de mejoras e implementaciones es llamada **Iteración**.
Es importante saber que funciones o elementos son necesarios iterar para no desperdiciar tiempo en funciones que no van a tener el impacto que esperamos en la plataforma.

[Releasing Design Systems - Medium by Nathan Curtis](https://medium.com/eightshapes-llc/releasing-design-systems-57fca91a23f6)

[Metodologia de Desarrollo de Software](https://aspgems.com/metodologia-de-desarrollo-de-software-ii-modelo-de-diseno-iterativo/)

## Cierre

Ya estás listo para crear la estructura de tu sistema de diseño y utilizarlo para coordinar tu trabajo en equipo a la hora de crear productos de software. Recuerda que puedes seguir aprendiendo cómo crear estas herramientas y llevarlas del diseño a producción con el Curso de Sistemas de Diseño para Desarrolladores.

>  Un sistema de diseño por lo general es construido por diferentes miembros de un equipo para tener varias propuestas y por ende un sistema más sólido, claro y objetivo para cada sección de la plataforma. El objetivo de un Sistema de Diseño es crear tecnología de manera rápida y eficaz.

[nathanacurtis](https://medium.com/@nathanacurtis)

