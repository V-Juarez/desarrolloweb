<h1>Tailwind CSS</h1>

<h3>Víctor Yoalli Domínguez</h3>

<h1>Tabla de Contenido/h1>


- [1. Introducción, instalación y ambiente de desarrollo](#1-introducción-instalación-y-ambiente-de-desarrollo)
  - [¿Qué es Tailwind CSS?](#qué-es-tailwind-css)
  - [Instalación y ambiente de desarrollo](#instalación-y-ambiente-de-desarrollo)
  - [Directivas de Tailwind](#directivas-de-tailwind)
- [2. Conceptos básicos](#2-conceptos-básicos)
  - [Personalización y configuración](#personalización-y-configuración)
  - [Responsive Design, Mobile First y Utility First](#responsive-design-mobile-first-y-utility-first)
- [3. Utilerías](#3-utilerías)
  - [Colores](#colores)
  - [Cómo crear grids o columnas en Tailwind CSS](#cómo-crear-grids-o-columnas-en-tailwind-css)
  - [Dimensiones y Espacios](#dimensiones-y-espacios)
  - [Cambiando las propiedades de la tipografía](#cambiando-las-propiedades-de-la-tipografía)
  - [Ajustando el espaciado entre letras y líneas](#ajustando-el-espaciado-entre-letras-y-líneas)
  - [Display](#display)
  - [Flexbox](#flexbox)
  - [Crea tus propias utilerías](#crea-tus-propias-utilerías)
  - [Entendiendo las variantes y las pseudo-clases](#entendiendo-las-variantes-y-las-pseudo-clases)
  - [¡Es hora de practicar!](#es-hora-de-practicar)
- [4. Proyecto: PlatziFood](#4-proyecto-platzifood)
  - [Creando una card](#creando-una-card)
  - [Aplicando formato a la card](#aplicando-formato-a-la-card)
  - [Responsive design en la card](#responsive-design-en-la-card)
  - [Construye el header](#construye-el-header)
  - [Crea el footer](#crea-el-footer)
  - [Crea un banner](#crea-un-banner)
  - [Cómo integrar una card](#cómo-integrar-una-card)
  - [Forms](#forms)
  - [Directivas de Apply](#directivas-de-apply)
  - [Extraer componentes](#extraer-componentes)
  - [Navbar](#navbar)
  - [Alpine JS](#alpine-js)
  - [Optimiza tu archivo: PurgeCSS y NanoCSS](#optimiza-tu-archivo-purgecss-y-nanocss)
- [5. Conclusiones](#5-conclusiones)
  - [Conclusiones y qué sigue](#conclusiones-y-qué-sigue)

# 1. Introducción, instalación y ambiente de desarrollo

## ¿Qué es Tailwind CSS?

**[Qué es TailWind CSS](https://tailwindcss.com/)?** 🧐
Es un [*framework*](https://en.wikipedia.org/wiki/Software_framework) que permite la contrucción de diseños altamente personalizados y de bajo nivel.

**¿[Por qué usar TailWind CSS](https://tailwindcss.com/#what-is-tailwind)?** 🤨
A diferencia de otros frameworks css, como lo es [bootstrap](https://getbootstrap.com/), que otorgan componentes prediseñados, Tailwind provee clases a bajo nivel de css que nos permiten construir diseños completamente personalizados por nostros y sin tener que pelear con el framework en sí.

Nos externa una [responsividad](https://tailwindcss.com/docs/responsive-design/) desde la misma sintaxis.

Trabaja de manera amigable en el crecimiento de nuestro proyecto al proveernos herramientas para [extraer clases](https://tailwindcss.com/docs/extracting-components/).

Pero sobre todo, nos abre su código para [personalizarlo](https://tailwindcss.com/docs/configuration/) ya que esta escrito en [PostCSS](https://postcss.org/) y configurado en JavaScript.

Tailwind is more than a CSS framework, *it’s an engine for creating design systems*.

**Ventajas de Tailwind:**

1. el Atomic design permite poner las clases dentro de las mismas etiquetas de HTML, por lo tanto, es mas sencillo que el diseño de BEM. es decir, nos ahorramos nombrar las clases Bloque__elemento–modificador que en un proyecto grande puede tonarse complejo.
2. a diferencia de bootstrap, Tailwind solo incluye el codigo que necesitamos y elimina las clases que no fueron utilizadas. esto se hace automaticamente en el proceso de compilacion y optimizacion, que mejora el performance[ Charla de Expertos en Css hablan de Tailwind](https://www.youtube.com/watch?v=w1QUT2luc2I&t=2822s)
3. tiene una comunidad que genera componentes gratuitos, al igual que bootstrap, pero con las ventajas del Tailwind (atomic Design). [Pagina Gratuita de Componentes con Tailwind](https://tailwindui.com/components).
4. hay funciones llamadas [apply y layer ](https://www.youtube.com/watch?v=TrftauE2Vyk)que nos permiten ahorrar mucho codigo. es decir, tailwind nos da la flexibilidad del sistema Atomic design y la fortaleza componentes porque tambien podemor crear clases para crear nuestros propios componentes.
5. hay un plugin maravilloso llamado [Tailwind CSS intellisense](https://marketplace.visualstudio.com/items?itemName=bradlc.vscode-tailwindcss) que te autocompleta las variables, lo cual hace el desarrollo muy agradable.
6. en el documento tailwind.config.js podemos configurar TODO, desde colores.

para visualizar este documento puedes escribir en la linea de comandos

```css
<npx tailwindcss init tailwind.config.js --full>
```

1. es una de las mejor rankeadas en el Informe de [state of CSS 2020](https://2020.stateofcss.com/en-US/technologies/css-frameworks/)

Tailwind CSS es un framework de CSS para crear rápidamente interfaces de usuario personalizadas con la siguiente ideología:

- Utility First
- Mobile First
- Responsive to the Core

Te permite crear tus propios componentes y utilizarlos cuando necesites.

![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)[GitHub - victoryoalli/platzifood: Material del curso de Tailwind CSS en @platzi](https://github.com/victoryoalli/platzifood)

## Instalación y ambiente de desarrollo

Iniciar la configuración 

```bash
## Dependencias

npm init -y

# Instalar tailwindCSS
npm install tailwindcss autoprefixer postcss-cli

### Inicializamos las herramientas instaladas:

# <!-- Genera archivo configuracion vacio de nombre tailwind.config.js -->

npx tailwindcss init

# <!-- Genera archivo configuracion completo -->
npx tailwindcss init tailwind.config.full.js --full

# <!-- Plugin recomendado para VSCode: Tailwind CSS IntelliSense -->

### Creamos archivo de configuracion postcss.config.js

# <!-- Instrucciones archivo postcss.config.js: -->

touch postcss.config.js
module.exports = { plugins: [require('tailwindcss'), require('autoprefixer')], };

### Creacion archivo html y origen CSS

mkdir css
touch css/tailwind.css

### Configuracion archivo css/tailwind.css

@tailwind base; @tailwind components; @tailwind utilities;

### Completamos script en package.json

"scripts": { "build": "postcss css/tailwind.css -o public/css/styles.css",
# <!-- Para autoregenerar el tailwind.css cuando creamos paquetes -->
"dev": "postcss css/tailwind.css -o public/css/styles.css --watch"}
# <!-- Ejecutar para compilar -->
npm run dev

### Inicializamos script para crear el css

# <!-- genera una directorio css con su styels.css en la carpeta public -->

npm run build
```

Hola, puedes ahorrarte muchas confusiones siguiendo la documentación de [Tailwind](https://tailwindcss.com/docs/installation), siendo **Abril del 2021**, los comandos cambiaron, aquí va un resumen rápido, aclaro que todo lo que escribo está en la página, pero si te da flojera, espero te sirva:

- Después de crear tu carpeta e irte en consola a su dirección debes inicializar npm, es importante que hagas este paso pues si no lo haces, no se te creara el archivo package.js (como me paso a mí :v) lo inicializas con:

```
npm init -y
```

Ahora comienza la instalación:

1. Estando en tu carpeta por terminal usas:

```bash
npm install -D tailwindcss@latest postcss@latest autoprefixer@latest

# Tienes inconvenientes con la version anterior, prueba con esto.
npm install tailwindcss autoprefixer postcss-cli -D
```

No te espantes si ves cosas diferentes, es lo mismo que usa el profe, solo que para el año 2021.

1. Creas el archivo *tailwind.config.js* usando el comando en consola:

```bash
npx tailwindcss init
```

1. Creas tu archivo *postcss.config.js* , no es necesario que uses “touch” como el profe o cualquier otro comando, basta con hagas add new file en VSC y le pones este nombre, usa Ctrl+C y Ctrol+V si quieres y ahí dentro metes el siguiente código:

```js
module.exports = {
  plugins: {
     tailwindcss: {},
     autoprefixer: {},
  }
}
```

1. Creamos nuestro archivito tailwind.css como lo dice el profe y ahi agregas esto:

```css
@tailwind base;
@tailwind components;
@tailwind utilities;
```

1. No es necesario que instales *live server*, busca la extensión en VSC y una vez agregada lo enciendes o apagas con click derecho y *Open with Live Server*.
2. A lo mejor te espantes cuando ingreses en el script lo que puso el profe y al dar:

```bash
npm run build
```

Nomas no furule y te marqué una serie de errores, iniciando por algo como: no se reconoce el comando interno o externo, tranquilo, es porque lo que escribe el profe esta desactualizado, el código actual lo puedes encontrar en la sección Using Tailwind whitout PosCSS, pero aquí esta de todos modos:

```json
"build": "npx tailwindcss-cli@latest build ./src/tailwind.css -o ./dist/tailwind.css"
```

Lo único que haces es agregar las direcciones que pone el profe así:

```json
"build": "npx tailwindcss-cli@latest build css/tailwind.css -o public/css/style.css"
```

¡Y ya! Es todo, si te quedan dudas lee la documentación y los aportes ahí está todo, la mayoría de cosas las entendí leyendo ambas partes. Saludos

![img](https://www.google.com/s2/favicons?domain=https://tailwindcss.com/apple-touch-icon.png)[Tailwind CSS - A Utility-First CSS Framework for Rapidly Building Custom Designs](https://tailwindcss.com)

![img](https://www.google.com/s2/favicons?domain=https://autoprefixer.github.io/assets/icon/apple-touch-icon.png)[Autoprefixer CSS online](https://autoprefixer.github.io)

## Directivas de Tailwind

Directiva es una instrucción que utiliza tailwind para insertar código en el archivo final de css que genera.

`@tailwind base`
Esto inyecta los estilos base de Tailwind y cualquier estilo base registrado por plugins.

`@tailwind components`
Esto inyecta las clases de componentes de Tailwind y cualquier clase de componente registrado por los plugins.

`@tailwind utilities`
Esto inyecta las clases de utilidad de Tailwind y cualquier clase de utilidad registrada por los plugins.

`@tailwind screens`
Esta directiva sirve para controlar donde Tailwind inyecta las variaciones responsivas de cada utilidad. Si se omite, Tailwind añadirá estas clases al final de tu css, por defecto.

**Las directivas de tailwind sirven para insertar código en el archivo css final que genera.**

**@tailwind base** -> Inicializa mis elementos de html para que no tengan estilos.

**@tailwind utilities** -> Nos ayuda a generar código de las utilidades.

# 2. Conceptos básicos

## Personalización y configuración

Ya que Tailwind es un framework para construir UI a la medida, por default, se tiene un archivo *opcional* llamado `tailwind.config.js` en la raíz de la carpeta, donde está el `package.json`.

## Creando un archivo de configuración

Para generar un archivo de configuración para Tailwind, podemos usar el *Tailwind CLI*:

```bash
npx tailwind init
```

**Nota**. Podemos utilizar esta herramienta cuando instalamos la dependencia via `npm`.
Donde como resultado tendremos `tailwind.config.js`:

```js
module.exports = {
  theme: {},
  variants: {},
  plugins: [],
}
```

Cabe mencionar que cada sección, del archivo de configuración, es opcional.

## La sección *Theme*

Esta sección es donde definimos los aspectos relacionados con el diseño visual de nuestro sitio.

```javascript
...
  theme: {
    screens: {
      sm: '640px',
      md: '768px',
      lg: '1024px',
      xl: '1280px',
    },
    fontFamily: {
      display: ['Gilroy', 'sans-serif'],
      body: ['Graphik', 'sans-serif'],
    },
    borderWidth: {
      default: '1px',
      '0': '0',
      '2': '2px',
      '4': '4px',
    },
    extend: {
      colors: {
        cyan: '#9cdbff',
      },
      spacing: {
        '96': '24rem',
        '128': '32rem',
      }
    }
  }
...
```

## La sección *Variants*

Esta sección nos permite controlar el comportamiento de las utilidades core, como *responsive variants* y *pseudo-class variants*.

```javascript
...
  variants: {
    appearance: ['responsive'],
    // ...
    borderColor: ['responsive', 'hover', 'focus'],
    // ...
    outline: ['responsive', 'focus'],
    // ...
    zIndex: ['responsive'],
  },
...
```

## La sección *Plugins*

Esta sección nos permite registrar plugins de terceros con el objetivo de extender utilidades, componentes, estilos, etc.

```javascript
...
  plugins: [
    require('tailwindcss-transforms'),
    require('tailwindcss-transitions'),
    require('tailwindcss-border-gradients'),
  ],
...
```

para que no sufran con la configuración del archivo **tailwind.config.js** les recomiendo que en la terminal escriban este comando para que les genere un archivo totalmente completo

```bash
npx tailwindcss init tailwind.config.full.js --full
```

con ese comando se ahorran mucho con la configuración, espero les haya servido amigos/as

![img](https://www.google.com/s2/favicons?domain=https://tailwindcss.com/docs/installation/apple-touch-icon.png)[Installation - Tailwind CSS](https://tailwindcss.com/docs/installation)

## Responsive Design, Mobile First y Utility First

La web como la conocemos, hoy en día, no es una tecnología pensando en un **UX** o *User Experience* generando, desafortunadamente, que muchos de los sitio web no estén optimizados para los **dispotivos móviles**.

Definido por [Ethan Marcotte](https://alistapart.com/article/responsive-web-design/), es una filosofía que responde a las necediades de los usuarios y a los dispositivos que estamos usando.

## Mobile First

Como su nombre sugiere, significa que iniciaremos con el diseño de móviles y expandiendo éstas características para crear una verión en tableta o escritorio/web tradicional.

Cabe mencionar que esta filosofía no es sinónimo de limitación, por lo que tenemos que tener el mismo contenido tanto en escritorio como en móvil. Google describe las [*best practices*](https://developers.google.com/search/mobile-sites/mobile-first-indexing) en su sitio.

## Utility first

CSS posee diferentes tipos de paradigmas para abstraer un diseño, como BEM descrito por [Tailwind](https://tailwindcss.com/docs/utility-first/) en ésta sección:

```html
<div class="chat-notification">
  <div class="chat-notification-logo-wrapper">
    <img class="chat-notification-logo" src="/img/logo.svg" alt="ChitChat Logo">
  </div>
  <div class="chat-notification-content">
    <h4 class="chat-notification-title">ChitChat</h4>
    <p class="chat-notification-message">You have a new message!</p>
  </div>
</div>
```

Sin embargo, llega a ser muy complicado establecer un armonía entre desarrolladores-diseñadores si no hay una concesión de sintaxis o *linter*. Por ello, Tailwind establece su propio orden con el objetivo:

- No invertir tiempo en inventar nombres para las clases
- Detener el crecimiento, sin fin, de CSS
- Simplificando y asegurando los cambios al CSS

## Herramienta para desarrollo 🚀

Como desarrolladores necesitamos herramientas que nos permita exlotar nuestra creatividad al máximo por ellos exiten algunas DevTools:

- [Safari for Developers](https://developer.apple.com/safari/)
- Chrome o [Chrome for Developers](https://www.google.com/chrome/dev/)
  Y otras más … 🤯

![img](https://www.google.com/s2/favicons?domain=//static.platzi.com/media/favicons/platzi_favicon.png)[Por qué es importante aprender a diseñar interfaces para móviles](https://platzi.com/blog/diseno-interfaces-moviles-mobile-first/)

![img](https://www.google.com/s2/favicons?domain=https://tailwindcss.com/docs/utility-first//apple-touch-icon.png)[Utility-First - Tailwind CSS](https://tailwindcss.com/docs/utility-first/)

![img](https://www.google.com/s2/favicons?domain=https://sizzy.co//favicon.ico)[Sizzy](https://sizzy.co/)

# 3. Utilerías

## Colores

Los elementos que pueden ser afectados por los colores son:

- Fondo.
- Texto.
- Bordes.
- Placeholder.

Taildwind por default tiene valores ya predefinidos, para conocerlos tenemos que generar un archivo de configuración con todos los valores completos:
`npx tailwindcss init tailwind.config.full.js --full`

Se puede ver la paleta de [colores por defecto](https://tailwindcss.com/docs/customizing-colors/#default-color-palette)

[cheatsheet tailwindcss](https://nerdcave.com/tailwind-cheat-sheet)

## Cómo crear grids o columnas en Tailwind CSS

En esta clase vamos a aprender a crear un grid de columnas utilizando flex en Tailwind.

## Grid Básico

Verás que con lo que hemos aprendido hasta ahora es muy sencillo crear un grid.

1. Crearemos un elemento que contendrá las columnas de nuestro Grid con la clase de flex.

```html
<div class=“flex”>;
</div>
```

1. Teniendo de base el elemento del punto anterior insertaremos una etiqueta por cada columna que queramos tener de la siguiente manera.

> Una columna

```html
<div class="flex">
	<div
    class="w-full
    p-4 bg-blue-800"
  ></div>
</div>
```

*Resultado*
![1.png](https://static.platzi.com/media/user_upload/1-ff78efd3-e529-470f-9105-71cdea0c4593.jpg)

> Dos columnas

```html
<div class="flex">
    <div
    class="w-1/2
    p-4 bg-blue-800"
    ></div>
    <div
    class="w-1/2
    p-4 bg-blue-600"
    ></div>
</div>
```

*Resultado*

![2.png](https://static.platzi.com/media/user_upload/2-032d8f91-dea6-4219-8a73-664e47ac4ce5.jpg)

> Tres Columnas

```html
<div class="flex">
	<div
    class="w-1/3
    p-4 bg-blue-800"
  ></div>
  <div
    class="w-1/3 
    p-4 bg-blue-600"
  ></div>
  <div
    class="w-1/3
    p-4 bg-blue-500"
  ></div>
</div>
```

*Resultado*

![3.png](https://static.platzi.com/media/user_upload/3-2622e8e4-05fd-4602-9aab-be142773d75f.jpg)

> Cuatro Columnas

```html
<div class="flex">
	<div
    class="w-1/4
    p-4 bg-blue-700"
  ></div>
  <div
    class="w-1/4 
    p-4 bg-blue-400"
  ></div>
  <div
    class="w-1/4
    p-4 bg-blue-600"
  ></div>
  <div
    class="w-1/4
    p-4 bg-blue-500"
  ></div>
</div>
```

> Cinco columnas

```html
<div class="flex">
	<div
    class="w-1/5
    p-4 bg-blue-700"
  ></div>
  <div
    class="w-1/5 
    p-4 bg-blue-400"
  ></div>
  <div
    class="w-1/5
    p-4 bg-blue-600"
  ></div>
  <div
    class="w-1/5
    p-4 bg-blue-500"
  ></div>
  <div
    class="w-1/5
    p-4 bg-blue-800"
  ></div>
</div>
```

*Resultado*

![5.png](https://static.platzi.com/media/user_upload/5-02452bb9-7a47-43a0-a29b-7ff312bedfd8.jpg)

> Seis columnas

```html
<div class="flex">
	<div
    class="w-1/6
    p-4 bg-blue-700"
  ></div>
  <div
    class="w-1/6 
    p-4 bg-blue-400"
  ></div>
  <diva
    class="w-1/6
    p-4 bg-blue-600"
  ></div>
  <div
    class="w-1/6
    p-4 bg-blue-500"
  ></div>
  <div
    class="w-1/6
    p-4 bg-blue-300"
  ></div>
  <div
    class="w-1/6
    p-4 bg-blue-800"
  ></div>
</div>
```

*Resultado*

![6.png](https://static.platzi.com/media/user_upload/6-a95e1c2b-5694-4ba1-a7f5-e71358b7ac22.jpg)

## Grid Responsivo con Columnas de Anchura Fija

La diferencia para lograr que nuestro grid sea responsivo es que al elemento base donde tenemos la clase de `flex` le agregamos `flex-wrap` con esto logramos que cuando uno de los elemento exceda el tamaño, ese elemento pase al siguiente renglón. Y con el uso de las pseudo-classes responsivas de `sm`, `md`, `lg` y `xl` logramos el numero de columnas deseada por tamaño de pantalla.

```html
<div class="flex flex-wrap m-4">
  <div
    class="w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/6 
    p-4 bg-green-500 mb-1"
  ></div>
  <div
    class="w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/6 
    p-4 bg-green-700 mb-1"
  ></div>
  <div
    class="w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/6 
    p-4 bg-green-500 mb-1"
  ></div>
  <div
    class="w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/6 
    p-4 bg-green-700 mb-1"
  ></div>
  <div
    class="w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/6 
    p-4 bg-green-500 mb-1"
  ></div>
  <div
    class="w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/6 
    p-4 bg-green-700 mb-1"
  ></div>
  <div
    class="w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/6 
    p-4 bg-green-500 mb-1"
  ></div>
  <div
    class="w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/6 
    p-4 bg-green-700 mb-1"
  ></div>
  <div
    class="w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/6 
    p-4 bg-green-500 mb-1"
  ></div>
  <div
    class="w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/6 
    p-4 bg-green-700 mb-1"
  ></div>
  <div
    class="w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/6 
    p-4 bg-green-500 mb-1"
  ></div>
  <div
    class="w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/6 
    p-4 bg-green-700 mb-1"
  ></div>
</div>
```

### Resultados

> **default o xs**

![7.PNG](https://static.platzi.com/media/user_upload/7-6aee0e94-a38b-44ef-be60-d70009555744.jpg)

> **sm**

![8.PNG](https://static.platzi.com/media/user_upload/8-d1e52bc9-e665-41be-85ff-32cc419fb24d.jpg)

> **md**

![9.PNG](https://static.platzi.com/media/user_upload/9-e4472224-501f-4fbd-b43c-e92c6d2c9440.jpg)
&amp;amp;amp;amp;gt; **lg**

![10.PNG](https://static.platzi.com/media/user_upload/10-ed191c67-c51b-4619-b917-5247e6244176.jpg)

> **xl**

![11.PNG](https://static.platzi.com/media/user_upload/11-1254aea4-326c-405e-be3d-8fe54c1ebb14.jpg)

## Grid Responsivo con Columnas de Anchura Variable

Al igual que con el grid responsivo anterior, agregamos en el elemento base de `flex` la clase de `flex-wrap` y de igual manera hacemos uso de las pseudo-classes responsivas de `sm`, `md`, `lg`y `xl`.

```html
<div class=“flex flex-wrap m-4”>
  <div
    class="w-full sm:w-1/2 md:w-2/5 lg:w-1/6 xl:w-7/12 
    p-4 bg-green-700 mb-1"
  ></div>
  <div
    class="w-full sm:w-1/3 md:w-1/5 lg:w-3/6 xl:w-1/12 
    p-4 bg-green-500 mb-1"
  ></div>
  <div
    class="w-full sm:w-1/6 md:w-2/5 lg:w-2/6 xl:w-4/12 
    p-4 bg-green-900 mb-1"
  ></div>

  <div
    class="w-full sm:w-1/6 md:w-1/5 lg:w-1/6 xl:w-1/12 
    p-4 bg-green-700 mb-1"
  ></div>
  <div
    class="w-full sm:w-1/3 md:w-3/5 lg:w-1/6 xl:w-5/12 
    p-4 bg-green-500 mb-1"
  ></div>
  <div
    class="w-full sm:w-1/2 md:w-1/5 lg:w-4/6 xl:w-6/12 
    p-4 bg-green-900 mb-1"
  ></div>

  <div
    class="w-full sm:w-1/3 md:w-1/5 lg:w-2/6 xl:w-3/12 
    p-4 bg-green-700 mb-1"
  ></div>
  <div
    class="w-full sm:w-1/6 md:w-1/5 lg:w-2/6 xl:w-4/12 
    p-4 bg-green-500 mb-1"
  ></div>
  <div
    class="w-full sm:w-1/2 md:w-3/5 lg:w-2/6 xl:w-5/12 
    p-4 bg-green-900 mb-1"
  ></div>

  <div
    class="w-full sm:w-1/2 md:w-3/5 lg:w-3/6 xl:w-8/12 
    p-4 bg-green-700 mb-1"
  ></div>
  <div
    class="w-full sm:w-1/6 md:w-1/5 lg:w-2/6 xl:w-2/12 
    p-4 bg-green-500 mb-1"
  ></div>
  <div
    class="w-full sm:w-1/3 md:w-1/5 lg:w-1/6 xl:w-2/12 
    p-4 bg-green-900 mb-1"
  ></div>

  <div
    class="w-full sm:w-1/2 md:w-2/5 lg:w-2/6 xl:w-5/12 
    p-4 bg-green-700 mb-1"
  ></div>
  <div
    class="w-full sm:w-1/3 md:w-1/5 lg:w-1/6 xl:w-3/12 
    p-4 bg-green-500 mb-1"
  ></div>
  <div
    class="w-full sm:w-1/6 md:w-2/5 lg:w-3/6 xl:w-4/12 
    p-4 bg-green-900 mb-1"
  ></div>
</div>
```

### Resultados

> **default o xs**

![12.PNG](https://static.platzi.com/media/user_upload/12-805b2b1f-7a6d-4150-83e5-510e0161210d.jpg)

> **sm**

![13.PNG](https://static.platzi.com/media/user_upload/13-9cb58230-1020-4f43-aba0-0343c76f6bec.jpg)

> **md**

![14.PNG](https://static.platzi.com/media/user_upload/14-a42d71a2-5d08-4fe6-85a5-f94241d65d85.jpg)

> **lg**

![15.PNG](https://static.platzi.com/media/user_upload/15-0dc64691-aac2-4482-b2ee-2cee9502eae4.jpg)

> **xl**
> ![16.PNG](https://static.platzi.com/media/user_upload/16-1d9c2d7d-245e-4064-9fdf-0c7c5bec4e8f.jpg)

## Conclusión

Crear grids en Tailwind usando flex es muy sencillo.
En `flex` podemos hacer uso de las clases de dimensiones porcentuales, algunos ejemplos son:
w-1/2, w-2/3, w-3/4, w-5/6, w-7/12,.
Estas mediciones nos facilita poder crear columnas de 2, 3, 4, 5, 6 y 12 columnas y sus combinaciones.

## Dimensiones y Espacios

Tailwind utiliza un sistema de espacios que se puede configurar. Se maneja internamente utilizando rem. Se puede visualizar desde el archivo tailwind.config.full.js.

Se puede utilizar en:

- Height.
- Width.
- Margin.
- Padding.

En las clases de los elementos se añaden cosas como:
`h-32 w-1/2 pt-2 mx-auto`

Para manajear el height:
https://tailwindcss.com/docs/height/#app

Para manejar width en porcentajes se puede ver esta documentación:
https://tailwindcss.com/docs/width/#app

propiedades del sistema de espacios vamos a ir a nuestro index.html empezaremos con la altura y el ancho

- h-8 - modifica altura
- w-16 - modifica el ancho

Otra forma de cambiar el ancho de nuestros elementos por porcentajes, si vamos los a la documentación veremos que el porcentaje lo maneja en fracciones entonces tenemos

- w-1/2 - 50% - toma el 50% del ancho
- w-1/3 - 33.33333%

Medidas

```css
      '0': '0',		= 0px
      '1': '0.25rem',	= 4px
      '2': '0.5rem',	= 0.8px
      '3': '0.75rem',	= 12px
      '4': '1rem',	= 	16px
      '5': '1.25rem',	= 20px
      '6': '1.5rem',	= 24px
      '8': '2rem',	= 	32px
      '10': '2.5rem',	= 40px
      '12': '3rem',	= 48px
      '16': '4rem',	= 64px
      '20': '5rem',	= 80px
      '24': '6rem',	= 96px
      '32': '8rem',	= 336px
      '40': '10rem',	= 640px
      '48': '12rem',	= 768px
      '56': '14rem',	= 896px
      '64': '16rem',	= 1024px
```

Aquí pueden ver los anchos
https://tailwindcss.com/docs/width

Aquí pueden ver los padding
https://tailwindcss.com/docs/padding

Aquí pueden ver los margin
https://tailwindcss.com/docs/margin

![img](https://www.google.com/s2/favicons?domain=//static.platzi.com/media/favicons/platzi_favicon.png)[Unidades de medida en CSS, la guía definitiva](https://platzi.com/blog/unidades-de-medida-en-css/)

## Cambiando las propiedades de la tipografía

## @font-faces

Para aprovechar la regla `@font-face`para realizar cualquier personalización fuera de *Tailwindcss* modificamos el archivo CSS principal de la siguiente manera:

```css
@tailwind base;

@font-face {
  font-family: Proxima Nova;
  font-weight: 400;
  src: url(/fonts/proxima-nova/400-regular.woff) format("woff");
}
@font-face {
  font-family: Proxima Nova;
  font-weight: 500;
  src: url(/fonts/proxima-nova/500-medium.woff) format("woff");
}

@tailwind components;

@tailwind utilities;
```

**Nota**. Para definir cualquier personalización hacia los estilos base, agregamos nuestros estilos después de `@tailwind base` y antes de `@tailwind components` para evitar problemas de compilación.

Se puede cambiar:

- Tipo de letra.
- Tamaño.
- Cursiva y negrita.

Se aplican clases como:
`text-3xl font-sans font-light`

Las características que puedes cambiar de las fuentes son:

- Tipo de letra
- Tamaño
- Cursiva
- Negrita

Se puede consultar la documentación en:

[font-family](https://tailwindcss.com/docs/font-family/#app)

[font-wight](https://tailwindcss.com/docs/font-weight/#app) 

[font-size](https://tailwindcss.com/docs/font-size/#app)

## Ajustando el espaciado entre letras y líneas

2 instrucciones que debemos aprender:
**Tracking** espaciamiento entre letras
**Leading** espaciamiento entre lineas

```css
text-left Alineado a la Izquierda
text-right Alineado a la Derecha
text-center Alineación al centro
text-justify Texto justificado
```

**Transformando el texto**

```css
uppercase text-transform: uppercase;
lowercase text-transform: lowercase;
capitalize text-transform: capitalize;
normal-case text-transform: none;
```

```css
/* Margen entre letras */
tracking-tight /Letras juntas
tracking-tighter /Letras muy juntas
tracking-wide /Letras separadas
tracking-widest /Letras muy

/* Margen entre lineas */
leading-tight /Lineas juntas
leading-snug /Lineas juntas (un poco mas separadas que leading-tight)
leading-relaxed /Lineas separadas
leading-loose /Lineas separadas (un poco mas que leading-relaxed)

/*Alineacion*/
text-left /Alineado a la Izquierda
text-right /Alineado a la Derecha
text-center /Alineación al centro
text-justify /Texto justificado

line-through /Tachar texto
underline /Subrayar texto
no-underline /Sin subrayar (quita el subrayado)
uppercase /Letras en mayusculas
capitalized /Primera letra de cada palabra en mayuscula
lowercase /Letras en minuscula
```

## Display

Las propiedades de display son útiles para poder controlar las dimensiones o espacios. Las propiedades básicas de display son:

- Block → Con esta configuración los bloques abarcan toda la pantalla.

  ```html
  <div class="text-center">
  		<div class="block bg-blue-800">1</div>
      <div class="block bg-blue-500">2</div>
      <div class="block bg-blue-300">3</div>
  </div>
  ```

- Inline-block → Sólo ocupan el espacio necesario para mostrar lo que hay en su interior.

  ```html
  <div class="text-center">
  		<div class="inline-block bg-blue-800">1</div>
      <div class="inline-block bg-blue-500">2</div>
      <div class="inline-block bg-blue-300">3</div>
  </div>
  ```

- Inline → Sólo ocupan el espacio necesario para mostrar lo que hay en su interior y la altura del elemento es indiferente.

  ```html
  <div class="text-center">
  		<div class="inline bg-blue-800">1</div>
      <div class="inline bg-blue-500">2</div>
      <div class="inline bg-blue-300">3</div>
  </div>
  ```

- Hidden → El elemento en cuestión no se muestra.

  ```html
  <div>
      <div class="inline-block bg-blue-800">1</div>
      <div class="inline-block bg-blue-500">2</div>
      <div class="hidden bg-blue-300">3</div>
      <div class="inline-block bg-blue-100">4</div>
  </div>
  ```

> ¿Que atajo de teclado usa el profesor para crear archivos y rutas asi como se estilaba en sublime text?
>
> Se llama advanced-new-file, está en la sección de plugins de VS Code.

Propiedades para controlar dimensiones y espacios de las cajas de presentación (display) :

- Block: abarca toda la pantalla
- Inline-Block: ocupan el espacio necesario
- Inline: ocupan el espacio necesario sin altura
- Hidden: el bloque no se muestra

## Flexbox


Flex es otra propiedad de display pero nos habilita otras propiedades para manipular nuestras cajas. Las propiedades propias de flexbox son:

1. Flex direction

   ```html
   <div class="flex flex-col">
       <div class="bg-blue-300">1</div>
       <div class="bg-blue-300">2</div>
       <div class="bg-blue-300">3</div>
   </div>
   ```

2. Align items

   ```html
   <div class="flex items-center">
       <div class="bg-blue-300">1</div>
       <div class="bg-blue-300">2</div>
       <div class="bg-blue-300">3</div>
   </div>
   ```

3. Justify content

   ```html
   <div class="flex justify-center">
       <div class="bg-blue-300">1</div>
       <div class="bg-blue-300">2</div>
       <div class="bg-blue-300">3</div>
   </div>
   ```

4. Order

   ```html
   <div class="flex justify-center items-center">
       <div class="order-3 bg-blue-300">1</div>
       <div class="order-1 bg-blue-300">2</div>
       <div class="order-2 bg-blue-300">3</div>
   </div>
   ```

> Flex justify-around y justify between crea espacios igual entre los elementos con la diferencia que en around si da margen en cada extremo de los elementos

Flexbox es un tema muy amplio, así que los invito a leer más a detalle sobre este tema en este otro artículo.

[Una guía completa de Flexbox!](https://medium.com/@alexcamachogz/una-guía-completa-de-flexbox-768b038de5e9)

[Documentacion sobre Flexbox](https://tailwindcss.com/docs/display/#flex)

## Crea tus propias utilerías

Para que les suceda como en el caso del profesor y solo tengan que guardar el archivo de css para que se actualicen los estilos, podemos añadir un script adicional a nuestro package.json, el cual es el siguiente:

```js
"scripts": {
    "build": "postcss css/tailwind.css -o public/css/styles.css",
    "dev": "postcss css/tailwind.css -o public/css/styles.css --watch"
  },
```

Con esto solo tienen que ejecutar “npm run dev” en su terminal para que cada que guarden el archivo se actulize automaticamente.

*Nota: el comando dev continuara su ejecución hasta que le indiquemos lo contrario, para escaparla solo usa control + C en la terminal

Dado los cambios en la última versión de Tailwind para habilitar las variantes debes primero declararlas en el archivo tailwind.config.js

Del siguiente modo;

```js
module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    scale: ['hover'],
    rotate:['responsive'],
    extend: {},
  },
  plugins: [],
}
```

Luego en el archivo tailwind.css después de las utilidades se definen las funciones de las variantes acorde a lo que se busque realizar, del siguiente modo:

```css
@tailwind base;
@tailwind components;
@tailwind utilities;

.responsive\.rotate-45 {
    transform: rotate(45deg);
  }

.hover\.zoom {
    transform: scale(2);
  }
```

También le puedes agregar transiciones y animaciones como css puro

Si no te funciona corre el comando

```bash
npm run build
```

dentro del directorio del proyecto

```css
@variants hover{
	.zoom{
		transform: scale(2);
		transition: 2s
	}
}

@responsive {
	.rotate-45{
		transform: rotate(45deg);
		transition: 2s
	}
}
```

## Entendiendo las variantes y las pseudo-clases

Variantes: Instrucciones para que el diseño cambie dependiendo del evento (html)

- Responsive: se ejecuta al cambio de resolución u orientación
- Hover: se ejecuta cuando colocamos el cursor encima o fuera de un elemento
- Focus: se ejecuta cuando el elemento tiene el foco
- Disabled: se ejecuta cuando el elemento es deshabilitado

Pseudo-Class: Palabra clave que se añade a la clase para lograr un diseño deseado

- Responsive: sm, md, lg, xl
- Hover: hover
- Focus: focus
- Disabled: disabled

![img](https://www.google.com/s2/favicons?domain=https://tailwindcss.com/docs/pseudo-class-variants//apple-touch-icon.png)[Pseudo-Class Variants - Tailwind CSS](https://tailwindcss.com/docs/pseudo-class-variants/)

## ¡Es hora de practicar!

### Colores y Dimensiones

Tip: Una buena práctica al combinar colores, como en botones por ejemplo, es no poner la letra color blanco o negro. En su lugar utiliza las tonalidades del mismo color para hacerla resaltar.

● Crea un botón color azul siguiendo estos consejos, recuerda que las tonalidades predefinidas van del 100 al 900.
● Crea un botón que se adapte según el tamaño de dispositivo.
● Cuando sea de tamaño pequeño, este botón debe abarcar el ancho de la pantalla y cuando sea un poco más grande debe tener un tamaño predeterminado.

Si no estás seguro sobre cuáles colores escoger, te recomiendo que uses Paletton, ahí podrás escoger un color y el programa te sugerirá colores que puedan combinar. (https://paletton.com/)

### Display y Flexbox

Tip: Una buena práctica es utilizar el margen de cada elemento para posicionarlo con respecto a otros. Esto se logra con margin left (ml-x), margin-top (mt-x). Si usamos margin right y margin bottom lo que hacemos es mover otros elementos respecto del elemento con el que estamos trabajando.

● Crea lo que se conoce como group button de 3 buttons o más. Esto lo logras utilizando lo aprendido en display o flexbox, así como margin y padding.

### Pseudo—Class Variants

Utilizando variantes como hover y active, logra crear efectos cambiando el color de los botones creados anteriormente.

● Cuando pases el cursor encima de uno de los botones deberá cambiar el fondo a un color más oscuro, de tal manera que resalte con el texto. Y al estar en estado normal, el fondo deberá ser claro y el texto, color oscuro.

### Extraer componentes usando @apply

Utilizando la directiva @apply, crea un componente de “alert” con sus variaciones para que se comporte distinto según la clase que le acompañe.

Hay que crear:
● alert (default) - Utiliza un color neutro
● alert (danger) - Utiliza un color rojo
● alert (warning) - Utiliza un color amarillo o naranja
● alert (info) - Utiliza un color azul

Para desarrollar este reto tendrás que hacer uso de:
● margin
● padding
● border
● background
● text color

El código de tu componente de poderse utilizar como sigue:
El código de tu componente debería verse así:

```html
	Este mensaje es una alerta default.
</div>

<div class=“alert alert-danger”>
	Este mensaje es una alerta danger.
</div>


<div class=“alert alert-warning”>
	Este mensaje es una alerta warning.
</div>

<div class=“alert alert-info”>
	Este mensaje es una alerta info.
</div>
```

**HTML**

```html
	<h1 class="text-2xl mt-10">Colores y Dimensiones</h1>
	<button class="bg-blue-800 text-blue-200 py-2 px-5 mt-5 rounded w-full sm:w-48">
		Submit
	</button>

	<h1 class="text-2xl mt-10">Display y Flexbox</h1>
	<div class="flex flex-wrap">
		<button class="bg-blue-800 text-blue-200 py-2 px-5 ml-5 mt-5 rounded w-full sm:w-48">
			Submit
		</button>
		<button class="bg-blue-800 text-blue-200 py-2 px-5 ml-5 mt-5 rounded w-full sm:w-48">
			Submit
		</button>
		<button class="bg-blue-800 text-blue-200 py-2 px-5 ml-5 mt-5 rounded w-full sm:w-48">
			Submit
		</button>
	</div>

	
	<h1 class="text-2xl mt-10">Pseudo—Class Variants</h1>
	<p>Nota: en Variants se agrega 'active' a backgroundColor y textColor</p>
	<div class="flex flex-wrap">
		<button class="hover:bg-blue-800 hover:text-blue-200 active:bg-blue-600 focus:bg-blue-800 focus:text-blue-200 focus:outline-none focus:shadow-outline bg-blue-200 text-blue-800 py-2 px-5 ml-5 mt-5 rounded w-full sm:w-48">
			Submit
		</button>
		<button class="hover:bg-blue-800 hover:text-blue-200 active:bg-blue-600 focus:bg-blue-800 focus:text-blue-200 focus:outline-none focus:shadow-outline bg-blue-200 text-blue-800 py-2 px-5 ml-5 mt-5 rounded w-full sm:w-48">
			Submit
		</button>
		<button class="hover:bg-blue-800 hover:text-blue-200 active:bg-blue-600 focus:bg-blue-800 focus:text-blue-200 focus:outline-none focus:shadow-outline bg-blue-200 text-blue-800 py-2 px-5 ml-5 mt-5 rounded w-full sm:w-48">
			Submit
		</button>
	</div>


	<h1 class="text-2xl mt-10">Extraer componentes usando @apply</h1>
	<div class="flex flex-wrap">
		<div class="alert alert-default">
			Este mensaje es una alerta default.
		</div>
		
		<div class="alert alert-danger">
			Este mensaje es una alerta danger.
		</div>
		
		<div class="alert alert-warning">
			Este mensaje es una alerta warning.
		</div>
		
		<div class="alert alert-info">
			Este mensaje es una alerta info.
		</div>
	</div>
```

**TailwindCSS**

```js
.alert {
  @apply py-2 px-4 mt-5 ml-5 rounded;
}
.alert-default {
  @apply bg-gray-100 text-gray-700 border border-gray-400;
}
.alert-danger {
  @apply bg-red-100 text-red-700 border border-red-400;
}
.alert-warning {
  @apply bg-orange-100 text-orange-700 border border-orange-400;
}
.alert-info {
  @apply bg-blue-100 text-blue-700 border border-blue-400;
}
```

<img src="https://i.ibb.co/4RGW4CY/tarea.webp" alt="tarea" border="0">

# 4. Proyecto: PlatziFood

## Creando una card

Así quedó, tan sencillo como no me lo imaginé, increíble.

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Card /Producto </title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body class="min-h-screen bg-gray-400 mx-4">
    <div class="bg-white mt-4">
        <img src="https://images.unsplash.com/photo-1568901346375-23c9450c58cd?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&h=500&q=80" alt="">
        <h2>titulo</h2>
        <p>descripcion</p>
        <div>precio</div>
        <div>Calificación /reseñas </div>
    </div>
    
</body>
</html>
```

Emmet abreviation `div>(div>img)h2+p+div*2`

![img](https://www.google.com/s2/favicons?domain=https://unsplash.com/apple-touch-icon.png)[Beautiful Free Images & Pictures | Unsplash](https://unsplash.com/)

## Aplicando formato a la card

no sabía lo de ☆ Gracias profe 😃

```html
<body class="min-h-screen bg-gray-400 m-4">
  <div>
    <div>
      <img
        class="rounded-lg"
        src="https://images.unsplash.com/photo-1568901346375-23c9450c58cd?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&h=500&q=80"
        alt="Hamburger"
      />
    </div>
    <div class="bg-white rounded-lg shadow-lg relative -mt-2 mx-2 p-2">
      <h2 class="text-xl tracking-tight uppercase font-semibold text-gray-900">
        Hamburguesa con Queso
      </h2>
      <p class="text-gray-700 leading-snug">Hamburguesa con queso y aderezo de chipotle</p>
      <div class="my-2 text-sm font-semibold text-gray-700">MXN $15</div>
      <div class="text-yellow-600">&starf;&starf;&starf;&starf;&star; / 36 reseñas</div>
    </div>
  </div>
</body>
```



## Responsive design en la card

Le coloque un detalle respecto a los bordes de la imagen cuando esta en md.
**Lo hice con:** md:rounded-r-sm

<img src="https://i.ibb.co/SsNwnXk/hamburger.webp" alt="hamburger" border="0">

**Codigo**

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>macrameStore</title>
    <link rel="icon" type="image/png" href="img/favicon.png" />
    <link rel="stylesheet" href="css/styles.css" />
  </head>
  <body class="min-h-screen bg-gray-500 mx-4 mb-8">
    <div class="mt-4 md:w-1/2 md:flex md:bg-white rounded-lg">
      <div>
        <img class="rounded-lg" src="img/macrame_1.jpg" alt="colgante_1" />
      </div>

      <div
        class="md:w-full relative mx-2 p-2 rounded-lg shadow-lg bg-white md:bg-transparent -mt-2"
      >
        <h2 class="text-xl tracking-tight font-bold uppercase text-gray-900 md:text-lg">
          Titulo
        </h2>
        <p class="text-gray-700 leading-snug">Colgante con piedra de amatista</p>
        <div class="mt-2 text-sm text-gray-700">85 €</div>
        <div class="mt-2 text-sm text-yellow-700">
          &starf;&starf;&starf;&starf;&starf;Calificación
        </div>
      </div>
    </div>
    <!-- <script src="https://kit.fontawesome.com/055f901897.js"crossorigin="anonymous"></script> -->
  </body>
</html>
```

![img](https://www.google.com/s2/favicons?domain=https://tailwindcss.com/components/cards//apple-touch-icon.png)[Cards - Tailwind CSS](https://tailwindcss.com/components/cards/)

## Construye el header

En el archivo tailwind.config.js

```js
 extend: {

	  colors: {
		'primary': '#f3c614',
		'secondary': '#353535'
	  },
```

Crear archivo header.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Header</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <header class="px-2 bg-secondary text-white flex justify-between item-center">
        <div><img class="py-1 h-8" src="./images/logo.svg" alt="logo">
        </div>
        <div class="text-2xl">&equiv;</div>
    </header>
</body>
</html>
```

En la consola

```bash
npm run build
```

 Son caracteres reservados que nos permiten injectar caracteres especiales. [Puedes ver un listado completo](https://dev.w3.org/html5/html-author/charref)

## Crea el footer

Para agregarle un transition al hover pueden usar las siguientes etiquetas

```html
<div class="hover:text-primary cursor-pointer transition ease-out duration-200" > <i class="fa fa-facebook"></i> Facebook </div>
<div class="hover:text-primary cursor-pointer transition ease-out duration-200" > <i class="fa fa-twitter"></i> Twitter </div>
<div class="hover:text-primary cursor-pointer transition ease-out duration-200" > <i class="fa fa-instagram"></i> Instagram </div>
```

Con la etiqueta **cursor-pointer** van a hacer que aparezca el dedo del click al hacer hover.
Con la etiqueta **transition** van a hacer que el hover tenga una transición en las propiedades que cambian con el hover.
Con la etiqueta **ease-out** van a colocar la velocidad de la animación de transición. Pueden ver toda la documentación en [www.tailwindcss.com/docs/transition-timing-function/#/app](https://platzi.com/clases/1822-tailwind-css/26616-crea-el-footer/www.tailwindcss.com/docs/transition-timing-function/#/app)
Con la etiqueta **duration-200** pueden modificar el tiempo que dura la animación. Esta puede ser modificada dede el 200.

El enlace a fontawesome:

```html
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
```

Barra horizontal codigo:

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>Header</title>
</head>
<body>
    <div class="flex justify-between items-center px-2 bg-secondary text-white">
        <div>
            <img class="h-8 py-1" src="images/logo-w.svg" alt="Platzi Food">
        </div>
        <div class="text-2xl">&equiv;</div>
    </div>
    <footer class="absolute lg:flex-row-reverse md:items-center lg:flex lg:justify-between bottom-0 w-full bg-secondary text-white">
        <div class="mt-4 md:mb-4 flex justify-center items-center">
            <div class="mx-2 hover:text-blue-700"><i class="fa fa-facebook"></i> Platzi Food</div>
            <div class="mx-2 hover:text-blue-500"><i class="fa fa-twitter"></i> @platzifood</div>
            <div class="mx-2 hover:text-pink-500"><i class="fa fa-instagram"></i> @platzifood</div>
        </div>
        <div class="mt-4 mb-4">
            <ul class="flex justify-center items-center">
                <li class="mx-2">About us</li>
                <li class="mx-2">Support</li>
                <li class="mx-2">Resgitry</li>
                <li class="mx-2">Privacy</li>
            </ul>
        </div>
    </footer>
</body>
</html>
```

![img](https://www.google.com/s2/favicons?domain=https://getbootstrap.com//docs/4.4/assets/img/favicons/apple-touch-icon.png)Bootstrap · The most popular HTML, CSS, and JS library in the world.https://getbootstrap.com/![img](https://www.google.com/s2/favicons?domain=https://unsplash.com/apple-touch-icon.png)Beautiful Free Images & Pictures | Unsplashhttps://unsplash.com/

## Crea un banner

Main section

```html
 <main>
        <div class="h-56 md:h-100 md:bg-center bg-no-repeat bg-cover" style="background-image: url('https://images.unsplash.com/photo-1555396273-367ea4eb4db5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=667&q=80');">
        </div>
    </main>
```

![img](https://www.google.com/s2/favicons?domain=https://www.bootstrapcdn.com/fontawesome//assets/img/favicons/apple-touch-icon.6126de5.png)[Font Awesome · BootstrapCDN by StackPath](https://www.bootstrapcdn.com/fontawesome/)

## Cómo integrar una card

Cards

```html
 <!-- CARD -->
        <div class="container mx-auto md:flex">

            <div class= "mx-2 mt-4 rounded-lg md:mx-auto md:w-5/12 md:flex md:justify-around md:flex-wrap md:bg-white">
                <div>
                    <img class="md:h-full object-cover md:w-48 rounded-lg md:rounded-r-none" src="https://images.unsplash.com/photo-1568901346375-23c9450c58cd?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&h=500&q=80" alt="Hamburger"> <!-- object cover para que ocupe la imagen todo el espacio en vertical -->
                </div>
        
                <div class="relative shadow-lg mx-2 p-2 bg-white rounded-lg -mt-2 md:bg-transparent"> <!-- relative para colocarlo encima, shadow para aplicar efecto de sombra p de padding, mx-2 para que haya una separacion con los bordes  -->
                    <h2 class="flex justify-center text-xl tracking-tight uppercase font-semibold text-gray-900 md:text-lg">Hamburger Cheese</h2>
                    <p class="text-gray-700 leading-snug"> Hamburguesa de queso con champiñones y carne</p> <!-- con leading snug se lo junta un poquito con el titulo  -->
                    <div class="mt-2 text-sm font-semibold text-gray-700"> $2.50 USD </div>
        
                    <!-- agregar estrellas -->
                    <div class=" mt-2 text-xs text-yellow-700"> &starf; &starf; &starf; &starf; &star; / 36 reseñas </div>
                </div>
            </div>

            <!-- SECOND CARD -->

            <div class= "mx-2 mt-4 rounded-lg md:mx-auto md:w-5/12 md:flex md:justify-around md:flex-wrap md:bg-white">
                <div>
                    <img class="md:h-full object-cover md:w-48 rounded-lg md:rounded-r-none" src="https://images.unsplash.com/photo-1568901346375-23c9450c58cd?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&h=500&q=80" alt="Hamburger"> <!-- object cover para que ocupe la imagen todo el espacio en vertical -->
                </div>
        
                <div class="relative shadow-lg mx-2 p-2 bg-white rounded-lg -mt-2 md:bg-transparent"> <!-- relative para colocarlo encima, shadow para aplicar efecto de sombra p de padding, mx-2 para que haya una separacion con los bordes  -->
                    <h2 class="flex justify-center text-xl tracking-tight uppercase font-semibold text-gray-900 md:text-lg">Hamburger Cheese</h2>
                    <p class="text-gray-700 leading-snug"> Hamburguesa de queso con champiñones y carne</p> <!-- con leading snug se lo junta un poquito con el titulo  -->
                    <div class="mt-2 text-sm font-semibold text-gray-700"> $2.50 USD </div>
        
                    <!-- agregar estrellas -->
                    <div class=" mt-2 text-xs text-yellow-700"> &starf; &starf; &starf; &starf; &star; / 36 reseñas </div>
                </div>
            </div>

        </div> 
```

![cards.png](https://static.platzi.com/media/user_upload/cards-d3c4193a-51ff-452f-92c1-d8b2c3e57927.jpg)

![img](https://www.google.com/s2/favicons?domain=https://unsplash.com/apple-touch-icon.png)[Beautiful Free Images & Pictures | Unsplash](https://unsplash.com/)

## Forms

Con diseño responsive:

```html
    <div class="mt-8 mx-2 md:mx-auto w-full md:w-1/2">
      <div>
        <div>Nombre</div>
        <input class="w-full py-1 px-4 rounded border border-blue-300 bg-gray-300 text-gray-800 placeholder-secondary leading-snug hover:bg-white focus:outline-none focus:shadow-outline" type="text" placeholder="Nombre">
      </div>
      <div>
        <div>Email</div>
        <input class="w-full py-1 px-4 rounded border border-blue-300 bg-gray-300 text-gray-800 placeholder-secondary leading-snug hover:bg-white focus:outline-none focus:shadow-outline" type="text" placeholder="Email">
      </div>
      <div>
        <div>Comentarios</div>
        <textarea class="w-full py-1 px-4 rounded border border-blue-300 bg-gray-300 text-gray-800 placeholder-secondary leading-snug hover:bg-white focus:outline-none focus:shadow-outline" name="" id="" rows="3"></textarea>
      </div>
      <div>
        <button class="block mx-auto px-4 py-2 rounded shadow-xl bg-gray-500 hover:shadow-none hover:bg-gray-700 hover:text-white">Enviar</button>
      </div>
    </div>
```

![Capture.JPG](https://static.platzi.com/media/user_upload/Capture-18ee2a51-0729-4da2-b2fd-dc555a31030c.jpg)

## Directivas de Apply

La directiva **@apply**
sirve para inicializar elementos de html.

 puedes instanciar clases:

```css
.navProduct{
    @apply cursor-pointer py-4 border-r border-t border-red-200
}
.navSubProduct{
    @apply w-full h-full
}
/*SM*/
@media (min-width: 640px) { }
/*MD*/
@media (min-width: 768px) { }
/*LG*/
@media (min-width: 1024px) {
    .navSubProduct{
        @apply grid
    }
}
/*XL*/
@media (min-width: 1280px) { }
```

> Las `@apply` son de los mejores features que tiene TailwindCSS. Actualmente ya cuenta con soporte para Grid-System 😉 [documentacion](https://tailwindcss.com/docs/functions-and-directives) sobre apply de tailwind.

## Extraer componentes

Desde la v2.0 de Tailwind las pseudo clases como hover, focus y demas se pueden aplicar directamente con @apply

[@apply Docs](https://blog.tailwindcss.com/tailwindcss-v2#use-apply-with-anything)

```css
.btn {
  @apply bg-indigo-500 hover:bg-indigo-600 focus:ring-2 focus:ring-indigo-200 focus:ring-opacity-50;
}
```

> Booom justo lo que veía que era lo malo de Tailwind de tener un html con muuuchas clases con la directiva @apply❤️ se soluciona!

 **Codigo**:

```css
.form-control {
  @apply w-full leading-snug placeholder-secondary text-gray-800 py-1 px-4 rounded bg-gray-300 border border-blue-300 hover:bg-white focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent;
}
```

Luego deben ir a su terminal y ejecutar **npm run build** y listo.

En el siguiente enlace pueden ver la documentación oficial de Tailwind.

- [Hover, Focus, & Other States](https://tailwindcss.com/docs/hover-focus-and-other-states)

## Navbar

agregar el icono de la lupa en el input!! si quieren solo tienen que poner:

```html
<input class="form-control " type="text" placeholder="&#xf002; Search">
```

y cualquier icono se puede solo tienen que reemplazar el codigo unicode.

Talwind no tiene su sistema de iconografía como Bootstrap basado en glifos?

- Aunque se puede integrar el sistema de iconos boostrap con Talwind. Talwind debería de tener su propio sistema, recursos: [tailwind.com/resources](https://tailwindcss.com/resources).

## Alpine JS

Con las siguientes etiquetas pueden animar la transición del toggle del navbar.
Estas etiquetas van en donde tienen la propiedad **x-show=‘open’** .

```js
x-transition:enter="transition ease-linear duration-300"
x-transition:enter-start="opacity-0 transform -translate-y-4"
x-transition:enter-end="opacity-100 transform translate-y-0"
x-transition:leave="transition ease-linear duration-300"
x-transition:leave-start="opacity-100 transform translate-y-0"
x-transition:leave-end="opacity-0 transform -translate-y-4"
```

En la etiqueta **x-transition:enter** van todas las propiedades de la duración de la animación de entrada
En la etiqueta **x-transition:enter-star** van todas las propiedades iniciales de estilos de la etiqueta, algo así como como el “frame 0”% de la entrada
En la etiqueta **x-transition:enter-end** van todas las propiedades finales de estilos de la etiqueta, algo así como como el “frame 100%” de la entrada

En la etiqueta **x-transition:leave** van todas las propiedades de la duración de la animación de salida
En la etiqueta **x-transition:leave-start** van todas las propiedades iniciales de estilos de la etiqueta, algo asi como como el “frame 0”% de la salida
En la etiqueta **x-transition:leave-end** van todas las propiedades finales de estilos de la etiqueta, algo así como como el “frame 100%” de la salida.

Para este caso es excelente poner

```css
cursor-pointer
```

Sobre el ícono de la hamburguesa para dar a entender que se puede hacer click sobre ella.

CDN:

```html
<script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
```

![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)[GitHub - alpinejs/alpine: A rugged, minimal framework for composing JavaScript behavior in your markup.](https://github.com/alpinejs/alpine)

## Optimiza tu archivo: PurgeCSS y NanoCSS

### Purge CSS & Nano CSS

#### Installation

```BASH
npm i -D @fullhuman/postcss-purgecss postcss
```

Este es el contenido del archivo de configuración `postcss.config.js`completo.
Es Importante agregar la línea de:
`defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || []`
Para el correcto funcionamiento de Pseudo-classes.

```js
const purgecss = require('@fullhuman/postcss-purgecss')
module.exports = {
 plugins: [
 require('tailwindcss'),
 require('autoprefixer'),
 purgecss({
 content: [
 './**/*.html',
 //Para agregar soporte para otro tipo de archivos.
 // './**/*.js',
 // './**/*.vue'
 ],
 //IMPORTANTE: Para soportar pseudo-clases
 defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || []
 }),
 require('cssnano')({
 preset: 'default',
 })
 ]
}
```

**cssnano**

```bash
npm install cssnano postcss --save-dev
```

**Importante** No olvidar la línea defaultExtractor para que sigan funcionando los breakpoints del responsive design y las pseudoclases que utilizamos en el proyecto!

*`post.config.js`*

```js
const purgecss = require("@fullhuman/postcss-purgecss");

module.exports = {
  plugins: [
    require("tailwindcss"),
    require("autoprefixer"),
    purgecss({
      content: ["./**/*.html"],
      defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || [],
    }),
    require("cssnano")({
      preset: "default",
    }),
  ],
};
```

- PurgeCSS : Sirve en Tailwind para quitar el código CSS que no estemos usando.
- NanoCSS: Minifica el código CSS para que pese lo menos posible.

Ambas librerías nos ahorran mucho almacenamiento.

![img](https://www.google.com/s2/favicons?domain=https://purgecss.com/#table-of-contents/apple-touch-icon.png)[Introduction | PurgeCSS](https://purgecss.com/#table-of-contents)

![img](https://www.google.com/s2/favicons?domain=https://cssnano.co//favicon.png)[cssnano: A modular minifier based on the PostCSS ecosystem](https://cssnano.co/)

# 5. Conclusiones
## Conclusiones y qué sigue

[utilidades con tailwind](https://github.com/aniftyco/awesome-tailwindcss)

Usando Github, la siguiente instrucción envía lo de la carpeta public a GitHub pages y genera un link para visualizarlo:

```bash
git subtree push --prefix PlatziFood/public origin gh-pages
```

