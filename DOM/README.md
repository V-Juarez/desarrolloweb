<h1>Manipulación del DOM</h1>

<h2>Jonathan Alvarez</h2>

<h1>Tabla de Contenido</h1>

- [1. DOM y Web API](#1-dom-y-web-api)
  - [Y entonces nació internet...](#y-entonces-nació-internet)
  - [¿Qué es el DOM?](#qué-es-el-dom)
  - [Web APIs modernas](#web-apis-modernas)
- [2. Operaciones básicas](#2-operaciones-básicas)
  - [Leer nodos](#leer-nodos)
  - [NodeLists vs Array](#nodelists-vs-array)
  - [Crear y agregar](#crear-y-agregar)
  - [Otras formas de agregar](#otras-formas-de-agregar)
  - [Atributos y propiedades](#atributos-y-propiedades)
  - [Eliminar nodos](#eliminar-nodos)
  - [Operaciones en lote](#operaciones-en-lote)
- [3. orkshop 1: Fetch](#3-orkshop-1-fetch)
  - [Presentación del proyecto](#presentación-del-proyecto)
  - [Descargando información y creando nodos](#descargando-información-y-creando-nodos)
  - [Enriqueciendo la información](#enriqueciendo-la-información)
  - [Usando la API de internacionalización del browser](#usando-la-api-de-internacionalización-del-browser)
  - [Comparte el resultado](#comparte-el-resultado)
- [4. Eventos](#4-eventos)
  - [Reaccionar a lo que sucede en el DOM](#reaccionar-a-lo-que-sucede-en-el-dom)
  - [Event propagation](#event-propagation)
  - [Event delegation](#event-delegation)
- [5. Workshop 2: Lazy loading](#5-workshop-2-lazy-loading)
  - [Presentación del proyecto](#presentación-del-proyecto-1)
  - [Nuestro propio plugin Lazy Loading](#nuestro-propio-plugin-lazy-loading)
  - [Creando las imagenes con JavaScript](#creando-las-imagenes-con-javascript)
  - [Intersection Observer](#intersection-observer)
  - [Aplicando Lazy loading](#aplicando-lazy-loading)
  - [Comparte el resultado](#comparte-el-resultado-1)
- [6. Workshop 3](#6-workshop-3)
  - [Proyectos propuestos](#proyectos-propuestos)
- [7. LIbrerías relacionadas](#7-librerías-relacionadas)
  - [¿Y jQuery?](#y-jquery)
  - [¿Y JSX?](#y-jsx)
- [8. Conclusiones](#8-conclusiones)
  - [Conclusiones](#conclusiones)

# 1. DOM y Web API

## Y entonces nació internet...

> "Dominando JavaScript puedes dominar cualquier framework"

[![](https://es.javascript.info/img/sitetoolbar__logo_small_en.svg)JavaScript Moderno](https://es.javascript.info/)

[![img](https://www.google.com/s2/favicons?domain=https://nodejs.org/es/download//static/images/favicons/apple-touch-icon.png)Descarga | Node.js](https://nodejs.org/es/download/)

[![img](https://www.google.com/s2/favicons?domain=https://miro.medium.com/fit/c/152/152/1*sHhtYhaCe2Uc3IU0IgKwIQ.png)Múltiples versiones de Node con NVM | by Nicolás Avila | devsChile | Medium](https://medium.com/devschile/múltiples-versiones-de-node-con-nvm-63b2ac715c38)

## ¿Qué es el DOM?

**Critical Rendering Path** Es el proceso que se encarga de convertir en pixeles el HTML, CSS y JS

El **DOM** arbol para el HTML que contiene toda nuestra estructura HTML

El **CSSOM** es el arbol para el CSS

- Todos los navegadores los utilizan para trabajar en un sitio web

- Todo comienza con el proceso llamado “

  Critical Rendering Path

  - Se puede dividir en 5 partes
  - A lo largo del proceso el navegador crea dos arboles
    - DOM
      - Es una representación del HTML
      - Estructura en forma de árbol de nodos
      - Es un modelo que puede ser modificado
    - CSSOM

![dom.png](https://static.platzi.com/media/user_upload/dom-16fbee89-dab7-4233-9a5c-153e53484fad.jpg)

**RESUMEN:** Para mostrar un sitio hay un proceso llamado Critical Render Path en él se crean dos árboles el DOM y el CSSOM. El Dom sera aquel que se encargará de tener todo nuestro contenido en una representación de arbol que contendrá nodos.

**DOM(Document Object Model):** no es mas que un árbol de nodos en el cual esta representado cada una de las etiquetas HTML que nosotros colocamos en nuestro proyecto.

**CSSOM(CSS Object Model):** es un conjunto de APIs que permite manipular CSS desde JavaScript. Así como el DOM (Document Object Model) es para HTML, el CSSOM (CSS Object Model) es para CSS. Permite leer y modificar el estilo de CSS de forma dinámica.

Para llegar al dom y cssom ocurre un proceso que se llama **El Camino Crítico de Renderización(Critical Rendering Path)** es la secuencia de pasos que el navegador realiza para convertir el HTML, CSS y JavaScript en píxeles en la pantalla.

[![img](https://www.google.com/s2/favicons?domain=https://caniuse.com//img/favicon-128.png)Can I use... Support tables for HTML5, CSS3, etc](https://caniuse.com/)

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/es//static/img/favicon144.e7e21ca263ca.png)Documentación web de MDN](https://developer.mozilla.org/es/)

## Web APIs modernas

**¿Qué rayos son las API?**

Puede parecernos un concepto muy abstracto o confuso al principio, ya que como dice el profesor _‘’lo utilizamos sin discreción para referirnos a todo’’_. Pero, en pocas palabras, una API es **todo** lo que sirva para comunicar fácilmente un pedazo de software con otro.

**APIs de terceros**
Twitter, por ejemplo, nos proporciona una manera sencilla de mostrar tweets de algún usuario a través de su API. Tan solo tenemos que hacer una petición GET al siguiente Endpoint:

GET https://api.twitter.com/2/users/:id/tweets

**APIs de servicios**
Si quisieramos mostrar mapas de Google Maps, tambien podriamos hacerlo a través de su API.

Por ejemplo, para mostrar la ubicación de Sydney, New South Wales, Australia, lo haríamos de la siguiente manera:

```js
function initMap() {
  map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: -34.397, lng: 150.644 },
    zoom: 8
  });
}
```

> Un ejemplo para entender el back, front y apis
>
> - el backend es la cocina
> - el frontend serían las mesas, la decoración de ellas
> - las apis serían los meseros

**Conclusión**
Si prestamos atención, nos damos cuenta de que son una manera sencilla de acceder a información o funcionalidades de otro pedazo de código. Es por eso que se les llama ‘’intermediarios’’ o ‘’puentes’’.

Si tienes algún otro ejemplo de APIs coméntalo o si dije alguna imprecisión corrígeme con confianza. Estamos para ayudarnos compañeros, _**nunca paren de aprender.**_

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/es//static/img/favicon144.e7e21ca263ca.png)Documentación web de MDN](https://developer.mozilla.org/es/)

[![img](https://www.google.com/s2/favicons?domain=https://caniuse.com//img/favicon-128.png)Can I use... Support tables for HTML5, CSS3, etc](https://caniuse.com/)

# 2. Operaciones básicas

## Leer nodos

Básicamente tenemos 4 formas de leer nodos con JS:

- **`parent.getElementById(‘id’)`** => nos permite obtener un elemento a través de su id.

- **`parent.getElementsByClassName(‘class’)`** => obtiene un array con todos los elementos hijos que tengan esa clase, ojo “getElementByClassName” no existe, es decir no podremos obtener solo 1 elemento con esa clase.

- **`parent.getElementsByTagName(‘div’)`** => con este método obtenemos una lista o “array list” con todos los elementos que tengan esa etiqueta, ejemplo todos los divs. Al igual que con el método anterior no hay posibilidad de usarlo en singular, siempre tendremos que usar **`get Elements`**

- **`parent.querySelector()`** => nos permite buscar de 3 formas, con id, clase o tagName. A diferencia de los 2 anteriores este nos devuelve 1 solo elemento, el primero que contenga el valor que se le paso. Id => (’#id’), class => (’.class’), tagName (‘div’)

- **`parent.querySelectorAll()`** => este método retorna una array list con todos los elementos que tengan ese selector (id, class o tagName)

  Casi siempre el elemento **“padre o parent”** es **document**. ya que estamos haciendo referencia a todo el DOM, todo el documento y esto en ciertos casos nos permite evitar errores.
  ejemplo = **const button = document.querySelector(’#button)**

Desde JavaScript podemos leer nodos, tenemos 3 formas de hacerlo:

- `document.getElementById():` Obtiene un elemento por su ID
- `parent.getElementByTagName():` Retorna un arreglo (realmente es un `HTMLCollection`) con todos los resultados coincidentes por el nombre de su etiqueta
- `parent.getElementByClassName():` Retorna un arreglo (realmente es un `HTMLCollection`) con todos los resultados coincidentes por su clase
  Sin embargo, existen 2 selectores más poderosos 👀:

- `parentElement.querySelector():` Selecciona un solo elemento y lo devuelve, la selección se hace a través de lo que le pases entre paréntesis, y la selección puede ser con sintaxis de CSS!! Retornará la primera coincidencia
- `parentElement.querySelectorAll():` Selecciona varios elemento y los devuelve, la selección se hace a través de lo que le pases entre paréntesis, y la selección puede ser con sintaxis de CSS!! Retornará un arreglo (en realidad es un `NodeList`)

Como pueden ver, es muy parecido a lo que teníamos con jQuery 👀

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/static/images/logos/platzi_favicon.png)Selectores de CSS en Curso de Preprocesadores CSS](https://platzi.com/clases/1665-preprocesadores/22294-selectores-de-css/)

## NodeLists vs Array

La diferencia entre NodeList y Array, es que el NodeList carece de métodos que si están disponibles en los Arrays, pero podemos pasar esto fácilmente usando el operador de propagación.

```js
// De esta forma pasamos todos los elementos en el NodeList a un arreglo al cual si podremos usar los métodos filter, map, reduce entre otros.
const nodeList = document.querySelectorAll("selector css");
const elementList = [...nodeList];
```

> _Recomendación importante cada vez que obtengamos un NodeList pásemelo a Array ya que los motores de javascript como el motor V8 de google están mas optimizados para Arrays que para NodeList._

## Crear y agregar

Al decir “crear nodos” nos referimos a crear elementos dentro de nuestro DOM. Para ello podemos hacer uso de:

- `createElement`: Para crear una etiqueta HTML:

```js
// Solo se ha creado, aún no se agrega al DOM
const etiquetaH1 = document.createElement("h1");
```

- `createTextNode`: Para crear un texto:

```js
// Solo se ha creado, aún no se agrega al DOM
const texto = document.createTextNode("¡Hola, Mundo!");
```

Como solo creamos, necesitamos formas de agregarlos al DOM, para ello, JavaScript nos provee de estas formas:

- `parentElement.appendChild()`: Agrega un hijo **al final** algún elemento

```js
// Obtengo el elemento padre
const parentElement = document.querySelector("selector");
// Creo el nodo a insertar
const h3 = document.createElement("h3");
// Creo el texto del nodo
const texto = document.createTextNode("Hola!");
// Inserto el texto al nodo
h3.appendChild(h3);
// Inserto el nodo al padre
parentElement.appendChild(h3);
```

- `parentElement.append()`: Es la evolución de `appendChild`, podemos agregar más de un nodo, puedes agregar texto y… no es soportado por Internet Explorer ¬¬!

  Un polyfill es una adaptación del código para dar soporte a navegadores que no lo soportan, aquí está el polyfill de append:
  https://developer.mozilla.org/es/docs/Web/API/ParentNode/append#polyfill

```js
// Obtengo el elemento padre
const parentElement = document.querySelector("selector");
// Agrego al elemento padre
parentElement.append("agrego un texto", document.createElement("div"));
```

- `parentElement.insertBefore()`: Inserta nodos antes del elemento que le pasemos como referencia, este nodo de referencia **tiene que ser un hijo DIRECTO del padre**

```js
// Obtengo el elemento padre
const parentElement = document.querySelector("selector");
// Creo un elemento
const titulo = document.createElement("h1");
// Obtengo la referencia del elemento del que quiero insertar antes:
const referencia = document.querySelector("selector");
// ¡Lo insertamos!
parentElement.insertBefore(titulo, referencia);
```

- ```js
  parentElement.insertAdjacentElement();
  ```

  : Inserta nodos según las opciones que le pasemos:

  1. **beforebegin:** Lo inserta antes del nodo
  2. **afterbegin:** Lo inserta despues del nodo
  3. **beforeend:** Lo inserta antes de donde finaliza el nodo
  4. **afterend:** Lo inserta después de donde finaliza el nodo

```js
// Obtengo el elemento padre
const parentElement = document.querySelector("selector");
// Creo un elemento
const nodo = document.createElement("span");
parentElement.insertAdjacentElement("beforebegin", nodo);
```

- Podemos crear nodos de dos formas

  - Crear elementos

  ```jsx
  document.createElement("Elemento");
  ```

  - Podemos crear cualquier elemento que tenga su respectiva etiqueta

  - El hecho de crear un elemento no significa que se agregue al DOM, estará guardado en memoria

  - Crear texto

    ```jsx
    document.createTextNode("Texto");
    ```

    - Solo sera un simple texto creado para agregarlo en algún momento a un elemento

<h3>Agregar Nodos</h3>

<h4>AppendChild</h4>

- Nos agrega un nodo al final del elemento en que apliquemos la operación
- Es un método que existe desde hace mucho tiempo, tiene algunas limitaciones

![slides-curso-de-manipulacion-del-dom_526610a4-4bf9-4457-acc4-8c10126f02fa_page-0023.jpg](https://static.platzi.com/media/user_upload/slides-curso-de-manipulacion-del-dom_526610a4-4bf9-4457-acc4-8c10126f02fa_page-0023-f59c6d2c-f8b3-42c3-97c6-ea4756f7640f.jpg)

![slides-curso-de-manipulacion-del-dom_526610a4-4bf9-4457-acc4-8c10126f02fa_page-0024.jpg](https://static.platzi.com/media/user_upload/slides-curso-de-manipulacion-del-dom_526610a4-4bf9-4457-acc4-8c10126f02fa_page-0024-8ef59a30-54c2-4142-a39d-f7f71a965ee2.jpg)

<h4>Append</h4>

- Es la evolución de appendChild
- Puedes agregar más de un nodo
- Puedes agregar texto sin necesidad de usar createTextNode
- IE 11: No soportado

![slides-curso-de-manipulacion-del-dom_526610a4-4bf9-4457-acc4-8c10126f02fa_page-0026.jpg](https://static.platzi.com/media/user_upload/slides-curso-de-manipulacion-del-dom_526610a4-4bf9-4457-acc4-8c10126f02fa_page-0026-eb2683da-ac63-4860-8d70-831f9a30b90a.jpg)

<h4>InsertBefore</h4>

- Nos permite poner un nodo antes de alguna referencia que tengamos
- Este método recibe dos parámetros
  - Nuevo Nodo
  - Referencia ⇒ Hijo directo del nodo base

![slides-curso-de-manipulacion-del-dom_526610a4-4bf9-4457-acc4-8c10126f02fa_page-0028.jpg](https://static.platzi.com/media/user_upload/slides-curso-de-manipulacion-del-dom_526610a4-4bf9-4457-acc4-8c10126f02fa_page-0028-a4d14765-7834-46cc-ba6c-5fb6d24a9f43.jpg)

<h4>InsertAdjacentElement</h4>

- Es un método complejo pero el cual cuenta con una basta flexibilidad
- Nos permite especificar donde poner el nuevo nodo con una lista de posiciones predefinidas

![slides-curso-de-manipulacion-del-dom_526610a4-4bf9-4457-acc4-8c10126f02fa_page-0035.jpg](https://static.platzi.com/media/user_upload/slides-curso-de-manipulacion-del-dom_526610a4-4bf9-4457-acc4-8c10126f02fa_page-0035-78ee0d95-71b1-409f-88eb-f41d2536939e.jpg)

![slides-curso-de-manipulacion-del-dom_526610a4-4bf9-4457-acc4-8c10126f02fa_page-0036.jpg](https://static.platzi.com/media/user_upload/slides-curso-de-manipulacion-del-dom_526610a4-4bf9-4457-acc4-8c10126f02fa_page-0036-b36aec23-4b3e-4bfa-9330-ecb00eb3e65f.jpg)

> Para poder crear elementos y texto de una forma imperativa, es decir mediante el JS podemos usar createElement y createTextNode. Para agregar al DOM son otros métodos como appendChild para poner al final de un elemento, append es similar solo que nos permite agregar listas y textos (sin necesidad de usar textos), insertBefore para poner antes de alguna referencia e ### insertAdjacentElement

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/es/docs/Web/API/ParentNode/append/apple-touch-icon.png)ParentNode.append() - Referencia de la API Web | MDN](https://developer.mozilla.org/es/docs/Web/API/ParentNode/append)

## Otras formas de agregar

Existen otras formas de agregar nodos:

- `node.outerHTML`: Sirve para leer HTML para leer HTML
- `node.innerHTML:` Sirve para escribir HTML

PEEEEEEERO, tienen un problema de seguridad 👀☝️

**hack:** Cuando en el inspector de elementos seleccionas un elemento y en la consola escribes `$0`, este te devolverá el elemento tal como si lo hubieses seleccionado con `document.querySelector()`.

Aquí les dejo el playground que usó el profesor para hacer las pruebas:
https://codepen.io/jonalvarezz/pen/OJXeNab?editors=0110

El problema con estas formas de inserciones es que permiten la inserción XSS, es decir, código maligno, y cualquier usuario programador malicioso podría meter scripts molestos, imagina que cada que un usuario llegue a tu página le salga un alert… ¡Sería catastrófico! Siempre sanitiza (remover caracteres especiales) los inputs de tus usuarios

### Playground innerHTML

index.html

```html
<div class="max-w-md mx-auto mt-6">
  <h1 class="font-bold text-xl mb-2 text-center">Playground innerHTML</h1>
  <textarea
    name=""
    id=""
    cols=""
    rows=""
    class="border-solid border-2 border-gray-600 h-40 p-4"
    style="width: 100%"
  ></textarea>
  <button
    class="bg-blue-500 hover:bg-blue-400 text-white font-bold py-2 px-4 border-b-4 border-blue-700 hover:border-blue-500 rounded"
  >
    Leer y Escribir con innerHTML
  </button>

  <div class="mt-12">
    <h2 class="font-bold text-xl mb-2">Resultado</h2>
    <div
      id="output"
      class="bg-gray-300 p-4 border-radius"
      style="min-height: 200px"
    ></div>
  </div>

  <div class="mt-8 pb-2">
    <span
      class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2"
      >#platzi</span
    >
    <span
      class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2"
      >@jonalvarezz</span
    >
  </div>
</div>
```

<div class="max-w-md mx-auto mt-6">
<h1 class="font-bold text-xl mb-2 text-center">Playground innerHTML</h1>
<textarea name="" id="" cols="" rows="" class="border-solid border-2 border-gray-600 h-40 p-4" style="width: 100%"></textarea>
  <button class="bg-blue-500 hover:bg-blue-400 text-white font-bold py-2 px-4 border-b-4 border-blue-700 hover:border-blue-500 rounded">
  Leer y Escribir con innerHTML
</button>
 <div class="mt-12">
   <h2 class="font-bold text-xl mb-2">Resultado</h2>
   <div id="output" class="bg-gray-300 p-4 border-radius" style="min-height: 200px"></div>
 </div>  
<div class="mt-8 pb-2">
    <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#platzi</span>
    <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">@jonalvarezz</span>
  </div>
</div>

main.js

```js
const input = document.querySelector("textarea");
const button = document.querySelector("button");
const output = document.querySelector("#output");

button.addEventListener("click", () => {
  output.innerHTML = input.value;
});
```

Los **ataques XSS** son un tipo de inyección en la cual un atacante logra ejecutar código en los navegadores de los usuarios que acceden a un sitio web legítimo

- Existen otras formas de leer y agregar nodos que son muchas convenientes usando cadenas de texto

  - node.outerHTML

    (leer)

    - Nos dejar leer el HTML como una cadena del nodo seleccionado

  - node.innerHTML

    (escribir)

    - Nos deja modificar el contenido del nodo

- La desventaja es que convierte el texto en HTML pudiendo crear inyecciones de XSS

  - Debido a que convierte todo el texto en HTML, habiendo la posibilidad de que se pueda inyectar códigos de terceros

- Si es que fuera muy necesario usar estos métodos y el usuario necesitara ingresar los datos, se debe hacer si o si un proceso de **sanitize** (Limpieza)

**> ** Existe otras formas de leer y escribir el HTML, pero tienen un riesgo de ataques XSS, no es mala siempre y cuando se tenga un cuidado con las posibilidades del usuario

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)Attention Required! | Cloudflare](https://codepen.io/jonalvarezz/pen/OJXeNab)

## Atributos y propiedades

Básicamente un **atributo** es el estado inicial en nuestro HTML, es HTML solo podemos escribir atributos porque es el estado inicial con el que se renderizan y una **propiedad** es la variable que podemos cambiar a lo largo de la ejecución del programa mediante JavaScript, es decir, podemos acceder a dichos atributos y cambiarlo, haciendo que sean propiedades, [aquí un poco más de información sobre ello](https://lenguajejs.com/webcomponents/lit-element/propiedades-y-atributos/#:~:text=Una propiedad es una variable,siempre contendrá valores de tipo .&text=Sin embargo%2C dicho componente puede,se puede acceder mediante this.).

Lo genial de JavaScript es que podemos cambiarlas de forma dinámica. Recordemos que JavaScript son en su mayoría objetos, por lo que los nodos HTML dentro de JavaScript son representados como objetos. Teniendo eso en cuenta, podemos acceder a cualquier atributo de dichos nodos desde HTML y cambiar sis propiedades, por ejemplo:

```js
// Al seleccionar el nodo HTML, JavaScript lo convierte en un objeto!
const input = document.querySelector("input");

// Y of course, podemos modificarlo como cualquier otro objeto de JavaScript:
input.placeholder = "Escribe algo";
input.value = 2;
input.type = "number";
```

Los **atributos de HTML** son palabras especiales utilizadas dentro de la etiqueta de apertura, para controlar el comportamiento del elemento. Los atributos de HTML son un modificador de un tipo de elemento de HTML

**Propiedad del DOM** sera inicializada por los atributos del HTML para posteriormente modificarlas

[What is the difference between properties and attributes in HTML? - GeeksforGeeks](https://www.geeksforgeeks.org/what-is-the-difference-between-properties-and-attributes-in-html/)

- El 80% del tiempo que estemos manipulando el DOM, lo que haremos sera cambiando dinámicamente con JS las propiedades de un elemento
- Podemos acceder y modificar las propiedades mediante las propiedades del elemento deseado

```jsx
// 👀 Lectura
console.log(inputName.value); // "Fer"
// 📝 Modificación
inputName.value = "Fernando";
```

<h4>**Atributo 🆚 Propiedad**</h4>

- Atributo
  - Definidas por el HTML
  - Constantes
  - Solo se usan para iniciar el HTML y las propiedades del DOM
- Propiedad
  - Son lo que irán cambiando en JS
  - Definidas por el DOM
  - Valor de una propiedad es variable

> Las propiedades de los elementos serán aquellas que mayormente se modificarán. La diferencia entre un atributo y una propiedad es que el atributo para iniciar el HTML y DOM, son constantes mientras que las propiedades vienen del DOM y se pueden ir cambiando

## Eliminar nodos

- Existen 3 formas de eliminar nodos en el DOM

1. **Remove Child**

   - Podemos eliminar un nodo según a un método que proviene del padre y pasamos la referencia a eliminar

   ```jsx
   padre.removeChild(nodoAEliminar);
   ```

2. **Remove**

   - Es la evolución de remove child
   - No esta soportado en internet explorer

   ```jsx
   nodoAEliminar.remove();
   ```

3. **Replace Child**

   - Nos permite remplazar un nodo

   ```jsx
   padre.replaceChild(nuevoNodo, aRemplazar);
   ```

> Podemos eliminar nodos con diferentes métodos que nos provee el DOM como ser **removeChild**, el cual necesita la referencia del padre a eliminar y del nodo que se eliminara, **remove** el cual se encarga de eliminar el nodo solo con la referencia deseada a borrar y finalmente **replaceChild** que nos ayuda remplazar un elemento con las referencias del padre, el nuevo nodo y el elemento a remplazar.

- `parentElement.removeChild()`: Elimina un elemento hijo a partir del elemento padre:

```js
// Nota: En la clase se hizo con $0, pero yo te lo dejo en cómo lo harías normalmente con JavaScript

// Selecciono el elemento que quiero eliminar
const nodoAEliminar = document.querySelector("selector");
// Selecciono a su padre directo
const parentElement = nodoAEliminar.parentElement;
// Lo elimino
parentElement.removeChild(nodoAEliminar);
```

- `docuement.remove()`: Es la evolución de `removeChild` Y… tampoco es soportado por Internet Explorer ¬¬!

  No importa, aquí está el polyfill:D
  https://developer.mozilla.org/es/docs/Web/API/ChildNode/remove#polyfill

```js
// Selecciono el elemento que quiero eliminar
const nodoAEliminar = document.querySelector("selector");

// Lo elimino uwu
nodoAEliminar.remove();
```

- `document.replaceChild()`: Reemplaza un nodo (en pocas palabras lo elimina y mete otro)

```js
// Selecciono un padre
const padre = document.querySelector("selector");

// Selecciono el elemento al que voy a reemplazar
const toReplace = document.querySelector("selector");

// Creo el nodo por el cual lo voy a reemplazar
const node = document.createElement("h1");
// Le pongo un texto
node.textContent = "Un texto cualquiera";

// Lo reemplazo >:D!!!
parent.replaceChild(node, toReplace);
```

## Operaciones en lote

![Ee3PlLlUMAARqT5.jpg](https://static.platzi.com/media/user_upload/Ee3PlLlUMAARqT5-3e39919b-9cc1-426f-881e-9a3b2df38c72.jpg)

El spread operator trabaja con arreglos y objetos, cuando lo pasamos en la llamada a una función, lo que hará es deconstruir ese arreglo y lo pasará como parámetros individuales para la función… aquí un ejemplo para que me entiendas:

```js
function funcionQueRecibeTresParametros(param1, param2, param3) {
  // code...
}

// La forma normal de llamarla sería:
funcionQueRecibeTresParametros(1, 2, 3);

// Pero, ¿qué pasa si tengo un arreglo que contiene esos 3 parámetros?
const arregloDeParametros = [1, 2, 3];

// Bueno, pues el spread operator puede deconstruir ese arreglo y poner cada elemento en donde irían mis parámetros :D!
funcionQueRecibeTresParametros(...arregloDeParametros);

// Eso sería equivalente a esto:
funcionQueRecibeTresParametros(
  arregloDeParametros[0],
  arregloDeParametros[1],
  arregloDeParametros[2]
);
```

Esto es muy útil cuando tenemos demasiados valores, recuerda, mientras menos modifiques el DOM, más eficiente será tu programa, y recordemos que tenemos a `append()` que nos permite insertar múltiples elementos en el DOM en una sola llamada, ¡aprovechémoslo!

**Dato curioso:**

En algunos frameworks de JavaScript como Vue, existe una cosa llamada el **Virtual DOM**, no es más que un objeto JavaScript que simula al DOM real, al menos en Vue, esto tiene un proxy que está observando por cambios en ese Virtual DOM para reaccionar y renderizar solo una pequeña parte en el DOM (en lugar de reescribirlo completamente).

- Realizar mutaciones en el DOM tiene un costo
- Cuando usamos frameworks y liberáis lo que más cuesta en estas librerías es estar haciendo operaciones en el DOM
- Entre menos operaciones realicemos en el DOM especialmente escribir y eliminar cosas más rápidas sera el website

<h3>**Ejemplo: Escribir 100 inputs**</h3>

---

<h3>**🙅‍♂️ No optimo**</h3>

```jsx
for (let i = 0; i < 100; i++) {
  const node = document.createElement("input");
  document.body.appendChild(node); // Modificamos 100 veces
}
```

---

<h3>**👨‍💻 Optimo**</h3>

```jsx
const nodes = [];
for (let i = 0; i < 100; i++) {
  const node = document.createElement("input");
  nodes.push(nodes);
}
document.body.append(...nodes); // Modificamos 1 sola vez
```

> Mientras menos modificaciones hagamos en el DOM mayor sera el rendimiento del website

# 3. Workshop 1: Fetch

## Presentación del proyecto

El fetch() con async/await

```js
const url = "https://platzi-avo.vercel.app/api/avo";

//web api
async function fetchData() {
  const response = await fetch(url),
    data = await response.json(),
    allItems = [];

  data.data.forEach(item => {
    // create image
    const image = document.createElement("img");
    // create title
    const title = document.createElement("h2");
    // create price
    const price = document.createElement("div");

    const container = document.createElement("div");
    container.append(image, title, price);

    allItems.push(container);
  });

  document.body.append(...allItems);
}

fetchData();
```

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - jonalvarezz/snowpack-template-tailwind: Snowpack template featuring Tailwind, Prettier and auto publish with GitHub Actions](https://github.com/jonalvarezz/snowpack-template-tailwind)

[Fetch API](https://developer.mozilla.org/es/docs/Web/API/Fetch_API)

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)platzi-dom/workshop-fetch at main · jonalvarezz/platzi-dom · GitHub](https://github.com/jonalvarezz/platzi-dom/tree/main/workshop-fetch)

[![img](https://www.google.com/s2/favicons?domain=https://miro.medium.com/fit/c/152/152/1*sHhtYhaCe2Uc3IU0IgKwIQ.png)Múltiples versiones de Node con NVM | by Nicolás Avila | devsChile | Medium](https://medium.com/devschile/múltiples-versiones-de-node-con-nvm-63b2ac715c38)

## Descargando información y creando nodos

El fetch() con async/await

```javascript
const url = "https://platzi-avo.vercel.app/api/avo";

//web api
async function fetchData() {
  const response = await fetch(url),
    data = await response.json(),
    allItems = [];

  data.data.forEach(item => {
    // create image
    const image = document.createElement("img");
    // create title
    const title = document.createElement("h2");
    // create price
    const price = document.createElement("div");

    const container = document.createElement("div");
    container.append(image, title, price);

    allItems.push(container);
  });

  document.body.append(...allItems);
}

fetchData();
```

**😎🤙**

```javascript
//URL API
const url = "https://platzi-avo.vercel.app/api/avo";

/*Web API Fetch
 
 La utilizamos para traer recursos desde cualquier otro sitio web

 Lo unico que tenemos que pasarle es nuestra url
 
 1. Nos conectaremos al servidor

 */

window
  .fetch(url)
  /*2. Procesar la respuesta y despues la convertimos en JSON
    Fetch es algo que nos devuelve una promesa asi que
    trabajaremos con promesas para obtener la respuesta
    y transformarla en JSON

*/
  .then(respuesta => respuesta.json())

  /*3.
 Luego de que convirtamos la respuesta en JSON lo que obtenemos
 ahora es informacion y la obtenemos concatenando otra promesa

 
 Cuando tengamos el JSON  tendremos esa informacion que
 nos servira para renderizar esa info en nuestro navegador*/
  .then(responseJson => {
    const todosLosItems = [];
    /*recorremos cada uno de los elementos que estan en arrays
        con un forEach
        
        */

    responseJson.data.forEach(item => {
      /*atraves del parametro de la funcion del forEach accedemos
        a los elementos de la respuesta json*/

      //creamos nuestros elementos
      const imagen = document.createElement("img");

      const titulo = document.createElement("h2");

      const precio = document.createElement("div");

      // cremos el contenedor donde vamos a poner nuestros elementos

      const container = document.createElement("div");

      /*agregamos los elementos a un contenedor
    
        container.appendChild(imagen);
        container.appendChild(titulo);
        container.appendChild(precio);
    
    */

      container.append(imagen, titulo, precio);

      //agregamos el contenedor en nuestro body
      //document.body.appendChild(container);
      todosLosItems.push(container);

      console.log(item.name);
    });

    document.body.append(...todosLosItems);
  });

/*RESUMEN: NOS CONECTAMOS A UNA API QUE ES UN PUENTE CON LA INFORMACION 
  DE UN SERVIDOR Y ESE SERVIDOR NOS DEVUELVE ESA INFORMACION, Y UTILIZAMOS
  UN CICLO POR CADA UNO DE LOS ELEMENTOS QUE NOS DEVUELVE ESE SERVIDOR
  CREAMOS NODOS Y SE LOS AGREGAMOS AL FINAL A NUESTRO HTML*/

/*RETO PARA MEJORAR ESTE CODIGO  Y ES HACERLO CON ASYNC Y AWAIT EN VES 
  DE PROMESAS */
```

Fragmento de documento vacio el cual este listo para insertar nodo en el de la siguiente forma

```javascript
let fragment = document.createDocumentFragment();
fetch(url)
  .then(res => res.json())
  .then(data => {
    //creamos el fragment
    let fragment = document.createDocumentFragment();

    data.data.forEach(item => {
      let image = document.createElement("img");

      let title = document.createElement("h2");

      let price = document.createElement("span");

      const container = document.createElement("div");
      container.append(image, title, price);
      //agregamos los nodos al fragment y no al DOM directamente
      fragment.appendChild(container);
    });
    //solo renderizamos una sola vez el DOM
    document.body.append(fragment);
  });
```

## Enriqueciendo la información

Les recomiendo la extensión de Google Chrome JSON viewer, con ella podemos visualizar formato JSON en nuestro navegador de una forma ordenada. Además, es completamente personalizable.

Al descargarlo, podrás visualizar algo como esto:

![img](https://lh3.googleusercontent.com/NAeRlSzOVLUl2YJ66_r3yfQl-YbFIwe4aZkpzpUX8Ou15vbcuL1ODChTAtVfD40OTumu_SCUiPRdRAlUxgDhj48rbg=w640-h400-e365-rj-sc0x00ffffff)

**😎👌👌**

```js
const appNode = document.querySelector("#app");
//const url = "https://platzi-avo.vercel.app/api/avo";

/* Cambiamos la url que tenemos por una url base
y lo que hacemos es agregarle atraves de  un template string
el resto de la url que teniamos en el fetch*/

window
  .fetch(`${baseUrl}/api/avo`)
  .then(respuesta => respuesta.json())
  .then(responseJson => {
    const todosLosItems = [];
    responseJson.data.forEach(item => {
      const imagen = document.createElement("img");
      /* Para asignar la url que obtenemos de la api
        a nuestra imagen lo hacemos en la propiedad src

        Si la agregaramos solo con lo que obtenemos de la API nos 
        daria un error ya que lo que obtenemos es una ruta obsuluta
        mas no una url por lo tanto nos dara error porque no
        encontraria la ruta de la imagen
        
        */
      imagen.src = `${baseUrl}${item.image}`;

      const titulo = document.createElement("h2");

      titulo.textContent = item.name;

      const precio = document.createElement("div");

      /* Le asignamos al texto del elemento precio que es un div
           la informacion que obtenemos de respuesta JSON Y atraves
           del parametro que tenemos en la funcion del forEach
           accederemos al precio 
        
        */
      precio.textContent = item.price;

      const container = document.createElement("div");

      container.append(imagen, titulo, precio);

      todosLosItems.push(container);

      console.log(item.name);
    });

    appNode.append(...todosLosItems);
  });

/*
Luego agregaremos en nuestro HTML una etiqueta div con un id para meter todos nuestros nodos dentro de el, generalmente se le llama mount o app o container.
*/

<div id="app"></div>;

/* 
Después vamos a nuestro javascript y agregamos nuestra etiqueta div con id app
*/

const appNode = document.querySelector("#app");

/*
y cambiamos la parte donde agregábamos todos los items al body
*/
appNode.append(...todosLosItems);
```

## Usando la API de internacionalización del browser

### Utilizar css & class CSS

```css
title.style = "font-size: 2rem";
title.style.fontSize = "3rem";
// 2da forma
title.className = "nombre-de-clase";
```

1. Aunque JavaScript nos provee de todas estas formas de controlar estilos, por buenas prácticas y por tener **código limpio**, es más recomendable usar clases y alternar entre clases que estén escritas en CSS, esto para no mezclar JavaScript y CSS
2. Además de la propiedad `className`, hay una más interesante llamada `classList`, esta nos permite añadir y eliminar elementos de forma dinámica fácilmente (útil para cuando queires eliminar o añadir una sola clase de manera dinámica, si usaras `className` tendrías que volver a escribirlas todas):

```js
// Primero puedes usar clases iniciales (aunque para código limpio lo mejor es definirlas directamente en el HTML)
imagen.className = "h-16 w-16 md:h-24";

// Y ahora podemos usar classList para añadir/borrar dinámicamente
imagen.classList.add("md:w-24"); // Añade una clase
imagen.classList.remove("h-16"); // Elimina una clase
```

1. El profesor hizo un corte repentino y añadió más código sin dejarlo en la sección de archivos y enlaces (muy mal), aquí les dejo el enlace a mi repositorio que **sí contiene todo el código con las clases y los estilos incluidos que uso el profesor**:
   [Adición del API de internacionalización](https://github.com/RetaxMaster/curso-manipulacion-dom/tree/5a883a469dd4c367c5568558cac4d94dac78d08c)

[Guatemala](https://www.localeplanet.com/icu/es-GT/index.html)

```js
const formatPrice = price =>
  new Intl.NumberFormat("es-GT", {
    style: "currency",
    currency: "GTQ"
  }).format(price);
```

[Formatos de moneda que se podrían incorporar](https://www.currency-iso.org/dam/downloads/lists/list_one.xml)

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/apple-touch-icon.png)Intl - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl)

## Comparte el resultado

![Screenshot_39.png](https://static.platzi.com/media/user_upload/Screenshot_39-f9fcb488-da5d-4b5a-82e9-22426f31407a.jpg)

![Screenshot_40.png](https://static.platzi.com/media/user_upload/Screenshot_40-a82ce1bd-fd36-4fd3-b1de-66b7ad47669a.jpg)

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)platzi-dom/workshop-fetch at main · jonalvarezz/platzi-dom · GitHub](https://github.com/jonalvarezz/platzi-dom/tree/main/workshop-fetch)

# 4. Eventos

## Reaccionar a lo que sucede en el DOM

```jsx
miElemento.addEventListener("evento", manejador);
```

En este caso, el manejador debe ser una función callback que se ejecutará cuando el evento sea disparado. Es muy común verlo como una función anónima:

```jsx
button.addEventListener("click", () => {
  alert("Me has clickado 😄");
});
```

Sin embargo, la mejor práctica es crear funciones por separado, así siempre tendremos una referencia a dicha función (con una función anónima no tenemos nada que la identifique, de ahí su nombre)

```jsx
const miFuncionManejadora = () => {
  alert("Me has clickado 😄");
};

button.addEventListener("click", miFuncionManejadora); // Presta atención como la estamos mandando sin paréntesis, porque de esa forma solo le pasamos la referencia de la función, si le pusieramos paréntesis entonces la estaríamos ejecutando
```

Y esto tiene la ventaja de que podemos remover los eventos cuando queramos ya que tenemos la referencia de la función manejadora 😄

```jsx
const miFuncionManejadora = () => {
  alert("Me has clickado 😄");
};

// Agrego el evento
button.addEventListener("click", miFuncionManejadora);

// Quito el evento
button.removeEventListener("click", miFuncionManejadora);
```

También podemos definir funciones de esta otra manera 👀

```jsx
button.onClick = () => {
  alert("Me has clickado 😄");
};
```

Esta sintaxis es `onEvento` pero no es muy común ^^

Como dato adicional, esta es otra forma de añadir eventos desde HTML:

**HTML**

```jsx
<button onclick="miFuncionManejadora">Clicame</button>
```

**JavaScript**

```jsx
const miFuncionManejadora = () => {
  alert("Me has clickado 😄");
};
```

De esta forma, el botón, mediante un atributo estaría llamando a la función 😄

> “onEvent” generalmente no es buena idea porque solo puedes definirlo una vez.
>
> addEventListener te deja agregar listeners de forma ilimitada.

<h4>Ideas / conceptos claves</h4>

Una **función anónima** es una definición de función que no está vinculada a un identificador

<h4>Apuntes</h4>

- JS es un lenguaje que está basado en eventos

✨ Toda la magia sucede cuando escuchamos los eventos y reaccionamos con lo que sucede

#### Eventos Add Event Listener

- Agrega un evento

```js
node.addEventListener(tipoDeEvento, callback);
```

- Todos los eventos envían información del evento como un parámetro al callback
  - Existen eventos específicos para elementos HTML especiales como ser video o audio

#### Remove Event Listener

- Elimina un evento
- Debemos especificar el tipo de evento y la referencia de la función al momento de invocar el método del elemento HTML
- Es recomendable si deseamos eliminar eventos a futuro no crear **funciones anónimas** por que se perderá la referencia

```jsx
node.removeEventListener(tipoDeEvento, callback);
```

**RESUMEN:** Todos los elementos del DOM pueden tener las propiedades para agregar o eliminar eventos según diferentes acciones. Algunos elementos tienen acciones específicas como ser los videos o audios

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/Events/apple-touch-icon.png)Event reference | MDN](https://developer.mozilla.org/en-US/docs/Web/Events)

## Event propagation

Básicamente la propagación de eventos se produce cuando tienes puestos algunos eventos en contenedores que son hijos de otro, por ejemplo:

```js
<div id="div1">
  <div id="div2">
    <div id="div3">Hola</div>
  </div>
</div>
```

Si le ponemos un event listener a los 3 divs, y clicas dentro del div 3, también estás clicando el div2 (porque el div3 está **dentro** del div2), y a su vez estás clicando el div1 (porque estos 2 divs están dentro de div1), por tanto, el evento se va a propagar hacia los 3 divs.
.
La forma de deterlo es usando el método `stopPropagation()` que viene dentro del argumento `event` que cualquier evento nos provee, por tanto, yo puedo decirle al div3: “Oiga, yo solo lo quiero clicar a usted, no a los demás, sí, ya se que usted está dentro de los demás, pero yo solo lo quiero a usted”, de tal forma que al event listener del programation le puedo poner:

```jsx
div3.addEventListener("click", event => {
  event.stopPropagation();
});
```

De esta forma, el evento de div2 y div1 no serán ejecutados

Dato curioso, cuando tu defines un elemento con un ID en HTML, en JavaScript se crea automágicamente una variable con ese id que creaste, por eso es completamente posible que yo pueda usar la variable `div3` sin tener que seleccionar el elemento 👀.

**Bubbling** es la forma en que se propaga desde lo más bajo hasta lo más alt

- El DOM es un arbol que renderiza nodos de forma jerárquica
  - Cuando un evento sucede se propaga a lo largo de ese nodo
- Los eventos suceden desde el elemento más interno hacia afuera
  - Propagándose entre cada padre que tiene el elemento escuchado
- Si deseamos borrar este comportamiento podemos usar el parámetro de evento

```jsx
node.addEventListener("click", event => {
  event.stopPropagation();
  // Acciones ...
});
```

- Se debe tener cuidado con este tipo de operaciones por que puede existir códigos de otras personas o de librerías que necesiten este tipo de eventos
- Por lo general se debería dejar que los eventos fluyan por su ruta

> Cuando se tiene eventos estos pueden flotar desde el más específico hasta el más grande, si se desea quitar este comportamiento se puede usar el método del parámetro del evento `stopPropagation` por lo general no es necesario usar este métodoEvent delegation

[Fuente](https://dmitripavlutin.com/javascript-event-delegation/)

![img](https://dmitripavlutin.com/static/9a8fc772a94452ca819295094c99b1a9/ff59c/javascript-event-propagation-5.png)

_Event Propagation 🦠_

```js
// imprimiremos el evento click para ver toda su informacion
const accion = event => console.log(event);

$0.addEventListener("click", accion);

/* Bucamos la propiedad llamada target que es la que nos dice cual elemento 
se esta ejecutando, target representa un nodo cuando utilizamos createElement
es igual a lo que esta dentro del target 


Dentro del target hay una propiedad que se llama nodeName que contiene
el nombre del nodo


*/

const accion = event => {
  console.log(`Hola desde ${event.currentTarget.nodeName}`);
};

//obtenemos el elemento al cual le agregaremos el evento

const avocado = document.querySelector("id");

//obtenemos el padre del elemento

const card = document.querySelector("id");

//obtenos el body

const body = document.querySelector("body");

//agregamos el mismo evento a cada nodo

avocado.addEventListener("click", accion);

card.addEventListener("click", accion);

body.addEventListener("click", accion);

/* cuando damos click en el elemento hijo 
   vemos que aunque el evento sucedio en el primer elemento este
   se propago hasta el elemento padre que seria hasta el elemento
   mas externo que tambien esta escuchando el mismo evento que en
   este caso es el body

    Hola desde + H2
    Hola desde + DIV
    Hola desde + BODY

   Y si unicamente damos click en un elemento padre del que anteriormente
   le dimos click vemos como el evento propaga desde ahi hasta
   el elemento mas externo en este caso el body

   Hola desde + DIV
   Hola desde + BODY

    Entonces sabremos que desde el elemento que inicia hacia arriba
    el evento se propagara
*/

/*

    La propagacion de eventos se puede detener usando la misma web API

    La forma de deterlo es usando el método stopPropagation() que viene
    dentro del argumento event que cualquier evento nos provee

*/

const accion = event => {
  console.log(`Hola desde ${event.currentTarget.nodeName}`);
};

//obtenemos los elementos y le aplicamos los eventos
const body = document.querySelector("body");

body.addEventListener("click", accion);

const h3 = document.querySelector("h3");

h3.addEventListener("click", evento => {
  //utilizamos el metodo stopPropagation y con esto pararemos la propagacion
  // es como decirle al elemento que solo queremos ejecutarlo a el y a los demas no
  evento.stopPropagation();
  console.log(`Hola desde: + ${evento.currentTarget.nodeName}`);
});
```

## Event delegation

La delegación de eventos es básicamente un contenedor padre que le pasa el evento a todos sus hijos (en realidad no se los está pasando, sino que el contenedor padre sigue estando presente en todos los hijos, es por eso que cuando clicamos a un hijo el evento es disárado).

Entendiendo esto, cuando obtenemos el target podemos saber cuál elemento hijo del padre fue clicado, por tanto, con una simple condicional puede ver si el elemento clicado es el que yo quiero.

**Ojo:** Eso no significa que el evento se quite de los demás elementos hijos, si tu clicas cualquier otro elemento hijo el evento se va a disparar sí o sí, pero lo que sucede es que la condición del tarjet no se cumple, por eso no hace nada.

Y sabiendo esto, puedes hacer cosas chulas como crear funciones que escuchen eventos dinámicamente, una característica de los eventos es que solo se le aplican a los elementos que están desde el inicio, pero si tu agregas otro nodo desde JavaScript **los eventos no se van a escuchar para ese nuevo nodo**. Entonces, una técnica que se suele usar es escuchar al padre (o en ocasiones a todo el document) y cada vez que el evento ocurra buscar a todos sus hijos que coincidan con el selector al que queremos aplicarle el evento, de esta forma no importa si los nodos se añaden posteriormente desde JavaScript, el evento será escuchado pues JavaScript directamente irá a buscar todos los nodos hijos que cumplan con dicho selector, por ejemplo:

**HTML**

```html
<div id="divPadre">
  <div class="div">
    Hola
  </div>
</div>
```

**JavaScript**

```jsx
document.querySelector(".div").addEventListener("click", () => {
  // Hace algo
});
```

En este caso, si al div padre yo le añadiera desde JavaScript otro elemento con la clase div, el evento **NO** funcionaría:

```jsx
const nuevoDiv = document.createElement("div");
nuevoDiv.className = "div";
nuevoDiv.textContent = "Nuevo div";
divPadre.append(nuevoDiv);
```

Sin emabrgo, al usar la delegación de eventos, puedo escuchar al padre y buscar al hijo que me interesa:

```jsx
nuevoDiv.addEventListener("click", event => {
  if (event.target.classList.contains("div")) {
    // Código
  }
});
```

De esta forma, no importa cuantos elementos nuevos agregues al padre desde JavaScript, esto siempre va a funcionar.

Ahora, si quieres hacer algo más pro, puedes crear una función en el cual tu le pases un selector en específico para usar dentro del div, así solo tienes que llamar a esa función y pasarle el selector de tal manera que se quede escuchando por cualquier elemento nuevo que sea agregado, algo así:

```jsx
eventAll("click", parentElement, elementToListen, () => {
  // Has algo
});
```

Una función de ese tipo sería muy útil, porque así puedo mantener escuchando cada elemento, no importa que se agregue después con JavaScript, y sí, yo ya la cree, de hecho hice una mini librería para escuchar eventos partiendo de esta base, pueden indagar el código aquí:

[events.js](https://github.com/RetaxMaster/Funciones-para-JavaScript/blob/master/js/events.js)

Claro, este código está desactualizado porque tiene un pequeño bug y hay ciertos elementos con los que no funciona, pero para eso podemos usar un `MutationObserver` que mire cuando el padre haya sido modificado (se le haya agregado un hijo nuevo) y a ese hijo aplicarle el evento, yo ya lo hice pero se los dejo de tarea si tienen cursiodidad sobre eso 👀.

# 5. Workshop 2: Lazy loading

## Presentación del proyecto

![19-Lazy-loading.png](https://static.platzi.com/media/user_upload/19-Lazy-loading-fb2ae5af-c4f2-401b-8ec9-2f3b3b0a95c9.jpg)

Es importante mencionar que hoy en día el navegador ya tiene implementada una api nativa en HTML de Lazy Loading:

https://developer.mozilla.org/en-US/docs/Web/Performance/Lazy_loading

Sin embargo, aún no es compatible con todos los navegadores:

[https://www.caniuse.com/?search=lazy%20loading](https://www.caniuse.com/?search=lazy loading)

El motivo de usar Lazy Loading es la optimización de la página, es decir, es mejor que de inicio un usuario cargue pocos recursos de la página, de esta forma se acelera la carga de la misma, porque no tiene que cargar 100 imagenes de golpe, sino que de poquito a poquito va cargando las imágenes y se hace todo más ameno 😄

**HTML puro, es decir ya sin JavaScript y esa API de Intersection Observer**

![lazy-loading.jpg](https://static.platzi.com/media/user_upload/lazy-loading-2d900162-8749-480b-bc5c-430506c5538a.jpg)

#### **Lazy Loading vs Eager Loading**

Para que nuestro proyecto cargue más rápido teniendo en cuenta todo tipo de usuarios (Con una débil señal de internet o con un dispositivo de gama baja) podemos utilizar diferentes técnicas de loading:

- **Lazy Loading:** Es cuando retrasamos la carga de algunos elementos para reducir el tiempo de descarga inicial, y los vamos descargando a medida que el usuario avanza en nuestra página.
- **Eager Loading:** Descarga todo a la vez.

Técnicas de lazy loading:

- **Nativo:** En el caso de las imágenes podemos asignar el atributo `loading="lazy"`
- **Intersection Observer:** Utilizando esta [API](https://developer.mozilla.org/es/docs/Web/API/Intersection_Observer_API) podemos detectar cuando uno o varios elementos están cerca o dentro del área visible del viewport.
- **Scroll listener:** Escuchando el evento de scroll, obtener la altura de un o varios elementos y detectar cuando vayan a entrar a la pantalla.

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)platzi-dom/workshop-lazy-loading at main · jonalvarezz/platzi-dom · GitHub](https://github.com/jonalvarezz/platzi-dom/tree/main/workshop-lazy-loading)

## Nuestro propio plugin Lazy Loading

Crear proyecto

```sh
npx create-snowpack-app workshop-2-lazy --template snowpack-template-tailwind
```

Editar `public/index.html`

```html
<div id="images">
  <div class="p-4">
    <img
      class="mx-auto"
      width="320"
      src="https://randomfox.ca/images/2.jpg"
      alt="Fox"
    />
  </div>

  <div class="p-4">
    <img
      class="mx-auto"
      width="320"
      src="https://randomfox.ca/images/9.jpg"
      alt="Fox"
    />
  </div>
</div>
```

## Creando las imagenes con JavaScript

**[API is Available](https://randomfox.ca/floof)**

ya con eso hacen la petición fetch y les entrega el resultado. no he visto el video pero observe que todos los compañeros calcularon un número random para tener ese dato aleatorio en la imagen

codigo:

```jsx
const getRandomInt = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

const addImage = () => {
  const app = document.querySelector("#app");

  const image = document.createElement("img");
  image.width = "320";
  image.src = `https://randomfox.ca/images/${getRandomInt(1, 120)}.jpg`;
  image.alt = "cute fox";
  image.className = "mx-auto rounded";

  const imageContainer = document.createElement("div");
  imageContainer.className = "p-4";
  imageContainer.append(image);

  app.append(imageContainer);
};

const adButton = document.createElement("button");
adButton.textContent = "Agregar Imagen";
adButton.className =
  "text-white px-3 py-2 rounded-lg bg-indigo-600 focus:outline-none";
adButton.addEventListener("click", addImage);

const cleanButton = document.createElement("button");
cleanButton.className =
  "rounded-lg mt-5 px-3 py-2 text-indigo-600 border border-indigo-600 focus:outline-none";
cleanButton.textContent = "Limpiar";

const container = document.createElement("div");
container.className = "flex flex-col items-center";
container.append(adButton, cleanButton);

const app = document.querySelector("#app");
app.append(container);
```

Dato curioso, la función `random()` que creamos aquí es la misma función random que vimos en el curso gratis de programación básica 👀.

De nuevo me gustaría recordar que no es necesario agregar un `mountNode = document.querySelector("images")`, esto porque HTML al ver que existe un id llamado `images` automágicamente crea una variable dentro de JavaScript con ese nombre 😄.

ASYNC/AWAIT

![img](https://i.ibb.co/5Yc1RYL/code.png)

### Problemas tipicos en despliegue o consumos de APIS

Hay un problema cuando consumes una API y la despliegas en un servidor y es que el servidor al menos en github, no deja que tu API o codigo AJAX obtenga o envie información a sitios no seguros, esto ni siquiera lo sabia, y cuando desplegué mi sitio en github pages me tope con este problema.
.

### Como lo solucionas ?

Muy simple debes de asegurarte de que tu JS obtenga todo via HTTPS, la S es muy importante y lo otro es colocar la etiqueta
Meta con su equivalente http-equiv o name tambien es posible, añadir un policy y listo todo quedo !

```html
<!--Esta etiqueta es muy importante para tu API -->
<meta
  http-equiv="content-security-policy"
  content="upgrade-insecure-requests"
/>
```

## Intersection Observer

![22-IntersectionObserver.png](https://static.platzi.com/media/user_upload/22-IntersectionObserver-2b1a47aa-8bed-48ed-ba1c-8e83a8a5feac.jpg)

![intersectionObserver.jpg](https://static.platzi.com/media/user_upload/intersectionObserver-200bfa6b-10f5-40e1-9385-938f482d6e12.jpg)

## Aplicando Lazy loading

Los atributos `data-cualquiercosa` sirven para definir **atributos personalizados** dentro de HTML, es decir, puedes inventarte atributos, yo los he usado desde siempre porque son muy útiles para pasar datos entre HTML y JavaScript, su sintaxis consta en que **SIEMPRE** deben iniciar con `data-` y después de eso puedes poner cualquier cosa: `data-este-es-un-atributo-data-personalizado`, y se pueden usar de esta manera:

```html
<div id="myDiv" data-atributo="hola" data-un-atributo="el-valor-del-atributo">
  Hola 😄
</div>
```

De esta forma podemos tener atributos personalizados en HTML:

[Atributos personalizados en HTML5, más datos con un simple “data-…”](https://www.genbeta.com/desarrollo/atributos-personalizados-en-html5-mas-datos-con-un-simple-data)

La forma de acceder a estos elementos desde JavaScript es mediante la propiedad `dataset`, esta propiedad contiene la lista de todos los atributos personalizados que le pusiste a tu elemento:

```js
const atributo = myDiv.dataset.atributo; // hola
```

Para los atributos que tienen más de un guion, JavaScript es inteligente y los convierte a camel case 🐫:

```js
const unAtributo = myDiv.dataset.unAtributo; // el-valor-del-atributo
```

Dejo el código de esta clase 😄

Ya el Lazy Loading es nativo en HTML:

```html
<img src="imagen.png" loading="lazy" />
```

Pero todavía no está 100% soportado por los navegadores.

Hay varias maneras de obtener la imagen del container sin necesidad de usar querySelector, como estas:

- container.childNodes[0] // childNodes devuelve un nodeList con todos los hijos, tomamos el primero que es la imagen.
- container.children[0] // Devuelve un HTMLCollection, es similar al nodeList, así que también podemos tomar el primer elemento que sería la imagen.
- container.firstChild = Es un atributo que contiene el primer elemento hijo del container, que sería en este caso la imagen.
  **IMPORTANTE**: Este método devuelve el primer elemento del container, por lo que si hay comentarios o texto como primer elemento devolverá esto y no el nodo.
- container.firstElementChild = Es muy similar al fisrtChild, pero este solamente devuelve nodos HTML ignorando cualquier comentario o texto.

[can i use](https://caniuse.com/loading-lazy-attr)

[Adición del Lazy Loading](https://github.com/RetaxMaster/curso-manipulacion-dom/tree/65601b8a2011f60c82e36e97601e365f84238185/workshop-2)

## Comparte el resultado

wrapper que encerrara a la imagen y que se adaptara al tamaño de la misma con un mínimo de 100px, y este wrapper tiene el fondo gris, así cuando se inserta sale con fondo gris:

```js
...

imagen.width = "320";
imagen.dataset.src = `https://randomfox.ca/images/${random()}.jpg`

const imageWrapper = document.createElement("div");
imageWrapper.className = "bg-gray-300";
imageWrapper.style.minHeight = "100px";
imageWrapper.style.display = "inline-block";

imageWrapper.appendChild(imagen);
container.appendChild(imageWrapper);
...
```

Para eliminar las imagenes, le puse id a los botones:

```js
<button class="p-3" id="add">Agregar Imagen</button>
<button class="p-3" id="clean">Limpiar</button>
```

En JavaScript los selecciono y les pongo sus eventos, para eliminarlos simplemente recorro todos los nodos hijos de `mountNode`, los recorro y los elimino:

```js
const cleanImages = () => {
  console.log(mountNode.childNodes);

  [...mountNode.childNodes].forEach(child => {
    child.remove();
  });
};

addButton.addEventListener("click", addImage);
cleanButton.addEventListener("click", cleanImages);
```

Y para imprimir los logs, en el HTML agregue variables y una función super globales para tener acceso desde `index.js` y desde `lazy.js`:

```js
<script>
    let appendedImages = 0;
    let loadedImages = 0;

    const printLog = () => {
    console.log(`⚪ Se han agregado ${appendedImages} imágenes`);
    console.log(`🟣 Se han cargado ${loadedImages} imágenes`);
    console.log("---------------------------------------");
    }
</script>
<script type="module" src="%PUBLIC_URL%/_dist_/index.js"></script>
```

Entonces, en la función `createImageNode` antes de retornar el container, aumento uno a las imagenes que se han agregado y llamo a la función:

```jsx
...
imageWrapper.appendChild(imagen);
container.appendChild(imageWrapper);

appendedImages++;
printLog();

return container;
...
```

Y dentro de `lazy.js` en `loadImage` antes de dejar de observar hago lo mismo, pero aquí incremento las imágenes cargadas:

```js
const loadImage = entry => {
  const container = entry.target;
  const imagen = container.firstChild;
  const imagen = container.querySelector("img");
  const url = imagen.dataset.src;
  imagen.src = url;

  loadedImages++;
  printLog();

  observer.unobserve(container);
};
```

![Captura de pantalla 2021-03-25 010339.png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%202021-03-25%20010339-2c3a01be-7613-48be-8f39-d215aa94e0c1.jpg)

[![img](https://www.google.com/s2/favicons?domain=https://openweathermap.org/api/themes/openweathermap/assets/vendor/owm/img/icons/favicon.ico)Weather API - OpenWeatherMap](https://openweathermap.org/api)

# 6. Workshop 3

## Proyectos propuestos

![2021-05-13_14h49_29.png](https://static.platzi.com/media/user_upload/2021-05-13_14h49_29-142291a6-1042-428d-a6ed-3083bfc176d0.jpg)

[Repo](https://josegarcia2001.github.io/Simple-Weather-App/public/)

El reproductor de video lo hice hace años xD

Para que se les haga fácil les recomendaré lo que a mi me sirvió hace años cuando estaba haciendo la práctica, y es esta documentación de la API de Audio/Video (que ya lo puse en una clase anterior):

[HTML Audio and Video DOM Reference](https://www.w3schools.com/tags/ref_av_dom.asp)

Y por último, después de que termine de hacer esa práctica por mi cuenta hace unos años, hice un tutorial sobre ello:

**ADVERTENCIA:** Estos tutoriales los hice hace años, la calidad no es la mejor, y es posible que el código sea código espagueti ¬¬, además yo lo hice con jQuery, pero por si le sirve a alguien, dejo aquí la [lista de reproducción](https://www.youtube.com/watch?v=S39lMiUcVuM&list=PLD8e-erNIEhp-YeSbiI6oLt9N-Jf3OvPU), te puede servir para saber cómo interactuar con la API de Audio/Video del DOM.

Aquí dejo un preview del reproductor que hice ;-; (de hecho tengo una versión más actualizada de este reproductor hecho con JavaScript puro pero no he hecho el tutorial para ello xD) Si quieren puedo subir el repositorio de la versión actualizada ya hecho directamente con JavaScript y con código más legible jajaja, como sea, aquí está el preview:

Lo logré!, aquí están:

Reproductor:

- [Proyecto](https://wfercanas.github.io/Mediaplayer/)
- [Repo](https://github.com/wfercanas/Mediaplayer)

Weather:

- [Proyecto](https://wfercanas.github.io/Weather/)

- [Repo](https://github.com/wfercanas/Weather)

- [Repo](https://github.com/misaellvz/workshop3)

  ![Preview](https://static.platzi.com/media/user_upload/Screenshot%20from%202021-02-05%2016-34-55-5d056e2c-0a04-4f10-8a3f-0ae7f2376ead.jpg)

  https://github.com/robmvsk/workshop-3-media

  Me costo un poco, pero le puse varias cosas:

  - Personalizar la barra de volumen
  - Botón de play al cargar
  - Barra de avance del video.
  - Full-screen.
  - Adelantar 25 segundos el vídeo.
  - Atrasar 10 segundos el vídeo.
  - Temporizador para ocultar/mostrar los controls

  Así es como luce:

  ![workshop-3-media-player.JPG](https://static.platzi.com/media/user_upload/workshop-3-media-player-17fedaa1-884e-49fe-8a54-5ed6a1e5deef.jpg)

¿Qué opinan? ¿Estrellita a los repos? jeje…

# 7. Librerías relacionadas

## ¿Y jQuery?

## ¿Y JSX?

# 8. Conclusiones

## Conclusiones
