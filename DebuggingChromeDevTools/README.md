<h1>Debugging con Chrome DevTools</h1>

<h3>Diego De Granda</h3>

<h1>Tabla de Contenido</h1>

- [1. Introducción](#1-introducción)
  - [Introducción del curso e historia de Debugging Dev Tools](#introducción-del-curso-e-historia-de-debugging-dev-tools)
  - [Introducción a DevTools](#introducción-a-devtools)
- [2. Elementos y Estilos](#2-elementos-y-estilos)
  - [Editando HTML](#editando-html)
  - [Editando CSS](#editando-css)
  - [Colores en DevTools](#colores-en-devtools)
  - [Manejo de sombras en DevTools](#manejo-de-sombras-en-devtools)
  - [Animaciones en DevTools](#animaciones-en-devtools)
  - [¿Cómo medir el código que no ocupamos?](#cómo-medir-el-código-que-no-ocupamos)
  - [JavaScript y el DOM](#javascript-y-el-dom)
  - [DevTools como editor (IDE)](#devtools-como-editor-ide)
- [3. Mobile Simulation](#3-mobile-simulation)
  - [Simular una ventana móvil](#simular-una-ventana-móvil)
  - [Manejo de sensores](#manejo-de-sensores)
- [4. JavaScript](#4-javascript)
  - [Debugging JS con DevTools](#debugging-js-con-devtools)
  - [Reproduciendo y reparando un bug](#reproduciendo-y-reparando-un-bug)
- [5. Network](#5-network)
  - [Nota: proyecto de la clase siguiente](#nota-proyecto-de-la-clase-siguiente)
  - [¿Cuándo utilizar Network en DevTools?](#cuándo-utilizar-network-en-devtools)
  - [Revisando detalles con Network](#revisando-detalles-con-network)
- [6. Performance](#6-performance)
  - [¿Cuando utilizar la sección de Performance?](#cuando-utilizar-la-sección-de-performance)
- [7. Audits](#7-audits)
  - [Auditoría en mobile](#auditoría-en-mobile)
  - [Auditoría en desktop](#auditoría-en-desktop)
- [8. Cierre](#8-cierre)
  - [Cierre de curso](#cierre-de-curso)
  - [Contenido Bonus: PWA](#contenido-bonus-pwa)

# 1. Introducción

## Introducción del curso e historia de Debugging Dev Tools

El debugging nos ayuda a solucionar problemas que tengas con el software, ese problema debemos encontrar y solucionarlo, este proceso se le llama **debugging.**

> Debugging es el proceso de encontrar y resolver defectos o problemas dentro de un programa (software) que impide el funcionamiento correcto del programa o sistema.

**Debugging** es el proceso de encontrar y resolver defectos o problemas dentro de un programa (software) que impide el funcionamiento correcto del programa o sistema.

"Entre el equipo que encontró el primer error informático informado se encontraba la pionera del lenguaje informático Grace Hopper. A menudo se le da crédito por informar el error, pero eso no es cierto. Sin embargo, ella fue la persona que probablemente hizo famoso el incidente."

## Introducción a DevTools

Las dev tools son herramientas de desarrollo que vienen en todos los navegadores. No todas son iguales por lo que hay tools muy particulares dependiendo del navegador.

Chrome DevTools es un conjunto de herramientas de creación web y depuración integrado en Google Chrome.

Todos los navegadores tienen sus propias **DevTools,** estas son la herramientas de desarrollo donde podemos revisar nuestro código o el de otros.

**F12 (tecla)**, para abrir los DevTools

# 2. Elementos y Estilos

## Editando HTML

Podemos ver la estructura HTML de cualquier página en la pestaña **Elements.** Desde ahí podrémos cambiar el contenido de todas las etiquetas, remover y añadir más, cambiar la posición de los nodos, y más.

- Podemos ver la estructura de cada elemento en pantalla con el inspector ubicado en la esquina superior izquierda o click derecho en éste e inspeccionar elemento.
- Para hacer busquedas por palabra damos click en cualquier nodo y presionamos **control + f**

1. Los tres puntos al lado izquierdo de cada elemento HTML nos ayudan a mover estos a otras posiciones. Por ejemplo: cambiar de posición los elementos de una lista del nav.
2. **Settings -> Shorcouts**: Te permiten ver los atajos de teclado que puedes utilizar en las dev tools dependiendo de tu sistema operativo.

[![img](https://www.google.com/s2/favicons?domain=https://www.gstatic.com/devrel-devsite/prod/v84899ba5ac366dd19b845bb4579ea9262ac5ac73d5db61a8fa440a5f2fc65a26/developers/images/favicon.png)Chrome DevTools  | Tools for Web Developers  | Google Developershttps://developers.google.com/web/tools/chrome-devtools](https://developers.google.com/web/tools/chrome-devtools)

## Editando CSS

Para agregar una declaración CSS a un elemento use la pestaña Estilos cuando desee cambiar o agregar declaraciones CSS a un elemento.

- Haga clic derecho en el Add A Background Color To Me!texto a continuación y seleccione Inspeccionar .
- Haga clic element.stylecerca de la parte superior de la pestaña Estilos .
- Escribe background-color y presiona Enter.
- Escribe honeydewy presiona Enter. En el Árbol DOM , puede ver que se aplicó una declaración de estilo en línea al elemento.

![declaration.png](https://static.platzi.com/media/user_upload/declaration-5c85c4ab-c02a-4c9f-9d5f-46915d261463.jpg)

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)Live Server - Visual Studio Marketplacehttps://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)

## Colores en DevTools

Me parece muy útil el tema de las paletas, no sabía que podia guardar esas paletas y que también en chrome me identificaba los colores que he agregado como variables en mi CSS.

**Modificar colores con el Selector de colores**

- Para abrir el Selector de colores, encuentra una declaración de CSS en el panel Styles

  que defina un color (como color: blue). A la derecha del valor de la declaración se encuentra un cuadrado pequeño de color. El color del cuadrado coincide con el valor de la declaración. Haz clic en este pequeño cuadrado para abrir el Selector de colores.

  ![open-color-picker.jpg](https://static.platzi.com/media/user_upload/open-color-picker-62213788-f947-46a8-8522-f6c70e23c7a3.jpg)

  Respecto a los colores de Material Design , para poder ver la combinación de colores que se pueden aplicar a una pagina , En la sección mencionada de Material Design recordar dar un click sostenido a cada uno para visualizar la linea de colores de esta forma :

  Entrar a la paleta de colores y dirigirse a la sección de Material Design

  ![img](https://i.ibb.co/VDvR7zC/Captura-de-pantalla-de-2020-12-14-17-34-37.png)

  Click sostenido sobre la linea de colores que desee visualizar

  ![img](https://i.ibb.co/2YPJzmH/Captura-de-pantalla-de-2020-12-14-17-35-22.png)

## Manejo de sombras en DevTools

A quienes no les aparezcan los tres puntitos para editar las sombras hagan ésto:

1. Agreguen el estilo **box-shadow**.
2. Seleccionen la opción: **0 0 black**
3. Les aparecerán dos cuadritos superpuestos los cuales sacan un diálogo que dice: **“Open shadow editor”**
4. Hacen click ahí
5. Disfruten.

![Screenshot from 2020-09-07 11-33-41.png](https://static.platzi.com/media/user_upload/Screenshot%20from%202020-09-07%2011-33-41-940d8aee-54f0-43d1-a4f0-7d8bec93f3ed.jpg)

## Animaciones en DevTools

Bibliotecas de animaciones y efectos CSS:

- [Hover](https://ianlunn.github.io/Hover/)
- [CSShake](https://elrumordelaluz.github.io/csshake/)
- [Anijs](https://anijs.github.io/)
- [CSS Wand](https://www.csswand.dev/?ref=producthunt)
- [Animate Components](https://animate-components.surge.sh/)
- [Keyframes](https://keyframes.app/)
- [AniCollection](https://anicollection.github.io/#/)
- [SpinKit Loader](https://tobiasahlin.com/spinkit/)
- [Animejs](https://animejs.com/)

DevTools cuenta con una sección de animaciones para que puedas
interactuar con ella.
![ANIMATIONS.png](https://static.platzi.com/media/user_upload/ANIMATIONS-8ed0ac50-801a-4bc8-9588-e0873474b4dc.jpg)

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)Animate.css](https://daneden.github.io/animate.css/)

## ¿Cómo medir el código que no ocupamos?

¿Cómo podemos verificar cuál es la cantidad de código que no ocupamos?. Accediendo a: **Menú > More tools > Coverage**

Comenzando a grabar: Lo Rojo es código que no usamos. Lo verde es código que si ocupamos.

> En Coverage aparece en rojo las clases que tienen la pseudoclase `hover`, para que pase a verde solo deben hacer hover sobre los elementos y así visualizan mejor el código que si ocupan

Creo que en este caso si trabajas con webpack puedes implementar los critical CSS hay varios plugins para webpack que te permiten hacerlo de forma automática, a continuación te comparo uno:

- [Critters](https://github.com/GoogleChromeLabs/critters)

## JavaScript y el DOM

Optener un elemento del DOM lo seleccionamos desde la DevTool de navegador y despues vamos a consola y copiamos $0
esto nos traera ese elemento

para empezarlos a traer los elementos para agregarle javascript podemos usar estas dos formas:

1. `getElementById()` = para llamar a un id.

2. vamos al elemento, damos click derecho, nos saldra un menu vamos a donde dice (copy) y despues donde dice ( copy JS-PATH )

   querySelector(“Body > header”)

![Captura de pantalla (56).png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%20%2856%29-60297e13-8b70-4090-b5d9-f0518461ff09.jpg)

La consola nos puede ayudar a realizar cambios.

- Si ponemos **$0** nos trae los elementos que seleccionamos. Para acceder al path seleccionamos el elemento con **$0** click derecho - Copy - CopyJSPath y lo pegamos.
- Ya tendremos la dirección para poderla agregar a una variable o realizar interacciones con los elementos del DOM.

## DevTools como editor (IDE)

**SOURCES - FILESYSTEM**

- Ir a la pestaña de sources.
- Agregar la carpeta donde están tus arhivos HTML y CSS desde la
  pestaña de Filesystem.
- Modificar tu código HTML o CSS dentro del tab de sources.

![FileSystem.PNG](https://static.platzi.com/media/user_upload/FileSystem-4d4f78b3-9bfa-4427-9d16-82c621038e54.jpg)

| Devtools - IDE                                               |
| ------------------------------------------------------------ |
| Vamos a la pestaña de orígenes y en sistema de archivos o filesystem seleccionamos agregar una carpeta (+) y buscamos la carpeta donde tenemos nuestro proyecto. Las DevTools nos dará un aviso para otorgar permisos a la carpeta, aceptamos y listo |

DevTools:
[Overrides, cómo activarlo y usarlo](https://platzi.com/tutoriales/1867-devtools/5334-overrides-como-activarlo-y-usarlo-2/)

# 3. Mobile Simulation

## Simular una ventana móvil

 [***Screen Size\***](http://screensiz.es/) y [***Device Metrics\***](https://material.io/resources/devices/)
Es interesante para llegar a una media de accesibilidad de su sitio web:
![Common-screen-sizes-1024x550.jpg](https://static.platzi.com/media/user_upload/Common-screen-sizes-1024x550-f0e120a3-30bd-402f-82fb-e2952c89cdc2.jpg)

El **Device Mode** es útil para saber de manera aproximada cuál será el aspecto y el rendimiento de tu página en un dispositivo móvil.

Device Mode es el nombre de un conjunto variado de funciones en Chrome DevTools que te ayudan a simular dispositivos móviles. Estas funciones incluyen:

- Simulación del viewport de un dispositivo móvil
- Limitación de la red
- Limitación de la CPU
- Simulación de la ubicación geográfica
- Configuración de la orientación
- ![device-toolbar.png](https://static.platzi.com/media/user_upload/device-toolbar-8018d87f-5156-41d0-b987-b7dd18d086f6.jpg)

## Manejo de sensores

IU de anulación de la ubicación geográfica, haz clic en Customize and control DevTools Customize and control DevTools y selecciona **More tools > Sensors.**
![sensors.png](https://static.platzi.com/media/user_upload/sensors-84844c28-0df0-447c-9c26-5ce87b3d3cea.jpg)

posiciones de orientación de un celular necesitamos escuchar el evento [DeviceOrientationEvent](https://developer.mozilla.org/en-US/docs/Web/API/DeviceOrientationEvent).

**Pantalla de Sensores.**
![12-Sensores.PNG](https://static.platzi.com/media/user_upload/12-Sensores-f0144d83-81b3-476c-ad73-f826f621d7dc.jpg)

| Devtools - Simulación de sensores                            |
| ------------------------------------------------------------ |
| En la pestaña de performance (rendimiento), se podrá ajustar configuraciones como la velocidad de la red y la potencia de la cpu… |
| Si vamos a los tres puntos de la esquina superior derecha, abrimos “Más herramientas” y seleccionamos sensores. Podremos simular y obtener datos de algunos sensores que generalmente traen los dispositivos móviles ya sea giroscopio o sensor de geolocalización. |

Sensores, [API de orientación de dispositivo](https://w3c.github.io/screen-orientation/), está bien interesante.

![img](https://miro.medium.com/max/3816/1*3u2IM7wyXIIqGmXklygaHQ.gif)

# 4. JavaScript

## Debugging JS con DevTools

La IU del panel Sources cuenta 3 partes fundamentales:

- Panel File Navigator. Aquí se muestran todos los archivos que solicita la página.
- Panel Code Editor. Después de seleccionar un archivo en el panel File Navigator, el contenido del archivo se muestra aquí.
- Panel JavaScript Debugging. Diversas herramientas para inspeccionar el JavaScript de la página. Si la ventana de DevTools es ancha, este panel aparece a la derecha del panel Code Editor.

![sources-annotated.png](https://static.platzi.com/media/user_upload/sources-annotated-126758ec-103a-4af6-b136-f42826d5915e.jpg)

> [Comenzar a depurar JavaScript en Chrome DevTools](https://developers.google.com/web/tools/chrome-devtools/javascript)

[![img](https://www.google.com/s2/favicons?domain=https://googlechrome.github.io/devtools-samples/debug-js/get-started/devtools-samples/favicon-96x96.png)Demo: Get Started Debugging JavaScript with Chrome DevTools](https://googlechrome.github.io/devtools-samples/debug-js/get-started)

[Comenzar a depurar JavaScript en Chrome DevTools](https://developers.google.com/web/tools/chrome-devtools/javascript)

## Reproduciendo y reparando un bug

**DEBUGGING JS**

- En javascript **typeof** retorna el tipo de dato que tiene una variable.
- El panel **Scope** te muestra las variables locales y globales actualmente definidas, junto con el valor de cada variable.

Otra forma de hace la coerción expplícita de un String a Número es usar la función Number() y pasar la variable dentro de los paréntesis.

| **`Coerción explicita`**                                     |
| ------------------------------------------------------------ |
| Es la forma en que nosotros obligamos a que un valor de un tipo cambie a otro valor de otro tipo, si necesitamos que ese valor sea de un tipo distinto. |

```jsx
4 + "7"; // 47 (string, porque JS hace la concatenación con elímbolo de +)
4 * "7" // 28 (Número, porque no se realiza ninguna concatenación)
2 + true; // 3 (porque, true equivale a un valor de 1) 
2 + false; // 2 (al contrario de true, false equicale a un valor de 0)
```

¿Cómo hacemos la coerción explícita?

```jsx
var a = 20;
var b = a + "";
//typeof b --> sería un string
var c = String(a);
//typeof c --> sería un string
var d = Number(c);
//typeof d --> sería un número
```

> para hacer debuggin podemos ir a la siguiente ventana.

![image-20200503025145758.png](https://static.platzi.com/media/user_upload/image-20200503025145758-627158a6-7bb9-4573-b9af-d21112e1438f.jpg)

> vemos que tenemos una sección de **Event Listener Breakpoints** ahí podemos captura eventos en especifico de nuestra pagina web y una ves que suceda el evento que seleccionamos nos posicione en que parte del código sucede y a partir de ahí pode debuggear.

# 5. Network

## ¿Cuándo utilizar Network en DevTools?

El panel Network es utilizado para asegurarse de que los recursos se descarguen o carguen como se esperaba.

Los casos de uso más comunes para el panel Network son:

- Asegurarse de que los recursos se estén cargando o descargando.
- Inspeccionar las propiedades de un recurso individual, como sus encabezados HTTP, contenido, tamaño, etc.
- ![log.png](https://static.platzi.com/media/user_upload/log-df5a3df0-2d41-498a-b022-48842945685a.jpg)

**Secciones de Network**:

1. **Name**: Nombre del archivo
2. **Status**: Estatus del http. En este caso 200 significa OK (Todo bien)
3. **Type**: Tipo de archivo. Por ejemplo: document (html), stylesheet(css), js, png, etc…
4. **Inialitator**: Cuál archivo solicitó a otro archivo. En este caso, el documento html solicitó a todos los demás archivos.
5. **Size**: Tamaño del archivo. Se muestra el tamaño no optimizado y optimizado por el navegador.
6. **Time**: Tiempo total que tarda en descargarse el archivo.
7. **Waterfall**: Tiempo (sección por sección) que le tomó al archivo descargarse.

[status http](https://developer.mozilla.org/es/docs/Web/HTTP/Status)

[![img](https://www.google.com/s2/favicons?domain=https://raw.githubusercontent.com/ChromeDevTools/devtools-logo/master/48.png)Inspect Network Activity Demo](https://devtools.glitch.me/network/getstarted.html)

[Proyecto DevTools](https://googlechrome.github.io/devtools-samples/debug-js/get-started)

## Revisando detalles con Network

Mostrando más propiedades: **Domain**. Para mostrar el dominio de donde proviene el archivo.

Simulando conexiones de red: **Online** (Normal) **Fast 3G** **Slow 3G** **Offline** **Custom**

[6Filtrando o bloqueando archivos para que no se descarguen en el navegador. **Ctrl+Shift+P > Show Request blocking > + > Archivo.ext**

[![img](https://www.google.com/s2/favicons?domain=https://http.cat//favicon.png)HTTP Cats](https://http.cat/)

# 6. Performance

## ¿Cuando utilizar la sección de Performance?

1° Hice click en el link que aparece en MAIN, casilla morada con una esquina triangular roja, me llevo a la línea de código que era parte del optimizador.

![fra.png](https://static.platzi.com/media/user_upload/fra-089b70a4-f7bc-4651-9cca-16ca5170060e.jpg)

2° Encontré que el código de “if(!app.optimize)” es lo que lee cuando NO esta optimizado y el “else” es el código optimizado.
3° Compare las líneas de código (las cuales ya describieron su función muchos).
4° Puse como comentarios algunas líneas de código del “If” que eran distintas a las del “else” .
5° Finalmente copie y pegue las líneas del “else” que optimizaban el código, en el “if” y funcionó.

![fra2.png](https://static.platzi.com/media/user_upload/fra2-76821b98-a90c-48a2-87ce-4f465a33de55.jpg)

[![img](https://www.google.com/s2/favicons?domain=https://www.gstatic.com/devrel-devsite/prod/v84899ba5ac366dd19b845bb4579ea9262ac5ac73d5db61a8fa440a5f2fc65a26/developers/images/favicon.png)Get Started With Analyzing Runtime Performance](https://developers.google.com/web/tools/chrome-devtools/evaluate-performance)

# 7. Audits

## Auditoría en mobile

**“Lighthouse”** Y es una muy buena herramienta para mejorar el performance de cualquier página, yo la he usado mucho para mejorar cuestiones de SEO, permormance y accesibilidad.

[![img](https://www.google.com/s2/favicons?domain=https://www.gstatic.com/devrel-devsite/prod/v84899ba5ac366dd19b845bb4579ea9262ac5ac73d5db61a8fa440a5f2fc65a26/developers/images/favicon.png)Tu primera Progressive Web App  | Web Fundamentals  | Google Developers](https://developers.google.com/web/fundamentals/codelabs/your-first-pwapp?hl=es)

[![img](https://www.google.com/s2/favicons?domain=https://www.gstatic.com/devrel-devsite/prod/v84899ba5ac366dd19b845bb4579ea9262ac5ac73d5db61a8fa440a5f2fc65a26/developers/images/favicon.png)Accessibility  | Web Fundamentals  | Google Developers](https://developers.google.com/web/fundamentals/accessibility)

## Auditoría en desktop

La accesibilidad, entonces, se refiere a la experiencia de los usuarios que pueden estar fuera del corto alcance de usuario “típico”, que puede acceder o interactuar con cosas de una manera distinta a la que esperas. Específicamente, involucra a los usuarios que experimentan algún tipo de discapacidad, teniendo en cuenta que esta puede ser no física ni temporal.

[![img](https://www.google.com/s2/favicons?domain=https://www.gstatic.com/devrel-devsite/prod/v84899ba5ac366dd19b845bb4579ea9262ac5ac73d5db61a8fa440a5f2fc65a26/developers/images/favicon.png)Tu primera Progressive Web App  | Web Fundamentals  | Google Developers](https://developers.google.com/web/fundamentals/codelabs/your-first-pwapp?hl=es)

# 8. Cierre

## Cierre de curso

_Nunca pares de aprender_

## Contenido Bonus: PWA

Las Progressive Web Apps proporcionan un instalable, experiencia de aplicación como en ordenadores de escritorio y móviles que se crean y entregan directamente a través de la web. Son aplicaciones web que son rápidas y confiables. Y lo más importante, son aplicaciones web que funcionan en cualquier navegador. Si estás creando una aplicación web hoy, ya estás en el camino hacia la creación de una Progressive Web App.

>  PWA, son buena practicas para optimizar los sitios web.

Pre-caché. Estrategia por medio del manifiesto.

Una PWA tiene que correr rápido. Una página debe poder generar interacción con el usuario en 2 segundos. Es una buena práctica.

Creación de un acceso directo al escritorio del dispositivo. Allí podemos customizar toda la vista del navegador para que el usuario no detecte que está en un navegador.

Generar push notifications.

[![img](https://www.google.com/s2/favicons?domain=https://www.gstatic.com/devrel-devsite/prod/v84899ba5ac366dd19b845bb4579ea9262ac5ac73d5db61a8fa440a5f2fc65a26/developers/images/favicon.png)Tu primera Progressive Web App  | Web Fundamentals  | Google Developers](https://developers.google.com/web/fundamentals/codelabs/your-first-pwapp?hl=es)