<h1>Materialize</h1>

<h3>Diego De Granda</h3>

<h1>Tabla de Contenido</h1>

- [1. Introducción a Materialize](#1-introducción-a-materialize)
  - [Frameworks ¿Qué son?](#frameworks-qué-son)
  - [¿Qué es Material Design y Materialize?](#qué-es-material-design-y-materialize)
  - [La lógica detrás del Grid system](#la-lógica-detrás-del-grid-system)
  - [Documentación y características de Materialize](#documentación-y-características-de-materialize)
  - [Componentes de Materialize](#componentes-de-materialize)
- [2. Desarrollo del sitio](#2-desarrollo-del-sitio)
  - [Presentación del proyecto](#presentación-del-proyecto)
  - [Instalación del proyecto](#instalación-del-proyecto)
  - [Elaboración del header](#elaboración-del-header)
  - [Estilos al header](#estilos-al-header)
  - [Construcción del main homepage](#construcción-del-main-homepage)
  - [Finalizando cards en el homepage](#finalizando-cards-en-el-homepage)
  - [Terminando el index.html y estilos en el mainpage](#terminando-el-indexhtml-y-estilos-en-el-mainpage)
  - [Desarrollo de la página de artículo](#desarrollo-de-la-página-de-artículo)
  - [Desarrollo del formulario de registro](#desarrollo-del-formulario-de-registro)
  - [Desarrollo de checkboxes en el formulario](#desarrollo-de-checkboxes-en-el-formulario)
- [3. Despliege del proyecto](#3-despliege-del-proyecto)
  - [Despliegue del proyecto en Firebase](#despliegue-del-proyecto-en-firebase)
  - [Conclusiones](#conclusiones)

# 1. Introducción a Materialize

## Frameworks ¿Qué son?

Materialize es un framework de CSS
**Frameworks:** Son clases y estilos pre-definidos para construir paginas web de forma rápida.

**Un framework CSS** proporciona a los desarrolladores web una estructura básica, que incluye cuadrícula, patrones interactivos de IU, [tipografía web, información sobre herramientas, botones, elementos de formulario, iconos.](https://www.mockplus.com/blog/post/css-framework)

>  Materialize es un Framework de css

**Qué es Materialize**
Es como sets de clases y estilos predefinidos que podemos usar para construir webs de forma más rápida.

## ¿Qué es Material Design y Materialize?

**Material Design:** Es un concepto de diseño que sacó Google con buenas prácticas para poder empezar a generar aplicaciones y páginas web. Ellos entendieron que hay cierta interacción con los usuarios y los dispositivos, las aplicaciones y las páginas.

Se trata de un diseño más limpio, en el que predominan animaciones y transiciones de respuesta, el relleno y los efectos de profundidad tales como la iluminación y las sombras que proporcionan significado sobre lo que se puede tocar y cómo se va a mover.

> **Material Design** es una guía de estilo, una propuesta de diseño detallada sobre cómo deben de ser las interfaces de usuario en las aplicaciones de dispositivos Android. Esas mismas guías de diseño las podemos usar para crear sitios web usables, donde las personas se sientan cómodamente, por tener interfaces que están acostumbradas a utilizar comúnmente. [Material Desing](https://www.youtube.com/watch?v=Q8TXgCzxEnw)

> framework basado en material design que nos da un set de clases reutilizables.
>
> **Materialize**
> Es un Framework que contiene las reglas y elementos de Material Design para implementarlos fácilmente en sitios web y Webapps usando CSS y que sean escalables.

![img](https://www.google.com/s2/favicons?domain=https://www.youtube.com/s/desktop/ebcf1b0f/img/favicon.ico)[Material makes it possible - YouTube](https://youtu.be/m1diVY4Uzjc)

![img](https://www.google.com/s2/favicons?domain=https://material.io/design//static/assets/favicon.ico)[Material Design](https://material.io/design/)

![img](https://www.google.com/s2/favicons?domain=https://www.youtube.com/s/desktop/ebcf1b0f/img/favicon.ico)[Making Material Design - YouTube](https://www.youtube.com/watch?v=rrT6v5sOwJg)

## La lógica detrás del Grid system

Un Grid System trae 12 columnas y es como divides la posición de las etiquetas contenedoras entre cuantas columnas tenemos que utilizar para maquetar un diseño y luego poder reposicionar todos los elementos al momento de que estemos trabajando de forma responsive.

Materialize CSS utiliza el formato de cuadrícula de 12 columnas de Bootstrap para que pueda crear rápidamente diseños de página receptivos. Comenzará aún más rápido con los conocimientos básicos de un proyecto Bootstrap.

Más características de Materialize CSS:

- Menús móviles
- Compatible con Sass

[css-framework](https://www.mockplus.com/blog/post/css-framework)

## Documentación y características de Materialize

¿Por qué es bueno utilizar en algunas ocasiones el CDN? Si alguna persona que ya ha utilizado alguna página que ocupe Materialize, en el CDN ya se han quedado ciertas cosas en caché.

Entonces si de repente viene a mi pagina mi página va a cargar un poco más rápido porque ya no tendría que estar haciendo esas peticiones.

Los helpers nos ayudan para el posicionamiento de los elementos como **Alignment**

> Materialize puedes descargalo con npm tambien a tu proyecto.
>
> Materialize puede trabajar con SASS, tambien directo con el CDN

Existen varias formas de poder trabajar con Materialize

- Trabajar con la versión Standard con CSS y JS
- Podemos trabajar proyectos en conjunto con Sass
- Usar CDN → Es recomendable trabajar con CDN ya que al ser Materialize un Framework OpenSource, hay probabilidades que los usuarios ya tengan en caché muchos elementos que ayuden a cargar mas rapido nuestro sitio web
- Usar NPM
- Templates

[CDN](https://cdnjs.com/)

![img](https://www.google.com/s2/favicons?domain=https://materializecss.com/images/favicon/apple-touch-icon-152x152.png)[Documentation - Materialize](https://materializecss.com/)

## Componentes de Materialize

La documentacion de Materialize es muy limpa y facil de consumir

Es cierto los componentes es lo que mas nos ahorra tiempo empezando por ese navbar que aveces puede ser un dolor de cabeza dependiendo de su complejidad. Hay otros que tambien son muy funcionales y nos pueden aportar demasiado.

Dentro de los componentes que tiene Materialize, tenemos: Badges, Buttons, Cards, Navbar, Footer y más!
Esto nos ayuda a ahorrar mucho tiempo.
Es sumamente importante leer la documentación ya que aquí encontramos todo

![img](https://www.google.com/s2/favicons?domain=https://materializecss.com/images/favicon/apple-touch-icon-152x152.png)[Documentation - Materialize](https://materializecss.com/)

# 2. Desarrollo del sitio

## Presentación del proyecto

Ejercicio de “Repasar el diseño” sobre el papel es muy importante, sobre todo si estas usando BEM pues te ayudará a identificar los Bloques, los elementos y los modificadores.

[Imagenes](https://drive.google.com/drive/folders/1rEzssj7yRxGrK2AURT4xh3pbXv4wGUjK?usp=sharing)

## Instalación del proyecto

#### INSTALACION DEL PROYECTO

Crear archivo `index.html`

```html
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Aviation News</title>
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>
    
    
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    </body>
    </html>
	
```

En la web de Materialize, en la sección Getting Started, en CDN copiamos el código de CSS justo después de nuestro Título y la parte de JS justo antes de que termine el Body

En la sección HTML Setup copiamos el código de las fuentes de Google y lo pegamos en nuestro código justo antes de la parte de CSS que pegamos.
Finalmente con la etiqueta `<link>` llamamos a nuestro archivo CSS creado

![img](https://www.google.com/s2/favicons?domain=https://materializecss.com/getting-started.htmlimages/favicon/apple-touch-icon-152x152.png)[Getting Started - Materialize](https://materializecss.com/getting-started.html)

![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)[Curso-Materialize-/Clase 6 Setup at master · degranda/Curso-Materialize- · GitHub](https://github.com/degranda/Curso-Materialize-/tree/master/Clase%206%20Setup)

## Elaboración del header

 codigo creado en esta clase

```html
<header>
    <nav class="container">
      <a href="#" class="brand-logo valign-wrapper">
        <img class="responsive-img left" src="assets/img/Logo.jpg" alt="Logo del Blog">
      </a>
      <ul>
        <li><a href="#">About Us</a> </li>
        <li><a href="#">Contact Us</a> </li>
        <li><a class="btn" href="#">Sign Up</a> </li>
      </ul>
    </nav>
    <section class="header-main-pic"></section>
  </header>
```

Clase container: espacio que delimita el crecimiento del contenedor a trabajar, de aquí hacia adentro se ocuparan las doce columnas que propone el sistema de grid.

## Estilos al header

Materialize utiliza por defecto el display flex.

- Esta clase alinea de forma vertical el contenido: **valign-wrapper**
- Esta clase hace responsive una imagen: **brand-logo, responsive-img**
- Clase que alinea al lado izquierdo: **left**
- Clase que alinea al lado derecho: **right**
- Clase que delimita un boton: **btn**

**Como dar estilos a la imagen del header:**

```css
.header-main-pic {
  width: 100%;
  height: 400px;
  background-image: url('../assets/img/Banner.jpg');
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
}
```

```html
<footer class=“page-footer”>
<div class=“container”>
<div class=“row”>
<div class=“col l6 s12”>
<h5 class=“white-text”>Footer Content</h5>
<p class=“grey-text text-lighten-4”>You can use rows and columns here to organize your footer content.</p>
</div>
<div class=“col l4 offset-l2 s12”>
<h5 class=“white-text”>Links</h5>
<ul>
<li><a class=“grey-text text-lighten-3” href="#!">Link 1</a></li>
<li><a class=“grey-text text-lighten-3” href="#!">Link 2</a></li>
<li><a class=“grey-text text-lighten-3” href="#!">Link 3</a></li>
<li><a class=“grey-text text-lighten-3” href="#!">Link 4</a></li>
</ul>
</div>
</div>
</div>
<div class=“footer-copyright”>
<div class=“container”>
© 2014 Copyright Text
<a class=“grey-text text-lighten-4 right” href="#!">More Links</a>
</div>
</div>
</footer>
```

![img](https://www.google.com/s2/favicons?domain=https://materializecss.com/footer.htmlimages/favicon/apple-touch-icon-152x152.png)[Footer - Materialize](https://materializecss.com/footer.html)

## Construcción del main homepage

- Materialize utiliza 4 tamaños de pantalla para manejar
  el Grid System: s, m, l y xl
- Las cards son un medio conveniente para mostrar contenido compuesto por diferentes tipos de objetos. También son adecuados para presentar objetos similares cuyo tamaño o acciones admitidas pueden variar considerablemente, como fotos con subtítulos de longitud variable.

**Codigo**

```html
<div class="col s12 m2">
  <p class="z-depth-1">z-depth-1</p>
</div>
<div class="col s12 m2">
  <p class="z-depth-2">z-depth-2</p>
</div>
<div class="col s12 m2">
  <p class="z-depth-3">z-depth-3</p>
</div>
<div class="col s12 m2">
  <p class="z-depth-4">z-depth-4</p>
</div>
<div class="col s12 m2">
  <p class="z-depth-5">z-depth-5</p>
</div>
```

> medidas que determinan materialize como pantallas chica (S), mediana(M) grande (L)
>
> Mobile Devices <= 600px (.s)
> Tablet Devices > 600px (.m)
> Desktop Devices > 992px (.l)
> Large Desktop Devices > 1200px (.xl)
>
> [Creating Responsive Layouts](https://materializecss.com/grid.html)

![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)[Curso-Materialize-/Clase 9 main html at master · degranda/Curso-Materialize- · GitHub](https://github.com/degranda/Curso-Materialize-/tree/master/Clase%209%20main%20html)

## Finalizando cards en el homepage

- Clase que delimita el color del texto
  `grey-text`

- Etiqueta i utilizada para importar iconos de Materialize

  ```html
  <i class=“material-icons right”>more_vert</i>
  ```

  > La clase activator se usa cuando se quiera lanzar la interacción de la tarjeta en la que esta. Por eso se usa en la imagen y en el titulo, para revelar el otro contenido de la tarjeta.

El código de Materialize y encontré que esa clase “activator” en el CSS únicamente le agrega:

```css
.card .card-title.activator {
  cursor: pointer;
}
```

Y su funcionalidad esta en el JavaScript, donde card tiene un evento clic y si tiene existe el .activator realiza la animación y mostrar la información que estaba abajo oculta. Así mas o menos:

```js
$(document).on('click', '.card', function (e)
{
	//Luego de mucho otro código de otras partes
	else if ($(e.target).is($('.card .activator')) || $(e.target).is($('.card .activator i'))) {
        $card.css('overflow', 'hidden');
        $cardReveal.css({ display: 'block' });
        anim({
          targets: $cardReveal[0],
          translateY: '-100%',
          duration: 300,
          easing: 'easeInOutQuad'
        });
      }

}
```

Me dí cuenta que partes de Materialize están programadas con JQuery, curioso.

[Cards](https://materializecss.com/cards.html)

## Terminando el index.html y estilos en el mainpage

Otra opción es utilizar las clase de Materialize prefix que nos permite alinear el icono con el input quedando algo as.

```html
 <div class="input-field col s6">
          <i class="material-icons prefix">account_circle</i>
          <input id="icon_prefix" type="text" class="validate">
          <label for="icon_prefix">First Name</label>
   </div>
```

- Clase para agregar sombras:

  ```css
  z-depth-2
  ```

  

- Para aplicar la parte de cascade de css se deben colocar las clases
  de la siguiente manera y así no hayan conflictos con los estilos.

  ```css
  .input-field.input-container
  ```

  

- La herencia en css se hace de la siguiente forma:

  ```css
  .input-field > #search
  ```

  Si la altura del footer no se vé afectada por el estilo propuesto en la clase, lo que deben cambiar es `height` por `min-height` según la documentación.

  ```css
  footer.page-footer {
    min-height: 80px;
    background-color: gray;
    bottom: 0;
  }
  ```

![img](https://www.google.com/s2/favicons?domain=https://materializecss.com/shadow.htmlimages/favicon/apple-touch-icon-152x152.png)[Shadow - Materialize](https://materializecss.com/shadow.html)

## Desarrollo de la página de artículo

[Hide Content](https://materializecss.com/helpers.html)
`.hide` añadir a nuestro section del input search

Clase `.section` genera un espacio para marcar una seccion diferente para diferenciarla del contenido

[Cards](https://materializecss.com/cards.html)

[Pagina de imagenes random](https://picsum.photos/)

> No conocía lo de Hide, es mejor borrar o comentar esa parte del código, pero no esta demás saber como ocultarlo con esa clase ❤️

> Muestra aquí el hide solo para que conozcamos que existe, ya que ya en un proyecto esto no sería una buena practica al menos qué con alguna función de JS después se vaya a mostrar por algún evento del usuario.

- Clase que oculta contenedores
  **.hide**
- Para obtener imagenes ramdom:
  **https://picsum.photos/200/300**

![img](https://www.google.com/s2/favicons?domain=https://picsum.photos/assets/images/favicon/favicon-32x32.png)[Lorem Picsum](https://picsum.photos/)

## Desarrollo del formulario de registro

- Clase que da estilo a una caja de un formulario
  **card-panel**
- Clase que da estilo a una caja de un input
  **input-field**
- Alinea al centro un texto
  **center-align**

Don’t repeat yourself: Es aconsejable tener dividido el codigo en componentes para no repetir el mismo codigo en las demas paginas. Por este momento, es válido porque solo se esta usando HTML y CSS

- Aplicar hide al section de la imagen principal para que no aparezca en nuestro `registro.html`
- class `container` delimitara la zona del contenido y se ajustara al tamaño del `nav`
- HTML

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Aviation News</title>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>

    <!-- HEADER -->
    <header>
        <nav class="container">
            <a href="#" class="brand-logo valign-wrapper"><!-- valing-wrapper centra verticalemnte el contenido -->
                <img src="assets/img/Logo.jpg" class="responsive-img left z-depth-2" alt="Logo"> <!-- make responsive images -->
            </a>

            <ul class="right valign-wrapper">
                <li><a href="">About us</a></li>
                <li><a href="">Contact us</a></li>
                <li><a class="btn" href="registro.html">Register</a></li>
            </ul>
        </nav>

        <!-- Promotional image -->

        <section class="header-main-pic z-depth-2 hide">

        </section>

        <!-- TITLE -->

        <section class="container">
            <div class="row registro-titulo">
                <div class="col s12">
                    <h1 class="center-align">Register in Aviation News</h1>
                </div>
            </div>
        </section>
    </header>

    <!-- END OF HEADER -->

    <!-- MAIN -->

    <main class="container">

       <div class="row">
           <div class="col s12">
               <div class="card-panel">
                   <form action="">

                       <div class="row section">
                           <div class="input-field col s12 m6">
                               <input type="text" id="first-name" class="activate">
                               <label for="first-name">Name</label>
                            </div>
                           <div class="input-field col s12 m6">
                               <input type="text" id="last-name" class="activate">
                               <label for="last-name">Last Name</label>
                            </div>
                       </div>

                       <div class="row">
                            <div class="input-field col s12 m6">
                                <input type="text" id="email" class="activate">
                                <label for="email">Email</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <input type="password" id="password" class="activate">
                                <label for="password">Password</label>
                            </div>
                       </div>

                   </form>
               </div>
           </div>
       </div>


    </main>

    <!-- END OF MAIN -->

    <footer class="page-footer"></footer>


<!-- Compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
</body>
</html>
```

En la parte del password, deben cambiar el TYPE para que lo que se escriba sea cifrado

```html
<input id="password" type="password" class="validate">
```

Materialize le tiene unos estilos a esa etiqueta, le hace:

Define una separación arriba y abajo a la clase y a la etiqueta la pone como bloque

```css
section {
  display: block;
}

.section {
  padding-top: 1rem;
  padding-bottom: 1rem;
}
```

## Desarrollo de checkboxes en el formulario

- Materialize sugiere la siguiente estructura para los inputs tipo
  checkbox:
  **<label class=“col s12 m4”>
  <input type=“checkbox”>
  <span>Política</span>
  </label>**
- Sirven para dar diseño especial a los botones:
  **waves-effect waves-light**

> Reto: 1) Enlazar todas las páginas, 2) Crear las otras páginas 3) En la pantalla de registro había opción de poner imagen.

Durante la clase se aplicó la clase center-align al button pero este permaneció en la parte izquierda, para lograr centrarlo mi solución fué la siguiente:

```html
<div class="section">
                <div class="row">
                  <div class="col s12">
                    <div class="center-align">
                      <button class="btn waves-effect waves-light">Enviar
                          <i class="material-icons right">send</i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
```

![img](https://www.google.com/s2/favicons?domain=https://materializecss.com/checkboxes.htmlimages/favicon/apple-touch-icon-152x152.png)[Checkboxes - Materialize(]https://materializecss.com/checkboxes.html)

# 3. Despliege del proyecto

## Despliegue del proyecto en Firebase

- Les comparto sobre algunos detalles:
  *Para instalar firebase CLI sin problemas usé los permisos de dueño en la consola y lo instalé en la carpeta home (la que abre cuando abres la consola):
  sudo npm install -g firebase-tools
- Luego corres estos comando en la carpeta del proyecto,
  firebase login
  firebase init
  firebase deploy

A los que les aparezca error después de poner esto.

```bash
firebase init
```

(deben tener el node.js instalado para instalar el CLI)

**Deben poner.**

```bash
firebase login
```

y les abrirá su navegador que tengan por defecto y les pedirá iniciar sesión (tiene q ser con la cuenta de google que utilizaron en la web de firebase)

luego de eso todo funcionara 😄

**detecta tu sistema operativo y descarga última actualización de firebase**

```bash
curl -sL [https://firebase.tools](https://firebase.tools/) | bash 
```

si tienen problemas instalando firebase prueben con esto:

```bash
sudo npm install -g firebase-tools --unsafe-perm
```

![img](https://www.google.com/s2/favicons?domain=https://www.gstatic.com/devrel-devsite/prod/vcd1bbe5dda31d2b800805cc4c730b0229f847f2d108be33386b6e78644e79178/firebase/images/favicon.png)[Firebase Hosting  | Firebase](https://firebase.google.com/docs/hosting?hl=es-419)


## Conclusiones

Excelente! un Framework más aparte de bootstrap. Aunque me hubiese gustado ver sobre el apartado de JS que tiene materialize, su implementación, el control de las opciones que hay en cada uno de los componente y al igual que se hizo en el CSS las modificaciones que se podrían hacer.

[Hosting URL:P ](https://sincere-passage-322106.web.app)