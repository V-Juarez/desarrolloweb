<h1>Single Page Application con JavaScript Vanilla</h1>

<h3>Oscar Barajas</h3>

<h1>Tabla de Contenido</h1>

- [1. Development](#1-development)
  - [¿Qué vamos a aprender?](#qué-vamos-a-aprender)
  - [Introducción a SPA](#introducción-a-spa)
  - [Configurar el entorno de trabajo](#configurar-el-entorno-de-trabajo)
  - [Preparar Webpack](#preparar-webpack)
- [2. Templates](#2-templates)
  - [Crear el Home](#crear-el-home)
  - [Crear template de personajes](#crear-template-de-personajes)
  - [Crear página de error 404](#crear-página-de-error-404)
- [3. Router](#3-router)
  - [Crear rutas del sitio](#crear-rutas-del-sitio)
  - [Conectar las rutas con los templates](#conectar-las-rutas-con-los-templates)
  - [Implementar y probar las conexiones](#implementar-y-probar-las-conexiones)
- [4. Fetch Data](#4-fetch-data)
  - [Obtener personajes con la función de llamado a la API](#obtener-personajes-con-la-función-de-llamado-a-la-api)
  - [Conectar la función con la descripción de personajes](#conectar-la-función-con-la-descripción-de-personajes)
- [5. Deploy](#5-deploy)
  - [Configurar CSS para administrar elementos visuales](#configurar-css-para-administrar-elementos-visuales)
  - [Github Pages](#github-pages)
  - [Crear el script para enviar a producción](#crear-el-script-para-enviar-a-producción)
  - [Repaso, recomendaciones y tips](#repaso-recomendaciones-y-tips)

# 1. Development

## ¿Qué vamos a aprender?

Webpack

JavaScript

## Introducción a SPA

**SPA** Son aplicaciones construidas con JavaScript que nos permiten cargar nuestro contenido una sóla vez, es decir, se envían los archivos HTML, CSS y JS una sóla vez al navegador y ahí es donde va a vivir toda nuestra aplicación, de tal forma que la aplicación no necesita ser recargada, por ello, si se necesita navegar en la aplicación, se navega entre secciones y no páginas.

*Ventajas*

- Son fáciles de debuggear.
- Son fáciles de crear.

*Desventajas*

- No es tan compatible con el SEO.
- No es recomendable aplicarlas para grandes aplicaciones (Aplicación grande => más de 1000 usuarios y más de 50 secciones en la página).

De hecho, hoy en día hay aplicaciones como Netflix, Facebook y Gmail que utilizan SPA, por lo cual ya no es un problema la cantidad de usuarios. Los únicos problemas serian:

- SEO
- Historial de búsqueda
- Ciber ataques

SPA es asociada con “UNA PARA TODOS" Y “SECCIONES”. Un SPA solo cargar un archivo HTML y con una configuración especial (debes ver el curso) el navegador cargara todas las páginas y estas estarán a la escucha para ser renderizadas. ¿En que nos ayuda esto te preguntaras? Pues tiempo de carga, convencionalmente un sitio web está conformado con muchas secciones (about, principal, contáctanos) y cada una debe ser cargada por el navegador por separado tomando un tiempo de carga que hace que el sitio sea tedioso. Una ventaja es que su estructura una vez dominada (vean el curso) es “FACIL DE CREAR” y una de las desventajas principales (no todo es bello en la vida papa) es que al ser cargada un solo archivo de HTML el SEO (se recomienda ver el curso de seo) solo mostrara el del archivo principal dando desventaja contra otros sitios web. Depende mucho el ¿Para qué vas a desarrollar este sitio? y una vez definido puedes tomar la decisión si hacer un SPA o una estructura convencional.

![img](https://www.google.com/s2/favicons?domain=https://rickandmortyapi.com//icons/icon-48x48.png?v=8632baf722d4dad90912539b3bdf7513)[The Rick and Morty API](https://rickandmortyapi.com/ )

## Configurar el entorno de trabajo

Herramientas:

- Terminal
- Editor de codigo
- Navegador

Todo proyecto inicia:

```bash
git init 
```

Inicializamos proyecto node:

```bash
npm init
```

Abrimos visual studio code:

```bash
code . 
```

Definicion de arquitectura de carpetas

```js
/src //archivos
	/routes // las rutas de la aplicacion
	/utils // utilerias
	/styles // estilos
	/pages // paginas que va a tener nuestro proyecto
	index.js //archivo principal
/public // lo que mandamos a produccion
	index.html //template
```

## Preparar Webpack

Configuracion e instalacion

```bash
comando para ahorrar tiempo

npm i @babel/core babel-loader html-webpack-plugin webpack webpack-cli webpack-dev-server --save-dev
```

configuracion `webpack`

```js
// Permitir traer path. Acceder dentro de la carpte no importa el lugar donde se encuentre
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'main.js'
    },
    resolve: {
        extensions: ['.js'],
    },
    module: {
        rules: [
            {
                test: /\.js?$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                }
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin([
            {
                inject: true,
                template: './public/index.html',
                filename: './index.html'
            }
        ])
    ]
}
```

**Dependencias:**

**@babel/core :** incluye toda la libreria de babel.

**babel-loader :** Este paquete permite transpilar archivos JavaScript utilizando Babel y webpack .

**html-webpack-plugin**: Permite copiar los archivos HTML del proyecto.

**webpack-cli:** Permite manejar ciertos comandos

**webpack-dev-server** : Permite crear un entorno de desarrollo local para probar los cambios que se esta realizando.

**–save-dev** : Permite instalar un conjunto de dependencia en la modalidad de desarrollo para el archivo packet jsom

![img](https://www.google.com/s2/favicons?domain=https://babeljs.io//img/favicon.png)[Babel · The compiler for next generation JavaScript](https://babeljs.io/)

# 2. Templates

## Crear el Home

Script correcto.

```js
"scripts": {
    "build":"webpack --mode production",
    "start": "webpack-dev-server --open --mode development"
  },
```

webpack,config.js

```js
const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname,'dist'),
        filename: 'main.js'
    },
    resolve: {
        extensions: ['.js']
    },
    module:{
        rules:[{
            test: /\.js?$/,
            exclude: /node_modules/,
            use:{
                loader: 'babel-loader'
            }
        }]
    },
    plugins: [
        new HtmlWebPackPlugin([
            {
                inject: true,
                template: './public/index.html',
                filename: './index.html',
            }
        ])
    ]

}
```

Instalar webpack actualizado

```bash
npm i webpack@4 webpack-cli@3
```

Para los que tienen el problema que tras correr el comando “npm run build” el arcivo main.js les queda igual que el archivo index.js aca esta el ¿por que?..

1)Tras la actualizacion a webpack 5 debemos instalar las siguientes dependdencias de babel: “npm install @babel/core babel-loader @babel/preset-env”.

2)Luego debemos generar en la raiz del proyecto el archivo “babel.config.json” alli debemos agregar lo siguiente:

```js
{
  "presets": [
    "@babel/preset-env"
  ]
}
```

1. Luego vamos al archivo "webpack.config.js y agregamos la siguiente regla:

```js
      { // Estructura de Babel
        test: /\.m?js$/, //Nos permite identificar los archivos según se encuentran en nuestro entorno.
        exclude: /node_modules/, //Excluimos la carpeta de node modules
        use: {
          loader: 'babel-loader',//Utilizar un loader como configuración establecida.
          }
      }
```

Al llegar a este paso y correr el comando “npm run build” se daran cuenta de que sigue igual el resultado de main.js, y es porque tras la actualizacion, babel solo agrega el resto de codigo cuando usamos sintaxys moderna de JavaScripts.

Como prueba agreguen el siguiente codigo en el archivo index.js:

```js
console.log("Hello webpack!");

const fancyFunc = () => {
  return [1, 2];
};

const [a, b] = fancyFunc();
```

Ahora compilen y veran que el archivo main.js si viene completo, porque estamos empleando una arrow function y es necesario hacer el codigo compatible con el resto de navegadores…

`template Home`

```js
const Home = () => {
  const view = `
    <div class="Characters">
      <article class="Character-item">
        <a href="#/1/">
          <img src="image" alt="name">
          <h2>Name</h2>
        </a>
      </article>
    </div>
  `;
  return view;
}

export default Home;
```



## Crear template de personajes

template/`Hearder.js`

```js
const Header = () => {
  const view = `
    <div class="Header-main">
      <div class="Header-logp">
        <h1>
          <a href="/">
            100tifi.co
          </a>
        </h1>
      </div>
      <div class="Header-nav">
        <a href="#/about/">
        About
        </a>
      </div>
    </div>
  `;
  return view;
}

export default Header;
```

`Character.js`

```js
const Character = () => {
  const view = `
    <div class="Characters-iner">
      <article class="Characters-card">
        <img src="image" alt="name" />
        <h2>Name</h2>
      </article>
      <article class="Characters-card">
        <h3>Episodios:</h3>
        <h3>Status:</h3>
        <h3>Species:</h3>
        <h3>Gender:</h3>
        <h3>Origin:</h3>
        <h3>Last Location:</h3>
      </article>
    </div>
  `;
  return view;
}

export default Character;
```



## Crear página de error 404



![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/static/img/favicon144.e7e21ca263ca.png)[HTTP response status codes - HTTP | MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status)

# 3. Router

## Crear rutas del sitio

La palabra reservada **async** sirve para indicarle a nuestro JavaScript que vamos a **definir una función asíncrona**.
Cuando se llama una función **async**, esta devuelve un elemento **Promise**. Cuando la función **async** devuelve un valor. **Promise** se resolverá con el valor devuelto por la función **async**

La expresión **await** pausa la ejecución de la función **async** y espera la resolución de la **Promise** pasada. Una vez esta es resuelta. Reanuda la ejecución de la función **async** y devuelve el valor resuelto.

*La finalidad de las funciones **async/await** es simplificar el comportamiento del uso síncrono de **promesas** y realizar algún comportamiento específico en un grupo de **Promises**. Del mismo modo que las **Promises** son semejantes a las devoluciones de llamadas estructuradas, **async/await** se asemejan a una combinación de generadores y promesas.*

`rotes/index.js`

```js
import Header from '../templates/Header';
import Home from '../pages/Home';
import Character from '../pages/Character';
import Error404 from '../pages/Error404';

// ROUTES TO BE RENDER
const  routes = {
    '/': Home,
    '/:id': Character,
    '/contact': 'Contact',
};

//ROUTER
const router = async () => {
    // stablishing templates to dom
    const header = null || document.getElementById('Header');
    const content = null || document.getElementById('content');

    header.innerHTML = await Header();
};

export default router;
```

`root/index.js`

```js
import router from './routes'

window.addEventListener('load', router);
```

Si llegas a tener un error referido a **regeneratorRuntime** es debido al asincronismo. Estuve toda la tarde de viendo cómo solucionarlo y resultó ser bastante sencillo. Les sugiero que lean y comprendan bien lo que pasa para saber el por qué del proceso

### ¿Por qué el problema?

Babel utiliza multiples modulos pequeños para hacer funcionar ciertas de sus caracteristicas. Una cosa a tener en cuenta es que **transpilar el codigo no lo hace 100% compatible** con navegadores antiguos, **a veces es necesario acompañarlo de polyfills** (fragmentos de código que emulan una funcion del lenguaje en caso de no tenerla disponible)

Una de las cosas que no soporta Babel de por sí son las funciones asíncronas, por lo que tenemos que darles soporte a partir de una librería. El profe en la clase de preparacion de webpack instala una, pero haciendo otro proyecto no me funcionó.

Para darle soporte general a metodos del lenguaje anteriormente Babel utilizaba un modulo propio llamado @babel/polyfill, que era una combinacion de 2 modulos independientes llamados core-js y regenerator-runtime. Este fue deprecado en la version 7. Por lo que ahora nos conviene directamente importar ambos módulos

### Instalación de las librerías

Primero instalan las siguientes librerías en npm o yarn:

```sh
npm install core-js
npm install regenerator-runtime
```

luego en el archivo principal de su aplicacion (index.js) lo importan

```js
// CommonJS
require("regenerator-runtime/runtime");
 
// ECMAScript 2015
import "regenerator-runtime/runtime.js";
```

### Péro ¿Por qué estos archivos?

**Core-JS** es una librería super util que reune un monton de polyfills que podés importar de forma general o específica. COmo buena practica, para evitar que tu programa sea pesado, se recomienda importar solamente los polyfills de los features que vayas a utilizar (ej, async, promesas, clases) Pero comó descubrí todo esto ayer lo reduzco a importarlo de forma general como hice arriba jeje 😛

**Regenerator-Runtime** es una librería que principalmente le da soporte a las funciones asincronas en entornos donde no son soportadas. Me imagino que tendrá otras funciones, pero principalmente se la usa por ese motivo. Realmente para que no nos de error solo es necesario ésta librería, pero acordemonos de que nuestra intención es darle soporte a todos los navegadores posibles

**Sugiero entender bien el uso de ambas librerías porque son muy importantes y les será de mucha ayuda cuando sean avanzados**

#### Enlaces para entrar en contexto (en ingles)

[regenerator-runtime - npm](https://www.npmjs.com/package/regenerator-runtime)

[core-js - npm](https://www.npmjs.com/package/core-js)

[Babel polyfill is dead. Long live core-js!](https://alexbogovich.com/blog/core-js/)

[What is regenerator-runtime npm package used for?
](https://stackoverflow.com/questions/65378542/what-is-regenerator-runtime-npm-package-used-for)

*Esta es toda la explicación, es problable que no sea 100% rigurosa, por lo que si tienen algo que aclarar o corregir, su feedback es bienvenido ^-^*

[MDN](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Sentencias/funcion_asincrona) 😄

## Conectar las rutas con los templates

Crear rutas para el usuario. `getHash.js`

```js
const getHash = () =>
  location.hash.slice(1).toLocaleLowerCase().split('/')[1] || '/';
export default getHash;
```

Donde

- **location.hash** trae el fragmento de la url a partir de donde encuentre un #. En este caso traería #/1/
- **.slice(1)** corta la url y muestra a partir del primer elemento. En este Caso el resultado es /1/
- **.toLocaleLowerCase()** convierte a minúscula la cadena
- **.split(’/’)** separa la cadena en un array, y elimina el / quedando un espacio vacio Ej [’’, ‘1’ ,’’]
- **[1]** trae el primer elemento del split anterior que en este caso es 1 (representa la id 1)

`utils/resolveRoutes.js`

```js
const resolveRoutes = (route) => {
  if (route.length <= 3) {
    let validRoute = route === '/' ? route : '/:id';
    return validRoute;
  }
  return `/${route}`;
};


export default resolveRoutes;
```



## Implementar y probar las conexiones

`routes/inde.js`

```js
import Header from '../templates/Header';
import Home from '../pages/Home';
import Character from '../pages/Character';
import Error404 from '../pages/Error404';
import getHash from '../utils/getHash';
import resolveRoutes from '../utils/resolveRoutes';


const routes = {
  '/': Home,
  '/:id': Character,
  '/contact': 'Contact',
};

const router = async ()  => {
  const header = null || document.getElementById('header');
  const content = null || document.getElementById('content');

  header.innerHTML = await Header();
  let hash = getHash();
  let route = await resolveRoutes(hash);
  let render = routes[route] ? routes[route] : Error404;
  content.innerHTML = await render();
};

export default router;
```

# 4. Fetch Data

## Obtener personajes con la función de llamado a la API

`pages/Home`

```js
import getData from "../utils/getData";

const Home = async () => {
  const characters = await getData();
  const view = `
    <div class="Characters">
      ${characters.results.map(character => `
        <article class="Character-item">
          <a href="#/${character.id}/">
            <img src="${character.image}" alt="${character.name}">
            <h2>${character.name}</h2>
          </a>
        </article>
      `).join('')}
    </div>
  `;
  return view;
};

export default Home;
```

`./utils/getData/js`

```js
const API = 'https://rickandmortyapi.com/api/character/';

const getData = async (id) => {
  const apiURL = id ? `${API}${id}` : API;
  try {
    const response = await fetch(apiURL);
    const data = await response.json();
    return data;
  } catch (error) {
    console.log('Fetch Error', error);
  }
};


export default getData;
```

<a href="https://rickandmortyapi.com/"><img src="https://i.ibb.co/3sf7vp2/b6740400-92d4-11ea-8a13-d5f6e0558e9b.png" alt="b6740400-92d4-11ea-8a13-d5f6e0558e9b" border="0" style="zoom:25%;" ></a>

## Conectar la función con la descripción de personajes

HTML 

```js
import getHash from "../utils/getHash";
import getData from "../utils/getData";


const Character = async () => {

  const id = getHash();
  const character = await getData(id);

  const { image, name,  episode, status, species, gender, origin, location} = character;

  const view = `
    <div class="Characters-inner">
      <article class="Characters-card">
        <img src="${image}" alt="${name}">
        <h2>${name}</h2>
      </article>
      <article class="Character-card">
        <h3>Episodes: ${episode.length}</h3>
        <h3>Status: ${status}</h3>
        <h3>Species: ${species}</h3>
        <h3>Gender: ${gender}</h3>
        <h3>Origin: ${origin.name}</h3>
        <h3>Last Location: ${location.name}</h3>
      </article
    </div>
  `;
  return view;
};
```

# 5. Deploy

## Configurar CSS para administrar elementos visuales

```sh
npm install copy-webpack-plugin --save-dev
```

`webpack.config.js` configuración de `CopyWebpackPlugin` 

```js
new CopyWebpackPlugin({
      patterns: [{ from: './src/styles/styles.css',
      to: '' }],
})
```

![img](https://www.google.com/s2/favicons?domain=https://gist.github.com/fluidicon.png)[styles.css · GitHub](https://gist.github.com/gndx/cf251e88979581d6228028710bbff87c)

## Github Pages

travis.yml

```yaml
language: node_js #establecer lenguaje
cache:            
  directories:  
    - ~/.npm    #ubicacion de nuestro directorio
node_js:      #version de node
  -"12"
git:
  depth:3  #va leer hasta el tercer nivel
script:
  - yarn build   #comando a ejecutar nuestro proyecto
deploy:
  provider: pages   #donde va estar alojado
  edge:true       #control de versiones
  skip-cleanup:true   #estructura de compilaciones
  keep-history:true  #historial
  github-token: $GITHUB_TOKEN #nuestro token generado
  local-dir: dist/  #directorio sobre el cual vamos a trabajar
  target-branch: gh-pages 
  commit_message:"Deploy Release" #nombre al commint
  on:
    branch: master #sobre quien va actuar
```

![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)[Travis CI User Documentation](https://docs.travis-ci.com/)

![img](https://www.google.com/s2/favicons?domain=https://yarnpkg.com//icons/icon-48x48.png?v=f5700f2542f6d562c3f654ed9d45683f)[Home | Yarn - Package Manager](https://yarnpkg.com/)

![img](https://www.google.com/s2/favicons?domain=//static.platzi.com/media/favicons/platzi_favicon.png)[10 Plugins esenciales de VSCode para Frontends](https://platzi.com/blog/10-plugins-esenciales-para-vscode/)

## Crear el script para enviar a producción

```js
language: node_js
cache:
  directories:
    - ~/.npm
node_js:
  - '12'
git:
  depth: 3
script:
  - yarn build
deploy: 
  provider: pages
  edge: true
  skip-cleanup: true
  keep-history: true
  github-token: $GITHUB_TOKEN
  local-dir: dist/
  target-branch: gh-pages
  commit_message: "Deploy Release"
  on:
    branch: master
```



## Repaso, recomendaciones y tips

\#NuncaParesDeAprender

- React.js
- Angular.js
- Vue.js
