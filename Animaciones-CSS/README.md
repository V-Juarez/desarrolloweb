<h1>Animaciones con CSS</h1>

<h1>Estefany Aguilar</h1>

<h1>Tabla de Contenido</h1>

- [1. Requisitos para tomar el curso](#1-requisitos-para-tomar-el-curso)
  - [Sabes más de animaciones de lo que crees](#sabes-más-de-animaciones-de-lo-que-crees)
- [2. Animationland game](#2-animationland-game)
  - [Creando contadores con CSS en Animationland](#creando-contadores-con-css-en-animationland)
  - [Maquetando a nuestros conejitos en Animationland: contexto de apilamiento](#maquetando-a-nuestros-conejitos-en-animationland-contexto-de-apilamiento)
  - [Dibujando a nuestros conejitos con CSS en Animationland](#dibujando-a-nuestros-conejitos-con-css-en-animationland)
- [3. Animation y keyframes en CSS](#3-animation-y-keyframes-en-css)
  - [Animation name y keyframe](#animation-name-y-keyframe)
  - [Animation duration](#animation-duration)
  - [Animation timing function, iteration count y delay](#animation-timing-function-iteration-count-y-delay)
  - [Animation direction, fill mode y play state](#animation-direction-fill-mode-y-play-state)
- [4. Rendimiento de animaciones con CSS](#4-rendimiento-de-animaciones-con-css)
  - [CSS Triggers: layout, paint y composite](#css-triggers-layout-paint-y-composite)
  - [Debugging de animaciones con DevTools](#debugging-de-animaciones-con-devtools)
  - [Buenas prácticas para optimizar animaciones web](#buenas-prácticas-para-optimizar-animaciones-web)
- [5. Libros recomendados y próximos pasos](#5-libros-recomendados-y-próximos-pasos)
  - [Continúa en el Curso Práctico de Animaciones](#continúa-en-el-curso-práctico-de-animaciones)

# 1. Requisitos para tomar el curso

## Sabes más de animaciones de lo que crees

Creo que una de las principales diferencias a recalcar entre transiciones y animaciones, es que en transiciones simplemente vas de un punto A a un punto B, no menos, no más. En cambio, en una animación puedes definir tantos puntos com desees (también llamado KeyFrames), es decir, puedes ir de un punto A a un punto D pasando por un punto B y otro punto C.

<img src="https://i.ibb.co/DwSJ4Xh/animations.gif" alt="animations" border="0">

Adobe Animate o After Effects, pues el concepto de KeyFrames es el mismo 😄

<img src="https://i.ibb.co/3NsNZFM/12-principles-of-animation.gif" alt="12-principles-of-animation" border="0">

### ¿Cuáles son los 12 principios de animación de Disney?

En este comentario te voy a mencionar los conceptos de cada uno de los 12 principios de animación. Sin embargo, hice un tutorial para que puedas ver un ejemplo de cada uno de los principios y además el código en html y css [aqui](https://platzi.com/tutoriales/2337-animaciones-css/11217-12-principios-de-animacion-para-la-web/).

Antes de empezar a describir los principios de animación de Disney, es importante mencionar que no todos aplican al desarrollo web, ya que lo que Disney trato de hacer fue representar elementos físicos al mundo digital. Sin embargo, en el desarrollo web, nosotros no tenemos que hacer eso, ya que normalmente todo es visto a través de cajas contenedoras.

#### Exprimir y estirar

Como su nombre lo indica, esta animación trata de representar a un elemento físico cuando este se estira y exprime. Esto lo podemos ver en animaciones de Disney muy frecuentemente cuando un personaje choca contra la pared, por ejemplo.


#### Anticipación

En la vida real, los movimientos no pasan de manera repentina. Antes de que un movimiento suceda, siempre hay una serie de movimientos que nos ayudan a entender que algo va a pasar. Por ejemplo, antes de dar un salto, lo primero que hacemos es tomar impulso y podemos inclinarnos. En el caso del desarrollo web, al momento de dar un hover a un elemento, esto puede hacerse un poco pequeño para posteriormente hacerse mas grande.


#### Puesta en escena

En algunas ocasiones, hay un personaje al que queremos que sea el protagonista en alguna escena en particular. Para que esto suceda, una buena forma es escondiendo o quitando del foco de atención a cualquier otro personaje alrededor. En el desarrollo web, podemos hacer esto con una sombra negra en los elementos que no queremos que tengan una protagonización, o incluso darle movimiento al elemento que queremos resaltar. Por ejemplo, a un botón de “Save” le podemos dar movimiento frecuentemente para que el usuario sepa que necesita guardar algún archivo.


#### Acción directa y pose a pose

El objetivo es hacer que la animación se vea tan suave como sea posible. Como se menciono anteriormente, hacer que los movimientos en la web luzcan tan similares como en la vida real. Para hacer esto hay dos formas de hacerlo.

- Manualmente (Acción directa)
- Pose a pose (Nos ayuda el navegador)

En la acción directa nosotros tenemos que determinar cada frame. En cambio, en el pose a pose, la transición entre los keyframes es manejada por el navegador.


#### Accion de seguimiento y superposicion

En la vida real **NO** todo se mueve a la misma velocidad y dirección. Por ejemplo, cuando vamos conduciendo muy rápido y frenamos de manera repentina, el auto todavía se sigue moviendo hacia adelante unos segundos para después regresar a la posición en la que debería detenerse. Además de esto, los pasajeros dentro del auto podrían salir volando si no llevan el cinturón de seguridad.


#### Acelerar y desacelerar

En la vida real rara vez las cosas van de 0 a 100 km/h en menos de un segundo. Normalmente, a las cosas les lleva unos segundos acelerar y desacelerar. En el mundo web, esto se logra con las transiciones, donde `ease-in` significa acelerar y `ease-out` es lo contrario, desacelerar.


#### Curvas

En la vida real las cosas no se mueven de manera lineal. Las lineas rectas no es algo que a la madre naturaleza le gusta hacer. En el mundo web, nosotros podemos hacer lucir a los elementos de una manera mas “natural” con el movimiento de ease-in & ease-out. Sin embargo, cuando queremos que un elemento simule una curva, tendremos que usar mas de una animación o movimientos. Por ejemplo, cuando una pelota esta cayendo, podemos simular una curva con 2 movimientos:

- El primero seria la pelota moviéndose de arriba a abajo
- El segundo movimiento seria la pelota moviéndose de izquierda a derecha.

#### Accion desencadenada

En el mundo real, cuando una acción sucede, hay otras acciones que nos ayudan a entender que la primera acción realmente paso. Por ejemplo, cuando un balón rebota en el césped, puede levantar pasto, o algún animales pueden salir asustados por el ruido del balón, etc.
En el caso del desarrollo web, nosotros podemos hacer que una acción desencadene a otra acción. Esto lo podemos hacer con timing donde ambas acciones están encadenadas y cuando no suceda la primera, no puede suceder la segunda.

#### Timing

El timing es simplemente cuanto tiempo le tomara a un elemento completar un movimiento. El timing nos ayuda a que un cierto elemento parezca mas pesado que otro cuando le agregamos mas timing. Podemos ajustar este tiempo que tarda un elemento en moverse con las propiedades de `animation-duration` o `transition-duration`.


#### Exageracion

La exageración es utilizada en Disney muy comúnmente para enfatizar una acción, tal como un golpe, una caída, etc. En el mundo web, nosotros podemos exagerar una acción cuando damos clic a un elemento o cuando una acción es completada, como la carga de un archivo. Con esto enfatizamos que la acción ya se realizo. Además de esto, la exageración puede ir acompañada de colores distintos en el elemento que esta siendo enfatizado.


#### Renderizacion en 3D

Cuando estamos trabajando con elemento 3D, tenemos que tener cuidado en que estos elementos cumplan con las reglas de perspectiva que rigen al mundo físico. De otra forma, estas formas en 3D lucen erróneas. La buena noticia es que la mayoría de los navegadores ya cumplen y pueden manejar renderizaciones en 3D.


#### Atraccion

Cuando usamos animaciones o transiciones, tenemos que estar seguros de que estas lucen amigables para el usuario. De otra forma pueden estresar al usuario si las animaciones van muy rápido o aburrirlo si van muy lentas. La idea es encontrar el punto medio.

![img](https://www.google.com/s2/favicons?domain=https://cssanimation.rocks/principles//images/favicons/favicon-192x192.png)[Animation Principles for the Web - CSS Animation](https://cssanimation.rocks/principles/)

# 2. Animationland game

## Creando contadores con CSS en Animationland

**Counter CSS**

`counter-reset`: Crea o reinicia un contador.
`counter-increment`: Incrementa un valor del contador.
`content`: Inserta el contenido generado (debe usarse con un pseudoelemento).
`counter()`: Función que agrega el valor de un contador a un elemento.

Abreviatura Emmet

```css
ul>(li>input:checkbox>div.shield)*10
```

Codigo de la clase

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        body{
            counter-reset:game; /* crea o reinicia un contador */
        }

        input:checked {
            counter-increment: game;/* incrementa un valor de contador */
        }

        .total-count::after{
            content: counter(game);/* inserta el contenido generado (usar con pseudoelemento) */
        } /* counter: funcion que agrega el valor de un contador a un elemento */
    </style>
</head>
<body>
    <ul>
        <li>
            <input type="checkbox" name="" id="">
                <div class="shield"></div>
            </input>
        </li>
        <li>
            <input type="checkbox" name="" id="">
                <div class="shield"></div>
            </input>
        </li>
        <li>
            <input type="checkbox" name="" id="">
                <div class="shield"></div>
            </input>
        </li>
        <li>
            <input type="checkbox" name="" id="">
                <div class="shield"></div>
            </input>
        </li>
        <li>
            <input type="checkbox" name="" id="">
                <div class="shield"></div>
            </input>
        </li>
        <li>
            <input type="checkbox" name="" id="">
                <div class="shield"></div>
            </input>
        </li>
        <li>
            <input type="checkbox" name="" id="">
                <div class="shield"></div>
            </input>
        </li>
        <li>
            <input type="checkbox" name="" id="">
                <div class="shield"></div>
            </input>
        </li>
        <li>
            <input type="checkbox" name="" id="">
                <div class="shield"></div>
            </input>
        </li>
        <li>
            <input type="checkbox" name="" id="">
                <div class="shield"></div>
            </input>
        </li>
    </ul>
    
    <h3 class="total-count">Score: </h3>
    
</body>
</html>
```

![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/CSS/animation/favicon-48x48.97046865.png)[animation - CSS: Cascading Style Sheets | MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/animation)

## Maquetando a nuestros conejitos en Animationland: contexto de apilamiento

cómo funciona el contexto de apilamiento en Animationland 👀👇

<img src="https://i.ibb.co/4RpkGYJ/conejo.gif" alt="conejo" border="0">

Les dejo un descubrimiento que hice: **para este caso, colocar los z-index no es necesario**

Esto se debe al propio contexto de apilamiento de CSS, por defecto, el orden en el que coloquemos los elementos hijos en el HTML hará que se organicen de esta manera, así que en el caso que nos corresponde

```html
<div class="phone">
        <div class="layer-1"></div>
        <div class="layer-2"></div>
        <div class="layer-3"></div>
        <div class="layer-4"></div>
        <div class="layer-5"></div>
        <div class="layer-6"></div>
        <div class="layer-7"></div>
        <div class="layer-8"></div>
        <div class="layer-9"></div>
        <div class="layer-10"></div>
    </div>
```

Al colocarlos en este orden por defecto layer-2 quedará por encima de layer-1, layer-3 por encima de layer-2 y de layer-1 y así sucesivamente. Lo que en este caso que estamos trabajando haría que el z-index por defecto coincida con el z-index que estabamos agregando manualmente, lo cual lo hace redundante.

![apilamiento.png](https://static.platzi.com/media/user_upload/apilamiento-c33736e9-9ff5-4aec-8883-3e601206b549.jpg)

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contexto de apilamiento</title>
</head>
<style>
    body {
        margin:0;
        height:100vh;
        width: 100%;
        display: grid;
        place-items: center;
    }

    .phone{
        position: relative;
        border: 8px solid black;
        border-radius: 20px;
        width: 300px;
        height: 600px;
        background: #CCEFFF;
        box-shadow: 0 19px 38px rgba(0,0,0,0.3), 0 15px 12px rgba(0,0,0,0.22);
    }

    .layer-1{
        position:absolute;
        z-index:1;
        height: 450px;
        width: 80px;
        background: white;
        bottom: 0;
        right: 60px;
    }

    .layer-2{
        position:absolute;
        z-index:2;
        height: 450px;
        width: 80px;
        background: white;
        bottom: 0;
        left: 60px;
    }

    .layer-3{
        position:absolute;
        z-index:3;
        left: 0;
        right: 0;
        margin: 0 auto;
        width: 220px;
        height: 400px;
        background: wheat;
        bottom: 0;

    }

    .layer-4{
        position:absolute;
        z-index:4;
        height: 350px;
        width: 80px;
        background: white;
        bottom: 0;
        left: 50px;
    }

    .layer-5{
        position:absolute;
        z-index:5;
        margin: 0 auto;
        width: 220px;
        height: 300px;
        background: gray;
        bottom: 0;
        left: 0;
        
    }

    .layer-6{
        position:absolute;
        z-index:6;
        height: 250px;
        width: 80px;
        background: white;
        bottom: 0;
        right: 20px;
    }

    .layer-7{
        position:absolute;
        z-index:7;
        margin: 0 auto;
        width: 220px;
        height: 200px;
        background: palegoldenrod;
        bottom: 0;
        right: 0;
    }

    .layer-8{
        position:absolute;
        z-index:8;
        width: 120px;
        height: 100px;
        background: palevioletred;
        bottom: 0;
        left: 0;
    }

    .layer-9{
        position:absolute;
        z-index:9;
        margin: 0 auto;
        width: 180px;
        height: 120px;
        background: lawngreen;
        bottom: 0;
        right: 0;
        left: 0;
    }

    .layer-10{
        position:absolute;
        z-index:10;
        width: 100px;
        height: 100px;
        background: olive;
        bottom: 0;
        right: 0;
    }

    
</style>
<body>
    <div class="phone">
        <div class="layer-1"></div>
        <div class="layer-2"></div>
        <div class="layer-3"></div>
        <div class="layer-4"></div>
        <div class="layer-5"></div>
        <div class="layer-6"></div>
        <div class="layer-7"></div>
        <div class="layer-8"></div>
        <div class="layer-9"></div>
        <div class="layer-10"></div>
    </div>
</body>
</html>
```

![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/CSS/@keyframes/favicon-48x48.97046865.png)[@keyframes - CSS: Cascading Style Sheets | MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/@keyframes)

## Dibujando a nuestros conejitos con CSS en Animationland

Usando los pseudo elementos ::after y ::before

![Conejos.JPG](https://static.platzi.com/media/user_upload/Conejos-55cfa40f-eb16-498c-a29c-c1b936d3553a.jpg)

HTML

```html
<div class="phone">
        <div class="layer_1">
            <div class="ear_left"></div>
            <div class="ear_right"></div>
            <div class="head"></div>
        </div>
        <div class="layer_2">
            <div class="ear_left"></div>
            <div class="ear_right"></div>
            <div class="head"></div>
        </div>
        <div class="layer_3"></div>
        <div class="layer_4">
            <div class="ear_left"></div>
            <div class="ear_right"></div>
            <div class="head"></div>
        </div>
        <div class="layer_5"></div>
        <div class="layer_6">
            <div class="ear_left"></div>
            <div class="ear_right"></div>
            <div class="head"></div>
        </div>
        <div class="layer_7"></div>
        <div class="layer_8">
            <div class="grass"></div>
        </div>
        <div class="layer_9"></div>
        <div class="layer_10"></div>
    </div>
```

CSS

```css
        .phone {
            position: relative;
            width: 375px;
            height: 700px;
            border: solid black 8px;
            border-radius: 50px;
            box-shadow: 7px 8px 20px 0 #868686;
            background-color: #ccefff;
            overflow: hidden;
        }

        .layer_1 {
            position: absolute;
            z-index: 1;
            width: 80px;
            height: 520px;
            bottom: 0;
            left: 70px;
        }

        .layer_2 {
            position: absolute;
            z-index: 2;
            width: 80px;
            height: 520px;
            bottom: 0;
            right: 70px;
        }
        .layer_3 {
            position: absolute;
            z-index: 3;
            width: 280px;
            height: 460px;
            background: #bb6d50;
            bottom: 0;
            left: 0;
            right: 0;
            margin: 0 auto;
            border-radius: 20px;
            box-shadow: 7px 8px 20px 0 #868686;
            overflow: hidden;
        }

        .layer_4 {
            position: absolute;
            z-index: 4;
            width: 80px;
            height: 380px;
            bottom: 0;
            left: 50px;
        }
        .layer_5 {
            position: absolute;
            z-index: 5;
            width: 280px;
            height: 320px;
            background: #bb6d50;
            bottom: 0;
            border-radius: 20px;
            box-shadow: 7px 8px 20px 0 #000000af;
            overflow: hidden;
        }

        .layer_6 {
            position: absolute;
            z-index: 6;
            width: 80px;
            height: 250px;
            bottom: 0;
            right: 50px;
        }
        .layer_7 {
            position: absolute;
            z-index: 7;
            width: 180px;
            height: 190px;
            background: #bb6d50;
            bottom: 0;
            right: 0;
            border-radius: 20px;
            box-shadow: 7px 8px 20px 0 #000000af;
            overflow: hidden;
        }

        .layer_8 {
            position: absolute;
            z-index: 8;
            width: 240px;
            height: 130px;
            background: #66a167;
            bottom: -30px;
            left: 0;
            right: 0;
            margin: 0 auto;
            border-radius: 50px;
        }

        .grass {
            width: 120px;
            height: 120px;
            background-color: #66a167;
            border-radius: 50%;
            position: absolute;
            left: 70px;
            top: -40px;
        }

        .grass::after {
            content: "";
            width: 90px;
            height: 90px;
            background-color: #66a167;
            border-radius: 50%;
            position: absolute;
            left: 90px;
            top: 30px;
        }

        .grass::before {
            content: "";
            width: 90px;
            height: 90px;
            background-color: #66a167;
            border-radius: 50%;
            position: absolute;
            left: -70px;
            top: 30px;
        }




        .layer_9 {
            position: absolute;
            z-index: 9;
            width: 180px;
            height: 80px;
            border-radius: 50px;
            background: white;
            bottom: 0;
            left: -50px;
        }
        .layer_10 {
            position: absolute;
            z-index: 10;
            width: 180px;
            height: 80px;
            border-radius: 50px;
            background: white;
            bottom: 0;
            right: -50px;
        }

        .layer_9::before,
        .layer_10::before {
            content: "";
            position: absolute;
            width: 80px;
            height: 80px;
            border-radius: 50%;
            background-color: white;
            top: -40px;
            left: 50px;
        }


        /* //////////////////////////////////////////////////////// */
        .layer_3::after,
        .layer_5::after,
        .layer_7::after {
            content: "";
            position: absolute; 
            left: -0px;
            top: 0px;
            width: 100%;
            height: 40px;
            background: linear-gradient(135deg, #6aa067 25%, transparent 25%) -50px 0,
                        linear-gradient(-135deg, #6aa067 25%, transparent 25%) -50px 0,
                        linear-gradient(45deg, #6aa067 25%, transparent 25%),
                        linear-gradient(-45deg, #6aa067 25%, transparent 25%);
            background-size: 40px 100px;
            background-position: 140px 0 0 30px 0;
        }

        .layer_3::before,
        .layer_5::before,
        .layer_7::before {
            content: "";
            position: absolute; 
            left: -0px;
            top: 15px;
            width: 100%;
            height: 40px;
            background: linear-gradient(135deg, #a6543d 25%, transparent 25%) -50px 0,
                        linear-gradient(-135deg, #a6543d 25%, transparent 25%) -50px 0,
                        linear-gradient(45deg, #a6543d 25%, transparent 25%),
                        linear-gradient(-45deg, #a6543d 25%, transparent 25%);
            background-size: 40px 100px;
            background-position: 140px 0 0 30px 0;
        }





        /* //////////////////////////////////////////////////////// */

        .ear_left {
            position: absolute;
            background-color: white;
            border-radius: 90%;
            width: 25px;
            height: 60px;
            left: 5px;
        }

        .ear_left::before {
            content: "";
            position: absolute;
            background-color: #f9c7d1;
            width: 15px;
            height: 50px;
            left: 5px;
            top: 4px;
            border-radius: 90%;

        }

        .ear_right {
            position: absolute;
            background-color: white;
            border-radius: 90%;
            width: 25px;
            height: 60px;
            right: 5px;
        }

        .ear_right::before {
            content: "";
            position: absolute;
            background-color: #f9c7d1;
            width: 15px;
            height: 50px;
            left: 5px;
            top: 4px;
            border-radius: 90%;

        }

        .head {
            position: absolute;
            background-color: white;
            width: 80px;
            height: 80px;
            border-radius: 50%;
            top: 30px;
        }

        .head::before {
            content: "";
            position: absolute;
            background-color: black;
            border-radius: 50%;
            width: 5px;
            height: 5px;
            left: 25px;
            top: 20px;
        }

        .head::after {
            content: "";
            position: absolute;
            background-color: black;
            border-radius: 50%;
            width: 5px;
            height: 5px;
            right: 25px;
            top: 20px;
        }
```

![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/CSS/animation/favicon-48x48.97046865.png)[animation - CSS: Cascading Style Sheets | MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/animation)

![img](https://www.google.com/s2/favicons?domain=https://res.cloudinary.com/practicaldev/image/fetch/s--E8ak4Hr1--/c_limit,f_auto,fl_progressive,q_auto,w_32/https://dev-to.s3.us-east-2.amazonaws.com/favicon.ico)[Explained: Creating a zigzag pattern with just CSS - DEV Community](https://dev.to/cchana/explained-creating-a-zigzag-pattern-with-just-css-13g1)

![img](https://www.google.com/s2/favicons?domain=https://www.magicpattern.design/tools/css-backgrounds/static/favicons/favicon.ico)[CSS Background Patterns by MagicPattern](https://www.magicpattern.design/tools/css-backgrounds)

![img](https://www.google.com/s2/favicons?domain=https://css-tricks.com/a-few-background-patterns-sites//favicon.ico)[A Few Background Patterns Sites | CSS-Tricks](https://css-tricks.com/a-few-background-patterns-sites/)

# 3. Animation y keyframes en CSS

## Animation name y keyframe

☝ Básicamente el uso de la directiva `@keyframes` te permite definir el comportamiento de tu animación punto por punto, y cualquier elemento puede usar esta animación por medio de la regla `animation-name`.

Dentro de `@keyframes` especificamos cada punto de nuestra animación por medio de porcentajes, por ejemplo, tomando en cuenta este ejemplo:

<img src="https://i.ibb.co/gt81xg9/key.gif" alt="key" border="0">


Podríamos tener un `@keyframes` de esta forma:

```css
@keyframes jump {

    /* Punto A */
    0% {

    }

    /* Punto B */
    40% {

    }

    /* Punto C */
    60% {

    }

    /* Punto D */
    90% {

    }

}
```

Los porcentajes pueden ser los que tú quieras, pero lo más importante a tener en cuenta es que mientras más cerca esté un porcentaje de otro más rápido será la transición de un punto a otro 😄.

Animation puede tener solo un valor, o varios

- animation-name: Con esta le colocamos un nombre a la fracción de código que queremos animar, para que el keyframe sepa a quién debemos animar.
- keyframes: Se va de A a B con ciertas manipulaciones de por medio. Está es la diferencia crucial, que podemos manipular el tiempo desde el inicio hasta el final usando palabras clave como from y to para decir que solo desde el inicio hasta el final.

No es necesario llegar de 0 a 100.

En esta clase vimos cual es la diferencia entre las animaciones y las transiciones, ya que uno se puede llegar a confundir bastante, esto debido a que ambas suenan bastante similar e incluso se podría llegar a pensar que son sinónimos.


#### Diferencia entre transiciones y animaciones

Las transiciones solo pueden ir de un punto A a uno B en determinado tiempo. Sin embargo, en las animaciones, en el lapso de tiempo que va del punto A al B (osea, en el punto intermedio), puede haber ciertos movimientos.

<img src="https://i.ibb.co/fddH3PD/key1.gif" alt="key1" border="0">

#### Keyframes

Entonces, sabiendo cual es la diferencia entre las animaciones y transiciones, es hora de saber como los @keyframes nos ayudan a animar a nuestros elementos.

Un keyframe no es mas que una linea del tiempo que empieza desde el punto A y termina en el punto B. Nosotros le podemos decir a ese keyframe en que parte del “camino” modifique al elemento. Además de que nosotros le decimos cuantos segundos tiene que durar ese “viaje”.

![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/CSS/animation/favicon-48x48.97046865.png)[animation - CSS: Cascading Style Sheets | MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/animation)

## Animation duration

Estas propiedades son de las más fáciles 👀. Simplemente debes decirle cuánto va a durar la animación y cuántas veces se va a repetir esa animación, es todo 👀. Únicamente con esas dos propiedades ya tienes suficiente paa que una animación empiece a correr 😄
.
Otro ejemplo de animación sencillo, además de el de la clase, podría ser el lanzamiento de algún objeto:

<img src="https://i.ibb.co/3R26HDB/duration.gif" alt="duration" border="0">

Cuando lanzamos un objeto es normal que a veces gire y se eleve un poco en el aire, para hacer una animación similar, solo necesitariamos 3 puntos:

**Punto A:** El inicio de la animación, el objeto tendrá un `left` inicial y un `transform` inicial:

```css
0% {
    left: 50px;
    transform: rotate(0deg);
}
```

**Punto B:** A la mitad de la animación el objeto debió elevarse un poco, por tanto le cambiamos su propiedad en `bottom`:

```css
50% {
    bottom: 60%;
}
```

**Punto C:** Al final de la animación el objeto debió caer y debió de haber dado unas cuantas vueltas, también debió de haberse movido, por lo que modificamos las propiedades `left` y `transform`, el `bottom` no lo modificamos porque por defecto volverá a su estado inicial:

```css
100% {
    left: 1080px;
    transform: rotate(360deg);
}
```

Al final nuestro Keyframe podría quedar así y esto simularía un objeto que es lanzado 👀.

```css
@keyframes throw {
  
  0% {
    left: 50px;
    transform: rotate(0deg);
  }
  
  50% {
    bottom: 60%;
  }
  
  100% {
    left: 1080px;
    transform: rotate(360deg);
  }
  
}
```

Obviamente necesitariamos tener un objeto al cual aplicarle la animación, este objeto debe tener un `position: absolute;` con respecto al contenedor padre, ya hice la tarea, así que pueden ver el código completo de esta animación aquí 👇👀.
.
[Animación del lanzamiento de un objeto](https://codepen.io/retaxmaster/pen/eYvGVOK)
.
Casi todos son estilos para que se vea más bonito y para preparar los elementos en su posición incial, pero el proceso de animado te lo acabo de explicar aquí 😄

## Animation timing function, iteration count y delay

- **Animation Timing Function:** Básicamente es la aceleración con la cual correrá nuestra animación.
- **Animation Iteration Count:** Es el número de veces que se repetirán nuestra animación.
- **Animation Delay:** Es el tiempo que nuestra animación tardará en empezar.

La función `cubic-bezier()` me encanta porque podemos definir velocidades personalizadas :3. Me gustaría complementar la clase con otro ejemplo práctico de animación, por ejemplo, esta animación que hice al principio de un objeto saltando:

<img src="https://i.ibb.co/gt81xg9/key.gif" alt="key" border="0">

Si quisiera hacer esta animación en CSS quedaría algo así, tomando en cuenta sus 4 puntos:

**Punto A:** Simplemente inicia con un `left` inicial:

```css
0% {
    left: 50px;
}
```

**Punto B:** Para este punto nuestro objeto simplemente se movió unos cuantos píxeles a la derecha y se encogió un poco:

```css
40% {
    left: 400px;
    height: 65px;
    width: 130px;
    bottom: 40%;
}
```

**Punto C:** Aquí el objeto ya dio el salto hacia más a la derecha y se hizo un poquito más grande, por lo que debería estar en su punto más alto:

```css
60% {
    left: 780px;
    height: 140px;
    width: 100px;
    bottom: calc(40% + 150px);
  }
```

**Punto D:** Al final el objeto cae de nuevo, obviamente moviéndose aún más a la derecha 😉

```css
90%, 100% {
    left: 1080px;
    bottom: 40%;
}
```

Aquí puse al 90% y al 100% juntitos porque quiero que tanto en el 90% commo en el 100% se conserven los mismos valores :3

Al final tendría este resultado:

<img src="https://i.ibb.co/bsKkZwX/timing.gif" alt="timing" border="0">

Así de fácil es crear animaciones con CSS, simplemente necesitas pensar qué acción estará haciendo tu objeto en cada punto y con base en ello puedes empezar a programar la animación :3.

Obviamente necesitariamos tener un objeto al cual aplicarle la animación, este objeto debe tener un `position: absolute;` con respecto al contenedor padre, ya hice la tarea, así que pueden ver el código completo de esta animación aquí 👇👀.

[Animación de un objeto saltando](https://codepen.io/retaxmaster/pen/KKWXyJw)

#### **transform: translateY();**

```css
animation: layout 2s ease-in-out alternate infinite;
@keyframes layout {
        0% {transform: translateY(0);}
        50% {transform: translateY(80px);}
        90% {transform: translateY(-80px);}

    }
```

- `animation-timing-function`: establece curvas de aceleración preestablecidas como `ease` o `linear`.
  .
- `animation-iteration-count`: el número de veces que se debe realizar la animación.
  .
- `animation-delay`: el tiempo entre el elemento que se está cargando y el inicio de la secuencia de animación ([ejemplos interesantes en este tweet](https://twitter.com/jh3yy/status/1246533538884845568)).

![img](https://www.google.com/s2/favicons?domain=https://cubic-bezier.com/#.17,.67,.83,.67about:blank)[cubic-bezier.com](https://cubic-bezier.com/#.17,.67,.83,.67)

## Animation direction, fill mode y play state

Animation direction, fill mode y play state

**PROPIEDAD ANIMATION-DIRECTION**

Mediante esta propiedad indicamos cómo debe ejecutarse una animación: hacia delante (del principio al final), hacia atrás (del final al principio), una vez hacia delante y otra hacia detrás, etc. La animación habrá sido definida en una regla `@keyframes` y va a ser aplicada a un elemento seleccionado mediante un selector CSS. El valor por defecto para esta propiedad es **normal** (implica que la animación se ejecutará hacia delante). Sus valores posibles son cualquiera de estas palabras clave:

- **normal:** la animación se ejecutará hacia delante. Si se repite, cuando vuelve a empezar parte de la situación inicial.
- **reverse:** la animación se ejecutará hacia detrás. Si se repite, cuando vuelve a empezar parte de la situación inicial.
- **alternate:** la animación se ejecutará una vez en un sentido y otra vez en otro, comenzando hacia delante, luego hacia detrás y así sucesivamente el número de repeticiones especificado.
- **alternate-reverse:** la animación se ejecutará una vez en un sentido y otra vez en otro, comenzando hacia detrás, luego hacia delante y así sucesivamente el número de repeticiones especificado.

Si se desean aplicar varias animaciones, se especificarán sus direcciones separadas por comas, siendo cada valor correspondiente a la animación cuyo nombre figura en el mismo orden en la propiedad animation-name.

Esta propiedad puede no ser reconocida por los navegadores antiguos o requerir del uso de prefijos específicos o no ser reconocida por algunos navegadores actuales. Consulta en Mozilla Developer Network para conocer si debes aplicar prefijos o si la propiedad será reconocida por un navegador concreto.

### **PROPIEDAD ANIMATION-PLAY-STATE**

Esta propiedad permite detener una animación que se está ejecutando (ponerla en pausa). Su valor por defecto es running y sus dos valores posibles son:

- **running:** la animación está en ejecución
- **paused:** la animación está en pausa

Esta propiedad puede no ser reconocida por los navegadores antiguos o requerir del uso de prefijos específicos para algunos navegadores actuales. Consulta en Mozilla Developer Network para conocer si debes aplicar prefijos.

Ejemplo: .pubMov:hover{ animation-play-state:paused; } con esta regla damos lugar a que la animación se detenga si el usuario pone el puntero del ratón sobre el elemento animado.

### **PROPIEDAD ANIMATION-FILL-MODE**

Esta propiedad permite detener definir cómo debe comenzar y cómo debe quedar un elemento que tiene una animación. Su valor por defecto es none (implica que el elemento comenzará y quedará en el estado que tenía antes de que comenzara la animación) y sus valores posibles son:

- **none:** el elemento comenzará y quedará en el estado previo a la animación.
- **forwards:** el elemento quedará en el estado final de la animación.
- **backwards:** el elemento se pondrá en el estado indicado para el comienzo de la animación inmediatamente y esperará en ese estado hasta que se cumpla el tiempo indicado en animation-delay. Una vez se cumpla ese tiempo la animación continuará la ejecución desde ese estado inicial.
- **both:** combinación de las dos opciones anteriores.

Esta propiedad puede no ser reconocida por los navegadores antiguos o requerir del uso de prefijos específicos para algunos navegadores actuales. Consulta en Mozilla Developer Network para conocer si debes aplicar prefijos.

**Ejemplo**: .pubMov:hover{ animation-fill-mode: forwards; } con esta regla damos lugar a que cuando la animación se detenga quede en el último estado definido (el correspondiente a to ó 100% dentro de la regla @keyframes).

para que el contador funciones correctamente.
En el codigo HTML
Si ponemos el contador del score arriba de lo que queremos contabilizar este no los va a contar.
Me pasaba que yo ponía lo siguiente (este es un codigo mas simplificado):

```
<input type="checkbox">

<h3 class="total-count"> Score: </h3>

<div class="layer-1">
 	<input type="checkbox">
 <div class="layer-2">
         <input type="checkbox">

<input type="checkbox">
```

El input de hasta arriba me lo contaba sin problema.
Pero los dos que tienen clase layer y el ultimo input (siendo igual al primero) no los contaba al hacer el checked al input.
Estuve un buen rato viendo que pasaba, hasta que me di cuenta que el counter solo toma los valores que estén arriba de este. No los que estan debajo.

Al cambiar el HTML a:

```html
<input type="checkbox">

<div class="layer-1">
 	<input type="checkbox">
 <div class="layer-2">
         <input type="checkbox">

<h3 class="total-count"> Score: </h3>

<input type="checkbox">
```

Así reconoce el primer input y los dos que tienen clase layer, pero el ultimo por estar debajo no lo contara.
Que la fuerza los acompañe

# 4. Rendimiento de animaciones con CSS

## CSS Triggers: layout, paint y composite

- **Composite**: Ordena las partes de la página. Propiedades como opacity y transform.
- **Paint**: Rellena píxeles. Implica colores, imágenes, textos, sombras…
- **Layout**: Diseño de la página. Ancho, margin, padding, border…

Existen algunas propiedades en css que hacen que las paginas tarden mas en cargar. Esto se debe a que tienen que pasar por ciertas “capas” de renderizado hasta que se pinten en la pantalla.

En pocas palabras, las propiedades que tengan relacion con una gran cantidad de pixeles nos van a afectar en el performance de la pagina web. Por eso el `background` afecta al desempeño de la pagina web.

👇
![img](https://www.google.com/s2/favicons?domain=https://web.dev/lcp//images/favicon.ico)[CSS Triggers](https://csstriggers.com/)

![img](https://www.google.com/s2/favicons?domain=https://web.dev/lcp//images/favicon.ico)[Largest Contentful Paint (LCP)](https://web.dev/lcp/)

![img](https://www.google.com/s2/favicons?domain=https://web.dev/cls//images/favicon.ico)[Cumulative Layout Shift (CLS)](https://web.dev/cls/)

## Debugging de animaciones con DevTools

### **El poder de Dev Tools** 😮

En esta herramienta podemos encontrar diferentes tipos de herramientas para trabajar en paralelo con nuestro código de una manera muy eficiente.
.
Algunas características en la cual nos puede servir son:

- Accesibilidad web
- Performance
- Renderizare

Y otros muchos mas 😃

Te recomiendo este curso dictado por nuestro profe Diego donde explica varios de estos temas a detalle.

En Chrome también hay una opción muy curiosa para debuguear animaciones 👀. Simplemente le dan click al botón de los 3 puntitos de las DevTools, seleccionan “More Tools” y eligen “Animations”:

<img src="https://i.ibb.co/m5ycx9H/settings.webp" alt="settings" border="0">

Si apenas lo acaban de abrir les aparecerá algo como “Waiting for an animation”, simplemente recarguen la página (con esta sección abierta) y automáticamente les registrará la animación que hayan hecho:

<img src="https://i.ibb.co/8YcW1fn/settings1.gif" alt="settings1" border="0">

Aquí también pueden jugar con los controles de sus animaciones 👀👇:

<img src="https://i.ibb.co/8gBhzZZ/settings2.gif" alt="settings2" border="0">

![img](https://www.google.com/s2/favicons?domain=https://2r4s9p1yi1fa2jd7j43zph8r-wpengine.netdna-ssl.com/wp-content/themes/Hax/favicon.ico)[The whole web at maximum FPS: How WebRender gets rid of jank - Mozilla Hacks - the Web developer blog](https://hacks.mozilla.org/2017/10/the-whole-web-at-maximum-fps-how-webrender-gets-rid-of-jank/)

![img](https://www.google.com/s2/favicons?domain=https://www.youtube.com/s/desktop/589757bc/img/favicon.ico)[CSS Animation Optimization and Performance 101 - YouTube](https://www.youtube.com/watch?v=0Xg6r_MKGJ4)

## Buenas prácticas para optimizar animaciones web

5 buenas prácticas para optimizar tus animaciones web:

1. Usa dentro de lo posible propiedades que solo tengan que pasar por el proceso de Composite.
2. Si necesitas animar alguna propiedad que sea muy costosa (como width, height, left, entre otras), trata de encontrar otra propiedad que sea menos costosa con la que puedas lograr el mismo resultado (o al menos un resultado similar).
3. Evita animar muchas propiedades al mismo tiempo.
4. Si necesitas esconder elementos, normalmente se usan las propiedades opacity y visibility en vez de usar la propiedad display (ya que es una propiedad no animable: [Propiedades que pueden ser animadas](https://developer.mozilla.org/es/docs/Web/CSS/CSS_Transitions/Using_CSS_transitions#propiedades_que_pueden_ser_animadas)).
5. Evita hacer animaciones que ocurran al hacer scroll, ya que el evento que escucha el scroll se ejecuta una gran cantidad de veces. Mejor espera a llegar a cierto punto de la pantalla y ahí ejecutas la animación.

Usa `opacity` y `transform` para animar. Propiedades como width, height, background y cualquier otras que utilicen una gran cantidad de pixeles son greedy.

# 5. Libros recomendados y próximos pasos

## Continúa en el Curso Práctico de Animaciones

AOS js o en su acrónimo (Animate on scroll library) ?

Esta es una herramienta desarrollada para hacer algunas animaciones en la web al hacer scroll donde tienen diferentes tipos de atributos y características esto seria un gran complemento para nuestro curso.
Aquí les dejo el enlace y una pequeña muestra de como funciona.

[AOS - Animate on scroll library](https://michalsnik.github.io/aos/)