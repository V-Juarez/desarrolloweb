<h1>Sass</h1>

<img src="https://i.ibb.co/bJHHSTC/245px-Sass-Logo-Color-svg.png" alt="245px-Sass-Logo-Color-svg" border="0">

<h3>Daniel Martínez</h3>

<h1>Tabla de Contenido</h1>

- [1. Preprocesador Sass](#1-preprocesador-sass)
  - [Qué aprenderás sobre Sass](#qué-aprenderás-sobre-sass)
  - [Introducción al Curso de Sass](#introducción-al-curso-de-sass)
  - [Diferencias entre Sass, Stylus y Less](#diferencias-entre-sass-stylus-y-less)
  - [Compilación de código en archivos CSS](#compilación-de-código-en-archivos-css)
- [2. Instalación](#2-instalación)
  - [Estructura de CSS](#estructura-de-css)
- [3. Variables](#3-variables)
  - [Variables](#variables)
  - [Reto](#reto)
  - [Solución al reto](#solución-al-reto)
- [4. Anidaciones](#4-anidaciones)
  - [Anidaciones](#anidaciones)
- [5. Mixins](#5-mixins)
  - [Mixins](#mixins)
  - [Continuando con Mixins](#continuando-con-mixins)
  - [Uso de la directiva `content` (`block` en Stylus)](#uso-de-la-directiva-content-block-en-stylus)
  - [Extend](#extend)
- [6. Funciones](#6-funciones)
  - [Funciones](#funciones)
  - [Directiva](#directiva)
  - [Ejemplos de funciones](#ejemplos-de-funciones)
  - [Reto](#reto-1)
  - [Reto - Solución](#reto---solución)
- [7. Controles de Flujo](#7-controles-de-flujo)
  - [Listas y directiva each](#listas-y-directiva-each)
  - [Ciclos FOR/EACH](#ciclos-foreach)
  - [Condicionales](#condicionales)

# 1. Preprocesador Sass

## Qué aprenderás sobre Sass

Optimizar todos los procesos de css.

## Introducción al Curso de Sass

Vamos a conocer las generalidades del uso de Preprocesadores. En este nos concentraremos en Sass que es uno de los preprocesadores más usados en la industria del Frontend.

## Diferencias entre Sass, Stylus y Less

Aunque hay muchos preprocesadores, en este curso nos vamos a concentrar en Sass, Stylus y Less.

**Less**, por ejemplo, es un preprocesador muy simple.
**Sass**, por otro lado, es una herramienta muy interesante gracias a su comunidad.
Finalmente, **Stylus** tiene la ventaja de que es muy completo e incluso complejo.

Elegir cuál es el mejor procesador depende de lo que queremos lograr con el proyecto. Algunas de las razones están relacionadas con el equipo y las necesidades que tenemos con el proyecto.

> Sass: es una herramienta muy interesante gracias a su comunidad, todas las dudas y preguntas asi como desarrollo es enorme.

> Less: es un preprocesador muy simple, compacto y rapido.

> Stylus: tiene la ventaja de que es de los más completos pero también más complejo

## Compilación de código en archivos CSS

Una de las primeras cosas que tenemos que aprender cuando estamos trabajando con Preprocesadores es la compilación de código que se compilará en archivos de CSS. En esta clase, te quiero mostrar cómo funcionan estos compiladores. En Mac utilizamos Codekit, y para Windows podemos usar Preposs.

#### Configuración de VSCode para Sass

Yo configuré una extensión en VSCode para windows porque prepros saca un pop-up cada cierto tiempo que es un poco molesto.
Las extensiones que tengo son:

- **[Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)**: Esta crea un servidor que se actualiza cuando guardamos, lo que nos permite ver los cambios sin necesidad de recargar manualmente. Es necesaria para la siguiente extensión.

- **[Live Sass Compiler](https://marketplace.visualstudio.com/items?itemName=ritwickdey.live-sass)**: Esta transforma nuestros archivos `.sccs` o `.sass` a `.css` y crea un servidor basado en **Live Server**

- **[Sass](https://marketplace.visualstudio.com/items?itemName=syler.sass-indented)**: Sirve para obtener sugerencias, autocompletado y formateo al escribir el código.

  Para asegurarte que los archivos se creen con la misma organización de carpetas que el profesor debes configurarlo con un archivo json de la siguiente forma:

1. Crea una carpeta en la raiz de tu proyecto con el nombre `.vscode`
2. Adentro crea un archivo con el nombre `settings.json`
3. En el archivo json copia lo siguiente:

```json
{
  "liveSassCompile.settings.formats": [
    // This is Default.
    {
      "format": "expanded",
      "extensionName": ".css",
      "savePath": "/css/"
    }
  ]
}
```

Esto va a hacer que el archivo css compilado se guarde en la carpeta `/project_folder/css/`.

Para instalar Sass desde la terminal debemos tener instalado npm (https://tutobasico.com/instalar-nodejs-y-npm/), y luego de eso debemos escribir el siguiente comando:

```sh
npm install -g sass
```

Luego si deseamos compilar algún archivo de Sass a css utilizaremos el siguiente comando:

```sh
sass input.scss output.css
```

Ya si deseamos visualizar los cambios al guardar, (Que se compile en vivo), lo único que deberemos hacer es memorizar esta linea de comando:

```sh
sass --watch input.scss output.css
```

**NOTA:** Podemos utilizar rutas de carpetas indicándolas antes del archivo.
cualquier duda te invito a que visites la documentación oficial de Sass donde explican todo un poco más a fondo. (https://sass-lang.com/guide)

**Fuente: [FAQ (for beginners)](https://github.com/ritwickdey/vscode-live-sass-compiler/blob/master/docs/faqs.md), [VSCode Live Sass Compiler Settings](https://github.com/ritwickdey/vscode-live-sass-compiler/blob/master/docs/settings.md).**

# 2. Instalación

## Estructura de CSS

La primera ventaja que se nos ocurre es que podemos organizar nuestro Sass. Lo clave es que podemos separar nuestro código en archivos. Ya no tenemos que revisar un archivo muy amplio, sino que podemos hacer modificaciones a un solo archivo, lo que nos hace el trabajo mucho más fácil.

Para organizar e importar archivos usamos `@import`

```scss
@import "nombre_archivo.scss";
```

La forma de nombrar los archivos que van a ser importados es con un `_`al inicio

```scss
@import "_nombre_archivo.scss";
```

2 maneras de hacer @import para el archivo que queremos gestionar por medio de preprosessing y mantener nuestro código limpio y ordenado.

**1- Un archivo para cada @import:** (haciendo un doble espaciado cuando cambia la carpeta)

```scss
@import "base/_mixins";
@import "base/_funciones";

@import "layout/_navbar";
@import "layout/_footer";

@import "components/_botones";
@import "components/_tablas";
```

**2- un @import para cada carpeta**

```scss
@import
	'base/_mixins';
	base/_funciones';

@import
	'layout/_navbar';
	'layout/_footer';

@import
	 'components/_botones';
	 'components/_tablas';
```

> Una ventaja que trae Sass es el poder organizar mejor nuestros archivos. Esto lo podemos lograr separando nuestros estílos en múltiples archivos. De tal modo, ya no tenemos que revisar un archivo muy amplio, sino que podemos separar nuestros estilos en varios módulos haciendo el trabajo mucho más fácil.

> Para organizar e importar archivos usamos `@import`. La forma de nombrar los archivos que van a ser importados es con un `_` al inicio.

> ```scss
> @import "_nombre_archivo.scss";
> ```

<img src="https://i.ibb.co/yST26n5/scss.png" alt="scss" border="0">

# 3. Variables

## Variables

Las variables son una forma de almacenar la información que se desea reutilizar a lo largo de la hoja de estilo.

Se puede almacenar cosas como colores, pilas de fuentes o cualquier valor de CSS que que se desea reutilizar. Sass usa el símbolo `$` para hacer que algo sea una variable.

Aquí hay un ejemplo:

```scss
$font-stack: Helvetica, sans-serif
$primary-color: #333

body {
  font: 100% $font-stack
  color: $primary-color
}
```

`BEM` — Block Element Modifier o Modificador de Bloques de Elementos

Como su nombre indica, BEM distingue claramente 3 conceptos: el Bloque, el Elemento y el Modificador.

## Escapar una variable

Para escapar una variable se usa el comodín `#`.

Esto es necesario en casos como, por ejemplo, cuando la variable está rodeada por comillas y de no ponerse el escape la variable pasaría como una cadena de caracteres…

```scss
$size: 10;

div {
  content: "#{$size}";
}
```

Es algo muy interesante ver como funcionan y como nos permite trabajar con las variables !

```scss
$color-brand: #ea83ee;
$color-secondary: rgb(219, 216, 121);

$color-black: rgb(0, 0, 0);
$color-darkgrey: #4d4d4d;
$color-grey: #737373;
$color-lighgrey: #bfbfbf;
$color-white: white;
```

Piense en las variables como una forma de almacenar la información que desea reutilizar a lo largo de su hoja de estilo. Puede almacenar cosas como colores, pilas de fuentes o cualquier valor de CSS que crea que desea reutilizar. Sass usa el símbolo \$ para hacer que algo sea una variable. Aquí hay un ejemplo:

```scss
$font-stack:    Helvetica, sans-serif
$primary-color: #333

body
  font: 100% $font-stack
  color: $primary-color
```

## Reto

Ahora hay un reto para ustedes.
Como sabemos tenemos que hacer la guía de estilos del proyecto de PlatziMusic. En el reto te propongo que hagamos de forma predictiva las variables que serán utilizadas y los parciales que se pueden utilizar. Por ahora tenemos tablas, pero tendremos muchos más elementos útiles.

![Captura de pantalla 2021-05-27 111224.jpg](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%202021-05-27%20111224-565c699b-409b-439c-8478-cdbcd8384eaf.jpg)

\_variables.scss:

```scss
/* ----------------------------- *\
|             $COLORS             |
\* ----------------------------- */

/*-------Branding colors */
$color-brand: #ea83ee;
$color-secondary: rgb(219, 216, 221);
$black: rgb(0, 0, 0);
$dark-grey: #4d4d4d;
$grey: #737373;
$white: #ffffff;

/*-------text colors */
$black-text: black;
$blue-text: blue;
$yellow-text: yellow;
$orange-text: orange;
$white-text: white;

/*-------alerts colors */
$danger-color: #ff0000;
$warning-color: #ffa500;
$success-color: #30e92c;
$info-color: #5474f5;
$primary-color: #584aff;

/*-------background colors */

/*------- fonts styles */
$basefont: Verdana, Geneva, Tahoma, sans-serif;

/*-------cols size's */

/*-------elements size's */
```

Estructura de archivos:
![plmsc.jpg](https://static.platzi.com/media/user_upload/plmsc-69d9fdd1-5112-43ef-b731-89fe3a25fccc.jpg)

## Solución al reto

styles.sass

```scss
// Librerias, settings y variables
@import "lib/_variables.sass" // Elementos base
  @import "_tablas.sass" @import "base/_botones.sass" @import "base/_tipografia.sass"
  // Components
  @import "components/_grids.sass" // Esto va de último
  @import "components/_utilities.sass";
```

\_variables.sass

```scss
/*---------- $COLORS -----------*/


// Branding colors

$color-brand: #ea83ee
$color-secondary: rgb(219, 216, 121)

$color-black: rgb(0,0,0)
$color-darkblack: #4d4d4d
$color-grey: #bfbfbf
$color-white: rgb(255,255,255)

// Texturas
$color-alert: rgb(252,228,207)
$color-error: rgb(218,79,73)
$color-info: rgb(66,104,221)
$color-success: rgb(91,183,91)


// Tipografía
$basefont: 'Quicksand', sans-serif
$basefontsize: 16px

$buttoncolor: $color-white
$buttonbackgroundcolor: $color-brand
```

\_tablas.sass

```scss
table tr:hover
  background-color: $color-secondary
```

index.html

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="css/styles.css" />
    <title>SASS</title>
  </head>
  <body>
    <h1>Platzi Styguide</h1>
    <section>
      <h2>Botones</h2>
      <input type="submit" />
    </section>
    <section>
      <h2>Tipografía</h2>
      <p>
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Adipisci nam
        dolor in quidem! Consectetur hic quam, veritatis aut, expedita aliquid
        beatae asperiores voluptas nobis vitae doloremque, sunt laboriosam id
        quo?
      </p>
    </section>
  </body>
</html>
```

Los otros no contienen nada por ahora, entonces sólo es crearlos.

# 4. Anidaciones

## Anidaciones

En esta clase nos vamos a concentrar en las anidaciones. Para mostrarte esto vamos a trabajar con los botones.

anidaciones pueden ser muy útiles para las pseudoclases. Algo así:

```scss
.btn {
  color: red;
  &:hover {
    color: blue;
  }
}
```

```scss
.btn {
  font-size: 15pt;
  &__icon {
    font-size: 0.6em;
  }
  &.btn--info {
    background-color: $color-info;
  }
}
```

El comodín `&` se usa para hacer referencia al padre.

Pueden ver el resumen completo del curso [aquí](https://github.com/MineiToshio/CursosPlatzi/tree/master/Curso de Sass).

# 5. Mixins

## Mixins

Lo primero que debemos hacer es definir qué son los mixins. Lo que hacen los mixins nos ayudan a reciclar declaraciones para evitar mucho trabajo. Dentro de los archivos de nuestro curso vamos a crear una carpeta llamada mixins. Para esto vamos a usar @mixin.

Los mixins nos ayudan a reciclar declaraciones para evitar mucho trabajo. Para esto vamos a usar @`mixin`.

Cuando se define un mixin, los argumentos se definen como una serie de variables separadas por comas, y todo ello encerrado entre paréntesis.

```scss
@mixin max-width($max-width: 800px) {
  max-width: $max-width
  margin-left: auto
  margin-right: auto
}
```

En este caso le estamos definiendo un valor por defecto. Si deseamos cambiar ese valor, cuando lo llamemos se lo podemos cambiar de esta forma:

```scss
@mixin max-width(1200px);
```

## Continuando con Mixins

Los **mixins** nos permite recibir parámetros, esta funcionalidad nos ayuda bastante, ya que con un solo mixins podemos obtener distintas salidas con solo cambiar el valor del parámetro.

Ejemplo: Nos ayuda bastante al momento de realizar mediaqueries
Mixins

![img](https://i.imgur.com/185Amea.png)

Mediaquerie

![img](https://i.imgur.com/n4ivUnR.png)

Resultado

![img](https://i.imgur.com/o5Z4lbj.png)

Mixin multi-parametrico

```scss
@mixin max-width($max-width : 800px, $ margin-x: auto) {
	max-width: $max-width
	margin-left: $margin-x
	margin-right: $margin-x
}
```

fuente:[Sass](https://desarrolloweb.com/articulos/mixin-sass.html)

## Uso de la directiva `content` (`block` en Stylus)

Una de las características que tienen los mixins es la directiva content. Esta nos permite incluir un bloque de contenido dentro de un mixin.

Una de las características que tienen los mixins es la directiva content. Esta nos permite incluir un bloque de contenido dentro de un mixin.

```scss
@mixin response-to($width) {
  @media only screen and (min-width: $width) {
    @content;
  }
}
```

Se usa de esta forma:

```scss
section {
  background: blue;
  @include response-to(800px) {
    background-color: red;
  }
}
```

## Extend

Permiten que una declaración herede estilos declarados por otra regla o placeholder. Los extend se declaran con el símbolo de porcentaje `%`.

```scss
%btn {
  color: red;
  width: 50px;
}

.btn-info {
  @extend %btn;
  background: blue;
}
```

# 6. Funciones

## Funciones

## Directiva

## Ejemplos de funciones

## Reto

## Reto - Solución

# 7. Controles de Flujo

## Listas y directiva each

## Ciclos FOR/EACH

## Condicionales
