<h1>Manipulación de Arrays en JavaScript</h1>

<h3>Nicolas Molina</h3>

<h1>Tabla de Contenido</h1>

- [1. Introducción](#1-introducción)
  - [Tu AS bajo la manga](#tu-as-bajo-la-manga)
- [2. Fundamentos del manejo de arrays](#2-fundamentos-del-manejo-de-arrays)
  - [ForEach](#foreach)
  - [Mutable o Inmutable](#mutable-o-inmutable)
  - [Map](#map)
  - [Map Reloaded](#map-reloaded)
  - [Filter](#filter)
  - [Reduce](#reduce)
  - [Reduce Reloaded](#reduce-reloaded)
- [3. Métodos en JavaScript](#3-métodos-en-javascript)
  - [Some](#some)
  - [Every](#every)
  - [Find y FindIndex](#find-y-findindex)
  - [Includes](#includes)
  - [Join](#join)
  - [Concat](#concat)
  - [Flat](#flat)
  - [FlatMap](#flatmap)
  - [Mutable functions](#mutable-functions)
  - [Sort](#sort)
- [4. Despedida](#4-despedida)
  - [Despedida](#despedida)

# 1. Introducción

## Tu AS bajo la manga

Clonamos el repo (https://github.com/platzi/curso-manipulacion-de-arrays/tree/main)

```sh
git clone --branch 1-init --single-branch git@github.com:platzi/curso-manipulacion-de-arrays.git
```

en el primer commit, en consola mandamos `npm install` y arrancamos con todo el curso!

El que venga en el futuro, recuerde instalar desde el
`package.json` las versiones así no tiene problemas de compatibilidad a futuro.

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - platzi/curso-manipulacion-de-arrays](https://github.com/platzi/curso-manipulacion-de-arrays/tree/main)

# 2. Fundamentos del manejo de arrays

## ForEach

En el curso de [Manipulación del DOM](https://platzi.com/clases/dom/) el profesor nos dice lo malo que es para el performance hacer muchas llamadas al DOM por lo que si quieres mejorar esto un poco, podrias:

En lugar de insertar en cada llamado al HTML, los almacenamos todos en un array

```jsx
const nodes = [];
```

y luego en cada llamado lo añadimos al array

```jsx
tasks.forEach(({ title, done }) => {
    const element = `
         <li>
          	 <input
                     type='checkbox'
                     id='${title}'
                     name='${title}'
                     ${done && 'checked'} />
                  <label for='${title}' >${title} </label>
         </li>`;
nodes.append(element)
```

por último añadimos todo el array al DOM

```js
app.append(nodes);
```

De esta forma reducimos la cantidad de inserciones al DOM, y mejoramos el performance un poco 😃

---

reto 😊

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Manipulación de arrays</title>
    <style>
      @import url("https://fonts.googleapis.com/css2?family=Montserrat:wght@300&display=swap");

      * {
        padding: 0;
        margin: 0;
        list-style: none;
        text-decoration: none;
        font-family: "Montserrat", sans-serif;
      }

      h2 {
        text-align: center;
        margin-bottom: 10px;
      }

      .list {
        display: grid;
        grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
        gap: 10px;
        justify-content: center;
        align-items: center;
        margin: auto;
        width: 700px;
        array_1.pngmargin-bottom: 20px;
      }

      .list > li {
        align-self: center;
        border: 1px solid gray;
        padding: 5px 10px;
        border-radius: 5px;
        text-transform: capitalize;
      }

      .list > li h4 {
        margin: 5px 0;
      }

      .list > li img {
        width: 100px;
        height: 100px;
      }

      .list > li span {
        font-size: 10px;
        font-weight: bold;
        background-color: gainsboro;
        padding: 2px 5px;
        border-radius: 5px;
      }

      .list > li p {
        display: flex;
        justify-content: space-between;
        font-size: 14px;
      }
    </style>
  </head>

  <body>
    <h2>Lista de frutas</h2>
    <ul id="app" class="list"></ul>
    <h2>Lista de tareas</h2>
    <ul id="tasks" class="list"></ul>
  </body>
  <script>
    const products = [
      {
        id: 1,
        name: "platano",
        stock: 10,
        price: 2.0,
        image: "https://i.ibb.co/7bC8szm/platano.png",
        category: {
          id: 1,
          name: "frutas frescas",
          image: "https://i.ibb.co/BqWwTkD/categoria-fruta.webp"
        }
      },
      {
        id: 2,
        name: "fresa",
        stock: 10,
        price: 5.0,
        image: "https://i.ibb.co/hKNwfmP/fresa.png",
        category: {
          id: 1,
          name: "frutas frescas",
          image: "https://i.ibb.co/BqWwTkD/categoria-fruta.webp"
        }
      },
      {
        id: 3,
        name: "mandarina",
        stock: 10,
        price: 3.5,
        image: "https://i.ibb.co/7bC8szm/platano.png",
        category: {
          id: 1,
          name: "frutas frescas",
          image: "https://i.ibb.co/M2Pt0xX/mandarina.png"
        }
      }
    ];
    const tasks = [
      {
        id: 1,
        name: "Comprar frutas",
        completed: false
      },
      {
        id: 2,
        name: "Comprar verduras",
        completed: false
      },
      {
        id: 3,
        name: "Comprar carne",
        completed: true
      }
    ];
    const formatoSoles = monto => {
      let formatoSoles = new Intl.NumberFormat("es-PE", {
        style: "currency",
        currency: "PEN"
      });
      return formatoSoles.format(monto);
    };
    const app = document.getElementById("app");
    products.forEach(item => {
      const list = document.createElement("li");
      list.innerHTML += `
            <span>${item.category.name}</span>
            <h4>${item.name}</h4>
            <p>
                <label>${formatoSoles(item.price)}</label>
                <label>${item.stock} productos</label>    
            </p>
            <img src="${item.image}" alt="${item.name}">
        `;
      app.appendChild(list);
    });
    const task = document.getElementById("tasks");
    tasks.forEach(item => {
      const list = document.createElement("li");
      list.innerHTML += `
            <label for="task"> ${item.name}</label><br>
        `;
      const checkbox = document.createElement("input");
      checkbox.type = "checkbox";
      checkbox.id = "completed";
      checkbox.name = "task";
      checkbox.checked = item.completed;
      list.insertAdjacentElement("afterbegin", checkbox);
      task.appendChild(list);
    });
  </script>
</html>
```

![array_1.png](https://static.platzi.com/media/user_upload/array_1-7d5b6703-96dc-45c5-bb3c-8a9a35f98ae9.jpg)

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach/favicon-48x48.97046865.png)Array.prototype.forEach() - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach)

## Mutable o Inmutable

Por tipos, los primitivos se pasan por valor y los objetos por referencia.

![1_t6HbQiO7mmPvaesR9nAriw.png](https://static.platzi.com/media/user_upload/1_t6HbQiO7mmPvaesR9nAriw-6c33bd61-5064-475e-976a-fc998fdf56e2.jpg)

Ejemplo gráfico:

![img](https://i.stack.imgur.com/8XAQ1.gif)

En JS los datos asignados a una variable pueden ser de dos tipos:

**Primitive type** (undefined, null, boolean, number, string, symbol), **Reference type** (objects, arrays , functions).

Una de las diferencia entre estas dos, está en la forma como se almacenan estos datos en memoria, para ser más claro un ejemplo.

```jsx
let name = "Javier";

let name2 = name;

let person = { name: "javier" };

let person2 = person;
```

Cuando creamos **name** js crea un espacio en memoria y guarda su valor, ahora cuando creamos **name2** js continúa crea un nuevo espacio en memoria y asigna el mismo valor de la varible name de esta forma el valor de la variable **name2** es totalmente independiente a **name**.

Ahora si creamos la variable **person** como un objeto que contiene un name, y si luego creamos otra variable **person2** y le asignamos el mismo objeto **person**, aquí es donde la cosa cambia con respectos a los datos primitivos, en este caso js guardara el objeto **person2** como una referencia o apuntador al objeto **person**, es decir que ambas variables apuntan al mismo objeto en memoria.

![Untitled.png](https://static.platzi.com/media/user_upload/Untitled-956901da-09b2-4cef-88ff-19e4b381946f.jpg)

Ahora si entendamos **Mutable o Inmutable.**

**Mutable:** es algo que se puede cambiar o agregar.

**Inmutable:** es algo que no puede cambiar ni agregar.

Los valores primitivos en js son algo agregado donde solo se pueden reasignar y por lo tanto, todos estos valores son **inmutables**. Entendamos con un ejemplo.

```jsx
console.log(name); //javier
console.log(name2); //javier

name2 = "platzi";

console.log(name); //javier
console.log(name2); //platzi''
```

Si imprimimos **name** y **name2**, ambas nos dan javier, pero si reasignamos un valor de **name2** y volvemos a imprimir ocurre que solo cambia el valor de **name2**, lo que demuestra que js guardas estás variables de forma separada, aun cuando el valor de **name2** se copio de name. Por eso los valores primitivos son **inmutables**.

ahora hagamos lo mismo con los objetos.

```jsx
console.log(person); //{name: 'javier'}
console.log(person2); //{name: 'javier'}

person2.name = "platzi";

console.log(person); //{name: 'platzi'}
console.log(person2); //{name: 'platzi'}
```

Al inicio obtenemos las mismas propiedades, ahora cambiemos una de las valores de las propiedades y veremos que js cambio el valor tanto de **person** y **peron2**, esto debido a que **person2** se creo haciendo referencia al objeto **person**, con reference type js crea una referencia al mismo objeto y el objeto permanece **mutable**.

ya que el mismo objeto es **mutable** se puede cambiar o se pueden agregar nuevas propiedades al mismo objeto.

En es6 se creo un operador de propagación que permirte copias un objeto de forma segura sin hacer referencia al mismo objeto y sería así.

```jsx
let person2 = { ...person };
```

Ahora vuelve a ver la clase y veras como todo es más claro y entendible.

[Mutable o inmutable](https://www.youtube.com/watch?v=Wo0qiGPSV-s&ab_channel=JSConf)

[![img](https://www.google.com/s2/favicons?domain=//static.platzi.com/media/favicons/platzi_favicon.png)Estructuras de datos inmutables](https://platzi.com/tutoriales/1642-javascript-profesional/4559-estructuras-de-datos-inmutables/)

## Map

> ¿Qué hace el `.map()`? TRANSFORMAR.

`.map()` es **INMUTABLE** por lo tanto no modifica el `array` original, sino que crea uno nuevo con la “transformación” aplicada.
.
Además, mantienes el mismo `length` que el array original, te devuelve en el nuevo array la misma cantidad que el array que le aplicaste el método.
.
Código de la clase:

```
const products = [
            { title: 'Burger', price: 121 },
            { title: 'Pizza', price: 202 },
        ];
        const app = document.getElementById('app');
        const list = products.map(product => {
            return `<li>${product.title} - ${product.price}</li>`;
        })

        app.innerHTML = list.join('');
```

El método `join()` une todos los elementos de una matriz (o un objeto similar a una matriz) en una cadena y devuelve esta cadena.

```jsx
const elements = ["Fire", "Air", "Water"];

console.log(elements.join());
// expected "Fire,Air,Water"

console.log(elements.join(""));
// expected  "FireAirWater"

console.log(elements.join("-"));
// expected "Fire-Air-Water"
```

> Esta imagen ilustra muy bien los 3 metodos mas usados para la manipulación de arrays

![metodos.png](https://static.platzi.com/media/user_upload/metodos-90c872e6-a574-490e-8b9d-96c5309c278b.jpg)

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map/favicon-48x48.97046865.png)Array.prototype.map() - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map)

## Map Reloaded

Utilizando el spread operator, cogemos los valores de person y los insertamos directamente en X. Así evitamos anidar un abjeto dentro de otro
Con el spread operator, podemos sumar arrays, hacer copias, añadir nuevos elementos…

![img](https://res.cloudinary.com/practicaldev/image/fetch/s--9W82KvAK--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_66%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/ha5kudoh10qt0qhb5rev.gif)

Usos comunes o clásicos de `map()` sobre los arrays:

- **Limpiar datos**, seleccionar datos dentro de un array y devolverlos para su utilización en futuras acciones.
- **Añadir un nuevo elemento**, modificar agregando un nuevo dato al objeto pero sin modificar el array original.

Tener en cuenta que cuando trabajamos con `objetos` y `map()` y retornamos el mismo objeto estamos copiando la referencia en memoria que tiene el objeto original que le aplicamos el `map()` (esto lo vimos en la clase de mutable vs inmutable, te dejo una lectura: https://platzi.com/tutoriales/1642-javascript-profesional/4559-estructuras-de-datos-inmutables/). Esto provoca que como estamos modificando la referencia en memoria, el `array` original también sea **modificado**. Entonces en conclusión, por más que `map()` sea **inmutable** en este punto estamos copiando la referencia en memoria y por eso hace el cambio en el original.

```jsx
// Estamos retornando el objeto
// por ende se copia la refencia en memoria
const rta = orders.map(item => {
  item.tax = 0.19;
  return item;
});
```

Para evitarlo, y poder realizar una copia y evitar la referencia en memoria, utilizamos el **spread operator** de ES6 (https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Operators/Spread_syntax), donde generamos un nuevo objeto con los valores del objeto original y luego agregamos el nuevo valor que nos interesa.

```jsx
const rta = orders.map(item => {
  // retornamos un nuevo objeto
  //pero evitamos hacer ref. en memoria
  return {
    ...item,
    tax: 0.19
  };
});
```

## Filter

`filter()` lo que hace es filtrar el array original en base a una **condición**, los que la cumplan estaran en el nuevo array creado.

Por lo tanto `filter()` es **inmutable** y el nuevo array creado solamente puede contener:

- cero coincidencias
- todas coincidencias
- algunas coincidencias

Pero nunca más coincidencias que el tamaño del array original.

```js
const words = ["spray", "limit", "elite", "exuberant"];

// con for
const newArray = [];
for (let index = 0; index < words.length; index++) {
  const element = words[index];
  if (element.length >= 6) {
    newArray.push(element);
  }
}

// VS

// con filter
const rta = words.filter((element) => element.length >= 6);

// en ambos casos, el resultado:
> [ 'exuberant' ]
```

**offtopic**: el método `includes()` determina si una matriz incluye un determinado elemento, devuelve `true` o `false` según corresponda.

```js
const array1 = [1, 2, 3];

console.log(array1.includes(2));
// expected truee

const pets = ["cat", "dog", "bat"];

console.log(pets.includes("cat"));
// expected true

console.log(pets.includes("at"));
// expected false
```

> Esta imagen ilustra muy bien los 3 métodos mas usados para la manipulación de arrays

![metodos.png](https://static.platzi.com/media/user_upload/metodos-dbaaaa5f-2d98-4b0a-8c8f-b778f14445e4.jpg)

## Reduce

![img](https://miro.medium.com/max/2000/1*dhTC_FFgiH3mKROrnDj12w.gif)

Este método **REDUCE**, efectivamente hace eso. Solo reduce a un **solo** valor y no devuelve otro array, simplemente un valor.

Se utiliza muchísimo para hacer cálculos a partir de la información de un array.

En su composición, a primeras, tiene como argumentos de la función del primer parámetro, al `acumulador` y como segundo parámetro al `elemento` por el que va iterando el loop. Y como segundo argumento del `reduce()`, se pasa el valor inicial del `acumulador`.

```js
const totals = [1, 2, 3, 4];
// primer argumento de la f() es el acumulador
// segundo argumento de la f() es el elemento
// segundo parámetro de la f() es el estado inicial del acumulador
const rta = totals.reduce((sum, element) => sum + element, 0);
console.log(rta);
```

Así funciona la iteración del `reduce()` por dentro:

![Captura.JPG](https://static.platzi.com/media/user_upload/Captura-adccdc0a-e7bc-45de-9b0a-71a78e58e9b9.jpg)

Reduce: coge un array y lo reduce a un solo a valor. Se utiliza para hacer cálculos, ejemplo: una suma total de las ventas.

```js
const totals = [1, 2, 3, 4, 5, 6];
const sumaTotals = totals.reduce((sum, element) => sum + element, 0);
console.log("Con el metodo reduce:", sumaTotals);
```

## Reduce Reloaded

![carbon (8).png](https://static.platzi.com/media/user_upload/carbon%20%288%29-de43eaf5-5b0f-4e18-abb1-df88c83c35ac.jpg)

```js
// reto
const arr = [3, 10, 9, 4, 3, 1, 8, 4, 7, 6];
const result = arr.reduce(
  (obj, item) => {
    if (item <= 5) {
      obj["1-5"]++;
    } else if (item <= 8) {
      obj["6-8"]++;
    } else {
      obj["9-10"]++;
    }
    return obj;
  },
  {
    "1-5": 0,
    "6-8": 0,
    "9-10": 0
  }
);

console.log(result);
```

# 3. Métodos en JavaScript

## Some

```js
const dates = [
  {
    startDate: new Date(2021, 1, 1, 10),
    endDate: new Date(2021, 1, 1, 11),
    title: "Cita de trabajo"
  },
  {
    startDate: new Date(2021, 1, 1, 15),
    endDate: new Date(2021, 1, 1, 15, 30),
    title: "Cita con mi jefe"
  },
  {
    startDate: new Date(2021, 1, 1, 20),
    endDate: new Date(2021, 1, 1, 21),
    title: "Cena"
  }
];

const newAppointment = {
  startDate: new Date(2021, 1, 1, 8),
  endDate: new Date(2021, 1, 1, 9, 30)
};

const areIntervalsOverlapping = require("date-fns/areIntervalsOverlapping");

const isOverlap = newDate => {
  return dates.some(date => {
    return areIntervalsOverlapping(
      { start: date.startDate, end: date.endDate },
      { start: newDate.startDate, end: newDate.endDate }
    );
  });
};
console.log(isOverlap(newAppointment));
```

Este método nos devuelve `true` o `false` sí al menos 1 elemento de nuestro array cumple con la **condición**.

```js
const array = [1, 2, 3, 4, 5];

const even = element => element % 2 === 0;

console.log(array.some(even));
// resultado true
```

Al día de hoy (21/9/21), la librería de fechas `date-fns` esta en la versión `2.24.0` y funciona correctamente el ejercicio. Sí vienes del futuro, recuerda instalar la versión que usa el profe para evitar incompatibilidades si es que la sintaxis o algo de la misma librería ha sido modificada.

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/some/favicon-48x48.97046865.png)Array.prototype.some() - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/some)

## Every

😃

```js
const team = [
  {
    name: "Nicolas",
    age: 12
  },
  {
    name: "Andrea",
    age: 8
  },
  {
    name: "Zulema",
    age: 2
  },
  {
    name: "Santiago",
    age: 18
  }
];
```

**Solution**

![reto2.png](https://static.platzi.com/media/user_upload/reto2-e61e5e7b-ff5d-4ee4-9794-a59de96cc9c5.jpg)

Este método es el contrario a `some()`, devuelve `true` o `false` sí **TODOS** los elementos del array cumplen la **condición**.

```js
const isBelowThreshold = currentValue => currentValue < 40;

const array1 = [1, 30, 39, 29, 10, 13];

console.log(array1.every(isBelowThreshold));
// expected output: true
```

Dejo también mi resolución al reto de esta clase:

```js
const team = [
  {
    name: "Nicolas",
    age: 12
  },
  {
    name: "Andrea",
    age: 8
  },
  {
    name: "Zulema",
    age: 2
  },
  {
    name: "Santiago",
    age: 18
  }
];

const allAreYounger = team.every(item => item.age < 18);
console.log(areYoung);
```

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/every/favicon-48x48.97046865.png)Array.prototype.every() - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/every)

## Find y FindIndex

El método `find()` devuelve el primer elemento del array que cumpla con la **condición** dada o no devuelve `undefined` si es que no encuentra ningún elemento que cumpla los requisitos pedidos.
.

```
const array1 = [5, 12, 8, 130, 44];

const found = array1.find(element => element > 10);
console.log(found);
// expected output: 12
```

En cambio el método `findIndex()` es una variante que te devuelve el `index` o **posición** donde esta ese primer elemento que encuentra con las características de la condición dada. De no encontrar ninguno devuelve `-1` como respuesta del `return` del método.

```js
const array1 = [5, 12, 8, 130, 44];

const isLargeNumber = element => element > 13;
console.log(array1.findIndex(isLargeNumber));
// expected output: 3
```

![img](https://res.cloudinary.com/practicaldev/image/fetch/s--ZaT0FkK9--/c_imagga_scale,f_auto,fl_progressive,h_420,q_auto,w_1000/https://dev-to-uploads.s3.amazonaws.com/uploads/articles/kyfxc8l116xdsk6ko449.png)

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/find/favicon-48x48.97046865.png)Array.prototype.find() - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/find)

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/findIndex/favicon-48x48.97046865.png)Array.prototype.findIndex() - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/findIndex)

## Includes

El método `includes()` determina si una array incluye un determinado elemento, devuelve `true` o `false` según corresponda.

```js
const array1 = [1, 2, 3];

console.log(array1.includes(2));
// expected output: true

const pets = ["cat", "dog", "bat"];

console.log(pets.includes("cat"));
// expected output: true

console.log(pets.includes("at"));
// expected output: false
```

También posee un segundo parámetro que es el `fromIndex`, que es la posición donde comenzar a buscar el valor en el array.

```js
[1, 2, 3].includes(2); // true
[1, 2, 3].includes(4); // false
[1, 2, 3].includes(3, 3); // false
[1, 2, 3].includes(3, -1); // true
[1, 2, NaN].includes(NaN); // true
```

Este `fromIndex` sí es `igual` o `mayor` que el tamaño del array, devuelve `false` automaticamente sin buscar en el vector. Sí el `fromIndex` es `negativo` busca en todo el array. Y para los casos `0, -0, +0` lo toma como **cero** y también lee todo el array.

## Join

El método `join()` une todos los elementos de un array en una cadena y devuelve esta cadena. Podemos pasarle cualquier elemento como separador que deseemos.

```js
const elements = ["Fire", "Air", "Water"];

console.log(elements.join());
// expected output "Fire,Air,Water"

console.log(elements.join(""));
// expected output "FireAirWater"

console.log(elements.join("-"));
// expected output "Fire-Air-Water"
```

Y el método `split()` divide un objeto de tipo `String` en un array de cadenas mediante la separación de la cadena en sub-cadenas. Acá esta muy bien explicado y con muchos ejemplos: https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/String/split

![Captura de Pantalla 2021-09-23 a la(s) 10.00.38 a. m..png](https://static.platzi.com/media/user_upload/Captura%20de%20Pantalla%202021-09-23%20a%20la%28s%29%2010.00.38%20a.%C2%A0m.-0b089a9a-2e25-438a-82cb-c2cb3e1705b7.jpg)

Otro ejemplo muy común del método join en el ejercicio del palíndromo (Básicamente una palabra que se escribe igual de derecha a izquierda y viceversa

```jsx
const word = "Arepera";

const palindrome = word => {
  // Lo que hace dividir, voltear y luego unirlo con el join...
  return word
    .split("")
    .reverse()
    .join("")
    .toLocaleLowerCase();
};

console.log(palindrome(word));
```

Es cool 😄

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/join/favicon-48x48.97046865.png)Array.prototype.join() - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/join)

## Concat

Si estas trabajando con un arrays de Objs igual una forma de copiar cada elemento sin la referencia podría ser:

```js
const newArray = myArray.map(a => ({…a}));
```

Recordar que al ser inmutable, los arrays (tanto el nuevo como el viejo) quedaran referenciados por memoria, por lo tanto sí modificamos alguno de los dos, los cambios se verán reflejados en ambos.

```js
const array1 = ["a", "b", "c"];
const array2 = ["d", "e", "f"];
const array3 = array1.concat(array2);

console.log(array3);
// expected output: Array ["a", "b", "c", "d", "e", "f"]
```

> La sintaxis de propagación (…) (Spread) permite que un iterable, como una expresión de matriz o una cadena, se expanda en lugares donde se esperan cero o más argumentos (para llamadas a funciones) o elementos (para literales de matriz), o que se expanda una expresión de objeto en lugares donde se esperan cero o más pares clave-valor (para objetos literales).

**Concat con Spread Operator**

```js
const elements = [1, 2, 3, 4];
const otherElements = [5, 6, 7, 8];

const spreadOperatorMetod = [...elements, ...otherElements];
console.log("Con spread operator: ", spreadOperatorMetod);
```

> **…** split operation, es muy útil 👍🏻

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/concat/favicon-48x48.97046865.png)Array.prototype.concat() - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/concat)

## Flat

#### La funcionalidad de Flat con recursión

Se busca recursivamente en cada elemento hasta que ya no se encuentre otra lista y se van uniendo los elementos encontrados con Cat.

![Captura de Pantalla 2021-09-21 a la(s) 11.40.05.png](https://static.platzi.com/media/user_upload/Captura%20de%20Pantalla%202021-09-21%20a%20la%28s%29%2011.40.05-73fbbe5f-0721-440b-828f-901f0ebca0df.jpg)

Dejo también el código por si lo quieren probar:

```js
const matriz = [
  [1, 2, 3],
  [4, 5, 6, [1, 2, [1, 2]]],
  [7, 8, 9]
];

function profundidad(list) {
  let newList = [];
  if (typeof list != "object") return [list];
  list.forEach(element => {
    newList = newList.concat(profundidad(element));
  });
  return newList;
}

const newArray = profundidad(matriz);

console.log(newArray);
```

> Otra forma de hacer este reto es llamar de forma recursiva a la función en un reduce.
>
> ```js
> const flatten = arr =>
>   arr.reduce((a, b) => a.concat(Array.isArray(b) ? flatten(b) : b), []);
> flatten([1, [2, 3], [4, [5, [6]]]]); // [1, 2, 3, 4, 5, 6]
> ```

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/flat/favicon-48x48.97046865.png)Array.prototype.flat() - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/flat)

## FlatMap

Te dejo a mano el objeto `calendars` para realizar el reto 😃

```js
const calendars = {
  primaryCalendar: [
    {
      startDate: new Date(2021, 1, 1, 15),
      endDate: new Date(2021, 1, 1, 15, 30),
      title: "Cita 1"
    },
    {
      startDate: new Date(2021, 1, 1, 17),
      endDate: new Date(2021, 1, 1, 18),
      title: "Cita 2"
    }
  ],
  secondaryCalendar: [
    {
      startDate: new Date(2021, 1, 1, 12),
      endDate: new Date(2021, 1, 1, 12, 30),
      title: "Cita 2"
    },
    {
      startDate: new Date(2021, 1, 1, 9),
      endDate: new Date(2021, 1, 1, 10),
      title: "Cita 4"
    }
  ]
};
```

`array de users:`

```js
const users = [
  { userId: 1, username: "Tom", attributes: ["Nice", "Cute"] },
  { userId: 2, username: "Mike", attributes: ["Lovely"] },
  { userId: 3, username: "Nico", attributes: ["Nice", "Cool"] }
];
```

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/flatMap/favicon-48x48.97046865.png)Array.prototype.flatMap() - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/flatMap)

## Mutable functions

El metodo .splice(), no solamente sirve para borrar en cierta
forma un elemento sino también para agregarlos al index del array que tu quieres.Ej:

Tenemos el siguiente array:
const holaMundo = [‘Maria’, ‘Andres’, ‘Cecilia’];

Y quizás lo que quieres es añadir a ‘Roberto’ después de Andres y antes de cecilia.

1. Buscamos el index con el findIndex de cecilia tal como el profe nos enseño.
2. holaMundo.splice(index, 0, ‘Roberto’)

& listo, pones tu index de celcilia, no le ponemos un 1
porque no queremos eliminar ningún elemento hacia la
derecha sino un 0, pones la coma y pones el o los
elementos que quieres agregar allí.

código como está al principio de la clase para los que no quieran escribir…

```js
const products = [
  { title: "Pizza", price: 121, id: "🍕" },
  { title: "Burger", price: 121, id: "🍔" },
  { title: "Hot cakes", price: 121, id: "🥞" }
];

const myProducts = [];

console.log("products", products);
console.log("myProducts", myProducts);
console.log("-".repeat(10));
```

### Reto 1

Eliminar un elemento sin borrarlo del la lista original.
**Solución: usar \*filter\*:**
![Captura de Pantalla 2021-09-21 a la(s) 12.14.19.png](https://static.platzi.com/media/user_upload/Captura%20de%20Pantalla%202021-09-21%20a%20la%28s%29%2012.14.19-a862c239-b146-4a95-9f17-801bd349f663.jpg)

### Reto 2

Modificar una nueva lista sin modificar la original.
**Solución: copiar solo los elementos para que no tengan la misma dirección de memoria.**

![Captura de Pantalla 2021-09-21 a la(s) 12.52.30.png](https://static.platzi.com/media/user_upload/Captura%20de%20Pantalla%202021-09-21%20a%20la%28s%29%2012.52.30-6818918d-333c-48a1-9f7e-6aa94342b908.jpg)

Segundo reto, solo se utiliza .map con una nueva variable, de esta forma:

```js
const newArray2 = productsV2.map(item => {
  if (item.id === update.id) {
    return {
      ...item,
      ...update.changes
    };
  } else {
    return {
      ...item
    };
  }
});
```

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/splice/favicon-48x48.97046865.png)Array.prototype.splice() - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/splice)

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/push/favicon-48x48.97046865.png)Array.prototype.push() - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/push)

## Sort

### ¿Por qué _a - b_ o _b - a_?

La función que le enviamos a sort es la función compareFn donde:

- Si compareFn(a, b) devuelve un valor mayor que 0, ordena **b** antes **a**.
- Si compareFn(a, b) devuelve un valor menor que 0, ordena **a** antes **b**.
- Si compareFn(a, b) devuelve 0 **a** y **b** se consideran iguales.

```js
const months = ["March", "Jan", "Feb", "Dec"];

const numbers = [1, 30, 4, 21, 100000];

const words = [
  "réservé",
  "premier",
  "communiqué",
  "café",
  "adieu",
  "éclair",
  "banana"
];

const orders = [
  {
    customerName: "Nicolas",
    total: 600,
    delivered: true
  },
  {
    customerName: "Zulema",
    total: 120,
    delivered: false
  },
  {
    customerName: "Santiago",
    total: 1840,
    delivered: true
  },
  {
    customerName: "Valentina",
    total: 240,
    delivered: true
  }
];
```

### Para ordenar los meses

Creé una función que le de un valor numérico a cada mes y luego le envié esa función a _Sort_:

```js
const months = ["Febrero", "Julio", "fsdf", "Diciembre", "Enero"];
function monthValue(month) {
  switch (month.toUpperCase()) {
    case "ENERO":
      return 1;
    case "FEBRERO":
      return 2;
    case "MARZO":
      return 3;
    case "ABRIL":
      return 4;
    case "MAYO":
      return 5;
    case "JUNIO":
      return 6;
    case "JULIO":
      return 7;
    case "AGOSTO":
      return 8;
    case "SEPTIEMBRE":
      return 9;
    case "OCTUBRE":
      return 10;
    case "NOVIEMBRE":
      return 11;
    case "DICIEMBRE":
      return 12;
    default:
      //Cualquier valor que no coincida se irá de último
      return 13;
  }
}

months.sort((a, b) => monthValue(a) - monthValue(b));

console.log(months);
```

### Resultado:

```sh
[ 'Enero', 'Febrero', 'Julio', 'Diciembre', 'fsdf' ]
```

Reto

```js
const orders = [
  {
    customerName: "Nicolas",
    total: 600,
    delivered: true,
    date: new Date(2021, 3, 8, 4)
  },
  {
    customerName: "Zulema",
    total: 120,
    delivered: false,
    date: new Date(2021, 9, 12, 3)
  },
  {
    customerName: "Santiago",
    total: 1840,
    delivered: true,
    date: new Date(2021, 8, 2, 2)
  },
  {
    customerName: "Valentina",
    total: 240,
    delivered: true,
    date: new Date(2021, 1, 1, 9, 30)
  }
];
orders.sort((a, b) => a.date - b.date);
console.log(orders);
```

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort/favicon-48x48.97046865.png)Array.prototype.sort() - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort)

# 4. Despedida

## Despedida

```js
const despedida = ["Gracias!", "Excelente", "Curso!"];
console.log(despedida.join(" "));
```
