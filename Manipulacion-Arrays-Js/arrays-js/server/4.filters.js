const words = ["spray", "limit", "elite", "exuberant"];

const newArray = [];
for (let index = 0; index < words.length; index++) {
  const item = words[index];
  if (item.length > 6) {
    newArray.push(item);
  }
}

console.log("newArray", newArray);
console.log("original", words);

// filter
const rta = words.filter(item => item.length >= 6);
console.log("rta", rta);
console.log("original", words);

const order = [
  {
    customerName: "Nicolas",
    total: 60,
    delivered: true
  },
  {
    customerName: "Zulema",
    total: 120,
    delivered: false
  },
  {
    customerName: "Santiago",
    total: 180,
    delivered: true
  },
  {
    customElements: "valentina",
    total: 240,
    delivered: true
  }
];

const rta3 = order.filter(item => item.delivered && item.total >= 100);
console.log("rta3", rta3);

const search = query => {
  return order.filter(item => {
    return item.customerName.includes(query);
  });
};

console.log(search("Nico"));
console.log(search("Vale"));
