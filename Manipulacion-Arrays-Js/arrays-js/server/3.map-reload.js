const order = [
  {
    customerName: "Nicolas",
    total: 60,
    delivered: true
  },
  {
    customerName: "Zulema",
    total: 120,
    delivered: false
  },
  {
    customerName: "Santiago",
    total: 180,
    delivered: true
  },
  {
    customElements: "valentina",
    total: 240,
    delivered: true
  }
];

console.log("original", order);
const rsa = order.map(item => item.total);
console.log("rsa", rsa);

// const rsa2 = order.map(item => {
//   item.tax = .19;
//   return item;
// });
// console.log('rsa2', rsa2);
// console.log('original', order);

const rsa3 = order.map(item => {
  return {
    ...item,
    tax: 0.19
  };
});
console.log("rsa3", rsa3);
console.log("original", order);
